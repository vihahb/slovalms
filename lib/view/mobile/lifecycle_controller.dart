import 'package:get/get.dart';
import 'package:task_manager/commom/utils/app_utils.dart';

class LifeCycleController extends SuperController {
  @override
  void onDetached() {
    AppUtils.shared.printLog("App state : onDetached");
  }

  @override
  void onInactive() {
    AppUtils.shared.printLog("App state : onInactive");
  }

  @override
  void onPaused() {
    AppUtils.shared.printLog("App state : onPaused");
    // WebSocketService().disconnectFromServer();
    // if (Platform.isIOS)WebSocketService().disconnectFromServer();
  }

  @override
  void onResumed() {
    AppUtils.shared.printLog("App state : onResumed");
    // WebSocketService().connectToServer();
    // WebSocketService().eventRefeshApiStreamController.add(true);
    // WebSocketService().channel.sink.add({"type":SocketConstant.SOCKET_TYPE_REFRESH_API});
  }
}
