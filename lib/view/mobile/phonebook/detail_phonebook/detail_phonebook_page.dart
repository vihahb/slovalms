import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:get/get.dart';
import 'detail_phonebook_controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DetailPhoneBook extends GetWidget<DetailPhoneBookController> {
  @override
  var controller = Get.put(DetailPhoneBookController());

  DetailPhoneBook({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Obx(() => SafeArea(
        child: Scaffold(
            appBar: AppBar(
              elevation: 0,
              backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
              title: Text(controller.getTitleAppbar(controller.type.value),
                  style: TextStyle(color: Colors.white, fontSize: 16.sp)),
            ),
            body: RefreshIndicator(
              color: const Color.fromRGBO(248, 129, 37, 1),
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  child: Column(
                    children: [
                      Container(
                        color: Colors.white,
                        padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                        child: Row(
                          children: [
                            Expanded(
                              child: SizedBox(
                                height: 40,
                                child: TextFormField(
                                  onFieldSubmitted: (value) => {
                                    if (controller
                                            .controllerName.value.text.length ==
                                        0)
                                      {
                                        controller.getClassContact(
                                            controller.classId.value,
                                            controller
                                                .getRole(controller.type.value))
                                      }
                                    else
                                      {
                                        controller.findUserByClassContact(
                                            controller.classId.value,
                                            controller
                                                .getRole(controller.type.value),
                                            controller
                                                .controllerName.value.text),
                                      }
                                  },
                                  controller: controller.controllerName.value,
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                          borderSide: const BorderSide(
                                              color: Color.fromRGBO(
                                                  177, 177, 177, 1)),
                                          borderRadius:
                                              BorderRadius.circular(6)),
                                      prefixIcon: const Icon(
                                        Icons.search,
                                        color: Color.fromRGBO(177, 177, 177, 1),
                                      ),
                                      hintText: "Tìm kiếm",
                                      isCollapsed: true,
                                      hintStyle: TextStyle(
                                          fontSize: 14.sp,
                                          color: const Color.fromRGBO(
                                              177, 177, 177, 1)),
                                      focusedBorder: const OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color.fromRGBO(
                                                  248, 129, 37, 1)))),
                                  textAlignVertical: TextAlignVertical.center,
                                  cursorColor:
                                      const Color.fromRGBO(248, 129, 37, 1),
                                ),
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(left: 8.w)),
                            InkWell(
                              onTap: () {
                                controller.findUserByClassContact(
                                    controller.classId.value,
                                    controller.getRole(controller.type.value),
                                    controller.controllerName.value.text);
                              },
                              child: Container(
                                height: 40,
                                padding: const EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(6),
                                  color: const Color.fromRGBO(248, 129, 37, 1),
                                ),
                                child: const Icon(
                                  Icons.filter_alt,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 16.h)),
                      Container(
                        margin: EdgeInsets.only(left: 16.w),
                        child: Row(
                          children: [
                            Text(
                              controller.getLabel(controller.type.value),
                              style: const TextStyle(
                                  color: Color.fromRGBO(133, 133, 133, 1),
                                  fontSize: 12),
                            ),
                            Visibility(
                              visible: controller
                                  .getTitleClass(controller.type.value),
                              child: Text(
                                " ${controller.className.value}",
                                style: const TextStyle(
                                    color: Color.fromRGBO(248, 129, 37, 1),
                                    fontSize: 12),
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 8.h)),
                      controller.itemContact.value.length == 0
                          ? Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                width: 100.w,
                                height: 100.h,
                                child: Image.asset(
                                    "assets/images/noFind.png"),
                              ),
                              Padding(padding: EdgeInsets.only(top: 4.h)),
                              Text(
                                'Không có tìm kiếm nào trùng khớp',
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12.sp),
                              ),
                              Padding(padding: EdgeInsets.only(top: 4.h)),
                              InkWell(
                                onTap: () {
                                  controller.findUserByClassContact(
                                      controller.classId.value,
                                      controller
                                          .getRole(controller.type.value),
                                      controller
                                          .controllerName.value.text);
                                },
                                child: Text(
                                  "Nhấn để thử lại",
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontSize: 10.sp,
                                      fontWeight: FontWeight.w400),
                                ),
                              )
                            ],
                          )
                          : ListView.builder(
                          shrinkWrap: true,
                          physics: ScrollPhysics(),
                          itemCount:
                          controller.itemContact.value.length,
                          itemBuilder: (context, index) {
                            controller.student.value = controller
                                .itemContact.value[index].student!;
                            controller.parent.value = controller
                                .itemContact.value[index].parent!;
                            controller.subject.value = controller
                                .itemContact.value[index].subject!;
                            controller.typeInList.value = controller
                                .itemContact.value[index].type!;
                            print(
                                "TYPE: ${controller.typeInList.value}");
                            print(
                                "Student: ${controller.student.value}");
                            return Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(6),
                              ),
                              margin: const EdgeInsets.fromLTRB(
                                  16, 0, 16, 12),
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.start,
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: InkWell(
                                      onTap: () {
                                        controller
                                            .goToDetailInformationPhoneBook(
                                            index);
                                      },
                                      child: Row(
                                        children: [
                                          Container(
                                            margin:
                                            const EdgeInsets.only(
                                                left: 8, top: 8),
                                            child: SizedBox(
                                              width: 40.w,
                                              height: 40.w,
                                              child: CircleAvatar(
                                                backgroundColor:
                                                Colors.white,
                                                backgroundImage:
                                                Image.network(
                                                  controller
                                                      .itemContact
                                                      .value[index]
                                                      .image ??
                                                      "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                                                  errorBuilder:
                                                      (context, object,
                                                      stackTrace) {
                                                    return Image.asset(
                                                      "assets/images/avatar.png",
                                                    );
                                                  },
                                                ).image,
                                              ),
                                            ),
                                          ),
                                          const Padding(
                                              padding: EdgeInsets.only(
                                                  left: 8)),
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                              CrossAxisAlignment
                                                  .start,
                                              children: [
                                                const Padding(
                                                    padding:
                                                    EdgeInsets.only(
                                                        top: 8)),
                                                Text(
                                                  "${controller.itemContact.value[index].fullName}",
                                                  style: TextStyle(
                                                      color:
                                                      Colors.black,
                                                      fontSize: 16.sp,
                                                      fontWeight:
                                                      FontWeight
                                                          .bold),
                                                ),
                                                Padding(
                                                    padding:
                                                    EdgeInsets.only(
                                                        top: 4.h)),
                                                getRole(
                                                    controller.getListRole(
                                                        controller
                                                            .typeInList
                                                            .value,
                                                        index),
                                                    controller
                                                        .typeInList
                                                        .value,
                                                    index),
                                                Padding(
                                                    padding:
                                                    EdgeInsets.only(
                                                        top: 4.h)),
                                                Row(
                                                  children: [
                                                    Text(
                                                      "Số điện thoại: ",
                                                      style: TextStyle(
                                                          fontSize:
                                                          14.sp,
                                                          color: const Color
                                                              .fromRGBO(
                                                              133,
                                                              133,
                                                              133,
                                                              1)),
                                                    ),
                                                    InkWell(
                                                      onTap: () {
                                                        launch(
                                                            "tel://${controller.itemContact.value[index].phone}");
                                                      },
                                                      child: Text(
                                                        controller
                                                            .itemContact
                                                            .value[
                                                        index]
                                                            .phone ??
                                                            "",
                                                        style: TextStyle(
                                                            color: Colors
                                                                .black,
                                                            fontSize:
                                                            14.sp),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                Padding(
                                                    padding:
                                                    EdgeInsets.only(
                                                        bottom:
                                                        8.h)),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () {},
                                    child: Container(
                                      margin: const EdgeInsets.only(
                                          right: 16, top: 10),
                                      child: SvgPicture.asset(
                                          "assets/images/icon_message.svg"),
                                    ),
                                  )
                                ],
                              ),
                            );
                          })
                    ],
                  ),
                ),
                onRefresh: () async {
                  controller.onRefresh();
                })
        )));
  }

  RichText getRole(List<dynamic> list, role, index) {
    return RichText(
      textAlign: TextAlign.start,
      text: TextSpan(
          text: "${controller.getTitle(role)}",
          style: TextStyle(
              color: const Color.fromRGBO(173, 173, 173, 1),
              fontSize: 14.sp,
              fontWeight: FontWeight.w500,
              fontFamily: 'static/Inter-Medium.ttf'),
          children: [
            TextSpan(
              text:
                  "${controller.getTextHomeRoomTeacher(controller.itemContact.value[index].positionName, index)}",
              style: controller.getColorTextHomeRoomTeacher(
                      controller.itemContact.value[index].positionName)
                  ? TextStyle(
                      color: const Color.fromRGBO(173, 173, 173, 1),
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w500,
                      fontFamily: 'static/Inter-Medium.ttf')
                  : TextStyle(
                      color: const Color.fromRGBO(248, 129, 37, 1),
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w500,
                      fontFamily: 'static/Inter-Medium.ttf'),
              children: list.map((e) {
                var index = list.indexOf(e);
                var showSplit = ", ";
                if (index == list.length - 1) {
                  showSplit = "";
                }
                if (controller.getRole(controller.typeInList.value) ==
                    "TEACHER") {
                  return TextSpan(
                      text: "Môn ${e.fullName}$showSplit",
                      style: TextStyle(
                          color: const Color.fromRGBO(248, 129, 37, 1),
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'static/Inter-Medium.ttf'));
                }

                return TextSpan(
                    text: "${e.fullName}$showSplit",
                    style: TextStyle(
                        color: const Color.fromRGBO(248, 129, 37, 1),
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'static/Inter-Medium.ttf'));
              }).toList(),
            ),
          ]),
    );
  }
}
