import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:task_manager/commom/app_cache.dart';
import 'package:task_manager/view/mobile/phonebook/detail_phonebook/Detail_information_by_phonebook/detail_information_phonebook_controller.dart';
import 'package:task_manager/view/mobile/phonebook/detail_phonebook/detail_phonebook_controller.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:get/get.dart';

class DetailInformationPhoneBookPage extends GetWidget<DetailInformationPhoneBookController>{
  @override
  var controller = Get.put(DetailInformationPhoneBookController());

  DetailInformationPhoneBookPage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: const Text("Chi tiết"),
          leading: BackButton(
            color: Colors.white,
            onPressed: () {
              Get.delete<DetailInformationPhoneBookController>();
              Get.back();
            },
          ),
          backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
        ),
        backgroundColor: const Color.fromRGBO(249, 249, 249, 1),
        body:   Container(
          margin: const EdgeInsets.only(top: 16, left: 16, right: 16),
          child:  Column(
            children: [profileAvatarWidget(), informationWidget()],
          ),
        ),
    );
  }
  informationWidget() {
    return Obx(() => controller.isReady.value?Container(
      margin: EdgeInsets.only(top: 16.h),
      padding: EdgeInsets.only(left: 6.w, right: 6.w),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        color: const Color.fromRGBO(255, 255, 255, 1),
      ),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(
                top: 16.h, bottom: 8.h, right: 9.w, left: 9.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'Thông Tin Cá Nhân',
                  style: styleTitle,
                ),
              ],
            ),
          ),
          Container(
              margin: const EdgeInsets.all(9),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        'Họ Và Tên: ',
                        style: styleTitle,
                      ),
                      Expanded(child: Container()),
                      Text(
                        controller.userProfile.value.fullName?? "",
                        style: TextStyle(
                            color: const Color.fromRGBO(248, 129, 37, 1),
                            fontSize: 14.sp,
                            fontFamily: 'static/Inter-Medium.ttf',
                            fontWeight: FontWeight.w500),
                      )
                    ],
                  ),

                  Padding(padding: EdgeInsets.only(top: 10.h)),
                  Row(
                    children: [
                      Text(
                        "${controller.getTitleClassDetail(controller.item.value.type)}",
                        style: styleTitle,
                      ),
                      Expanded(child: controller.item.value.type == "TEACHER" ?
                      getPositionParent(controller.getValueTitleDetail(controller.item.value.type), controller.item.value.type)
                          :getPositionStudent(controller.getValueTitleDetail(controller.item.value.type),controller.item.value.type)),
                      // getPossitionAll()


                    ],
                  ),
                  Padding(padding: EdgeInsets.only(top: 10.h)),

                  Row(
                    children: [
                      Text(
                        'Số Điện Thoại: ',
                        style: styleTitle,
                      ),
                      Expanded(child: Container()),
                      InkWell(
                        onTap: (){
                          launch(
                              "tel://${controller.userProfile.value.phone} " );
                        },
                        child:
                        Text(
                          controller.userProfile.value.phone?? "",
                          style: styleValue,
                        ),
                      )
                    ],
                  ),
                  Padding(padding: EdgeInsets.only(top: 10.h)),
                  Row(
                    children: [
                      Text(
                        'Email: ',
                        style: styleTitle,
                      ),
                      Expanded(child: Container()),
                      Text(
                        controller.userProfile.value.email?? "",
                        style: styleValue,
                      )
                    ],
                  ),
                  Padding(padding: EdgeInsets.only(top: 10.h)),
                  controller.getTitleSchoolDetail(controller.item.value.type)==""?Container(): Row(
                    children: [
                      Text(
                        '${controller.getTitleSchoolDetail(controller.item.value.type)}',
                        style: styleTitle,
                      ),
                      Expanded(child: Container()),
                      Text(
                        '${controller.getValueTitleSchoolDetail(controller.item.value.type) }',
                        style: styleValue,
                      )
                    ],
                  ),
                  Padding(padding: EdgeInsets.only(top: 10.h)),
                ],
              )
          )

        ],
      ),
    ):Container());
  }

  profileAvatarWidget() {
    return Obx(() => Container(
      width: 400.w,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6.r),
        color: const Color.fromRGBO(255, 255, 255, 1),
      ),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 10.h),
            child:    SizedBox(
                width: 80.w,
                height: 80.w,
                child:
                InkWell(
                  onTap: (){
                    controller.goToDetailAvatar();
                  },
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    backgroundImage: Image.network(
                      controller.userProfile.value.image ??
                          "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                      errorBuilder:
                          (context, object, stackTrace) {
                        return Image.asset(
                          "assets/images/avatar.png",
                        );
                      },
                    ).image,
                  ),
                )
            ),
          ),
          SizedBox(
            height: 8.h,
          ),
          Text(
            controller.userProfile.value.fullName ?? "",
            style: TextStyle(
                color: const Color.fromRGBO(26, 26, 26, 1),
                fontSize: 18.sp,
                fontFamily: 'static/Inter-Medium.ttf',
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 8.h,
          ),
          Text(
            controller.userProfile.value.email ?? "",
            style: TextStyle(
              color: const Color.fromRGBO(133, 133, 133, 1),
              fontSize: 10.sp,
            ),
          ),
          SizedBox(
            height: 16.h,
          ),
        ],
      ),
    ));
  }
  var styleValue = TextStyle(
      color: const Color.fromRGBO(26, 26, 26, 1),
      fontSize: 14.sp,
      fontFamily: 'static/Inter-Medium.ttf',
      fontWeight: FontWeight.w500);
  var styleTitle = TextStyle(
      color: const Color.fromRGBO(133, 133, 133, 1),
      fontSize: 12.sp,
      fontFamily: 'static/Inter-Medium.ttf',
      fontWeight: FontWeight.w500);

  RichText getPositionStudent(List<dynamic> list, role) {
    return RichText(
      textAlign: TextAlign.end,
      text: TextSpan(
        text: "",
        children: list.map((e) {
          var index = list.indexOf(e);
          var showSplit = ", ";
          if (index == list.length - 1) {
            showSplit = "";
          }
           if(controller.item.value.type == "STUDENT"){
             return TextSpan(
                 text: "${e.name}$showSplit",
                 style: TextStyle(
                     color: const Color.fromRGBO(248, 129, 37, 1),
                     fontSize: 14.sp,
                     fontWeight: FontWeight.w500,
                     fontFamily: 'static/Inter-Medium.ttf'));
           }
          return TextSpan(
              text: "${e.fullName}$showSplit",
              style: TextStyle(
                  color: const Color.fromRGBO(248, 129, 37, 1),
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'static/Inter-Medium.ttf'));

        }).toList(),
      ),
    );
  }


   getPositionParent(String ,role) {
    return Text("${controller.userProfile.value.position!.name} ",
          textAlign: TextAlign.end,
          style: TextStyle(
              color: const Color.fromRGBO(248, 129, 37, 1),
              fontSize: 14.sp,
              fontWeight: FontWeight.w500,
              fontFamily: 'static/Inter-Medium.ttf'));

  }



}