import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:task_manager/data/model/common/Position.dart';
import 'package:task_manager/view/mobile/phonebook/detail_phonebook/detail_phonebook_controller.dart';

import '../../../../../commom/utils/app_utils.dart';
import '../../../../../data/base_service/api_response.dart';
import '../../../../../data/model/common/contacts.dart';
import '../../../../../data/model/common/student_by_parent.dart';
import '../../../../../data/model/common/user_profile.dart';
import '../../../../../data/model/res/class/School.dart';
import '../../../../../data/repository/person/personal_info_repo.dart';
import '../../../../../routes/app_pages.dart';

class DetailInformationPhoneBookController extends GetxController{
  var userProfile = UserProfile().obs;
  var school = SchoolData().obs;
  var id = "".obs;
  var type ="".obs;
  var clasName= ''.obs;
  var student = <Student>[].obs;
  var parent = <Parent>[].obs;
  var subject = <SubjectItemContact>[].obs;
  var position = Position().obs;
  final PersonalInfoRepo _personalInfoRepo = PersonalInfoRepo();
  RxList<ItemContact>  itemContact = <ItemContact>[].obs;
  RxList<ItemUserProfile>  itemuser = <ItemUserProfile>[].obs;
  var typeInList = "".obs;
  var listStudent = <StudentByParent>[].obs;
  var item = ItemContact().obs;
  var isReady = false.obs;
  @override
  void onInit() {
    super.onInit();
    var data = Get.arguments;
    if (data != null) {
      item.value = data;
    }
    _personalInfoRepo.getUserProfileById(item.value.id).then((value){
      if (value.state == Status.SUCCESS) {
        userProfile.value = value.object!;
        if(userProfile.value.school== null){
          school.value.name = "";
        }
        else{
          school.value = userProfile.value.school!;
        }

      }
      isReady.value = true;
    });

    _personalInfoRepo.listStudentByParent(item.value.id).then((value) {
      if (value.state == Status.SUCCESS) {
        listStudent.value =value.object!;
      }
    });

  }


  void goToDetailAvatar(){
    Get.toNamed(Routes.getavatar, arguments:userProfile.value.image);
    print("image: ${userProfile.value.image}");
  }

  getTitle(role) {
    switch (role) {
      case "PARENT":
        return "Phụ huynh hs: ";
      case "STUDENT":
        return "Phụ huynh: ";
      case "TEACHER":
        return "Giáo viên: ";
      default:
        return "";
    }
  }


  getListRole(role){
    switch (role) {
      case "PARENT":
        return listStudent.value;
      case "STUDENT":
        return parent.value;
      case "TEACHER":
        return itemContact.value;
    }
  }
  getRole(role){
    switch(role){
      case "STUDENT":
        return "STUDENT";
      case "TEACHER":
        return "TEACHER";
      case "PARENT":
        return "PARENT";
      case "MANAGER":
        return "MANAGER";
      default:
        return "";

    }

  }
  getTitleClassDetail(role){
    switch(role){
      case "STUDENT":
        return "Lớp: ";
      case "PARENT":
        return "Phụ huynh em: ";
      case "TEACHER":
        return "Chức danh: ";
      default:
        return "";




    }

  }

  getTitleSchoolDetail(role){
    switch(role){
      case "STUDENT":
        return "Trường: ";
      case "PARENT":
        return "";
      case "TEACHER":
        return "Trường: ";
      default:
        return "";

    }

  }
  getValueTitleSchoolDetail(role){
    switch(role){
      case  "STUDENT":
        return school.value.name;
      case  "TEACHER":
        return school.value.name;
      case  "PARENT":
        return "";
      default:
        return "";
    }

  }

  getValueTitleDetail(role){
    switch(role){
      case  "STUDENT":
        return userProfile.value.item!.clazzs;
      case  "TEACHER":
        return userProfile.value.position!.name ?? "";
      case  "PARENT":
        return listStudent.value;
        default:
          return "";
    }

  }








}