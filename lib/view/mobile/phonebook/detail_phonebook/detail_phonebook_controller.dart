import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:task_manager/view/mobile/phonebook/detail_phonebook/Detail_information_by_phonebook/detail_information_phonebook_page.dart';
import '../../../../commom/utils/app_utils.dart';
import '../../../../data/base_service/api_response.dart';
import '../../../../data/model/common/contacts.dart';
import '../../../../data/model/common/user_profile.dart';
import '../../../../data/repository/contact/contact_repo.dart';
import '../../../../routes/app_pages.dart';

class DetailPhoneBookController extends GetxController{
  var type = "".obs;
  var className = "".obs;
  var classId = "".obs;
  var userProfile = UserProfile().obs;
  var student = <Student>[].obs;
    var parent = <Parent>[].obs;
    var subject = <SubjectItemContact>[].obs;
    final ContactRepo _contactRepo = ContactRepo();
    var contacts = Contacts().obs;
    RxList<ItemContact>  itemContact = <ItemContact>[].obs;
    var controllerName = TextEditingController().obs;
    var typeInList = "".obs;
    var colorText = false.obs;



    @override
    void onInit() {
      super.onInit();
    var data = Get.arguments;

    if (data != null && data.isNotEmpty) {
      itemContact.value = data[0];
      type.value = data[1];
      className.value = data[2];
      classId.value = data[3];
    }
  }

  getTitle(role) {
    switch (role) {
      case "PARENT":
        return "Phụ huynh hs: ";
      case "STUDENT":
        return "Phụ huynh: ";
      case "TEACHER":
        return "Giáo viên: ";
      default:
        return "";
    }
  }


  getTextHomeRoomTeacher(isHomeRoom,index){
      var split = "";
    switch (isHomeRoom){
      case "":
        return "";
      case null:
        return "";
      case "Giáo viên chủ nhiệm":
        if(itemContact.value[index].subject?.length ==0) {
          split = "";
        }else{
          split = ",";
        };
        return "Giáo viên chủ nhiệm${split} ";
      default:
        return "";
    }
  }

  getPosition(positionName){
      switch(positionName){
        case "":
          return "";
        case null:
          return "Phụ huynh";
        default:
          return "";


      }
  }
  getColorTextHomeRoomTeacher(isHomeRoom){
    switch (isHomeRoom){
      case "":
        return colorText ==false;
      case null:
        return colorText ==false;
      case "Giáo viên chủ nhiệm":
        return colorText ==true;
      default:
        return colorText ==false;
    }
  }


  getItemCount(role){
    switch (role) {
      case "PARENT":
        return student.value.length;
      case "STUDENT":
        return parent.value.length;
      case "TEACHER":
        return subject.value.length;
      default:
        return 0;
    }
  }

  getFindDefault(){
    return AppUtils.shared.showToast("Tìm kiếm thành công!");
  }

  getListRole(role,index){
    switch (role) {
      case "PARENT":
        return student.value;
      case "STUDENT":
        return parent.value;
      case "TEACHER":
        return itemContact.value[index].subject;
    }
  }

  getTitleClass(role) {
    if (role == "STUDENT" || role == "PARENT") {
      return true;
    } else {
      return false;
    }
  }

  getLabel(role) {
    switch (role) {
      case "PARENT":
        return "Thông Tin Phụ Huynh";
      case "STUDENT":
        return "Thông Tin Học Sinh";
      case "TEACHER":
        return "Thông Tin Giáo Viên";
      default:
        return "Gợi ý";
    }
  }

  getTitleAppbar(role) {
    switch (role) {
      case "STUDENT":
        return "Danh Bạ Học Sinh";
      case "PARENT":
        return "Danh Bạ Phụ Huynh";
      case "TEACHER":
        return "Danh Bạ Giáo Viên";
      default:
        return "Tất Cả";
    }
  }

  getContent(role,index) {
    switch (role) {
      case "PARENT":
        if(student.value?.length==0){
          return "";
        }else{
          return "${student.value[index].fullName??""} ";
        }
      case "STUDENT":
        if(parent.value?.length==0){
          return "";
        }else{
          return "${parent.value[index].fullName??""} ";
        }
      case "TEACHER":
        if(subject.value?.length==0||subject.value == null){
          return "";
        }else{
          return "${subject.value[index].fullName??""} ";
        }
    }
  }
  getRole(role){
    switch(role){
      case "STUDENT":
        return "STUDENT";
      case "TEACHER":
        return "TEACHER";
      case "PARENT":
        return "PARENT";
      case "MANAGER":
        return "MANAGER";
      default:
        return "";

    }

  }


  onRefresh() {
    getClassContact(classId.value,getRole(type.value));
  }


  findUserByClassContact (classId, type, fullName) async {
    _contactRepo.findUserByClassContact(classId, type,fullName).then((value) {
      if (value.state == Status.SUCCESS) {
        contacts.value = value.object!;
        itemContact.value = contacts.value.item!;
      }
    });

  }

  getClassContact(classId,type) async{
    _contactRepo.getClassContact(classId, type).then((value) {
      if (value.state == Status.SUCCESS) {
        contacts.value = value.object!;
        itemContact.value = contacts.value.item!;
      }
    });
  }





  void goToDetailAvatar(index){
    Get.toNamed(Routes.getavatar, arguments: itemContact.value[index].image);
    print("image: ${userProfile.value.image}");
  }


  goToDetailInformationPhoneBook(index){
    Get.toNamed(Routes.infoInPhoneBook, arguments:itemContact.value[index]);
    print("id: ${itemContact.value[index].id}");
  }

}