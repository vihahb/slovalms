import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:tab_indicator_styler/tab_indicator_styler.dart';
import 'package:task_manager/commom/utils/app_utils.dart';
import 'package:task_manager/commom/utils/color_utils.dart';
import 'package:task_manager/view/mobile/home/home_controller.dart';
import 'package:task_manager/view/mobile/message/message_page.dart';
import 'package:task_manager/view/mobile/notification/notification_page.dart';
import 'package:task_manager/view/mobile/schedule/schedule_page.dart';
import 'package:badges/badges.dart';
import '../../../commom/app_cache.dart';
import '../../../commom/utils/textstyle.dart';
import '../../../notification/notification_service.dart';
import '../phonebook/phone_book_page.dart';
import 'package:badges/src/badge.dart' as badge;

class HomePage extends StatelessWidget{
  var controller = Get.find<HomeController>();

  HomePage({super.key});
  @override
  Widget build(BuildContext context) {
    DateTime oldTime = DateTime.now();
    DateTime newTime = DateTime.now();

    return WillPopScope(
      onWillPop: () async {
        newTime = DateTime.now();
        int difference = newTime.difference(oldTime).inMilliseconds;
        oldTime = newTime;
        if (difference < 2000) {
          return true;
        } else {
          AppUtils.shared.showToast('touch_again_to_exit'.tr,
              backgroundColor: ColorUtils.COLOR_BLACK.withOpacity(0.5),
              textColor: ColorUtils.COLOR_WHITE);
          return false;
        }
      },
      child: Obx(() => (controller.isReady.value)
          ? SafeArea(
              top: false,
              bottom: true,
              child: Scaffold(
                  backgroundColor: const Color.fromRGBO(229, 229, 229, 1),
                  body: TabBarView(
                    physics: const NeverScrollableScrollPhysics(),
                    controller: controller.tabController,
                    children: [
                      controller.getDashboard(),
                      controller.getSchedule(),
                      PhoneBookPage(),
                      NotificationPage(),
                      MessagePage(),
                    ],
                  ),
                  bottomNavigationBar: Container(
                      height: 60,
                      decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(10),
                              topLeft: Radius.circular(10))),
                      child: TabBar(
                        controller: controller.tabController,
                        physics: const NeverScrollableScrollPhysics(),
                        indicatorWeight: 3,
                        indicator: MaterialIndicator(
                          color: const Color.fromRGBO(248, 129, 37, 1),
                          topRightRadius: 10,
                          topLeftRadius: 10,
                          paintingStyle: PaintingStyle.fill,
                        ),
                        labelColor: Colors.black,
                        unselectedLabelColor: Colors.grey,
                        indicatorPadding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                        onTap: (index) {
                          controller.setIndex(index);
                        },
                        tabs: [
                          Tab(
                            icon: Image.asset(
                              "assets/images/icon_dasbroads.png",
                              height: 18.h,
                              width: 18.w,
                              color: controller.selectedPageIndex.value == 0
                                  ? const Color.fromRGBO(248, 129, 37, 1)
                                  : const Color.fromRGBO(177, 177, 177, 1),
                            ),
                          ),
                          Tab(
                              icon: Image.asset(
                            width: 18.w,
                            height: 18.h,
                            "assets/images/icon_schedule.png",
                            color: controller.selectedPageIndex.value == 1
                                ? const Color.fromRGBO(248, 129, 37, 1)
                                : const Color.fromRGBO(177, 177, 177, 1),
                          )),
                          Tab(
                              icon: controller.selectedPageIndex.value == 2
                                  ? Image.asset(
                                      controller.getItemTabbar(),
                                      width: 18.w,
                                      height: 18.h,
                                      color:
                                          const Color.fromRGBO(248, 129, 37, 1),
                                    )
                                  : Image.asset(
                                      controller.getItemTabbar(),
                                      width: 18.w,
                                      height: 18.h,
                                    )),
                          Tab(
                              icon: controller.selectedPageIndex.value == 3
                                  ? Obx(() => badge.Badge(
                                        badgeContent: Text(
                                          AppCache().notificationCount.value >
                                                  99
                                              ? " 99+"
                                              : "${AppCache().notificationCount.value}",
                                          style: TextStyleUtils
                                                  .sizeText9Weight400()
                                              ?.copyWith(
                                            fontFamily: "Montserrat",
                                            color: ColorUtils.COLOR_WHITE,
                                          ),
                                        ),
                                        badgeColor: ColorUtils.colorF62727,
                                        animationType: BadgeAnimationType.scale,
                                        showBadge: AppCache()
                                                .notificationCount
                                                .value !=
                                            0,
                                        position: BadgePosition.topEnd(
                                            top: -6.h, end: -6.w),
                                        child: Container(
                                          width: 18.w,
                                          height: 18.w,
                                          margin: EdgeInsets.only(bottom: 6.h),
                                          child: Image.asset(
                                            "assets/images/icon_notification.png",
                                            width: 24.w,
                                            height: 24.w,
                                            fit: BoxFit.contain,
                                          ),
                                        ),
                                      ))
                                  : Obx(() => badge.Badge(
                                        badgeContent: Text(
                                          AppCache().notificationCount.value >
                                                  99
                                              ? " 99+"
                                              : "${AppCache().notificationCount.value}",
                                          style: TextStyleUtils
                                                  .sizeText9Weight400()
                                              ?.copyWith(
                                            fontFamily: "Montserrat",
                                            color: ColorUtils.COLOR_WHITE,
                                          ),
                                        ),
                                        badgeColor: ColorUtils.colorF62727,
                                        animationType: BadgeAnimationType.scale,
                                        showBadge: AppCache()
                                                .notificationCount
                                                .value !=
                                            0,
                                        position: BadgePosition.topEnd(
                                            top: -6.h, end: -6.w),
                                        child: Container(
                                          width: 18.w,
                                          height: 18.w,
                                          margin: EdgeInsets.only(bottom: 6.h),
                                          child: Image.asset(
                                            "assets/images/icon_notification_nofocus.png",
                                            width: 24.w,
                                            height: 24.w,
                                            fit: BoxFit.contain,
                                          ),
                                        ),
                                      ))),
                          Tab(
                              icon: controller.selectedPageIndex.value == 4
                                  ? Image.asset(
                                      width: 18.w,
                                      height: 18.h,
                                      "assets/images/icon_message.png")
                                  : Image.asset(
                                      width: 18.w,
                                      height: 18.h,
                                      "assets/images/icon_message_nofocus.png")),
                        ],
                      ))))
          : const Center(
              child: CircularProgressIndicator(),
            )),
    );
  }
}
