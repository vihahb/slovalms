// import 'package:task_manager/commom/utils/app_utils.dart';
// import 'package:task_manager/commom/utils/color_utils.dart';
// import 'package:task_manager/commom/utils/date_time_utils.dart';
// import 'package:task_manager/commom/utils/screen/screen.dart';
// import 'package:task_manager/commom/utils/textstyle.dart';
// import 'package:task_manager/commom/widget/appbar_cusstom.dart';
// import 'package:task_manager/commom/widget/empty_screen.dart';
// import 'package:task_manager/commom/widget/error_screen.dart';
// import 'package:task_manager/view/mobile/home/notify/teaching_schedule_controller.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_html/flutter_html.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:get/get.dart';
//
// import '../../../../commom/constants/app_constants.dart';
//
// class NotifyPage extends GetView<NotifyController> {
//   const NotifyPage({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     controller.getNotifyData();
//     return Scaffold(
//       backgroundColor: ColorUtils.COLOR_WHITE,
//       appBar: PreferredSize(
//         preferredSize: Screen.appBarHeight,
//
//         child: AppBarCustom(
//           backgroundColor: ColorUtils.BG_COLOR,
//           elevation: 0,
//           leadingWidget: IconButton(
//               onPressed: () => Get.back(),
//               icon: const Icon(
//                 Icons.arrow_back,
//                 color: Colors.white,
//               )),
//           width: 24.w,
//           height: 21.h,
//           leadingFunction: () {
//             AppUtils.shared.popView(context);
//           },
//           centerTitle: Text("notify".tr,
//               style: TextStyleUtils.sizeText16Weight500()
//                   ?.copyWith(color: ColorUtils.COLOR_WHITE)),
//           // actionIcon: "filter.png",
//           iconNotify: false,
//           // actionFunction: () {
//           //   // Get.toNamed(Routes.NOTIFICATION);
//           // },
//         ),
//       ),
//       body: Padding(
//         padding: EdgeInsets.only(bottom: 16.h),
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.start,
//           crossAxisAlignment: CrossAxisAlignment.start,
//           mainAxisSize: MainAxisSize.max,
//           children: [
//             Expanded(
//                 child: controller.obx((state) {
//               return RefreshIndicator(
//                 onRefresh: () async {
//                   await await controller.onRefresh();
//                 },
//                 child: ListView.builder(
//                     padding: EdgeInsets.zero,
//                     itemCount: controller.listNotify.length,
//                     controller: controller.scrollController,
//                     itemBuilder: (context, index) {
//                       final model = controller.listNotify[index];
//                       if (index == controller.listNotify.length) {
//                         return !controller.isStopLoading
//                             ? Padding(
//                           padding: EdgeInsets.only(bottom: 16.h),
//                           child: const CupertinoActivityIndicator(
//                             color: ColorUtils.COLOR_WHITE,
//                           ),
//                         )
//                             : Padding(
//                           padding: const EdgeInsets.all(16.0),
//                           child: Center(
//                             child: Text('the_end'.tr),
//                           ),
//                         );
//
//                       }
//                       return Row(
//                         children: [
//                           Expanded(
//                             child: Column(
//                               mainAxisAlignment: MainAxisAlignment.start,
//                               crossAxisAlignment: CrossAxisAlignment.start,
//                               children: [
//                                 Container(
//                                   height: 0.2,
//                                   color: context.theme.disabledColor,
//                                 ),
//                                 const SizedBox(height: 16),
//                                 Padding(
//                                   padding:
//                                       EdgeInsets.only(left: 19.w, right: 19.w),
//                                   child: Column(
//                                     children: [
//                                       Row(
//                                         children: [
//                                           Image.asset(
//                                             "assets/icons/notification.png",
//                                             width: 28.w,
//                                             height: 28.h,
//                                             fit: BoxFit.contain,
//                                           ),
//                                           SizedBox(width: 10.w),
//                                           Expanded(
//                                             child: Column(
//                                                 crossAxisAlignment:
//                                                     CrossAxisAlignment.start,
//                                                 mainAxisAlignment:
//                                                     MainAxisAlignment.start,
//                                                 children: [
//                                                   Text(model.title ?? "",
//                                                       style: model.markStatus ==
//                                                               AppConstant.UNREAD
//                                                           ? TextStyleUtils
//                                                               .sizeText14Weight700()
//                                                           : TextStyleUtils
//                                                                   .sizeText14Weight500()
//                                                               ?.copyWith(
//                                                                   color: context
//                                                                       .theme
//                                                                       .primaryColorLight)),
//                                                   Text(
//                                                     DateTimeUtils.formatTime(
//                                                         model.createdTime ?? "",
//                                                         format:
//                                                             "dd/MM/yyyy HH:mm"),
//                                                     style: model.markStatus ==
//                                                             AppConstant.UNREAD
//                                                         ? TextStyleUtils
//                                                             .sizeText12Weight700()
//                                                         : TextStyleUtils
//                                                                 .sizeText12Weight700()
//                                                             ?.copyWith(
//                                                                 color: context
//                                                                     .theme
//                                                                     .primaryColorLight),
//                                                   )
//                                                 ]),
//                                           ),
//                                         ],
//                                       ),
//                                       Padding(
//                                         padding: EdgeInsets.only(left: 3.w),
//                                         child: Row(children: [
//                                           const Expanded(flex: 1, child: SizedBox()),
//                                           Expanded(
//                                             flex: 9,
//                                             child: GetBuilder<NotifyController>(
//                                               builder: (builder) => Column(
//                                                   mainAxisAlignment:
//                                                       MainAxisAlignment.start,
//                                                   crossAxisAlignment:
//                                                       CrossAxisAlignment.start,
//                                                   children: [
//                                                     AnimatedSize(
//                                                       curve: Curves.easeIn,
//                                                       duration: const Duration(
//                                                           milliseconds: 300),
//                                                       child: SizedBox(
//                                                           height: model.isMore
//                                                               ? 0
//                                                               : null,
//                                                           child: Html(
//                                                             data: model
//                                                                 .contentDetail,
//                                                           )),
//                                                     ),
//                                                     GestureDetector(
//                                                       onTap: () {
//                                                         controller
//                                                             .showMoreContent(
//                                                                 index);
//                                                         if (model.markStatus ==
//                                                             AppConstant
//                                                                 .UNREAD) {
//                                                           controller
//                                                               .noticeMarkStatus(
//                                                                   model.id ??
//                                                                       0);
//                                                         } else {
//                                                           return;
//                                                         }
//                                                       },
//                                                       child: Text(
//                                                           model.isMore
//                                                               ? "see_more".tr
//                                                               : "narrow_down"
//                                                                   .tr,
//                                                           style: model.markStatus ==
//                                                                   AppConstant
//                                                                       .UNREAD
//                                                               ? TextStyleUtils.sizeText12Weight700()
//                                                                   ?.copyWith(
//                                                                       color: ColorUtils
//                                                                           .BG_BUTTON_SELL)
//                                                                   .copyWith(
//                                                                       decoration:
//                                                                           TextDecoration
//                                                                               .underline)
//                                                               : TextStyleUtils.sizeText12Weight700()?.copyWith(
//                                                                   color: context
//                                                                       .theme
//                                                                       .primaryColorLight,
//                                                                   decoration: TextDecoration
//                                                                       .underline)),
//                                                     ),
//                                                   ]),
//                                             ),
//                                           ),
//                                         ]),
//                                       ),
//                                     ],
//                                   ),
//                                 ),
//                                 const SizedBox(height: 16),
//                               ],
//                             ),
//                           )
//                         ],
//                       );
//                     }),
//               );
//             },
//                     onError: (error) => ErrorScreen(),
//                     onLoading: const Center(child: CupertinoActivityIndicator()),
//                     onEmpty:
//                         EmptyScreen(onTap: (() => controller.onRefresh()))))
//           ],
//         ),
//       ),
//     );
//   }
// }
