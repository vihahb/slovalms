// class NotifyController extends GetxController
//     with StateMixin<List<NotifyModel>> {
//   final _listPostRepo = NotifyRepo();
//   List<NotifyModel> listNotify = [];
//   RxInt notiUnread = 0.obs;
//   num lastOffset = 0.0;
//   num page = 0;
//   num pageSize = 10;
//   bool isStopLoading = false;
//   final scrollController = ScrollController();
//   @override
//   void onInit() {
//     getNotiUnMark();
//     scrollController.addListener(() {
//       lastOffset = scrollController.position.pixels;
//       if (scrollController.position.pixels ==
//           scrollController.position.maxScrollExtent) {
//         if (!isStopLoading) {
//           page += pageSize;
//           getNotifyData();
//         }
//       }
//     });
//     super.onInit();
//   }
//
//   onRefresh() async {
//     page = 0;
//     listNotify.clear();
//     isStopLoading = false;
//     await getNotifyData();
//   }
//
//   void showMoreContent(index) {
//     listNotify[index].isMore = !listNotify[index].isMore;
//     update();
//   }
//
//   getNotiUnMark() {
//     _listPostRepo.getNotiUnMark().then((value) {
//       if (value.state == Status.SUCCESS) {
//         notiUnread.value = value.object ?? 0;
//       }
//     });
//   }
//
//   getNotifyData() {
//     _listPostRepo.getNotify(page, pageSize).then((value) {
//       if (value.state == Status.SUCCESS) {
//         if (value.listData != null) {
//           listNotify.addAll(value.listData!);
//           if (value.listData!.length != pageSize) isStopLoading = true;
//           change(listNotify, status: RxStatus.success());
//         }
//         if (listNotify.isEmpty) {
//           change(listNotify, status: RxStatus.empty());
//         }
//       } else {
//         change(listNotify, status: RxStatus.error(value.message));
//       }
//     });
//   }
//
//   noticeMarkStatus(num noticeID) {
//     _listPostRepo.noticeMark(noticeID).then((value) {
//       if (value.state == Status.SUCCESS) {
//         notiUnread.value--;
//         for (var element in listNotify) {
//           if (element.id == noticeID) {
//             element.markStatus = AppConstant.READ;
//             return;
//           }
//         }
//       } else {
//         AppUtils.shared.snackbarError("fail".tr, value.message);
//       }
//     });
//   }
//
//   @override
//   void dispose() {
//     scrollController.dispose();
//     super.dispose();
//   }
// }
