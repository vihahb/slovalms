import 'package:flutter/material.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:get/get.dart';
import 'package:task_manager/commom/action/role_action.dart';
import 'package:task_manager/commom/app_cache.dart';
import 'package:task_manager/data/model/common/schedule.dart';
import 'package:task_manager/view/mobile/role/manager/manager_home_controller.dart';
import 'package:task_manager/view/mobile/role/manager/schedule_manager/schedule_manager_controller.dart';
import 'package:task_manager/view/mobile/role/manager/schedule_manager/schedule_manager_page.dart';
import 'package:task_manager/view/mobile/role/parent/parent_home_controller.dart';
import 'package:task_manager/view/mobile/role/student/student_home_controller.dart';
import 'package:task_manager/view/mobile/role/teacher/teacher_home_controller.dart';

import '../../../commom/utils/app_utils.dart';
import '../../../data/base_service/api_response.dart';
import '../../../data/repository/notification_repo/notification_repo.dart';
import '../notification/notification_controller.dart';
import '../role/student/personal_information_student/personal_information_student_page.dart';
import '../schedule/schedule_controller.dart';

class HomeController extends GetxController with SingleGetTickerProviderMixin  {
  TabController? tabController;
  var selectedPageIndex = 0.obs;
  var isReady = false.obs;
  final NotificationRepo _notificationRepo = NotificationRepo();
  @override
  void onInit() {

    super.onInit();
    tabController = TabController(length: 5, vsync: this);
    tabController?.addListener(() {
      selectedPageIndex.value = tabController!.index;
      if(selectedPageIndex.value == 1){
        if(AppCache().userType == "MANAGER"){
          Get.find<ScheduleManagerController>().getListBlock(AppCache().schoolYearId);
        }else{
        }
      }

      if(selectedPageIndex.value == 3){
        Get.find<NotificationController>().isChooseManyToday.value = false;
        Get.find<NotificationController>().isChooseManyBefore.value = false;
        Get.find<NotificationController>().getListNotification();
        if(AppCache().userType == "TEACHER"){
          Get.find<NotificationController>().classOfTeacher.value.clear();
          Get.find<NotificationController>().getListClassIdByTeacher();
          Get.find<NotificationController>().classOfTeacher.refresh();
        }
        Get.find<NotificationController>().isAllClass.value = true;
        Get.find<NotificationController>().isAllClass.refresh();
      }
    });
    getCountNotifyNotSeen();
    isReady.value = true;
  }

  void setIndex(int index) {
    if (selectedPageIndex.value == index) return;
    tabController?.animateTo(index);
  }

  getCountNotifyNotSeen(){
    _notificationRepo.getTotalNotifyNotSeen().then((value) {
      if (value.state == Status.SUCCESS) {
        FlutterAppBadger.updateBadgeCount(value.object?.total ?? 0);
        AppCache().notificationCount.value = value.object?.total ?? 0;
      } else {
        AppUtils.shared.snackbarError("Lỗi", value.message ?? "");
      }
    });
  }


  updateProfile(){
    RoleAction().getActionByRole(
        currentRole: AppCache().userType,
        teacherAction: () {
          Get.find<TeacherHomeController>().getDetailProfile();
        },
        managerAction: () {
          Get.find<ManagerHomeController>().getDetailProfile();
        },
        studentAction: () {
          Get.find<StudentHomeController>().getDetailProfile();
        },
        parentAction: () {
          Get.find<ParentHomeController>().getDetailProfile();
        });
  }

  getItemTabbar() {
    RoleAction().getActionByRole(
        currentRole: AppCache().userType,
        teacherAction: () {
          return "assets/images/phonebook_teacher.png";
        },
        managerAction: () {
          return "assets/images/phonebook_teacher.png";
        },
        studentAction: () {
          return selectedPageIndex == 2
              ? "assets/images/icon_phonebook.png"
              : "assets/images/icon_phonebook_nofocus.png";
        },
        parentAction: () {
          return selectedPageIndex == 2
              ? "assets/images/icon_phonebook.png"
              : "assets/images/icon_phonebook_nofocus.png";
        });
    return selectedPageIndex == 2
        ? "assets/images/icon_phonebook.png"
        : "assets/images/icon_phonebook_nofocus.png";
  }

  getPhoneBook() {
    return Container();
    // RoleAction().getActionByRole(
    //     currentRole: AppCache().userType,
    //     teacherAction: () {
    //       return MyPhoneBookPage(comeBackHome);
    //     },
    //     managerAction: () {
    //       return MyPhoneBookPage(comeBackHome);
    //     },
    //     studentAction: () {
    //       return MyPhoneBookPageStudent(comeBackHome);
    //     },
    //     parentAction: () {
    //       return MyPhoneBookPageParents(comeBackHome);
    //     });
  }

  Widget getDashboard() {
    return RoleAction().getDashboardUIByRole(
      currentRole: AppCache().userType,
    );
  }


  Widget getSchedule() {
    return RoleAction().getScheduleUIByRole(
      currentRole: AppCache().userType,
    );
  }


  comeBackHome() {
    //Move tab to first
    tabController?.animateTo(0);
  }

  comeCalendar() {
    tabController?.animateTo(1);
  }

  comeNotification() {
    tabController?.animateTo(3);
  }

  @override
  void onClose() {
    print("object");
    super.onClose();
  }
}
