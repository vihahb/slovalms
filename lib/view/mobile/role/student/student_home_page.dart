import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:task_manager/commom/utils/app_utils.dart';
import 'package:task_manager/commom/utils/color_utils.dart';
import 'package:task_manager/commom/widget/appointment_builder_timelineday.dart';
import 'package:task_manager/commom/widget/meeting_data_source.dart';
import 'package:task_manager/view/mobile/home/home_controller.dart';
import 'package:task_manager/view/mobile/phonebook/phone_book_controller.dart';
import 'package:task_manager/view/mobile/role/student/diligence_student/statistical_student/statistical_student_page.dart';
import 'package:task_manager/view/mobile/role/student/personal_information_student/personal_information_student_page.dart';
import 'package:task_manager/view/mobile/role/student/student_home_controller.dart';

import '../../../../commom/app_cache.dart';
import '../../schedule/schedule_controller.dart';

class StudentHomePage extends GetWidget<StudentHomeController> {
  @override
  final controller = Get.put(StudentHomeController());

  StudentHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    var isPhone = Get.context?.isPhone ?? true;

    DateTime oldTime = DateTime.now();
    DateTime newTime = DateTime.now();
    // LocalNotificationService.initialize(context);
    return WillPopScope(
      onWillPop: () async {
        newTime = DateTime.now();
        int difference = newTime.difference(oldTime).inMilliseconds;
        oldTime = newTime;
        if (difference < 2000) {
          return true;
        } else {
          AppUtils.shared.showToast("Nhấn back lần nữa để thoát",
              backgroundColor: ColorUtils.COLOR_BLACK.withOpacity(0.5),
              textColor: ColorUtils.COLOR_WHITE);
          return false;
        }
      },
      child: Obx(() => Scaffold(
            body: RefreshIndicator(
                color: const Color.fromRGBO(248, 129, 37, 1),
                child: SingleChildScrollView(
                  physics: const AlwaysScrollableScrollPhysics(),
                  child: Stack(
                    children: [
                      buildBanner(),
                      Container(
                        margin: EdgeInsets.only(top: (isPhone) ? 180.h : 200.h),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 8.w),
                              child: Column(
                                children: [
                                  buildIndicatorBanner(),
                                  Padding(
                                      padding: EdgeInsets.only(bottom: 8.h)),
                                  Container(
                                    decoration: BoxDecoration(
                                        color: const Color.fromRGBO(
                                            255, 255, 255, 1),
                                        borderRadius:
                                            BorderRadius.circular(8.r)),
                                    child: Column(
                                      children: [
                                        Padding(
                                            padding: EdgeInsets.only(top: 8.h)),
                                        Container(
                                          margin: EdgeInsets.fromLTRB(
                                              16.w, 8.h, 16.w, 8.h),
                                          child: Row(
                                            children: [
                                              Image.asset(
                                                  "assets/images/img_header.png", height: 48.h,width: 48.w,),
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      right: 8.h)),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Obx(
                                                    () => Text(
                                                      "Chào, ${controller.userProfile.value.fullName ?? ""}",
                                                      style: TextStyle(
                                                          fontSize: 16.sp,
                                                          color: const Color
                                                                  .fromRGBO(
                                                              26, 26, 26, 1)),
                                                    ),
                                                  ),
                                                  InkWell(
                                                      onTap: () {
                                                        Get.bottomSheet(
                                                            StatefulBuilder(
                                                                builder:
                                                                    (context,
                                                                        state) {
                                                          return Wrap(
                                                            children: [
                                                              Container(
                                                                decoration:
                                                                    BoxDecoration(
                                                                  borderRadius: BorderRadius.only(
                                                                      topRight:
                                                                          Radius.circular(32
                                                                              .r),
                                                                      topLeft: Radius
                                                                          .circular(
                                                                              32.r)),
                                                                  color: Colors
                                                                      .white,
                                                                ),
                                                                child: Column(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    Container(
                                                                      decoration: BoxDecoration(
                                                                          borderRadius: BorderRadius.circular(6
                                                                              .r),
                                                                          color: const Color.fromRGBO(
                                                                              210,
                                                                              212,
                                                                              216,
                                                                              1)),
                                                                      height:
                                                                          6.h,
                                                                      width:
                                                                          48.w,
                                                                      alignment:
                                                                          Alignment
                                                                              .topCenter,
                                                                      margin: EdgeInsets.only(
                                                                          top: 16
                                                                              .h),
                                                                    ),
                                                                    Padding(
                                                                        padding:
                                                                            EdgeInsets.only(top: 12.h)),
                                                                    Text(
                                                                      "Năm Học",
                                                                      style: TextStyle(
                                                                          fontSize: 16
                                                                              .sp,
                                                                          color: Colors
                                                                              .black,
                                                                          fontWeight:
                                                                              FontWeight.w500),
                                                                    ),
                                                                    Container(
                                                                      margin: EdgeInsets.symmetric(
                                                                          vertical: 16
                                                                              .h,
                                                                          horizontal:
                                                                              8.w),
                                                                      child: ListView.builder(
                                                                          physics: const ScrollPhysics(),
                                                                          itemCount: controller.schoolYears.value.length,
                                                                          shrinkWrap: true,
                                                                          itemBuilder: (context, index) {
                                                                            controller.classInSchoolYears.value =
                                                                                controller.schoolYears.value[index].clazz!;
                                                                            return InkWell(
                                                                              child: Column(
                                                                                children: [
                                                                                  Container(
                                                                                    color: controller.clickSchoolYear.value[index] == true ? const Color.fromRGBO(254, 230, 211, 1) : Colors.white,
                                                                                    padding: const EdgeInsets.fromLTRB(0, 8, 16, 8),
                                                                                    child: Row(
                                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                                                      children: [
                                                                                        SizedBox(
                                                                                          height: 40.h,
                                                                                          width: 40.h,
                                                                                          child: Image.network(
                                                                                            controller.schoolYears[index].image ?? "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                                                                                            errorBuilder: (context, object, stackTrace) {
                                                                                              return Image.asset(
                                                                                                "assets/images/avatar.png",
                                                                                              );
                                                                                            },
                                                                                          ),
                                                                                        ),
                                                                                        const Padding(padding: EdgeInsets.only(left: 8)),
                                                                                        Column(
                                                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                                                          children: [
                                                                                            RichText(
                                                                                              text: TextSpan(children: [
                                                                                                TextSpan(text: 'Học Sinh: ', style: TextStyle(color: Color.fromRGBO(248, 129, 37, 1), fontFamily: 'static/Inter-Regular.ttf', fontSize: 14.sp)),
                                                                                                TextSpan(text: "${controller.schoolYears.value[index].fullName!}", style: TextStyle(color: Colors.black, fontFamily: 'static/Inter-Regular.ttf', fontSize: 14.sp)),
                                                                                              ]),
                                                                                            ),
                                                                                            RichText(
                                                                                              textAlign: TextAlign.start,
                                                                                              text: TextSpan(
                                                                                                text: 'Lớp: ',
                                                                                                style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1), fontSize: 14.sp, fontWeight: FontWeight.w400, fontFamily: 'static/Inter-Medium.ttf'),
                                                                                                children: controller.classInSchoolYears.value?.map((e) {
                                                                                                  var index = controller.classInSchoolYears.value?.indexOf(e);
                                                                                                  var showSplit = ", ";
                                                                                                  if (index == (controller.classInSchoolYears.value.length)! - 1) {
                                                                                                    showSplit = "";
                                                                                                  }
                                                                                                  return TextSpan(text: "${e.name}$showSplit", style: TextStyle(color: const Color.fromRGBO(248, 129, 37, 1), fontSize: 14.sp, fontWeight: FontWeight.w500, fontFamily: 'static/Inter-Medium.ttf'));
                                                                                                }).toList(),
                                                                                              ),
                                                                                            ),
                                                                                          ],
                                                                                        ),
                                                                                        Expanded(child: Container()),
                                                                                        Row(
                                                                                          children: [
                                                                                            Text(
                                                                                              "${controller.schoolYears.value[index].fromYear}",
                                                                                              style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1), fontWeight: FontWeight.w400, fontSize: 14.sp),
                                                                                            ),
                                                                                            Text(
                                                                                              "-",
                                                                                              style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1), fontWeight: FontWeight.w400, fontSize: 14.sp),
                                                                                            ),
                                                                                            Text(
                                                                                              "${controller.schoolYears.value[index].toYear}",
                                                                                              style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1), fontWeight: FontWeight.w400, fontSize: 14.sp),
                                                                                            ),
                                                                                          ],
                                                                                        ),
                                                                                      ],
                                                                                    ),
                                                                                  ),
                                                                                ],
                                                                              ),
                                                                              onTap: () {
                                                                                updated(state, index);
                                                                               controller.onSelectSchoolYears(index);

                                                                              },
                                                                            );
                                                                          }),
                                                                    ),
                                                                    Padding(
                                                                        padding:
                                                                            EdgeInsets.only(bottom: 60.h))
                                                                  ],
                                                                ),
                                                              )
                                                            ],
                                                          );
                                                        }));
                                                      },
                                                      child: Row(
                                                        children: [
                                                          RichText(
                                                            textAlign:
                                                                TextAlign.start,
                                                            text: TextSpan(
                                                              text: 'Lớp: ',
                                                              style: TextStyle(
                                                                  color: const Color
                                                                          .fromRGBO(
                                                                      26,
                                                                      26,
                                                                      26,
                                                                      1),
                                                                  fontSize:
                                                                      14.sp,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontFamily:
                                                                      'static/Inter-Medium.ttf'),
                                                              children:
                                                                  controller
                                                                      .clazzs
                                                                      .value
                                                                      ?.map(
                                                                          (e) {
                                                                var index =
                                                                    controller
                                                                        .clazzs
                                                                        .value
                                                                        ?.indexOf(
                                                                            e);
                                                                var showSplit =
                                                                    ", ";
                                                                if (index ==
                                                                    (controller
                                                                            .clazzs
                                                                            .value
                                                                            .length)! -
                                                                        1) {
                                                                  showSplit =
                                                                      "";
                                                                }
                                                                return TextSpan(
                                                                    text:
                                                                        "${e.name}$showSplit",
                                                                    style: TextStyle(
                                                                        color: const Color.fromRGBO(
                                                                            248,
                                                                            129,
                                                                            37,
                                                                            1),
                                                                        fontSize: 14
                                                                            .sp,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w500,
                                                                        fontFamily:
                                                                            'static/Inter-Medium.ttf'));
                                                              }).toList(),
                                                            ),
                                                          ),
                                                          Padding(
                                                              padding: EdgeInsets
                                                                  .only(
                                                                      left:
                                                                          8.w)),
                                                          Icon(
                                                            Icons
                                                                .keyboard_arrow_down_outlined,
                                                            size: 18.w,
                                                            color: Colors.black,
                                                          )
                                                        ],
                                                      )),
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                        Padding(
                                            padding:
                                                EdgeInsets.only(top: 16.h)),
                                        action(),
                                        Padding(
                                            padding:
                                                EdgeInsets.only(bottom: 16.h)),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(top: 8.h)),
                            Obx(() => Container(
                                  margin: EdgeInsets.fromLTRB(
                                      16.w, 20.h, 16.w, 6.h),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text("Môn Học",
                                              style: TextStyle(
                                                  fontSize: 20.sp,
                                                  color: const Color.fromRGBO(
                                                      26, 26, 26, 1))),
                                          Padding(
                                              padding:
                                                  EdgeInsets.only(top: 6.h)),
                                          Text(
                                            "Năm ${controller.setTextSchoolYears()} bạn có ${controller.items.value.length} môn học",
                                            style: TextStyle(
                                                fontSize: 12.sp,
                                                color: const Color.fromRGBO(
                                                    133, 133, 133, 1)),
                                          ),
                                        ],
                                      ),
                                      Flexible(child: Container()),
                                      InkWell(
                                        onTap: () {
                                          controller.toSubjectPage();
                                        },
                                        child: const Text(
                                          "Xem Tất cả",
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: Color.fromRGBO(
                                                  248, 129, 37, 1)),
                                        ),
                                      )
                                    ],
                                  ),
                                )),
                            scheduleSubject(),
                            scheduleTable()
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                onRefresh: () async {
                  await controller.onRefresh();
                }),
          )),
    );
  }

  buildIndicatorBanner() {
    return SizedBox(
      height: 6.h,
      child: ListView.builder(
          itemCount: controller.itemBanner.value.length,
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return controller.itemBanner.value[index].selected
                ? Container(
                    width: 12.w,
                    height: 10,
                    margin: EdgeInsets.only(left: 4.w, right: 4.w),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(6)),
                  )
                : SizedBox(
                    width: 10.w,
                    height: 10.h,
                    child: const CircleAvatar(
                      backgroundColor: Color.fromRGBO(252, 252, 254, 0.5),
                    ),
                  );
          }),
    );
  }

  buildBanner() {
    return Obx(() => SizedBox(
        height: 240.h,
        child: PageView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: controller.itemBanner.value.length,
            onPageChanged: (value) {
              controller.selectPageBanner(value);
            },
            itemBuilder: (context, index) {
              var url = "${controller.itemBanner.value[index].url}";
              return Container(
                child: Image.network(
                  url,
                  width: double.infinity,
                  fit: BoxFit.fitWidth,
                ),
              );
            })));
  }

  scheduleTable() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.fromLTRB(16.w, 16.h, 16.w, 6.h),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Thời khóa biểu",
                      style: TextStyle(
                          fontSize: 20.sp,
                          color: const Color.fromRGBO(26, 26, 26, 1))),
                  const Padding(padding: EdgeInsets.only(top: 6)),
                  Visibility(
                    visible: controller.isSubjectToday.value,
                      child: SizedBox(
                    width: 200,
                    child: Text(
                      "Hôm nay bạn có ${controller.scheduleToday.value.length} tiết học",
                      style: TextStyle(
                          fontSize: 12.sp,
                          color: const Color.fromRGBO(133, 133, 133, 1)),
                    ),
                  )),
                ],
              ),
              Expanded(child: Container()),
              const Text(
                "Xem Tất cả",
                style: TextStyle(
                    fontSize: 12, color: Color.fromRGBO(248, 129, 37, 1)),
              )
            ],
          ),
        ),
        Padding(padding: EdgeInsets.only(top: 8.h)),
        Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
          margin: const EdgeInsets.only(left: 16, right: 16, bottom: 20),
          child: Obx(() => SfCalendar(
                view: CalendarView.timelineDay,
                firstDayOfWeek: 1,
                allowViewNavigation: false,
                dataSource: MeetingDataSource(controller.timeTable.value),
                todayHighlightColor: const Color.fromRGBO(249, 154, 81, 1),
                appointmentBuilder: appointmentBuilderTimeLineDay,
                timeSlotViewSettings: TimeSlotViewSettings(
                  timeFormat: "H a",
                  timeIntervalWidth: 100.w,
                  timelineAppointmentHeight: 50,
                ),
                controller: controller.calendarController.value,
                scheduleViewSettings: const ScheduleViewSettings(
                    appointmentItemHeight: 20, hideEmptyScheduleWeek: true),
              )),
        ),
      ],
    );
  }

  scheduleSubject() {
    return Obx(() => GridView.builder(
        padding: const EdgeInsets.all(10),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          crossAxisSpacing: 5,
          mainAxisSpacing: 5,
        ),
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        physics: const ClampingScrollPhysics(),
        itemCount: (controller.items.value.length > 6)
            ? 6
            : controller.items.value.length,
        itemBuilder: (context, index) {
          return SizedBox(
            height: 96.h,
            child: Card(
              elevation: 0.1,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 50.h,
                    width: 50.h,
                    child: Image.network(
                      "${controller.items.value[index].image}",
                      errorBuilder: (context, error, stackTrace) {
                        return Image.asset("assets/images/imageMath.png");
                      },
                    ),
                  ),
                  const Padding(padding: EdgeInsets.only(top: 8)),
                  Text(
                    "${controller.items.value[index].name}",
                    style: const TextStyle(
                        color: Color.fromRGBO(26, 26, 26, 1), fontSize: 12),
                  )
                ],
              ),
            ),
          );
        }));
  }

  Row action() {
    return Row(
      children: [
        const Padding(padding: EdgeInsets.only(left: 16)),
        Expanded(
          flex: 3,
          child: InkWell(
            onTap: () {
              //To Diem danh
              controller.goToDiligenceStudentPage();
            },
            child: Column(
              children: [
                Image.asset("assets/images/icon_diligence_std.png",width: 24.w,height: 24.h),
                const Padding(padding: EdgeInsets.only(top: 11)),
                const Text(
                  "Chuyên cần",
                  style: TextStyle(
                      color: Color.fromRGBO(26, 26, 26, 1), fontSize: 12),
                )
              ],
            ),
          ),
        ),
        Expanded(
          flex: 3,
          child: InkWell(
            child: Column(
              children: [
                Image.asset("assets/images/icon_bangdiem_std.png",width: 24.w,height: 24.h),
                const Padding(padding: EdgeInsets.only(top: 11)),
                const Text(
                  "Bảng điểm",
                  style: TextStyle(
                      color: Color.fromRGBO(26, 26, 26, 1), fontSize: 12),
                )
              ],
            ),
          ),
        ),
        Expanded(
            flex: 3,
            child: InkWell(
              onTap: () {
                //To Person Info
                Get.to(PersonalInformationStudentPage());
              },
              child: Column(
                children: [
                  Image.asset("assets/images/icon_profile.png",width: 24.w,height: 24.h,),
                  const Padding(padding: EdgeInsets.only(top: 11)),
                  const Text(
                    "Cá nhân",
                    style: TextStyle(
                        color: Color.fromRGBO(26, 26, 26, 1), fontSize: 12),
                  )
                ],
              ),
            )),
        const Padding(padding: EdgeInsets.only(right: 16)),
      ],
    );
  }

  Future<Null> updated(StateSetter updateState, int index) async {
    updateState(() {
      controller.onClickShoolYears(index);
    });
  }
}
