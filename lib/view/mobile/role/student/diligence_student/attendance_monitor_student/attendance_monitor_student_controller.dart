import 'dart:ui';

import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:task_manager/commom/app_cache.dart';

import '../../../../../../commom/utils/app_utils.dart';
import '../../../../../../data/base_service/api_response.dart';
import '../../../../../../data/model/common/diligence.dart';
import '../../../../../../data/model/common/leaving_application.dart';
import '../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../student_home_controller.dart';


class AttendanceStudentController extends GetxController {
  DateTime now = DateTime.now();
  RxInt indexClick = 0.obs;
  RxList<bool> click = <bool>[].obs;
  var groupValue = <int>[].obs;
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var diligenceClass = DiligenceClass().obs;
  var listStudentDiligence = <Diligents>[].obs;
  var student = StudentDiligent().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var colorTextRadioListTile = <Color>[].obs;
  var isAttendance = false.obs;
  var isReady = false.obs;
  var isConfirmAttendance = false.obs;
  var isHomeRoomTeacher = false.obs;
  var status = "".obs;
  @override
  void onInit() {
    super.onInit();
    getListStudentDiligence();
  }

  showColor(index) {
    click.value = [];
    for (int i = 0; i < 3; i++) {
      click.value.add(false);
    }
    click.value[index] = true;
  }

  getListStudentDiligence() {
    var classId = Get.find<StudentHomeController>().clazzs.value[0].id;
    var fromdate = DateTime(DateTime.now().year, DateTime.now().month,
            DateTime.now().day, 00, 00)
        .millisecondsSinceEpoch;
    var todate = DateTime(DateTime.now().year, DateTime.now().month,
            DateTime.now().day, 23, 59)
        .millisecondsSinceEpoch;

    _diligenceRepo
        .getListStudentDiligence(classId, fromdate, todate, "")
        .then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value = value.object!;
        listStudentDiligence.value = diligenceClass.value.studentDiligent!;
        if(diligenceClass.value.status !=null){
          status.value = diligenceClass.value.status!;
        }else{
          status.value = "";
        }
        isAttendance.value = getStatusLock(diligenceClass.value.isLock);
        for (int i = 0; i < listStudentDiligence.value.length; i++) {
          groupValue.value.add(getStatusDiligence(listStudentDiligence.value[i].statusDiligent));
          if(isAttendance.value == false){
            colorTextRadioListTile.value.add(const Color.fromRGBO(133, 133, 133, 1));
          }else{
            colorTextRadioListTile.value.add( const Color.fromRGBO(26, 26, 26, 1));
          }
        }

        print("isAttendance ${isAttendance.value}");
        print(": ${diligenceClass.value.lockedByUser?.id}");
        print("userId: ${Get.find<StudentHomeController>().userProfile.value.id}");
        groupValue.refresh();

      }

    });
    isReady.value = true;
  }

  getStatusLock(status) {
    switch (status) {
      case "TRUE":
        return true;
      case "FALSE":
        return false;
    }
  }




  getStatusConfirmAttendance(status){
      switch (status) {
        case "DRAFT":
          return false;
        case "CONFIRM":
          return true;
        default:
          return false;
      }
  }

  getStatusDiligence(status) {
    switch (status) {
      case "ON_TIME":
        return 1;
      case "NOT_ON_TIME":
        return 2;
      case "EXCUSED_ABSENCE":
        return 3;
      case "ABSENT_WITHOUT_LEAVE":
        return 4;
      default:
        return 0;
    }
  }

  openAttendance() {
    var id = diligenceClass.value.id!;
    _diligenceRepo.toggleAttendance(id, "TRUE").then((value) {
      if (value.state == Status.SUCCESS) {
      }else{
        AppUtils().snackbarSuccess("Lỗi", value.message!);
      }
      if(value.object?.code == 400){
        getListStudentDiligence();
      }
    });
  }

  closeAttendance() {
    var id = diligenceClass.value.id!;
    _diligenceRepo.toggleAttendance(id, "FALSE").then((value) {
      if (value.state == Status.SUCCESS) {
      }else{
        AppUtils().snackbarSuccess("Lỗi", value.message!);
      }
      if(value.object?.code == 400){
        getListStudentDiligence();
      }
    });
  }

  setStatusLockAttendance() {
    if(isAttendance.value == true){
      if (diligenceClass.value.lockedByUser?.id == AppCache().userId){
        isAttendance.value = !isAttendance.value;
        colorAttendance(isAttendance.value);
      }else if (diligenceClass.value.lockedByUser?.id == ""||diligenceClass.value.lockedByUser?.id == null){
        isAttendance.value = !isAttendance.value;
        colorAttendance(isAttendance.value);
      }else{
        AppUtils().showToast("${diligenceClass.value.lockedByUser?.fullName??""} đang điểm danh");
      }
    }else{
      isAttendance.value = !isAttendance.value;
      colorAttendance(isAttendance.value);
    }
  }


  colorAttendance(isOpen) {
    if (isOpen != true) {
      closeAttendance();
      for (int i = 0; i < listStudentDiligence.value.length; i++) {
        colorTextRadioListTile.value[i] = Color.fromRGBO(133, 133, 133, 1);
      }
      colorTextRadioListTile.refresh();
    } else {
      openAttendance();
      for (int i = 0; i < listStudentDiligence.value.length; i++) {
        colorTextRadioListTile.value[i] = Color.fromRGBO(26, 26, 26, 1);
      }
      colorTextRadioListTile.refresh();
    }
  }


  attendanceStudent(statusDiligent, index) async {
    var id = diligenceClass.value.id;
    _diligenceRepo.attendanceStudent(id, statusDiligent, listStudentDiligence[index].id).then((value) {
      if (value.state == Status.SUCCESS) {}
    });
  }
  updateAttendanceStudent(statusDiligent, index) async {
    var id = diligenceClass.value.id;
    _diligenceRepo.updateAttendanceStudent(id, statusDiligent, listStudentDiligence[index].id).then((value) {
      if (value.state == Status.SUCCESS) {}
    });
  }

  onclickAttendance(index, value) {
    if(diligenceClass.value.status == "CONFIRM"){
    }else{
      if (diligenceClass.value.lockedByUser?.id == AppCache().userId || diligenceClass.value.lockedByUser?.id == ""|| diligenceClass.value.lockedByUser?.id == null) {
        if (isAttendance.value == true) {
          groupValue.value[index] = value!;
          switch (value) {
            case 1:
              attendanceStudent("ON_TIME", index);
              break;
            case 2:
              attendanceStudent("NOT_ON_TIME", index);
              break;
            case 3:
              attendanceStudent("EXCUSED_ABSENCE", index);
              break;
            case 4:
              attendanceStudent("ABSENT_WITHOUT_LEAVE", index);
              break;
          }
          groupValue.refresh();
        } else {
          AppUtils().showToast("${diligenceClass.value.lockedByUser?.fullName??""} đang điểm danh");
        }
      }
    }
  }

  updateAttendance(index, value){
    switch (value) {
      case 1:
        updateAttendanceStudent("ON_TIME", index);
        break;
      case 2:
        updateAttendanceStudent("NOT_ON_TIME", index);
        break;
      case 3:
        updateAttendanceStudent("EXCUSED_ABSENCE", index);
        break;
      case 4:
        updateAttendanceStudent("ABSENT_WITHOUT_LEAVE", index);
        break;
    }
    groupValue.refresh();

  }

  onRefresh(){

  }

}
