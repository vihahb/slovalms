import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get/get.dart';
import 'package:task_manager/view/mobile/role/student/diligence_student/statistical_student/statistical_student_page.dart';

import '../student_home_controller.dart';
import 'attendance_monitor_student/attendance_monitor_student_page.dart';
class DiligenceStudentController extends GetxController{


  String ON_TIME_TEXT = "Đúng giờ";
  String NOT_ON_TIME_TEXT= "Đi muộn";
  String ABSENT_WITHOUT_LEAVE_TEXT = "Không phép";
  var focusedDay = DateTime.now().obs;
  var selectedDay = DateTime.now().obs;
  var isvisibility = false.obs;
  var isvisibility_icon = false.obs;
  var isvisibility_latetime = false.obs;
  var text_color = false.obs;
  RxList<String> listStatus = <String>[].obs;

  @override
  void onInit() {
    super.onInit();
    print("monitorClassID: ${Get.find<StudentHomeController>().userProfile.value.classOfMonitorStudent}");
    print("currentClassID: ${Get.find<StudentHomeController>().clazzs.value[0].id}");
  }

  visibleDiligenceStudent(){
    if(Get.find<StudentHomeController>().userProfile.value.classOfMonitorStudent!.contains(Get.find<StudentHomeController>().clazzs.value[0].id)){
      return true;
    }else{
      return false;
    }


  }

  goToStatisticalStudent(){
    Get.to(StatisticalStudentPage());
  }
  goToAttendanceMonitorStudent(){
    Get.to(AttendanceMonitorStudentPage());
  }

}