import 'package:flutter/cupertino.dart';
import 'package:flutter_neat_and_clean_calendar/flutter_neat_and_clean_calendar.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';
class DetailStatisticalStudentController extends GetxController{
  var format = CalendarFormat.month.obs;
  var focusedDay = DateTime.now().obs;
  var selectedDay = DateTime.now().obs;
  var focusName = FocusNode().obs;
  var controllerName = TextEditingController().obs;
  var isAddShowJob = false.obs;
  var dateNow ="".obs;
  var showDetailDiligence = true.obs;
  String ON_TIME_TEXT = "Đúng giờ";
  String NOT_ON_TIME_TEXT= "Đi muộn";
  String ABSENT_WITHOUT_LEAVE_TEXT = "Không phép";
  String EXCUSED_TEXT = "Có phép";
  RxInt date= 0.obs;
  RxInt time= 0.obs;
  var status = ''.obs;
  var outputFormat = DateFormat('HH:mm:ss');
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var objectEvent = NeatCleanCalendarEvent("", startTime: DateTime.now(), endTime: DateTime.now()).obs;
  @override
  void onInit() {
    // TODO: implement onInit
    var data = Get.arguments;
    if(data != null){
      date.value = data[0];
      status.value = data[1];
      time.value= data[2];
    }
    dateNow.value=outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(date.value!));
    focusedDay.value= DateTime(int.parse(dateNow.value.substring(6,10)),int.parse(dateNow.value.substring(3,5)),int.parse(dateNow.value.substring(0,2)));
  }
  getColorStatus(state) {
    switch (state) {
      case "ON_TIME":
        return Color.fromRGBO(221, 246, 235, 1);
      case "NOT_ON_TIME":
        return Color.fromRGBO(255, 239, 203, 1);
      case "ABSENT_WITHOUT_LEAVE":
        return Color.fromRGBO(255, 200, 206, 1);
      default:
        return Color.fromRGBO(255, 200, 206, 1);
    }
  }

  getColorTextStatus(state) {
    switch (state) {
      case "ON_TIME":
        return Color.fromRGBO(77, 197, 145, 1);
      case "NOT_ON_TIME":
        return Color.fromRGBO(253, 185, 36, 1);
      case "ABSENT_WITHOUT_LEAVE":
        return Color.fromRGBO(255, 69, 89, 1);
      default:
        return Color.fromRGBO(255, 69, 89, 1);
    }
  }

  getTextStatus(state) {
    switch (state) {
      case "ON_TIME":
        return ON_TIME_TEXT;
      case "NOT_ON_TIME":
        return NOT_ON_TIME_TEXT;
      case "ABSENT_WITHOUT_LEAVE":
        return ABSENT_WITHOUT_LEAVE_TEXT;
      default:
        return EXCUSED_TEXT;
    }
  }

}