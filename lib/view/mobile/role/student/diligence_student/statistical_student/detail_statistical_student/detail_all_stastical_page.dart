import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:task_manager/view/mobile/home/home_controller.dart';
import 'package:task_manager/view/mobile/role/student/diligence_student/statistical_student/detail_statistical_student/detail_all_stastical_controller.dart';
import 'package:task_manager/view/mobile/role/student/student_home_controller.dart';
import 'package:flutter_neat_and_clean_calendar/flutter_neat_and_clean_calendar.dart';
import 'package:task_manager/view/mobile/role/student/student_home_page.dart';
import 'detail_statistical_student_controller.dart';

class DetailStatisticalAllStudentPage
    extends GetWidget<DetailStatisticalAllStudentController> {
  final controller = Get.put(DetailStatisticalAllStudentController());

  @override
  Widget build(BuildContext context) {

    return Obx(() => Scaffold(
        backgroundColor: Color.fromRGBO(245, 245, 245, 1),
        appBar: AppBar(
          actions: [
            IconButton(
              onPressed: () {
                Get.to(StudentHomePage());
              },
              icon: const Icon(Icons.home),
              color: Colors.white,
            )
          ],
          backgroundColor: Color.fromRGBO(248, 129, 37, 1),
          title: Text(
            'Chuyên Cần',
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.sp,
                fontFamily: 'static/Inter-Medium.ttf'),
          ),
        ),
        body: SingleChildScrollView(
            physics: ScrollPhysics(),
            child:
            Column(
              children: [
                Container(
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.only(top: 16.h, left: 16.w, bottom: 8.h),
                        child: Text(
                          'Chi Tiết',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(26, 26, 26, 1), fontSize: 12.sp),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 16.w, right: 16.w),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: Color.fromRGBO(255, 255, 255, 1)),
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 16.h, right: 16.h, left: 16.h),
                              child: Row(
                                children: [
                                  CircleAvatar(
                                    backgroundColor: Colors.white,
                                    backgroundImage: Image.network(
                                      Get.find<StudentHomeController>().userProfile.value.image??
                                          "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                                      errorBuilder:
                                          (context, object, stackTrace) {
                                        return Image.asset(
                                          "assets/images/img_Noavt.png",
                                        );
                                      },
                                    ).image,
                                  ),

                                  Padding(padding: EdgeInsets.only(left: 8.w)),
                                  Text(
                                    '${Get.find<StudentHomeController>().userProfile.value.fullName}',
                                    style: TextStyle(
                                        color: Color.fromRGBO(26, 26, 26, 1),
                                        fontSize: 12.sp),
                                  ),
                                  Expanded(child: Container()),
                                  Text("${controller.outputDateFormat.format(controller.focusedDay.value)}",
                                    style: TextStyle(
                                        color: Color.fromRGBO(26, 26, 26, 1),
                                        fontSize: 12.sp),
                                  )
                                ],
                              ),
                            ),
                           Obx(() =>  Container(
                             height: 425.h,
                             child:
                             Calendar(
                               eventListBuilder: (BuildContext context,
                                   List<NeatCleanCalendarEvent> selectesdEvents) {
                                 return
                               controller.ShowDatailDiligence.value ?
                               Obx(() =>
                                       Container(
                                         width: double.infinity,
                                           margin: EdgeInsets.only(top: 16.h, right: 16.w, left: 16.w),
                                           padding: EdgeInsets.all(8),
                                           decoration: BoxDecoration(
                                               color: Color.fromRGBO(245, 245, 245, 1),
                                               borderRadius: BorderRadius.circular(8)
                                           ),
                                           child:  Column(
                                             children: [
                                               Padding(padding: EdgeInsets.only(top: 8.h)),
                                               Container(
                                                   child: Row(
                                                     children: [
                                                       Image.asset('assets/images/icon_diligence_student.png', width: 12.w,height: 12.h,),
                                                       Padding(padding: EdgeInsets.only(right: 8.r)),
                                                       Text("Điểm danh lúc ${controller.objectEvent.value.location}")
                                                     ],
                                                   )
                                               ),
                                               Container(
                                                 margin: EdgeInsets.only(left:10.w, top: 4.h),
                                                 child:   Column(
                                                   crossAxisAlignment: CrossAxisAlignment.start,
                                                   children: [
                                                     Padding(padding: EdgeInsets.only(top:6.h)),
                                                     Row(
                                                       children: [
                                                         Icon(
                                                           Icons.location_on,
                                                           color: Color.fromRGBO(177, 177, 177, 1),
                                                           size: 12,
                                                         ),
                                                         Container(
                                                           height: 30.h,
                                                           width:250.w,
                                                           margin: EdgeInsets.only(left: 6),
                                                           child: Text(
                                                             '${Get.find<StudentHomeController>().userProfile.value.school!.name}, ${Get.find<StudentHomeController>().userProfile.value.school!.address}',
                                                             style: TextStyle(
                                                                 color: Color.fromRGBO(177, 177, 177, 1),
                                                                 fontSize: 10.sp),
                                                           ),
                                                         )
                                                       ],
                                                     ),
                                                     Padding(padding: EdgeInsets.only(top: 8.h)),
                                                     Container(
                                                       height: 20.h,
                                                       padding: EdgeInsets.only(top: 3,bottom: 3,right: 12,left: 12),
                                                       decoration: BoxDecoration(
                                                         borderRadius: BorderRadius.circular(6),
                                                         color:controller.objectEvent.value.color,
                                                       ),
                                                       child: Text('${controller.objectEvent.value.description}',textAlign: TextAlign.center,style: TextStyle(fontSize: 12.sp,color: Colors.white),),)
                                                   ],
                                                 )
                                               ),
                                             ],
                                           ),
                                       ))
                                   : Container();
                               },
                               startOnMonday: true,
                               weekDays: ['Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7', 'CN'],
                               eventsList: controller.eventList.value,
                               isExpandable: false,
                               eventDoneColor: Colors.green,
                               selectedColor: Colors.orange,
                               selectedTodayColor: Colors.orange,
                               todayColor: Colors.orange,
                               eventColor: null,
                               locale: 'vi',
                               initialDate: controller.focusedDay.value,
                               onDateSelected: (value){
                                 for(int i=0;i <controller.eventList.value.length; i++){
                                   controller.focusedDay.value = value;
                                   if(controller.eventList.value[i].startTime == value){
                                     controller.objectEvent.value =controller.eventList.value[i];
                                     controller.ShowDatailDiligence.value = true;
                                     return;
                                   }else{
                                     controller.ShowDatailDiligence.value = false;
                                   }
                                 }
                               },
                               todayButtonText: 'Ngày hôm nay',
                               multiDayEndText: 'Kết thúc',
                               allDayEventText: "Thời gian",
                               isExpanded: true,
                               onEventLongPressed: (value) {
                               },
                               hideTodayIcon: true,
                               hideBottomBar: true,
                               expandableDateFormat: 'EEEE, dd/MM/yyyy',
                               datePickerType: DatePickerType.hidden,
                               dayOfWeekStyle: TextStyle(
                                   color: Colors.orange, fontWeight: FontWeight.w800, fontSize: 10),
                             ),
                           ))
                            // dateTimeSelectV2()
                          ],
                        ),
                      ),
                    ],
                  ),
                ),

              ],
            )

        ),
    ));
  }

}
