import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:task_manager/view/mobile/role/student/student_home_controller.dart';
import 'package:flutter_neat_and_clean_calendar/flutter_neat_and_clean_calendar.dart';
import 'package:task_manager/view/mobile/role/student/student_home_page.dart';
import 'detail_statistical_student_controller.dart';

class DetailStatisticalStudentPage
    extends GetWidget<DetailStatisticalStudentController> {
  final controller = Get.put(DetailStatisticalStudentController());

  @override
  Widget build(BuildContext context) {
    var  eventList = [
      NeatCleanCalendarEvent('',
        isMultiDay: false,
        description: '${controller.getTextStatus(controller.status.value)}',
        startTime: DateTime.fromMillisecondsSinceEpoch(controller.date.value),
        endTime: DateTime.fromMillisecondsSinceEpoch(controller.date.value),
        color: controller.getColorTextStatus(controller.status.value),

      ),
    ].obs;
    return Obx(() => Scaffold(
          backgroundColor: Color.fromRGBO(245, 245, 245, 1),
          appBar: AppBar(
            actions: [
              IconButton(
                onPressed: () {
                  Get.to(StudentHomePage());
                },
                icon: const Icon(Icons.home),
                color: Colors.white,
              )
            ],
            backgroundColor: Color.fromRGBO(248, 129, 37, 1),
            title: Text(
              'Chuyên Cần',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.sp,
                  fontFamily: 'static/Inter-Medium.ttf'),
            ),
          ),
          body:     Column(
            children: [
              SingleChildScrollView(
                  physics: ScrollPhysics(),
                  child:
                  Column(
                    children: [
                      Container(
                        child: Column(
                          children: [
                            Container(
                              alignment: Alignment.centerLeft,
                              margin: EdgeInsets.only(top: 16.h, left: 16.w, bottom: 8.h),
                              child: Text(
                                'Chi Tiết',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Color.fromRGBO(26, 26, 26, 1), fontSize: 12.sp),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 16.w, right: 16.w),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(6),
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                              child: Column(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(top: 16.h, right: 16.h, left: 16.h),
                                    child: Row(
                                      children: [
                                        CircleAvatar(
                                          backgroundColor: Colors.white,
                                          backgroundImage: Image.network(
                                            Get.find<StudentHomeController>().userProfile.value.image??
                                                "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                                            errorBuilder:
                                                (context, object, stackTrace) {
                                              return Image.asset(
                                                "assets/images/img_Noavt.png",
                                              );
                                            },
                                          ).image,
                                        ),
                                        Padding(padding: EdgeInsets.only(left: 8.w)),
                                        Text(
                                          '${Get.find<StudentHomeController>().userProfile.value.fullName}',
                                          style: TextStyle(
                                              color: Color.fromRGBO(26, 26, 26, 1),
                                              fontSize: 12.sp),
                                        ),
                                        Expanded(child: Container()),
                                        Text("${controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.date.value!))}",
                                          style: TextStyle(
                                              color: Color.fromRGBO(26, 26, 26, 1),
                                              fontSize: 12.sp),
                                        )
                                      ],
                                    ),
                                  ),
                                  Obx(() => Container(
                                    height: 280.h,
                                    child:  Calendar(
                                      eventListBuilder: (BuildContext context,
                                          List<NeatCleanCalendarEvent> _selectesdEvents) {
                                        return new Text("");
                                      },
                                      bottomBarArrowColor: Colors.black87,
                                      bottomBarColor: Colors.orange,
                                      startOnMonday: false,
                                      weekDays: ['Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7', 'CN'],
                                      eventsList: eventList,
                                      isExpandable: false,
                                      eventDoneColor: Colors.green,
                                      selectedColor: Colors.orange,
                                      selectedTodayColor: Colors.orange,
                                      todayColor: Colors.orange,
                                      eventColor: null,
                                      locale: 'vi',
                                      initialDate: controller.focusedDay.value,
                                      onDateSelected: (value) {
                                        for(int i =0; i<eventList.value.length; i++){
                                          if(eventList.value[i].startTime == value){
                                            controller.objectEvent.value = eventList.value[i];
                                            controller.focusedDay.value= eventList.value[i].startTime;
                                            controller.showDetailDiligence.value = true;
                                            return;
                                          }else{
                                            controller.showDetailDiligence.value = false;
                                          }
                                        }
                                      },
                                      todayButtonText: 'Ngày hôm nay',
                                      multiDayEndText: 'Kết thúc',
                                      isExpanded: true,
                                      onEventSelected: (value) {
                                      },
                                      onEventLongPressed: (value) {
                                      },
                                      hideTodayIcon: true,
                                      hideBottomBar: true,
                                      expandableDateFormat: 'EEEE, dd/MM/yyyy',
                                      datePickerType: DatePickerType.hidden,
                                      dayOfWeekStyle: TextStyle(
                                          color: Colors.orange, fontWeight: FontWeight.w800, fontSize: 10),
                                    ),
                                  )),
                                  // dateTimeSelectV2()
                                ],
                              ),
                            ),
                            controller.showDetailDiligence.value?
                            Container(
                              margin: EdgeInsets.only(top: 16.h, right: 16.w, left: 16.w),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(6),
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                              child:
                              Column(
                                children: [
                                  ListTile(
                                    title : Column(
                                      children: [
                                        Padding(padding: EdgeInsets.only(top: 8.h)),
                                        Container(
                                            width: double.infinity,
                                            child: Row(
                                              children: [
                                                Image.asset('assets/images/icon_diligence_student.png', width: 12.w,height: 12.h,),
                                                Padding(padding: EdgeInsets.only(right: 8.r)),
                                                Text('Điểm danh lúc ${controller.outputFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.time.value))??""}',style: TextStyle(color: Colors.black, fontSize: 12.sp, fontWeight: FontWeight.w400),)
                                              ],
                                            )
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(left:10.w, top: 4.h),
                                            child:   Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Padding(padding: EdgeInsets.only(top:6.h)),
                                                Row(
                                                  children: [
                                                    Icon(
                                                      Icons.location_on,
                                                      color: Color.fromRGBO(177, 177, 177, 1),
                                                      size: 12,
                                                    ),
                                                    Container(
                                                      height: 30.h,
                                                      width:250.w,
                                                      margin: EdgeInsets.only(left: 6),
                                                      child: Text(
                                                        '${Get.find<StudentHomeController>().userProfile.value.school!.name}, ${Get.find<StudentHomeController>().userProfile.value.school!.address}',
                                                        style: TextStyle(
                                                            color: Color.fromRGBO(177, 177, 177, 1),
                                                            fontSize: 10.sp),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                Padding(padding: EdgeInsets.only(top: 8.h)),
                                                Container(
                                                  height: 20.h,
                                                  padding: EdgeInsets.only(top: 3,bottom: 3,right: 12,left: 12),
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(6),
                                                    color: controller.getColorStatus(controller.status.value),
                                                  ),
                                                  child:
                                                  Text('${controller.getTextStatus(controller.status.value)}', style:  TextStyle(color: controller.getColorTextStatus(controller.status.value), fontSize: 10.sp),),),
                                              ],
                                            )
                                        ),
                                        // Container(
                                        //   width: double.infinity,
                                        //   margin: EdgeInsets.only(left:10.w, top: 4.h),
                                        //   child: IntrinsicHeight(
                                        //     child: Row(
                                        //       mainAxisAlignment: MainAxisAlignment.start,
                                        //       children: [
                                        //         Column(
                                        //           crossAxisAlignment: CrossAxisAlignment.start,
                                        //           children: [
                                        //             Padding(padding: EdgeInsets.only(top:6.h)),
                                        //             Stack(
                                        //               children: [
                                        //                 Positioned(
                                        //                   top: 0,
                                        //                   left: -2.5,
                                        //                   child: Icon(
                                        //                     Icons.location_on,
                                        //                     color: Color.fromRGBO(177, 177, 177, 1),
                                        //                     size: 12,
                                        //                   ),
                                        //                 ),
                                        //                 Container(
                                        //
                                        //                   margin: EdgeInsets.only(left: 10),
                                        //                   child: Text(
                                        //                     '${Get.find<StudentHomeController>().userProfile.value.school!.name},${Get.find<StudentHomeController>().userProfile.value.school!.address ??""}',
                                        //                     style: TextStyle(
                                        //                         color: Color.fromRGBO(177, 177, 177, 1),
                                        //                         fontSize: 10.sp),
                                        //                   ),
                                        //                 )
                                        //               ],
                                        //             ),
                                        //             Padding(padding: EdgeInsets.only(top: 4.h)),
                                        //             Row(
                                        //               children: [
                                        //                 Container(
                                        //                   alignment: Alignment.center,
                                        //                   height: 20.h,
                                        //                   padding: EdgeInsets.only(left: 12, right: 12),
                                        //                   decoration: BoxDecoration(
                                        //                     borderRadius: BorderRadius.circular(6),
                                        //                     color: controller.getColorStatus(controller.status.value),
                                        //                   ),
                                        //                   child:
                                        //                   Text('${controller.getTextStatus(controller.status.value)}', style:  TextStyle(color: controller.getColorTextStatus(controller.status.value), fontSize: 10.sp),),),
                                        //
                                        //               ],
                                        //             )
                                        //           ],
                                        //         )
                                        //       ],
                                        //     ),
                                        //   ),
                                        // ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),

                            ): Container()
                          ],
                        ),
                      ),

                    ],
                  )

              ),

              // Expanded(child: Container()),
              // Container(
              //   color: Color.fromRGBO(255, 255, 255, 1),
              //   height: 70.h,
              //   padding: EdgeInsets.all(16),
              //   child: Row(
              //     children: [
              //      Expanded(child:  SizedBox(
              //        height: 46.h,
              //        child: ElevatedButton(
              //            style: ElevatedButton.styleFrom(
              //                side: BorderSide(
              //                    width: 1,
              //                    color: Color.fromRGBO(248, 129, 37, 1)),
              //                backgroundColor:
              //                Color.fromRGBO(255, 255, 255, 1)),
              //            onPressed: () {},
              //            child: Text(
              //              'Thống Kê',
              //              style: TextStyle(
              //                  color: Color.fromRGBO(248, 129, 37, 1),
              //                  fontSize: 16),
              //            )),
              //      )),
              //      Padding(padding: EdgeInsets.only(right: 16.w)),
              //      Expanded(child:  SizedBox(
              //        height: 46.h,
              //        child: ElevatedButton(
              //            style: ElevatedButton.styleFrom(
              //              backgroundColor: Color.fromRGBO(248, 129, 37, 1),
              //            ),
              //            onPressed: () {},
              //            child: Text(
              //              'Về Trang Chủ',
              //              style: TextStyle(
              //                  color: Color.fromRGBO(255, 255, 255, 1),
              //                  fontSize: 16),
              //            )),
              //      ))
              //     ],
              //   ),
              // ),
            ],
          )
        ));
  }
}
