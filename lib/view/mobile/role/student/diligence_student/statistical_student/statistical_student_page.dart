import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:task_manager/commom/constants/date_format.dart';
import 'package:task_manager/commom/utils/date_time_picker.dart';
import 'package:task_manager/view/mobile/role/student/diligence_student/statistical_student/statistical_student_controller.dart';
import 'package:task_manager/view/mobile/role/student/student_home_controller.dart';
import 'package:task_manager/view/mobile/role/student/student_home_page.dart';

import '../../../../../../commom/utils/time_utils.dart';

class StatisticalStudentPage extends GetView<StatisticalStudentController> {
  @override
  Widget build(BuildContext context) {
    var controller = Get.put(StatisticalStudentController());
    return Obx(() => Scaffold(
          backgroundColor: const Color.fromRGBO(245, 245, 245, 1),
          appBar: AppBar(
            actions: [
              IconButton(
                onPressed: () {
                  Get.to(StudentHomePage());
                },
                icon: const Icon(Icons.home),
                color: Colors.white,
              )
            ],
            backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
            title: const Text(
              'Chuyên Cần',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontFamily: 'static/Inter-Medium.ttf'),
            ),
          ),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.all(16),
                color: const Color.fromRGBO(255, 255, 255, 1),
                child: Row(
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.white,
                      backgroundImage: Image.network(
                        controller.userProfile.value.image??
                            "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                        errorBuilder:
                            (context, object, stackTrace) {
                          return Image.asset(
                            "assets/images/img_Noavt.png",
                          );
                        },
                      ).image,
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${controller.userProfile.value.fullName}',
                            style: TextStyle(
                                color: const Color.fromRGBO(26, 26, 26, 1),
                                fontSize: 12.sp),
                          ),
                          Padding(padding: EdgeInsets.only(top: 4.h)),
                          Text(
                            '${controller.userProfile.value.school!.name}',
                            style: TextStyle(
                                color: const Color.fromRGBO(177, 177, 177, 1),
                                fontSize: 12.sp),
                          ),
                        ],
                      ),
                    ),
                    Expanded(child: Container()),
                    InkWell(
                      onTap: (){
                        controller.goToDetailStatisticalStudent();
                      },
                      child:     Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              color: const Color.fromRGBO(254, 230, 211, 1)),
                          width: 80,
                          height: 18,
                          child:  Text("Chi tiết",
                              style: TextStyle(
                                  color: Color.fromRGBO(248, 129, 37, 1),
                                  fontSize: 12,
                                  fontFamily: 'static/Inter-Medium.ttf'))),
                    )
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(
                    top: 16, bottom: 8, right: 16, left: 16),
                child: Row(
                  children: [
                    const Text(
                      'Thống Kê',
                      style: TextStyle(
                          color: Color.fromRGBO(26, 26, 26, 1),
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'assets/font/static/Inter-Medium.ttf'),
                    ),
                    Expanded(child: Container()),
                    Row(
                      children: [
                        // Text(
                        //   'Tháng ${controller.outputMonthFormat.format(DateTime.now())} ',
                        //   style: TextStyle(
                        //       color: Color.fromRGBO(177, 177, 177, 1),
                        //       fontSize: 12),
                        // ),
                        InkWell(
                          onTap: (){
                            Get.dialog(
                                Dialog(
                                  child: IntrinsicHeight(
                                      child:
                                      Container(
                                        margin: EdgeInsets.symmetric(horizontal: 16,vertical: 16),
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(6)
                                        ),
                                        child:
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only( bottom: 8.h),
                                              child:
                                              Text("Vui lòng chọn ngày", style:  TextStyle(color: Colors.black, fontWeight: FontWeight.w500, fontSize: 12.sp),),),
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: Container(
                                                    alignment: Alignment.centerLeft,
                                                    padding: EdgeInsets.only(
                                                        left: 8.w),
                                                    decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        borderRadius:
                                                        const BorderRadius.all(Radius.circular(6.0)),
                                                        border: Border.all(
                                                          width: 1,
                                                          style: BorderStyle.solid,
                                                          color: const Color.fromRGBO(192, 192, 192, 1),
                                                        )),
                                                    child:
                                                    TextFormField(
                                                      keyboardType: TextInputType.multiline,
                                                      maxLines: null,
                                                      style: TextStyle(
                                                        fontSize: 10.0.sp,
                                                        color: const Color.fromRGBO(26, 26, 26, 1),
                                                      ),
                                                      onTap: () {
                                                        selectDateTimeStart(controller.controllerdateStart.value.text, DateTimeFormat.formatDateShort,context);
                                                      },
                                                      cursorColor: const Color.fromRGBO(248, 129, 37, 1),
                                                      controller:
                                                      controller.controllerdateStart.value,
                                                      readOnly: true,
                                                      decoration: InputDecoration(
                                                        suffixIcon: Container(
                                                            child:SizedBox(
                                                              height: 1,
                                                              width: 1,
                                                              child: SvgPicture.asset(
                                                                "assets/images/icon_date_picker.svg",
                                                                fit: BoxFit.scaleDown,
                                                              ),
                                                            )
                                                        ),
                                                        labelText: "Từ ngày",
                                                        border: InputBorder.none,
                                                        labelStyle: TextStyle(
                                                            color: const Color.fromRGBO(177, 177, 177, 1),
                                                            fontSize: 12.sp,
                                                            fontWeight: FontWeight.w500,
                                                            fontFamily: 'assets/font/static/Inter-Medium.ttf'
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Padding(padding: EdgeInsets.only(right: 16.w)),
                                                Expanded(
                                                  child: Container(
                                                    alignment: Alignment.centerLeft,
                                                    padding: EdgeInsets.only(
                                                        left: 8.w),
                                                    decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        borderRadius:
                                                        const BorderRadius.all(Radius.circular(6.0)),
                                                        border: Border.all(
                                                          width: 1,
                                                          style: BorderStyle.solid,
                                                          color: const Color.fromRGBO(192, 192, 192, 1),
                                                        )),
                                                    child: TextFormField(
                                                      keyboardType: TextInputType.multiline,
                                                      maxLines: null,
                                                      style: TextStyle(
                                                        fontSize: 10.0.sp,
                                                        color: const Color.fromRGBO(26, 26, 26, 1),
                                                      ),
                                                      onTap: () {

                                                        selectDateTimeEnd(controller.controllerdateEnd.value.text, DateTimeFormat.formatDateShort,context);
                                                        FocusScope.of(context).nextFocus();
                                                      },
                                                      readOnly: true,
                                                      cursorColor: const Color.fromRGBO(248, 129, 37, 1),
                                                      controller:
                                                      controller.controllerdateEnd.value,
                                                      decoration: InputDecoration(
                                                        suffixIcon:
                                                        SizedBox(
                                                          height: 1,
                                                          width: 1,
                                                          child: SvgPicture.asset(
                                                            "assets/images/icon_date_picker.svg",
                                                            fit: BoxFit.scaleDown,
                                                          ),
                                                        ),
                                                        label: const Text("Đến ngày"),
                                                        border: InputBorder.none,
                                                        labelStyle: TextStyle(
                                                            color: const Color.fromRGBO(177, 177, 177, 1),
                                                            fontSize: 12.sp,
                                                            fontWeight: FontWeight.w500,
                                                            fontFamily: 'assets/font/static/Inter-Medium.ttf'
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Padding(padding: EdgeInsets.only(top: 16)),
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                Expanded(child:
                                                SizedBox(
                                                  height: 24.h,
                                                  child:  ElevatedButton(
                                                      style: ElevatedButton.styleFrom(
                                                          backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
                                                          side: const BorderSide(
                                                              color: Color.fromRGBO(248, 129, 37, 1),width: 1
                                                          ),
                                                          shape: RoundedRectangleBorder(
                                                            borderRadius: BorderRadius.circular(6),
                                                          )
                                                      ),
                                                      onPressed: (){
                                                        Get.back();
                                                      },
                                                      child: Text('Hủy', style: TextStyle(color: const Color.fromRGBO(248, 129, 37, 1), fontSize: 12.sp, fontWeight: FontWeight.w400, fontFamily: 'assets/font/static/Inter-Regular.ttf'),)),
                                                )),
                                                Padding(padding: EdgeInsets.only(right: 8.w)),
                                                Expanded(child:SizedBox(
                                                  height: 24.h,
                                                  child: ElevatedButton(
                                                      style: ElevatedButton.styleFrom(
                                                          backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
                                                          side: const BorderSide(
                                                              color: Color.fromRGBO(248, 129, 37, 1),width: 1
                                                          ),
                                                          shape: RoundedRectangleBorder(
                                                            borderRadius: BorderRadius.circular(6),
                                                          )
                                                      ),
                                                      onPressed: (){
                                                        controller.dayAbsent.value.clear();
                                                        controller.dayExcused.value.clear();
                                                        controller.dayonShool.value.clear();
                                                        controller.dayNotontime.value.clear();


                                                        controller.getListDiligence(
                                                            DateTime(int.parse(controller.dateStart.value.substring(6,10)), int.parse(controller.dateStart.value.substring(4,5)),int.parse(controller.dateStart.value.substring(1,2))).millisecondsSinceEpoch,
                                                            DateTime(int.parse(controller.dateEnd.value.substring(6,10)), int.parse(controller.dateEnd.value.substring(4,5)),int.parse(controller.dateEnd.value.substring(0,2))).millisecondsSinceEpoch
                                                        );

                                                        controller.dayonShool.refresh();
                                                        controller.dayNotontime.refresh();
                                                        controller.dayAbsent.refresh();
                                                        controller.dayExcused.refresh();

                                                        Get.back();
                                                      },
                                                      child: Text('Xác Nhận', style: TextStyle(color: const Color.fromRGBO(255, 255, 255, 1), fontSize: 12.sp, fontWeight: FontWeight.w400, fontFamily: 'assets/font/static/Inter-Regular.ttf'),)),
                                                ))
                                              ],
                                            ),

                                          ],
                                        ),
                                      )
                                  ),
                                )
                            );
                          },
                          child:
                          Row(
                            children: [
                              Text(
                                "${controller.controllerdateStart.value.text} - ${controller.controllerdateEnd.value.text}",
                                style: TextStyle(
                                    color: Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                    fontFamily:
                                    'assets/font/static/Inter-Medium.ttf'),
                              ),
                              Padding(padding: EdgeInsets.only(right: 8)),
                              SizedBox(
                                child: Image.asset("assets/images/icon_calenda.png",height: 16, width: 16,),
                              ),
                            ],
                          )
                        ),
                        Padding(padding: EdgeInsets.only(right: 4.w)),

                      ],
                    )
                  ],
                ),
              ),
              SingleChildScrollView(
                child: Obx(() => Container(
                      margin: const EdgeInsets.only(left: 16, right: 16),
                      padding: const EdgeInsets.only(
                          top: 11, bottom: 11, right: 16, left: 16),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          color: const Color.fromRGBO(255, 255, 255, 1)),
                      child: SingleChildScrollView(
                        physics: ScrollPhysics(),
                        child: Column(
                          children: [
                            Column(
                              children: [
                                InkWell(
                                  onTap: () {
                                    controller.isvisibility.value =
                                        !controller.isvisibility.value;
                                    controller.text_color.value =
                                        !controller.text_color.value;
                                  },
                                  child: Row(
                                    children: [
                                      Text(
                                        'Ngày lên lớp:',
                                        style: controller.text_color.value
                                            ? const TextStyle(
                                                color: Color.fromRGBO(
                                                    26, 26, 26, 1),
                                                fontSize: 12)
                                            : const TextStyle(
                                                color: Color.fromRGBO(
                                                    133, 133, 133, 1),
                                                fontSize: 12),
                                      ),
                                      Expanded(child: Container()),
                                      Row(
                                        children: [
                                          Text(
                                            '${controller.getTotalDayOnTime() ?? 0} ngày',
                                            style: TextStyle(
                                                color: Color.fromRGBO(
                                                    133, 133, 133, 1),
                                                fontSize: 14),
                                          ),
                                          const Padding(
                                              padding:
                                                  EdgeInsets.only(right: 9)),
                                          Icon(controller.isvisibility.value
                                              ? Icons.keyboard_arrow_up
                                              : Icons
                                                  .keyboard_arrow_down_outlined),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                Visibility(
                                  visible: controller.isvisibility.value,
                                  child: Container(
                                      margin: const EdgeInsets.only(top: 11),
                                      child: Column(
                                        children: [
                                          ListView.builder(
                                              shrinkWrap: true,
                                              itemCount: controller
                                                  .dayOnTimeAndDayNotOnTime.value.length,
                                              itemBuilder: (context, index) {
                                                return GestureDetector(
                                                    onTap: () {
                                                      controller
                                                          .goToDetailStatisticalStudentOnSchoolPage(
                                                              index);
                                                    },
                                                    child: Container(
                                                      margin: EdgeInsets.only(
                                                          top: 8),
                                                      child: Row(
                                                        children: [
                                                          Text(
                                                            "${controller.getConvertDateOnTime(index)}",
                                                            style:
                                                            const TextStyle(
                                                                color: Color
                                                                    .fromRGBO(
                                                                    90,
                                                                    90,
                                                                    90,
                                                                    1),
                                                                fontSize:
                                                                12),
                                                          ),
                                                          Expanded(
                                                              child:
                                                                  Container()),
                                                          Container(
                                                              alignment:
                                                                  Alignment
                                                                      .center,
                                                              height: 18,
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      left: 12,
                                                                      right:
                                                                          12),
                                                              decoration:
                                                                  BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            25),
                                                                color: controller
                                                                    .getColorStatus(controller
                                                                        .dayOnTimeAndDayNotOnTime
                                                                        .value[
                                                                            index]
                                                                        .statusDiligent),
                                                              ),
                                                              child: Text(
                                                                  '${controller.getTextStatus(controller.dayOnTimeAndDayNotOnTime.value[index].statusDiligent)}',
                                                                  style: TextStyle(
                                                                      color: controller.getColorTextStatus(controller
                                                                          .dayOnTimeAndDayNotOnTime
                                                                          .value[
                                                                              index]
                                                                          .statusDiligent),
                                                                      fontSize:
                                                                          10)))
                                                        ],
                                                      ),
                                                    ));
                                              }),
                                          // ListView.builder(
                                          //     shrinkWrap: true,
                                          //     itemCount: controller
                                          //         .dayNotontime.value.length,
                                          //     itemBuilder: (context, index) {
                                          //       return GestureDetector(
                                          //           onTap: () {
                                          //             controller
                                          //                 .goToDetailStatisticalStudentNotOnTimePage(
                                          //                     index);
                                          //           },
                                          //           child: Container(
                                          //             margin: EdgeInsets.only(
                                          //                 top: 8),
                                          //             child: Row(
                                          //               children: [
                                          //                 Text(
                                          //                   "${controller.getConvertDateNotOn(index)}",
                                          //                   style:
                                          //                   const TextStyle(
                                          //                       color: Color
                                          //                           .fromRGBO(
                                          //                           90,
                                          //                           90,
                                          //                           90,
                                          //                           1),
                                          //                       fontSize:
                                          //                       12),
                                          //                 ),
                                          //                 Expanded(
                                          //                     child:
                                          //                         Container()),
                                          //                 Container(
                                          //                     alignment:
                                          //                         Alignment
                                          //                             .center,
                                          //                     height: 18,
                                          //                     padding:
                                          //                         const EdgeInsets
                                          //                                 .only(
                                          //                             left: 12,
                                          //                             right:
                                          //                                 12),
                                          //                     decoration:
                                          //                         BoxDecoration(
                                          //                       borderRadius:
                                          //                           BorderRadius
                                          //                               .circular(
                                          //                                   25),
                                          //                       color: controller
                                          //                           .getColorStatus(controller
                                          //                               .dayNotontime
                                          //                               .value[
                                          //                                   index]
                                          //                               .statusDiligent),
                                          //                     ),
                                          //                     child: Text(
                                          //                         '${controller.getTextStatus(controller.dayNotontime.value[index].statusDiligent)}',
                                          //                         style: TextStyle(
                                          //                             color: controller.getColorTextStatus(controller
                                          //                                 .dayNotontime
                                          //                                 .value[
                                          //                                     index]
                                          //                                 .statusDiligent),
                                          //                             fontSize:
                                          //                                 10)))
                                          //               ],
                                          //             ),
                                          //           ));
                                          //     })
                                        ],
                                      )),
                                )
                              ],
                            ),
                            const Padding(padding: EdgeInsets.only(top: 22)),
                            Column(
                              children: [
                                InkWell(
                                  onTap: () {
                                    controller.isvisibility_latetime.value =
                                        !controller.isvisibility_latetime.value;
                                    controller.text_color.value =
                                        !controller.text_color.value;
                                  },
                                  child: Row(
                                    children: [
                                      Text(
                                        'Ngày nghỉ:',
                                        style: controller.text_color.value
                                            ? const TextStyle(
                                                color: Color.fromRGBO(
                                                    26, 26, 26, 1),
                                                fontSize: 12)
                                            : const TextStyle(
                                                color: Color.fromRGBO(
                                                    133, 133, 133, 1),
                                                fontSize: 12),
                                      ),
                                      Expanded(child: Container()),
                                      Row(
                                        children: [
                                          Text(
                                            ' ${controller.getTotalDay()} ngày',
                                            style: TextStyle(
                                                color: Color.fromRGBO(
                                                    133, 133, 133, 1),
                                                fontSize: 14),
                                          ),
                                          const Padding(
                                              padding:
                                                  EdgeInsets.only(right: 9)),
                                          Icon(controller
                                                  .isvisibility_latetime.value
                                              ? Icons.keyboard_arrow_up
                                              : Icons
                                                  .keyboard_arrow_down_outlined),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                Visibility(
                                  visible:
                                      controller.isvisibility_latetime.value,
                                  child: Container(
                                      margin: const EdgeInsets.only(top: 11),
                                      child: Column(
                                        children: [
                                          // ListView.builder(
                                          //     shrinkWrap: true,
                                          //     itemCount: controller
                                          //         .dayExcused.value.length,
                                          //     itemBuilder: (context, index) {
                                          //       return GestureDetector(
                                          //           onTap: () {
                                          //             controller
                                          //                 .goToDetailStatisticalStudentExcusedAbsencePage(
                                          //                     index);
                                          //           },
                                          //           child: Container(
                                          //             margin:
                                          //                 const EdgeInsets.only(
                                          //                     top: 8),
                                          //             child: Row(
                                          //               children: [
                                          //                 Text(
                                          //                   "${controller.getConvertDateExcused(index)}",
                                          //                   style:
                                          //                   const TextStyle(
                                          //                       color: Color
                                          //                           .fromRGBO(
                                          //                           90,
                                          //                           90,
                                          //                           90,
                                          //                           1),
                                          //                       fontSize:
                                          //                       12),
                                          //                 ),
                                          //                 Expanded(
                                          //                     child:
                                          //                         Container()),
                                          //                 Container(
                                          //                   alignment: Alignment
                                          //                       .center,
                                          //                   height: 18,
                                          //                   padding:
                                          //                       const EdgeInsets
                                          //                               .only(
                                          //                           left: 12,
                                          //                           right: 12),
                                          //                   decoration:
                                          //                       BoxDecoration(
                                          //                     borderRadius:
                                          //                         BorderRadius
                                          //                             .circular(
                                          //                                 25),
                                          //                     color: controller
                                          //                         .getColorStatus(controller
                                          //                             .dayExcused
                                          //                             .value[
                                          //                                 index]
                                          //                             .statusDiligent),
                                          //                   ),
                                          //                   child: Text(
                                          //                       '${controller.getTextStatus(controller.dayExcused.value[index].statusDiligent)}',
                                          //                       style: TextStyle(
                                          //                           color: controller.getColorTextStatus(controller
                                          //                               .dayExcused
                                          //                               .value[
                                          //                                   index]
                                          //                               .statusDiligent),
                                          //                           fontSize:
                                          //                               10)),
                                          //                 )
                                          //               ],
                                          //             ),
                                          //           ));
                                          //     }),
                                          ListView.builder(
                                              shrinkWrap: true,
                                              itemCount: controller
                                                  .dayAbsendAndDayExcused.value.length,
                                              itemBuilder: (context, index) {
                                                return GestureDetector(
                                                    onTap: () {
                                            controller.goToDetailStatisticalStudentExcusedAbsencePage(index);
                                                    },
                                                    child: Container(
                                                      margin:
                                                          const EdgeInsets.only(
                                                              top: 8),
                                                      child: Row(
                                                        children: [
                                                          Text(
                                                            "${controller.getConvertDateAbsend(index)}",
                                                            style:
                                                            const TextStyle(
                                                                color: Color
                                                                    .fromRGBO(
                                                                    90,
                                                                    90,
                                                                    90,
                                                                    1),
                                                                fontSize:
                                                                12),
                                                          ),
                                                          Expanded(
                                                              child:
                                                                  Container()),
                                                          Container(
                                                            alignment: Alignment
                                                                .center,
                                                            height: 18,
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 12,
                                                                    right: 12),
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          25),
                                                              color: controller
                                                                  .getColorStatus(controller
                                                                      .dayAbsendAndDayExcused
                                                                      .value[
                                                                          index]
                                                                      .statusDiligent),
                                                            ),
                                                            child: Text(
                                                                '${controller.getTextStatus(controller.dayAbsendAndDayExcused.value[index].statusDiligent)}',
                                                                style: TextStyle(
                                                                    color: controller.getColorTextStatus(controller
                                                                        .dayAbsendAndDayExcused
                                                                        .value[
                                                                            index]
                                                                        .statusDiligent),
                                                                    fontSize:
                                                                        10)),
                                                          )
                                                        ],
                                                      ),
                                                    ));
                                              }),
                                        ],
                                      )),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    )),
              ),
              // Expanded(child: Container()),
              // Container(
              //   color: const Color.fromRGBO(255, 255, 255, 1),
              //   height: 70.h,
              //   padding: const EdgeInsets.all(16),
              //   child: Row(
              //     children: [
              //       Expanded(
              //           child: SizedBox(
              //         height: 46,
              //         child: ElevatedButton(
              //             style: ElevatedButton.styleFrom(
              //                 side: const BorderSide(
              //                     width: 1,
              //                     color: Color.fromRGBO(248, 129, 37, 1)),
              //                 backgroundColor:
              //                     const Color.fromRGBO(255, 255, 255, 1)),
              //             onPressed: () {},
              //             child: const Text(
              //               'Thống Kê',
              //               style: TextStyle(
              //                   color: Color.fromRGBO(248, 129, 37, 1),
              //                   fontSize: 16),
              //             )),
              //       )),
              //       Padding(padding: EdgeInsets.only(right: 16.w)),
              //       Expanded(
              //           child: SizedBox(
              //         height: 46,
              //         child: ElevatedButton(
              //             style: ElevatedButton.styleFrom(
              //               backgroundColor:
              //                   const Color.fromRGBO(248, 129, 37, 1),
              //             ),
              //             onPressed: () {
              //               Get.back();
              //             },
              //             child: const Text(
              //               'Về Trang Chủ',
              //               style: TextStyle(
              //                   color: Color.fromRGBO(255, 255, 255, 1),
              //                   fontSize: 16),
              //             )),
              //       ))
              //     ],
              //   ),
              // )
            ],
          ),
        ));
  }

  selectDateTimeStart(stringTime, format,context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      if (value != null) {
        date =
            TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
      }
    });

    if (date.isNotEmpty) {
      controller.controllerdateStart.value.text = "$date";
      controller.dateStart.value = date;
    }
    FocusScope.of(context).requestFocus(FocusNode());
  }

  selectDateTimeEnd(stringTime, format,context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      if (value != null) {
        date =
            TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
      }
    });

    if (date.isNotEmpty) {
      controller.controllerdateEnd.value.text = "$date";
      controller.dateEnd.value = date;

    }
    FocusScope.of(context).requestFocus(FocusNode());
  }
}
