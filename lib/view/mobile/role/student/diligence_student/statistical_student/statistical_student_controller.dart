import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:task_manager/commom/utils/app_utils.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/data/model/common/detail_item_list_student.dart';
import 'package:task_manager/data/model/common/diligence.dart';
import 'package:task_manager/data/model/common/school_year.dart';
import 'package:task_manager/data/model/common/user_profile.dart';
import 'package:task_manager/data/repository/diligence/diligence_repo.dart';
import 'package:task_manager/routes/app_pages.dart';
import 'package:task_manager/view/mobile/role/parent/parent_home_controller.dart';
import 'package:task_manager/view/mobile/role/student/diligence_student/statistical_student/detail_statistical_student/detail_all_stastical_page.dart';
import 'package:task_manager/view/mobile/role/student/diligence_student/statistical_student/detail_statistical_student/detail_statistical_student_page.dart';
import 'package:task_manager/view/mobile/role/student/student_home_controller.dart';

class StatisticalStudentController extends GetxController {

  String ON_TIME_TEXT = "Đúng giờ";
  String NOT_ON_TIME_TEXT= "Đi muộn";
  String ABSENT_WITHOUT_LEAVE_TEXT = "Không phép";
  String EXCUSED_ABSENCE_TEXT = "Có phép";
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var focusedDay = DateTime.now().obs;
  var selectedDay = DateTime.now().obs;
  var userProfile = UserProfile().obs;
  RxList<SchoolYear> dateTime = <SchoolYear>[].obs;
  var diligence = Diligence().obs;
  var outputDateFormat = DateFormat('EEEE,dd/MM/yyyy');
    var outputDateToDateFormat = DateFormat('dd/MM/yyyy');
  var outputMonthFormat = DateFormat('MM/yyyy');
  var isvisibility = true.obs;
  var isvisibility_icon = false.obs;
  var isvisibility_latetime = true.obs;
  var text_color = false.obs;
  RxList<String> listStatus = <String>[].obs;
  RxList<Items> item = <Items>[].obs;
  RxList<StatusDetailDiligence> dayonShool = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayAbsent = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayExcused = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayNotontime = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayOnTimeAndDayNotOnTime = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayAbsendAndDayExcused = <StatusDetailDiligence>[].obs;
  var listDayLeavingApplication = <DateTime>[].obs;
  var detailDiligence = DetailDiligence().obs;
  var studentOnTime = StudentOnSchool().obs;
  var studentNotOnTime = StudentNotOnTime().obs;
  var studentExcused = StudentExcusedAbsence().obs;
  var studentAbsend = StudentAbsentWithoutLeave().obs;
var totalDayOnTime = 0.obs;
var totalDayNotOnTime = 0.obs;
  var totalDayAbsend = 0.obs;
  var totalDayExsecud = 0.obs;
  var selectDate = "".obs;
  var firtDayOfMonth = 0.obs;
  var LastDayOfMonth = 0.obs;
  var focusdateStart = FocusNode().obs;
  var controllerdateStart = TextEditingController().obs;
  var focusdateEnd = FocusNode().obs;
  var controllerdateEnd = TextEditingController().obs;
  var dateStart ="".obs;
  var dateEnd ="".obs;
  var fromYearSelect ="".obs;
  var toYearSelect ="".obs;

  getColorStatus(state) {
    switch (state) {
      case "ON_TIME":
        return Color.fromRGBO(221, 246, 235, 1);
      case "NOT_ON_TIME":
        return Color.fromRGBO(255, 239, 203, 1);
      case "EXCUSED_ABSENCE":
        return Color.fromRGBO(255, 200, 206, 1);
      default:
        return Color.fromRGBO(255, 200, 206, 1);
    }
  }

  getColorTextStatus(state) {
    switch (state) {
      case "ON_TIME":
        return Color.fromRGBO(77, 197, 145, 1);
      case "NOT_ON_TIME":
        return Color.fromRGBO(253, 185, 36, 1);
      case "EXCUSED_ABSENCE":
        return Color.fromRGBO(255, 69, 89, 1);
      default:
        return Color.fromRGBO(255, 69, 89, 1);
    }
  }

  getTextStatus(state) {
    switch (state) {
      case "ON_TIME":
        return ON_TIME_TEXT;
      case "NOT_ON_TIME":
        return NOT_ON_TIME_TEXT;
      case "ABSENT_WITHOUT_LEAVE":
        return ABSENT_WITHOUT_LEAVE_TEXT;
      default:
        return EXCUSED_ABSENCE_TEXT;
    }
  }
  DateTime findFirstDateOfTheMonth(DateTime dateTime) {
    return DateTime(dateTime.year, dateTime.month, 1);
  }

  DateTime findLastDateOfTheMonth(DateTime dateTime) {
    return DateTime(dateTime.year, dateTime.month + 1, 0);
  }

  @override
  void onInit() {
    super.onInit();
    userProfile.value = Get.find<StudentHomeController>().userProfile.value;
    setDateSchoolYears();

    if(Get.find<StudentHomeController>().fromYearPresent == DateTime.now().year || Get.find<StudentHomeController>().toYearPresent == DateTime.now().year ){
      controllerdateStart.value.text = outputDateToDateFormat.format(DateTime( DateTime.now().year, DateTime.now().month));
      controllerdateEnd.value.text = outputDateToDateFormat.format(DateTime(DateTime.now().year, DateTime.now().month+1).subtract(Duration(days: 1)));
      dateStart.value = outputDateToDateFormat.format(DateTime( DateTime.now().year, DateTime.now().month));
      dateEnd.value = outputDateToDateFormat.format(DateTime(DateTime.now().year, DateTime.now().month+1).subtract(Duration(days: 1)));

    }else{
      controllerdateStart.value.text = outputDateToDateFormat.format(DateTime(int.parse(toYearSelect.value.substring(0,4)), DateTime.now().month));
      controllerdateEnd.value.text = outputDateToDateFormat.format(DateTime(int.parse(fromYearSelect.value.substring(0,4)), DateTime.now().month+1).subtract(Duration(days: 1)));
      dateStart.value=outputDateToDateFormat.format(DateTime(int.parse(toYearSelect.value.substring(0,4)), DateTime.now().month));
      dateEnd.value = outputDateToDateFormat.format(DateTime(int.parse(fromYearSelect.value.substring(0,4)), DateTime.now().month+1).subtract(Duration(days: 1)));
    }
  }

  getTotalDay(){
    if(totalDayExsecud.value == 0){
      return totalDayAbsend.value;
    }else if(totalDayAbsend.value ==0){
      return totalDayExsecud.value;
    }else{
      return totalDayExsecud.value + totalDayAbsend.value;

    }
  }

  setDateSchoolYears() {
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (Get.find<StudentHomeController>().fromYearPresent.value == DateTime.now().year) {
        getListDiligenceToMonth();
        return;
      } else {
        fromYearSelect.value = "${Get.find<StudentHomeController>().fromYearPresent}";
        toYearSelect.value = "${Get.find<StudentHomeController>().toYearPresent}";
        getListDiligenceToSelectYear(fromYearSelect.value, toYearSelect.value);
      }
    } else {
      if (Get.find<StudentHomeController>().toYearPresent.value == DateTime.now().year) {
      getListDiligenceToMonth();
        return;
      } else {
        fromYearSelect.value = "${Get.find<StudentHomeController>().toYearPresent}";
        toYearSelect.value = "${Get.find<StudentHomeController>().fromYearPresent}";
        getListDiligenceToSelectYear(fromYearSelect.value, toYearSelect.value);
      }
    }
  }

  getTotalDayOnTime(){
    if(totalDayOnTime.value == 0){
      return totalDayNotOnTime.value;
    }else if(totalDayNotOnTime.value ==0){
      return totalDayOnTime.value;
    }else{
      return totalDayOnTime.value + totalDayNotOnTime.value;

    }
  }
  getListDiligence(fromdate,todate){
    var userId = Get.find<StudentHomeController>().userProfile.value.id;
    var fromdate = DateTime(int.parse(dateStart.value.substring(6,10)), int.parse(dateStart.value.substring(3,5)),int.parse(dateStart.value.substring(0,2))).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(dateEnd.value.substring(6,10)), int.parse(dateEnd.value.substring(3,5)),int.parse(dateEnd.value.substring(0,2))).millisecondsSinceEpoch;
    _diligenceRepo.DetailListDiligence(userId, fromdate, todate,"").then((value) {
      if (value.state == Status.SUCCESS) {
        detailDiligence.value = value.object!;
        dayonShool.value = detailDiligence.value.studentOnSchool!.dayOnSchool! ;
        dayExcused.value = detailDiligence.value.studentExcusedAbsence!.dayExcusedAbsence!;
        dayAbsent.value = detailDiligence.value.studentAbsentWithoutLeave!.dayAbsentWithoutLeave!;
        dayNotontime.value = detailDiligence.value.studentNotOnTime!.dayNotOnTime!;
        getSortList();
        if(detailDiligence.value.studentOnSchool != null){
          totalDayOnTime.value = detailDiligence.value.studentOnSchool!.totalDaysOnSchool!;
        }
        if(detailDiligence.value.studentNotOnTime != null){
          totalDayNotOnTime.value = detailDiligence.value.studentNotOnTime!.totalDaysNotOnTime!;
        }
        if(detailDiligence.value.studentAbsentWithoutLeave != null){
          totalDayAbsend.value = detailDiligence.value.studentAbsentWithoutLeave!.totalDaysAbsentWithoutLeave!;
        }
        if(detailDiligence.value.studentExcusedAbsence != null){
          totalDayExsecud.value = detailDiligence.value.studentExcusedAbsence!.totalDaysExcusedAbsence!;
        }
        AppUtils.shared.hideLoading();
        Future.delayed(Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }
  getListDiligenceToMonth(){
    var userId = Get.find<StudentHomeController>().userProfile.value.id;
    var fromdate = DateTime(DateTime.now().year,DateTime.now().month).millisecondsSinceEpoch;
    var todate = DateTime(DateTime.now().year,DateTime.now().month+1).subtract(Duration(days: 1)).millisecondsSinceEpoch;
    _diligenceRepo.DetailListDiligence(userId, fromdate, todate,"").then((value) {
      if (value.state == Status.SUCCESS) {
        detailDiligence.value = value.object!;
        dayonShool.value = detailDiligence.value.studentOnSchool!.dayOnSchool! ;
        dayExcused.value = detailDiligence.value.studentExcusedAbsence!.dayExcusedAbsence!;
        dayAbsent.value = detailDiligence.value.studentAbsentWithoutLeave!.dayAbsentWithoutLeave!;
        dayNotontime.value = detailDiligence.value.studentNotOnTime!.dayNotOnTime!;
        dayOnTimeAndDayNotOnTime.value.addAll(dayonShool.value);
        dayOnTimeAndDayNotOnTime.value.addAll(dayNotontime.value);
        dayAbsendAndDayExcused.value.addAll(dayExcused.value);
        dayAbsendAndDayExcused.value.addAll(dayAbsent.value);
        getSortList();
        if(detailDiligence.value.studentOnSchool != null){
          totalDayOnTime.value = detailDiligence.value.studentOnSchool!.totalDaysOnSchool!;
        }
        if(detailDiligence.value.studentNotOnTime != null){
          totalDayNotOnTime.value = detailDiligence.value.studentNotOnTime!.totalDaysNotOnTime!;
        }
        if(detailDiligence.value.studentAbsentWithoutLeave != null){
          totalDayAbsend.value = detailDiligence.value.studentAbsentWithoutLeave!.totalDaysAbsentWithoutLeave!;
        }
        if(detailDiligence.value.studentExcusedAbsence != null){
          totalDayExsecud.value = detailDiligence.value.studentExcusedAbsence!.totalDaysExcusedAbsence!;
        }
        AppUtils.shared.hideLoading();
        Future.delayed(Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }
  getListDiligenceToSelectYear(fromdate, todate){
    var userId = Get.find<StudentHomeController>().userProfile.value.id;
    var fromdate = DateTime(int.parse(toYearSelect.value.substring(0,4)), DateTime.now().month).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(fromYearSelect.value.substring(0,4)), DateTime.now().month+1).subtract(Duration(days: 1)).millisecondsSinceEpoch;
    _diligenceRepo.DetailListDiligence(userId, fromdate, todate,"").then((value) {
      if (value.state == Status.SUCCESS) {
        detailDiligence.value = value.object!;
        dayonShool.value = detailDiligence.value.studentOnSchool!.dayOnSchool! ;
        dayExcused.value = detailDiligence.value.studentExcusedAbsence!.dayExcusedAbsence!;
        dayAbsent.value = detailDiligence.value.studentAbsentWithoutLeave!.dayAbsentWithoutLeave!;
        dayNotontime.value = detailDiligence.value.studentNotOnTime!.dayNotOnTime!;
        dayOnTimeAndDayNotOnTime.value.addAll(dayonShool.value);
        dayOnTimeAndDayNotOnTime.value.addAll(dayNotontime.value);
        dayAbsendAndDayExcused.value.addAll(dayExcused.value);
        dayAbsendAndDayExcused.value.addAll(dayAbsent.value);
        getSortList();
        if(detailDiligence.value.studentOnSchool != null){
          totalDayOnTime.value = detailDiligence.value.studentOnSchool!.totalDaysOnSchool!;
        }
        if(detailDiligence.value.studentNotOnTime != null){
          totalDayNotOnTime.value = detailDiligence.value.studentNotOnTime!.totalDaysNotOnTime!;
        }
        if(detailDiligence.value.studentAbsentWithoutLeave != null){
          totalDayAbsend.value = detailDiligence.value.studentAbsentWithoutLeave!.totalDaysAbsentWithoutLeave!;
        }
        if(detailDiligence.value.studentExcusedAbsence != null){
          totalDayExsecud.value = detailDiligence.value.studentExcusedAbsence!.totalDaysExcusedAbsence!;
        }
        AppUtils.shared.hideLoading();
        Future.delayed(Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }

  getConvertDateOnTime(index){
    var  dayOnTime = "${outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnTimeAndDayNotOnTime.value[index].date!))}";
    if(dayOnTime.substring(0,6)=="Monday"){
      return "Thứ 2, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnTimeAndDayNotOnTime.value[index].date!))}";
    }else if(dayOnTime.substring(0,7)=="Tuesday"){
      return "Thứ 3, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnTimeAndDayNotOnTime.value[index].date!))}";
    }else if(dayOnTime.substring(0,9)=="Wednesday"){
      return "Thứ 4, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnTimeAndDayNotOnTime.value[index].date!))}";
    }else if(dayOnTime.substring(0,8)=="Thursday"){
      return "Thứ 5, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnTimeAndDayNotOnTime.value[index].date!))}";
    }else if(dayOnTime.substring(0,6)=="Friday"){
      return "Thứ 6, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnTimeAndDayNotOnTime.value[index].date!))}";
    }else if(dayOnTime.substring(0,8)=="Saturday"){
      return "Thứ 7, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnTimeAndDayNotOnTime.value[index].date!))}";
    }else if(dayOnTime.substring(0,6)=="Sunday"){
      return "Chủ nhật, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnTimeAndDayNotOnTime.value[index].date!))}";
    }else{
      return "";
    }
  }
  getConvertDateAbsend(index){
    var  dayAbsend = "${outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsendAndDayExcused.value[index].date!))}";
    if(dayAbsend.substring(0,6)=="Monday"){
      return "Thứ 2, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsendAndDayExcused.value[index].date!))}";
    }else if(dayAbsend.substring(0,7)=="Tuesday"){
      return "Thứ 3, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsendAndDayExcused.value[index].date!))}";
    }else if(dayAbsend.substring(0,9)=="Wednesday"){
      return "Thứ 4, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsendAndDayExcused.value[index].date!))}";
    }else if(dayAbsend.substring(0,8)=="Thursday"){
      return "Thứ 5, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsendAndDayExcused.value[index].date!))}";
    }else if(dayAbsend.substring(0,6)=="Friday"){
      return "Thứ 6, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsendAndDayExcused.value[index].date!))}";
    }else if(dayAbsend.substring(0,8)=="Saturday"){
      return "Thứ 7, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsendAndDayExcused.value[index].date!))}";
    }else if(dayAbsend.substring(0,6)=="Sunday"){
      return "Chủ nhật, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsendAndDayExcused.value[index].date!))}";
    }else{
      return "";
    }
  }
  
  getSortList(){
    dayOnTimeAndDayNotOnTime.value.sort(
      (a, b) => a.date!.compareTo(int.parse(b.date.toString())),
    );
    dayAbsendAndDayExcused.value.sort(
          (a, b) => a.date!.compareTo(int.parse(b.date.toString())),
    );
  }

 


  goToDetailStatisticalStudentOnSchoolPage(index){
    Get.toNamed(Routes.detailDiligenceStudent, arguments:[dayOnTimeAndDayNotOnTime.value[index].date, dayOnTimeAndDayNotOnTime.value[index].statusDiligent,dayOnTimeAndDayNotOnTime.value[index].updatedAt] );
  }
  goToDetailStatisticalStudentExcusedAbsencePage(index){
    Get.toNamed(Routes.detailDiligenceStudent, arguments:[dayAbsendAndDayExcused.value[index].date, dayAbsendAndDayExcused.value[index].statusDiligent,dayAbsendAndDayExcused.value[index].updatedAt] );
  }
  goToDetailStatisticalStudent(){
    Get.toNamed(Routes.detailAllStasticalPage, arguments: [dateStart.value.toString(), dateEnd.value.toString()]);

  }



}

