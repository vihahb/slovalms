import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:task_manager/view/mobile/role/student/subjects/subjects_controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class SubjectPage extends GetWidget<SubjectController> {
  final controller = Get.put(SubjectController());

  SubjectPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
        child: Scaffold(
          backgroundColor: const Color.fromRGBO(245, 245, 245, 1),
          appBar: AppBar(
            backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
            title: const Text(
              'Môn Học',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontFamily: 'static/Inter-Medium.ttf'),
            ),
          ),
          body: GridView.builder(
              padding: const EdgeInsets.all(10),
              gridDelegate:
              const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                crossAxisSpacing: 5, //khoảng cách theo chiều dọc
                mainAxisSpacing: 5, //khoảng cách teho chiều ngang
              ),
              scrollDirection: Axis.vertical,
              shrinkWrap: true, //chiều cuộn
              physics: const ClampingScrollPhysics(),
              itemCount: controller.items.length,
              itemBuilder: (context, index) {
                return SizedBox(
                  height: 96,
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                    Container(
                      height: 50.h,
                      width: 50.w,
                      child: Image.network(
                        "${controller.items[index].image}",
                        errorBuilder: (context, error, stackTrace) {
                          return Image.asset("assets/images/imageMath.png");
                        },
                      ),
                    ),
                        const Padding(padding: EdgeInsets.only(top: 8)),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 8.w),
                          child: Text(
                            textAlign: TextAlign.center,
                            "${controller.items[index].name}",
                            style: const TextStyle(
                                color: Color.fromRGBO(26, 26, 26, 1),
                                fontSize: 12),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }),
        ));
  }
}
