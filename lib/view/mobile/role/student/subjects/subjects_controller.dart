import 'package:get/get.dart';

import '../../../../../data/model/common/subject.dart';
class SubjectController extends GetxController{
  var items = <SubjectItem>[];


  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    var detailItem = Get.arguments;
    if (detailItem != null && detailItem.isNotEmpty) {
      items = detailItem;
    }
  }


}