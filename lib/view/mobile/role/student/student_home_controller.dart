import 'dart:ui';

import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:task_manager/commom/app_cache.dart';
import 'package:task_manager/commom/utils/app_utils.dart';
import 'package:task_manager/data/model/common/diligence.dart';
import 'package:task_manager/data/model/common/subject.dart';
import 'package:task_manager/data/model/common/user_profile.dart';
import 'package:task_manager/data/repository/diligence/diligence_repo.dart';
import 'package:task_manager/routes/app_pages.dart';
import 'package:task_manager/view/mobile/role/student/diligence_student/statistical_student/statistical_student_controller.dart';
import 'package:task_manager/view/mobile/schedule/schedule_controller.dart';

import '../../../../../data/base_service/api_response.dart';
import '../../../../commom/widget/hexColor.dart';
import '../../../../data/model/common/banner.dart';
import '../../../../data/model/common/schedule.dart';
import '../../../../data/model/common/school_year.dart';
import '../../../../data/repository/common/common_repo.dart';
import '../../../../data/repository/person/personal_info_repo.dart';
import '../../../../data/repository/schedule/schedule_repo.dart';
import '../../../../data/repository/school_year/school_year_repo.dart';
import '../../../../data/repository/subject/class_office_repo.dart';
import '../../phonebook/phone_book_controller.dart';

class StudentHomeController extends GetxController {
  final PersonalInfoRepo _personalInfoRepo = PersonalInfoRepo();

  final CommonRepo _commonRepo = CommonRepo();
  final ClassOfficeRepo _subjectRepo = ClassOfficeRepo();
  final ScheduleRepo _scheduleRepo = ScheduleRepo();
  RxList<SubjectItem> items = <SubjectItem>[].obs;
  var userProfile = UserProfile().obs;
  RxList<Clazzs> clazzs = <Clazzs>[].obs;
  RxList<Clazzs> classInSchoolYears = <Clazzs>[].obs;
  var banner = BannerHomePage().obs;
  RxList<ItemBanner> itemBanner = <ItemBanner>[].obs;
  RxInt indexPageBanner = 0.obs;
  RxList<Schedule> scheduleStudent = <Schedule>[].obs;
  RxList<Appointment> timeTable = <Appointment>[].obs;
  final SchoolYearRepo _schoolYearRepo = SchoolYearRepo();
  var clickSchoolYear = <bool>[].obs;
  RxList<SchoolYear> schoolYears = <SchoolYear>[].obs;
  var currentStudentProfile = UserProfile().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  RxList<Schedule> scheduleToday = <Schedule>[].obs;
  var calendarController = CalendarController().obs;
  var isSubjectToday = true.obs;
  var fromYearPresent = 0.obs;
  var toYearPresent = 0.obs;

  @override
  void onInit() {
    // Get.put(NotifyController());
    super.onInit();
    getSchoolYearByUser();
    getBanner();
    calendarController.value.displayDate = DateTime(DateTime.now().year,
        DateTime.now().month, DateTime.now().day, DateTime.now().hour - 1);
  }

  getSchoolYearByUser() async {
    _schoolYearRepo.getListSchoolYearByUser().then((value) {
      if (value.state == Status.SUCCESS) {
        schoolYears.value = value.object!;
        clazzs.value = schoolYears.value[0].clazz!;
        for (int i = 0; i < schoolYears.value.length; i++) {
          clickSchoolYear.add(false);
        }
        if (schoolYears.value.length != 0) {
          fromYearPresent.value = schoolYears.value[0].fromYear!;
          toYearPresent.value = schoolYears.value[0].toYear!;
        }
        getDetailProfile();
      }
    });
  }

  setTextSchoolYears() {
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (fromYearPresent.value == DateTime.now().year) {
        return "nay";
      } else {
        return "${fromYearPresent.value} - ${toYearPresent.value}";
      }
    } else {
      if (toYearPresent.value == DateTime.now().year) {
        return "nay";
      } else {
        return "${fromYearPresent.value} - ${toYearPresent.value}";
      }
    }
  }

  onClickShoolYears(index) {
    clickSchoolYear.value = <bool>[].obs;
    for (int i = 0; i < schoolYears.value.length; i++) {
      clickSchoolYear.value.add(false);
    }
    clickSchoolYear.value[index] = true;
  }

  getScheduleTimeTableStudent(userId) {
    scheduleStudent.clear();
    Get.find<ScheduleController>().queryTimeTableByStudent(userId);
    _scheduleRepo.getScheduleStudent(userId).then((value) {
      if (value.state == Status.SUCCESS) {
        scheduleStudent.value = value.object!;
        getAppointments();
      }
    });
    scheduleStudent.refresh();
    scheduleToday.value = scheduleStudent.value
        .where((element) =>
            outputDateFormat.format(
                    DateTime.fromMillisecondsSinceEpoch(element.startTime!)) ==
                outputDateFormat.format(DateTime.now()) &&
            outputDateFormat.format(
                    DateTime.fromMillisecondsSinceEpoch(element.endTime!)) ==
                outputDateFormat.format(DateTime.now()))
        .toList();
  }

  getAppointments() {
    timeTable.value = <Appointment>[].obs;
    for (int i = 0; i < scheduleStudent.value.length; i++) {
      timeTable.value.add(Appointment(
          startTime: DateTime.fromMillisecondsSinceEpoch(
              scheduleStudent.value[i].startTime!),
          endTime: DateTime.fromMillisecondsSinceEpoch(
              scheduleStudent.value[i].endTime!),
          subject: scheduleStudent.value[i].subject!.subjectCategory!.name!,
          notes: scheduleStudent.value[i].subject?.clazz?.classCategory?.name!
              .toString(), // tên lớp
          resourceIds: scheduleStudent.value[i].subject!.clazz!.students,
          recurrenceId:
              scheduleStudent.value[i].subject!.teacher?.fullName!.toString(),
          color: Color.fromRGBO(249, 154, 81, 1)));
    }

    timeTable.refresh();
  }

  selectPageBanner(int index) {
    indexPageBanner.value = index;
    // for(int i = 0; i< itemBanner.value.length; i++){
    //   itemBanner.value[i].selected = false;
    // }
    itemBanner.value.forEach((element) => element.selected = false);
    itemBanner.value[index].selected = true;
    if (index != 0) {
      indexPageBanner.value - 1;
    } else {
      indexPageBanner.value = 0;
    }
    itemBanner.refresh();
  }

  getBanner() async {
    _commonRepo.getBanner().then((value) {
      if (value.state == Status.SUCCESS) {
        banner.value = value.object!;
        itemBanner.value = banner.value.items!;
        itemBanner.value[0].selected = true;
        itemBanner.refresh();
      }
    });
  }

  getDetailProfile() {
    _personalInfoRepo.detailUserProfile().then((value) {
      if (value.state == Status.SUCCESS) {
        userProfile.value = value.object!;
        AppCache().userId = userProfile.value.id ?? "";
        getListSubject(userProfile.value.id);
        getScheduleTimeTableStudent(userProfile.value.id);
      }
    });
  }

  onRefresh() {
    getBanner();
    _schoolYearRepo.getListSchoolYearByUser().then((value) {
      if (value.state == Status.SUCCESS) {
        schoolYears.value = value.object!;
        for (int i = 0; i < schoolYears.value.length; i++) {
          clickSchoolYear.add(false);
        }
      }
    });
    getListSubject(userProfile.value.id);
    calendarController.value.displayDate = DateTime(DateTime.now().year,
        DateTime.now().month, DateTime.now().day, DateTime.now().hour - 1);
    getScheduleTimeTableStudent(userProfile.value.id);
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      calendarController.value.displayDate = DateTime(fromYearPresent.value,
          DateTime.now().month, DateTime.now().day, DateTime.now().hour - 1);
    } else {
      calendarController.value.displayDate = DateTime(toYearPresent.value,
          DateTime.now().month, DateTime.now().day, DateTime.now().hour - 1);
    }
  }

  setTimeCalendar(index) {
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      calendarController.value.displayDate = DateTime(
          schoolYears.value[index].fromYear!.toInt(),
          DateTime.now().month,
          DateTime.now().day,
          DateTime.now().hour - 1);
    } else {
      calendarController.value.displayDate = DateTime(
          schoolYears.value[index].toYear!.toInt(),
          DateTime.now().month,
          DateTime.now().day,
          DateTime.now().hour - 1);
    }
  }

  getListSubject(userId) {
    items.clear();
    _subjectRepo.listSubject(userId).then((value) {
      if (value.state == Status.SUCCESS) {
        items.value = value.object!;
      }
    });
    items.refresh();
  }

  onSelectSchoolYears(index) {
    clazzs.value = schoolYears.value[index].clazz!;
    AppCache().setSchoolYearId(schoolYears.value[index].id??"");
    Get.find<PhoneBookController>().getListClassByStudent();
    // Get.find<StatisticalStudentController>().getListDiligenceToMonth();
    onSelectUser();
    setTimeCalendar(index);
    Get.find<ScheduleController>()
        .queryTimeCalendar(index, AppCache().userType);
    fromYearPresent.value = schoolYears.value[index].fromYear!;
    toYearPresent.value = schoolYears.value[index].toYear!;
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (schoolYears.value[index].fromYear == DateTime.now().year) {
        isSubjectToday.value = true;

      } else {
        isSubjectToday.value = false;
      }
    } else {
      if (schoolYears.value[index].toYear == DateTime.now().year) {
        isSubjectToday.value = true;
      } else {
        isSubjectToday.value = false;
      }
    }
    Get.back();
  }

  onSelectUser() {
    timeTable.value.clear();
    Get.find<ScheduleController>().timeTable.value.clear();
    getListSubject(userProfile.value.id);
    getScheduleTimeTableStudent(userProfile.value.id);
    timeTable.refresh();
    Get.find<ScheduleController>().timeTable.refresh();
  }



  toSubjectPage() {
    Get.toNamed(Routes.subject, arguments: items.value);
  }

  @override
  void onClose() {
    super.onClose();
  }

  void toDetailProfile() {
    Get.toNamed(Routes.personalInformation, preventDuplicates: false);
  }

  void goToDiligenceStudentPage(){
    Get.toNamed(Routes.diligenceStudent);
  }

  void SelectDate(index){
    fromYearPresent.value = schoolYears.value[index].fromYear!;
    toYearPresent.value = schoolYears.value[index].toYear!;
  }


}
