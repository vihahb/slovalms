import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:task_manager/view/mobile/home/home_controller.dart';
import '../../../../../../commom/app_cache.dart';
import '../../../../../../commom/utils/app_utils.dart';
import '../../../../../../commom/utils/color_utils.dart';
import '../../../../../../commom/widget/text_field_custom.dart';
import '../../../../../../data/base_service/api_response.dart';
import '../../../../../../data/model/common/req_file.dart';
import '../../../../../../data/model/common/user_profile.dart';
import '../../../../../../data/model/res/class/School.dart';
import '../../../../../../data/model/res/file/response_file.dart';
import '../../../../../../data/repository/file/file_repo.dart';
import '../../../../../../data/repository/person/personal_info_repo.dart';
import '../../../../../../routes/app_pages.dart';
import '../personal_information_student_controller.dart';
import '../personal_information_student_page.dart';




class UpdateInfoStudentControlller extends GetxController{
  var ischecked = false.obs;
  var controllerName = TextEditingController().obs;
  var controllerPhone = TextEditingController().obs;
  var controllerEmail = TextEditingController().obs;
  var controllerSchool = TextEditingController().obs;
  var controllerClass = TextEditingController().obs;
  var focusName = FocusNode().obs;
  var focusPhone = FocusNode().obs;
  var focusEmail = FocusNode().obs;
  var focusSchool = FocusNode().obs;
  var focusRole = FocusNode().obs;

  var files = <ReqFile>[].obs;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  var checkCorrectOrError = true.obs;
  var enable = true.obs;
  var setSubmit = false.obs;
  var showHelptext = false.obs;

  String helperText = "";
  String getTempIFSCValidation(String text) {
    return text.length > 7 ? "This line is helper text" : "";
  }
  var stateInput = StateType.DEFAULT;
  String icoSuffix = "";
  var tmpUserProfile = UserProfile().obs;
  var userProfile = UserProfile().obs;
  var school = SchoolData().obs;
  final PersonalInfoRepo _personalInfoRepo = PersonalInfoRepo();
  final FileRepo fileRepo = FileRepo();


  getDetailProfile() {
    _personalInfoRepo.detailUserProfile().then((value) {
      if (value.state == Status.SUCCESS) {
        userProfile.value = value.object!;
        if(userProfile.value.school == null){
          school.value.name = "";
        }
        else{
          school.value = userProfile.value.school!;
        }
        controllerName.value.text = userProfile.value.fullName!;
        controllerPhone.value.text = userProfile.value.phone!;
        controllerClass.value.text = userProfile.value.item!.clazzs![0].name!??"";
        controllerEmail.value.text = userProfile.value.email!;
        controllerSchool.value.text = school.value.name!;
        tmpUserProfile.value = value.object!;
        AppCache().userId = userProfile.value.id ?? "";
        Get.find<HomeController>().updateProfile();
      }
    });
  }


  updateInfoUser(profile,id) async {
    _personalInfoRepo.updateUserProfile(profile,id).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Cập nhật thông tin cá nhân thành công");
        Future.delayed(const Duration(seconds: 1), () {
          userProfile.refresh();
          tmpUserProfile.refresh();
          getDetailProfile();
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Cập nhật thông tin cá nhân thất bại", value.message ?? "");
      }
    });
  }

  /// Flow update infomation
  /// 1. get all person data
  /// 2. Check if file change (if not change => go to step 4)
  /// 3. upload file => get response
  /// 4. Update infomation with new url of avatar (if change)
  /// 5. Check response success => finish

  uploadFile(){
    var fileList = <File>[];
    files.forEach((element) {
      fileList.add(element.file!);
    });

    fileRepo.uploadFile(fileList).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Upload file thành công");
        var listResFile = value.object!;
        ResponseFileUpload updateAvatar = listResFile[0];
        if (updateAvatar!=null) {
          //update avatar
          tmpUserProfile.value.img = updateAvatar;
          updateInfoUser(tmpUserProfile.value, tmpUserProfile.value.id);
        } else{
          AppUtils.shared.showToast("Upload file thất bại!");
        }
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tải tệp lên thất bại", backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }

  @override
  void onInit() {
    super.onInit();
    getDetailProfile();
  }

  goToHome(){
    Get.toNamed(Routes.home);
  }
  void goToDetailAvatar(){
    Get.toNamed(Routes.getavatar, arguments: userProfile.value.image);
  }




}