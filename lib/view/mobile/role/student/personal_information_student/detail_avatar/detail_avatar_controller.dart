import 'package:get/get.dart';
import 'package:task_manager/commom/utils/app_utils.dart';
import 'package:task_manager/commom/utils/color_utils.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import '../../../../../../data/model/common/user_profile.dart';
import '../../../../../data/model/common/user_profile.dart';

class DetailAvatarController extends GetxController{
  var userProfile = UserProfile().obs;
  var path ="".obs;
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    var tmpPath = Get.arguments;
    if(tmpPath!=null){
      path.value = tmpPath;
    } else {
      AppUtils.shared.showToast("Không có dữ liệu ảnh đã chọn!", backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
    }
  }


  void gotoBack(){
    Get.back();
  }
}