import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:task_manager/view/mobile/role/student/personal_information_student/detail_avatar/detail_avatar_controller.dart';
import 'package:task_manager/view/mobile/role/teacher/student_list/student_list_controller.dart';

class DetailAvatarPage extends GetWidget<DetailAvatarController>{
  var controller = Get.put(DetailAvatarController());

  DetailAvatarPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body:
        SafeArea(
          child:Obx(() =>
              Stack(
                children: [
                  SizedBox(
                      width:double.infinity,
                      height: double.infinity,
                      child:Image.network(
                        controller.path.value ??
                            "https://noithatbinhminh.com.vn/wp-content/uploads/2022/08/anh-dep-40.jpg",
                        errorBuilder:
                            (context, object, stackTrace) {
                          return Image.asset(
                            "assets/images/img_Noavt.png",
                          );
                        },
                      )
                  ),
                  Positioned(
                      right: 0,
                      top: 0,
                      child:InkWell(
                        onTap: (){
                          controller.gotoBack();
                        },
                        child: SizedBox(
                          height: 50.h,
                          width: 50.w,
                          child: Icon(Icons.close,size: 30,color: Colors.black87,),
                        ),
                      )
                  ),
                ],
              ),),
        )
        );
  }

}