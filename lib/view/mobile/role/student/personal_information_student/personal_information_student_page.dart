import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:task_manager/view/mobile/account/changePassword/change_pass_page.dart';

import '../../../../../commom/utils/color_utils.dart';
import '../../../../../commom/utils/file_device.dart';
import '../../../../../commom/widget/custom_view.dart';
import 'personal_information_student_controller.dart';

class PersonalInformationStudentPage extends GetWidget<PersonalInformationStudentController> {
  var controller = Get.put(PersonalInformationStudentController());

  PersonalInformationStudentPage({super.key});

  _onBottomSheetLogout() {
    return Wrap(
      children: [
        Container(
          margin: const EdgeInsets.all(20),
          child: Column(
            children: [
              SizedBox(
                height: 50,
                child: Container(
                    decoration: const ShapeDecoration(
                        color: Color.fromRGBO(248, 129, 37, 1),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(6),
                              topRight: Radius.circular(6)),
                        )),
                    child: Container(
                      width: double.infinity,
                      padding: const EdgeInsets.all(10),
                      child: const Text("Đăng Xuất",
                          style: TextStyle(color: Colors.white)),
                    )),
              ),
              Container(
                  color: Colors.white,
                  width: double.infinity,
                  height: 57,
                  alignment: Alignment.center,
                  child: const Text("Bạn có muốn đăng xuất không?")),
              Row(
                children: [
                  Expanded(
                      child: SizedBox(
                    height: 50,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.grey,
                          shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(6)))),
                      onPressed: () {
                        Get.back();
                      },
                      child: const Text(
                        "Hủy",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  )),
                  Expanded(
                    child: SizedBox(
                      height: 50,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.red,
                            shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(6)))),
                        onPressed: () {
                          controller.logout();
                        },
                        child: const Text(
                          "Đăng Xuất",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          title: const Text("Chi tiết"),
          leading: BackButton(
            color: Colors.white,
            onPressed: () {
              Get.delete<PersonalInformationStudentController>();
              Get.back();
            },
          ),
          backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
        ),
        backgroundColor: const Color.fromRGBO(249, 249, 249, 1),
        body: Center(
          child: Container(
            margin: const EdgeInsets.only(top: 16, left: 16, right: 16),
            child: SingleChildScrollView(child: Column(
              children: [profileAvatarWidget(), informationWidget(), help()],
            ),),
          ),
        ));
  }

  Container help() {
    return Container(
      margin: EdgeInsets.only(top: 16.h),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6.r),
        color: const Color.fromRGBO(255, 255, 255, 1),
      ),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 8.h, left: 12.w, right: 12.w),
            child: Row(
              children: [
                Text(
                  'Cài Đặt',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: const Color.fromRGBO(133, 133, 133, 1),
                      fontSize: 12.sp,
                      fontFamily: 'static/Inter-Medium.ttf',
                      fontWeight: FontWeight.w500),
                ),
                Expanded(child: Container())
              ],
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 8.h)),
          Container(
            margin: EdgeInsets.only(left: 12.w,),
            child: Column(
              children: [
                InkWell(
                  onTap: () {
                    controller.getStaticPage("SUPPORT");
                  },
                  child: Row(
                    children: [
                      Text(
                        'Hỗ Trợ',
                        style: TextStyle(
                            color: const Color.fromRGBO(24, 29, 39, 1),
                            fontSize: 14.sp,
                            fontFamily: 'static/Inter-Medium.ttf'),
                      ),
                      Expanded(child: Container()),
                      IconButton(
                          iconSize: 24.w,
                          onPressed: () {},
                          icon: const Icon(
                            Icons.navigate_next,
                            color: Color.fromRGBO(248, 129, 37, 1),
                          ))
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    controller.goToChangePassPage();
                  },
                  child: Row(
                    children: [
                      Text(
                        'Đổi Mật Khẩu ',
                        style: TextStyle(
                            color: const Color.fromRGBO(24, 29, 39, 1),
                            fontSize: 14.sp,
                            fontFamily: 'static/Inter-Medium.ttf'),
                      ),
                      Expanded(child: Container()),
                      IconButton(
                          iconSize: 24.w,
                          onPressed: () {},
                          icon: const Icon(
                            Icons.navigate_next,
                            color: Color.fromRGBO(248, 129, 37, 1),
                          ))
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    controller.getStaticPage("RULES");
                  },
                  child: Row(
                  children: [
                    Text(
                      'Chính Sách Bảo Mật',
                      style: TextStyle(
                          color: const Color.fromRGBO(24, 29, 39, 1),
                          fontSize: 14.sp,
                          fontFamily: 'static/Inter-Medium.ttf'),
                    ),
                    Expanded(child: Container()),
                    IconButton(
                        iconSize: 24.w,
                        onPressed: () {},
                        icon: const Icon(
                          Icons.navigate_next,
                          color: Color.fromRGBO(248, 129, 37, 1),
                        ))
                  ],
                ),),
                InkWell(
                  onTap: () {
                    controller.getStaticPage("SECURITY");
                  },
                  child: Row(
                    children: [
                      Text(
                        'Điều Khoản Sử Dụng',
                        style: TextStyle(
                            color: const Color.fromRGBO(24, 29, 39, 1),
                            fontSize: 14.sp,
                            fontFamily: 'static/Inter-Medium.ttf'),
                      ),
                      Expanded(child: Container()),
                      IconButton(
                          iconSize: 24.w,
                          onPressed: () {},
                          icon: const Icon(
                            Icons.navigate_next,
                            color: Color.fromRGBO(248, 129, 37, 1),
                          ))
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    Get.bottomSheet(_onBottomSheetLogout());
                  },
                  child: Row(
                    children: [
                      Text(
                        'Đăng Xuất',
                        style: TextStyle(
                            color: const Color.fromRGBO(24, 29, 39, 1),
                            fontSize: 14.sp,
                            fontFamily: 'static/Inter-Medium.ttf'),
                      ),
                      Expanded(child: Container()),
                      IconButton(
                          iconSize: 24.w,
                          onPressed: () {},
                          icon: const Icon(
                            Icons.navigate_next,
                            color: Color.fromRGBO(248, 129, 37, 1),
                          ))
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  var styleValue = TextStyle(
      color: const Color.fromRGBO(26, 26, 26, 1),
      fontSize: 14.sp,
      fontFamily: 'static/Inter-Medium.ttf',
      fontWeight: FontWeight.w500);
  var styleTitle = TextStyle(
      color: const Color.fromRGBO(133, 133, 133, 1),
      fontSize: 12.sp,
      fontFamily: 'static/Inter-Medium.ttf',
      fontWeight: FontWeight.w500);

  informationWidget() {
    return Obx(() => Container(
          margin: EdgeInsets.only(top: 16.h),
          padding: EdgeInsets.only(left: 6.w, right: 6.w),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            color: const Color.fromRGBO(255, 255, 255, 1),
          ),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(
                    top: 16.h, bottom: 8.h, right: 9.w, left: 9.w),
                child: Row(
                  children: [
                    Text(
                      'Thông Tin Cá Nhân',
                      style: styleTitle,
                    ),
                    Expanded(child: Container()),
                    InkWell(
                      onTap: () {
                        controller.goToUpdateInfoUser();
                      },
                      child:
                      SizedBox(
                        height: 30.h,
                        width: 79.w,
                        child: Row(
                          children: [
                            Text('Chỉnh sửa',
                                style: TextStyle(
                                    color: const Color.fromRGBO(248, 129, 37, 1),
                                    fontSize: 12.sp,
                                    fontFamily: 'static/Inter-Regular.ttf')),
                            SizedBox(
                              width: 8.w,
                            ),
                            Icon(
                              Icons.edit_outlined,
                              color: const Color.fromRGBO(248, 129, 37, 1),
                              size: 16.w,
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                  margin: const EdgeInsets.all(9),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            'Họ và tên: ',
                            style: styleTitle,
                          ),
                          Expanded(child: Container()),
                          Text(
                            controller.userProfile.value.fullName?? "",
                            style: TextStyle(
                                color: const Color.fromRGBO(248, 129, 37, 1),
                                fontSize: 14.sp,
                                fontFamily: 'static/Inter-Medium.ttf',
                                fontWeight: FontWeight.w500),
                          )
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 10.h)),
                      Row(
                        children: [
                          Text(
                            'Lớp: ',
                            style: styleTitle,
                          ),
                          Expanded(child: Container()),
                          Text(
                            controller.clazz.value.name??"",
                            style: styleValue,
                          )
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 10.h)),
                      Row(
                        children: [
                          Text(
                            'Số Điện Thoại: ',
                            style: styleTitle,
                          ),
                          Expanded(child: Container()),
                          Text(
                            controller.userProfile.value.phone?? "",
                            style: styleValue,
                          )
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 10.h)),
                      Row(
                        children: [
                          Text(
                            'Email: ',
                            style: styleTitle,
                          ),
                          Expanded(child: Container()),
                          Text(
                            controller.userProfile.value.email?? "",
                            style: styleValue,
                          )
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 10.h)),
                      Row(
                        children: [
                          Text(
                            'Trường : ',
                            style: styleTitle,
                          ),
                          Expanded(child: Container()),
                          controller.userProfile.value.school == null ? Container(): Text(
                            '${controller.school.value.name}',
                            style: styleValue,
                          )
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 10.h)),
                    ],
                  )
              )
            ],
          ),
        ));
  }

  profileAvatarWidget() {
    return Obx(() => Container(
          width: 400.w,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6.r),
            color: const Color.fromRGBO(255, 255, 255, 1),
          ),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 10.h),
                child: Stack(
                  children: [
                    SizedBox(
                      width: 80.w,
                      height: 80.w,
                      child:
                      InkWell(
                        onTap: (){
                          controller.goToDetailAvatar();
                        },
                        child: CircleAvatar(
                          backgroundColor: Colors.white,
                          backgroundImage: Image.network(
                            controller.userProfile.value.image ??
                                "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                            errorBuilder:
                                (context, object, stackTrace) {
                              return Image.asset(
                                "assets/images/img_Noavt.png",
                              );
                            },
                          ).image,
                        ),
                      )
                    ),
                    Positioned(
                        right: 0,
                        bottom: 0,
                        child:
                        InkWell(
                          onTap: () {
                            pickerImage();
                          },
                          child: Image.asset(
                              'assets/images/img_cam.png',width: 24.w,height: 24.h,),
                        ))
                  ],
                ),
              ),
              SizedBox(
                height: 8.h,
              ),
              Text(
                controller.userProfile.value.fullName ?? "",
                style: TextStyle(
                    color: const Color.fromRGBO(26, 26, 26, 1),
                    fontSize: 18.sp,
                    fontFamily: 'static/Inter-Medium.ttf',
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 8.h,
              ),
              Text(
                controller.userProfile.value.email ?? "",
                style: TextStyle(
                  color: const Color.fromRGBO(133, 133, 133, 1),
                  fontSize: 10.sp,
                ),
              ),
              SizedBox(
                height: 16.h,
              ),
            ],
          ),
        ));
  }
  buildAvatarSelect() {
    var file = controller.files[0].file;
    return SizedBox(
      width: double.infinity,
      child: Container(
          width: 180,
          height: 180,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(100.r))),
          child: CircleAvatar(
            backgroundImage: Image.file(
              file!,
              width: 200,
              height: 200,
            ).image,
          )),
    );
  }

  Future<void> pickerImage() async {
    var file = await FileDevice.showSelectFileV2(Get.context!, image: true);
    if (file.isNotEmpty) {
      controller.files.value.clear();
      controller.files.value.addAll(file);
      controller.files.refresh();
      Get.bottomSheet(
          getDialogConfirm(
              "Chọn hình đại diện",
              Container(
                padding:
                const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                child: buildAvatarSelect(),
              ),
              colorBtnOk: ColorUtils.COLOR_GREEN_BOLD,
              colorTextBtnOk: ColorUtils.COLOR_WHITE,
              btnRight: "Xác nhận", funcLeft: () {
            Get.back();
          }, funcRight: () {
            Get.back();
            controller.uploadFile();
          }),
          isScrollControlled: true);
    }
  }
}

