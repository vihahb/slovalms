import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:task_manager/commom/utils/color_utils.dart';
import 'package:task_manager/commom/utils/file_device.dart';
import 'package:task_manager/commom/utils/global.dart';
import 'package:task_manager/commom/widget/custom_view.dart';
import 'package:task_manager/view/mobile/role/teacher/update_infomation_teacher/update_infomation_teacher_controller.dart';

import '../../../../../commom/utils/app_utils.dart';
import '../../../../../commom/widget/text_field_custom.dart';

class UpdateInfoTeacherPage extends GetWidget<UpdateInfoTeacherControlller> {
  final controller = Get.put(UpdateInfoTeacherControlller());

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
            elevation: 0,
            title: const Text(
              "Chỉnh Sửa Thông Tin Cá Nhân",
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  color: Colors.white),
            ),
          ),
          body: Obx(() => controller.isReady.value?SingleChildScrollView(
              reverse: true, // this is new
              physics: const BouncingScrollPhysics(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Center(
                    child: Column(
                      children: [
                        const Padding(padding: EdgeInsets.only(top: 16)),
                        Container(
                          height: 80,
                          margin: const EdgeInsets.only(top: 10),
                          child: Stack(
                            children: [
                              InkWell(
                                onTap: (){
                                  controller.goToDetailAvatar();
                                },
                                child:   SizedBox(
                                  width: 80.w,
                                  height: 80.w,
                                  child: CircleAvatar(
                                    backgroundColor: Colors.white,
                                    backgroundImage: Image.network(
                                      controller.userProfile.value.image ??
                                          "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                                      errorBuilder:
                                          (context, object, stackTrace) {
                                        return Image.asset(
                                          "assets/images/img_Noavt.png",
                                        );
                                      },
                                    ).image,
                                  ),
                                ),
                              ),
                              Positioned(
                                  right: 0,
                                  bottom: 0,
                                  child: InkWell(
                                    onTap: () {
                                      pickerImage();
                                    },
                                    child: SizedBox(width: 24.w,
                                      height: 24.w,child: Image.asset(
                                        'assets/images/img_cam.png',width: 24.w,height: 24.h,),),
                                  ))
                            ],
                          ),
                        ),
                        Container(
                            margin:
                            const EdgeInsets.only(top: 16, bottom: 8),
                            height: 40,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  controller.userProfile.value.fullName ?? "",
                                  style: const TextStyle(
                                      color: Color.fromRGBO(26, 26, 26, 1),
                                      fontSize: 16,
                                      fontFamily: 'static/Inter-Medium.ttf',
                                      fontWeight: FontWeight.bold),
                                ),
                                const Padding(
                                    padding: EdgeInsets.only(top: 4)),
                                Text(
                                  controller.userProfile.value.email ?? "",
                                  style: const TextStyle(
                                    color: Color.fromRGBO(133, 133, 133, 1),
                                    fontSize: 10,
                                  ),
                                )
                              ],
                            ))
                      ],
                    ),
                  ),
                  const Padding(padding: EdgeInsets.only(top: 8)),
                  OutlineBorderTextFormField(
                    enable: true,
                    focusNode: controller.focusName.value,
                    iconPrefix: "",
                    iconSuffix: "",
                    state: controller.stateInput,
                    labelText: "Họ và tên",
                    autofocus: false,
                    controller: controller.controllerName.value,
                    helperText: controller.stateInput == StateType.ERROR?"Họ và tên không được để trống":"",
                    showHelperText: false,
                    ishowIconPrefix: false,
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.text,
                    validation: (textToValidate) {
                      return controller
                          .getTempIFSCValidation(textToValidate);
                    },
                  ),
                  const Padding(padding: EdgeInsets.only(top: 8)),
                  OutlineBorderTextFormField(
                    enable: false,
                    focusNode: controller.focusPhone.value,
                    iconPrefix: null,
                    iconSuffix: "",
                    state: StateType.DEFAULT,
                    labelText: "Số Điện thoại",
                    autofocus: false,
                    controller: controller.controllerPhone.value,
                    helperText: "",
                    showHelperText: false,
                    textInputAction: TextInputAction.next,
                    ishowIconPrefix: false,
                    keyboardType: TextInputType.text,
                    validation: (textToValidate) {
                      return controller
                          .getTempIFSCValidation(textToValidate);
                    },
                  ),
                  const Padding(padding: EdgeInsets.only(top: 8)),
                  OutlineBorderTextFormField(
                    enable: false,
                    focusNode: controller.focusEmail.value,
                    iconPrefix: "",
                    iconSuffix: "",
                    state: StateType.DEFAULT,
                    labelText: "Email",
                    autofocus: false,
                    controller: controller.controllerEmail.value,
                    helperText: "",
                    showHelperText: false,
                    ishowIconPrefix: false,
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.text,
                    validation: (textToValidate) {
                      return controller
                          .getTempIFSCValidation(textToValidate);
                    },
                  ),
                  const Padding(padding: EdgeInsets.only(top: 8)),
                  OutlineBorderTextFormField(
                    enable: false,
                    iconPrefix: "",
                    iconSuffix: "",
                    focusNode: controller.focusSchool.value,
                    state: StateType.DEFAULT,
                    labelText: "Trường",
                    autofocus: false,
                    controller: controller.controllerSchool.value,
                    helperText: "",
                    showHelperText: false,
                    ishowIconPrefix: false,
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.text,
                    validation: (textToValidate) {
                      return controller
                          .getTempIFSCValidation(textToValidate);
                    },
                  ),
                  const Padding(padding: EdgeInsets.only(top: 8)),
                  ListView.builder(
                      shrinkWrap: true,
                      itemCount:  controller.userProfile.value.item!.teachers?.length,
                      itemBuilder: (context, index){
                        if(
                        controller.userProfile.value.item!.teachers!.isNotEmpty
                        ){
                          controller.listclass.value = controller.userProfile.value.item!.teachers![index].clazz!;
                        }
                        return
                          Container(
                            margin: EdgeInsets.only(left: 15.w, right: 15.w,bottom: 8.h),
                            width: double.infinity,
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(192, 192, 192, 1)
                              ),
                              color: Color.fromRGBO(246, 246, 246, 1),
                              borderRadius: BorderRadius.circular(6),
                            ),
                            padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.h),

                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text("Chức vụ", style:  TextStyle(fontSize: 10, fontWeight: FontWeight.w500,color:Color.fromRGBO(177, 177, 177, 1) ),),
                                Padding(padding: EdgeInsets.only(top: 4.h)),
                                RichText(
                                  textAlign: TextAlign.start,
                                  text: TextSpan(
                                    text: "${controller.getType(controller.userProfile.value.item!.teachers?[index].type, controller.userProfile.value.item!.teachers?[index].subjectName)} ",
                                    style: TextStyle(
                                        color: const Color
                                            .fromRGBO(26,
                                            26, 26, 1),
                                        fontSize: 12.sp,
                                        fontFamily: 'static/Inter-Medium.ttf'),
                                    children:
                                    controller.listclass.value.map((e) {
                                      var index = controller.listclass.value.indexOf(e);
                                      var showSplit = ", ";
                                      if (index == controller.listclass.value.length - 1) {
                                        showSplit = "";
                                      }
                                      return TextSpan(
                                          text: "${controller.listclass.value[index].name}$showSplit",
                                          style: TextStyle(
                                              color: const Color.fromRGBO(248, 129, 37, 1),
                                              fontSize: 12.sp,
                                              fontWeight: FontWeight.w500,
                                              fontFamily: 'static/Inter-Medium.ttf'));
                                    }).toList(),
                                  ),
                                ),

                              ],
                            ),
                          );
                      }

                  ),

                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.fromLTRB(16, 32, 16, 0),
                    child: SizedBox(
                      height: 46,
                      child: ElevatedButton(
                        onPressed: () {
                          dismissKeyboard();
                          controller.tmpUserProfile.value.fullName =
                              controller.controllerName.value.text;
                          if(controller.controllerName.value.text.trim().isEmpty){
                            controller.stateInput = StateType.ERROR;
                            AppUtils.shared.snackbarError(
                                "Họ và tên không được để trống","");
                          }else{
                            controller.updateInfoUser(controller.tmpUserProfile.value,
                                controller.userProfile.value.id);
                            controller.stateInput = StateType.DEFAULT;
                          }

                        },
                        style: ElevatedButton.styleFrom(
                            backgroundColor:
                            const Color.fromRGBO(248, 129, 37, 1),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6))),
                        child: const Text("Cập Nhật",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w400)),
                      ),
                    ),
                  ),
                  Padding(
                    // this is new
                      padding: EdgeInsets.only(
                          bottom:
                          MediaQuery.of(context).viewInsets.bottom)),
                ],
              )):const Center(
            child: CircularProgressIndicator(),
          ))),
    );
  }

  buildAvatarSelect() {
    var file = controller.files[0].file;
    return SizedBox(
      width: double.infinity,
      child: Container(
          width: 180,
          height: 180,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(100.r))),
          child: CircleAvatar(
            backgroundImage: Image.file(
              file!,
              width: 200,
              height: 200,
            ).image,
          )),
    );
  }

  Future<void> pickerImage() async {
    var file = await FileDevice.showSelectFileV2(Get.context!, image: true);
    if (file.isNotEmpty) {
      controller.files.value.clear();
      controller.files.value.addAll(file);
      controller.files.refresh();
      Get.bottomSheet(
          getDialogConfirm(
              "Chọn hình đại diện",
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                child: buildAvatarSelect(),
              ),
              colorBtnOk: ColorUtils.COLOR_GREEN_BOLD,
              colorTextBtnOk: ColorUtils.COLOR_WHITE,
              btnRight: "Xác nhận", funcLeft: () {
            Get.back();
          }, funcRight: () {
            Get.back();
            controller.uploadFile();
          }),
          isScrollControlled: true);
    }
  }
}
