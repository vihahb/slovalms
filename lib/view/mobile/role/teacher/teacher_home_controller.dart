import 'dart:ui';

import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:task_manager/commom/app_cache.dart';
import 'package:task_manager/commom/utils/app_utils.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/data/model/common/user_profile.dart';
import 'package:task_manager/data/model/res/class/School.dart';
import 'package:task_manager/data/repository/account/auth_repo.dart';
import 'package:task_manager/data/repository/person/personal_info_repo.dart';
import 'package:task_manager/data/repository/subject/class_office_repo.dart';
import 'package:task_manager/routes/app_pages.dart';
import 'package:task_manager/view/mobile/home/home_controller.dart';
import 'package:task_manager/view/mobile/role/student/personal_information_student/detail_avatar/detail_avatar_page.dart';
import 'package:task_manager/view/mobile/role/teacher/send_notification_teacher/send_notification_teacher_page.dart';
import 'package:task_manager/view/mobile/schedule/schedule_controller.dart';
import '../../../../commom/widget/appointment_builder_timelineday.dart';
import '../../../../commom/widget/hexColor.dart';
import '../../../../data/model/common/Position.dart';
import '../../../../data/model/common/schedule.dart';
import '../../../../data/model/common/schedule.dart';
import '../../../../data/model/common/school_year.dart';
import '../../../../data/model/common/static_page.dart';
import '../../../../data/model/res/class/classTeacher.dart';
import '../../../../data/repository/common/common_repo.dart';
import '../../../../data/repository/schedule/schedule_repo.dart';
import '../../../../data/repository/school_year/school_year_repo.dart';
import '../../account/login/login_controller.dart';
import '../../phonebook/phone_book_controller.dart';
import 'diligent_management_teacher/diligent_management_teacher_controller.dart';


class TeacherHomeController extends GetxController {
  final AuthRepo _authRepo = AuthRepo();
  final CommonRepo _commonRepo = CommonRepo();
  final ClassOfficeRepo _classRepo = ClassOfficeRepo();
  final PersonalInfoRepo _personalInfoRepo = PersonalInfoRepo();
  final ScheduleRepo _scheduleRepo = ScheduleRepo();
  final SchoolYearRepo _schoolYearRepo = SchoolYearRepo();
  RxList<ClassId> classOfTeacher = <ClassId>[].obs;
  Rx<ClassId> currentClass = ClassId().obs;
  var position = Position().obs;
  var school = SchoolData().obs;
  var subject = Subject().obs;
  RxList<Schedule> scheduleTeacher = <Schedule>[].obs;
  RxList<Appointment> timeTable = <Appointment>[].obs;
  var userProfile = UserProfile().obs;
  var staticPage = ItemsStaticPage().obs;
  RxList<Clazzs>  listclass = <Clazzs>[].obs;
  RxList<SchoolYear> schoolYears = <SchoolYear>[].obs;
  var clickSchoolYear = <bool>[].obs;
  var fromYearPresent = 0.obs;
  var toYearPresent = 0.obs;
  var isReady = false.obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  RxList<Schedule> scheduleToday = <Schedule>[].obs;
  var classUId = ClassId().obs;
  var calendarController = CalendarController().obs;
  var isSubjectToday = true.obs;
  @override
  void onInit() {
    super.onInit();
    getSchoolYearByUser();
    calendarController.value.displayDate = DateTime(DateTime.now().year,DateTime.now().month,DateTime.now().day,DateTime.now().hour-2);
  }


  onClickShoolYears(index){
    clickSchoolYear.value = <bool>[].obs;
    for(int i = 0;i<schoolYears.value.length;i++){
      clickSchoolYear.value.add(false);
    }
    clickSchoolYear.value[index] = true;
  }

  getSchoolYearByUser() async{
    _schoolYearRepo.getListSchoolYearByUser().then((value) {
      if (value.state == Status.SUCCESS) {
        schoolYears.value = value.object!;
        if(schoolYears.value.length !=0){
          fromYearPresent.value = schoolYears.value[0].fromYear!;
          toYearPresent.value = schoolYears.value[0].toYear!;
          isReady.value = true;
          getDetailProfile();
        }
        for(int i = 0;i<schoolYears.value.length;i++){
          clickSchoolYear.add(false);
        }
      }
    });
  }


  setTimeCalendar(index){
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      calendarController.value.displayDate = DateTime(schoolYears.value[index].fromYear!.toInt(),DateTime.now().month,DateTime.now().day,DateTime.now().hour-2);
    }else{
      calendarController.value.displayDate = DateTime(schoolYears.value[index].toYear!.toInt(),DateTime.now().month,DateTime.now().day,DateTime.now().hour-2);
    }
  }


  getDetailProfile() {
    _personalInfoRepo.detailUserProfile().then((value) {
      if (value.state == Status.SUCCESS) {
        userProfile.value = value.object!;
        if(userProfile.value.school == null || userProfile.value.school == ""){
          school.value.name = "";
          school.value.id = "";
        }else{
          school.value = userProfile.value.school!;
        }
        if(userProfile.value.position == null || userProfile.value.position == ""){
          position.value.name = "";
          position.value.id = "";
        }else{
          position.value = userProfile.value.position!;
        }
        AppCache().userId = userProfile.value.id ?? "";
        getListClassIdByTeacher();
      }
    });
  }

  getListClassIdByTeacher() {
    var teacherId = userProfile.value.id;
    _classRepo.listClassByTeacher(teacherId).then((value) {
      if (value.state == Status.SUCCESS) {
        classOfTeacher.value = value.object!;
        Get.find<ScheduleController>().queryTimeTableClassByTeacher(userProfile.value.id,classOfTeacher.value[0].classId);
        for (var f in classOfTeacher.value) {
          f.checked = false;
        }
        classOfTeacher.value.first.checked = true;
        currentClass.value = classOfTeacher.value.first;
        classOfTeacher.refresh();
        //Call next API
        if(userProfile.value != null || userProfile != ""){
          getTablePlan();
        }
      }
    });
  }

  //Lấy du lieu thoi khoa bieu theo lop hoc da chon
  void getTablePlan() {
    var idTeacher = userProfile.value.id;
    getScheduleTimeTableTeacher(idTeacher);
  }

  onSelectedSchoolYears(index){
    timeTable.value.clear();
    Get.find<ScheduleController>().timeTable.value.clear();
    Get.find<ScheduleController>().queryTimeCalendar(index, AppCache().userType);
    AppCache().setSchoolYearId(schoolYears.value[index].id??"");
    getScheduleTimeTableTeacher(userProfile.value.id);

    _classRepo.listClassByTeacher(userProfile.value.id).then((value) {
     if (value.state == Status.SUCCESS) {
       classOfTeacher.value = value.object!;
       for (var f in classOfTeacher.value) {
         f.checked = false;
       }
       classOfTeacher.value.first.checked = true;
       currentClass.value = classOfTeacher.value.first;
       Get.find<ScheduleController>().queryTimeTableClassByTeacher(userProfile.value.id,classOfTeacher.value[0].classId);
       classOfTeacher.refresh();
     }
   });
    fromYearPresent.value = schoolYears.value[index].fromYear!;
    toYearPresent.value = schoolYears.value[index].toYear!;
    timeTable.refresh();
    _personalInfoRepo.detailUserProfile().then((value) {
      if (value.state == Status.SUCCESS) {
        userProfile.value = value.object!;
        if(userProfile.value.school == null || userProfile.value.school == ""){
          school.value.name = "";
          school.value.id = "";
        }else{
          school.value = userProfile.value.school!;
        }
        if(userProfile.value.position == null || userProfile.value.position == ""){
          position.value.name = "";
          position.value.id = "";
        }else{
          position.value = userProfile.value.position!;
        }
      }
    });
    setTimeCalendar(index);
    Get.find<ScheduleController>().timeTable.refresh();
    Get.find<PhoneBookController>().getListClassByStudent();
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (schoolYears.value[index].fromYear == DateTime.now().year) {
        isSubjectToday.value = true;
      } else {
        isSubjectToday.value = false;
      }
    } else {
      if (schoolYears.value[index].toYear == DateTime.now().year) {
        isSubjectToday.value = true;
      } else {
        isSubjectToday.value = false;
      }
    }
    Get.back();
  }


  onRefresh() {
    _personalInfoRepo.detailUserProfile().then((value) {
      if (value.state == Status.SUCCESS) {
        userProfile.value = value.object!;
        if(userProfile.value.school == null || userProfile.value.school == ""){
          school.value.name = "";
          school.value.id = "";
        }else{
          school.value = userProfile.value.school!;
        }
        if(userProfile.value.position == null || userProfile.value.position == ""){
          position.value.name = "";
          position.value.id = "";
        }else{
          position.value = userProfile.value.position!;
        }
      }
    });

    _classRepo.listClassByTeacher(userProfile.value.id).then((value) {
      if (value.state == Status.SUCCESS) {
        classOfTeacher.value = value.object!;
        for (var f in classOfTeacher.value) {
          f.checked = false;
        }
        classOfTeacher.value.first.checked = true;
        currentClass.value = classOfTeacher.value.first;
        classOfTeacher.refresh();
        //Call next API
        if(userProfile.value != null || userProfile != ""){
          getTablePlan();
        }
      }
    });

    if(DateTime.now().month<=12&&DateTime.now().month>9){
      calendarController.value.displayDate = DateTime(fromYearPresent.value,DateTime.now().month,DateTime.now().day,DateTime.now().hour-1);
    }else{
      calendarController.value.displayDate = DateTime(toYearPresent.value,DateTime.now().month,DateTime.now().day,DateTime.now().hour-1);
    }

    Get.find<HomeController>().getCountNotifyNotSeen();
  }


  selectedClass(ClassId classId,index) {
    currentClass.value = classId;
    Get.find<ScheduleController>().queryTimeTableClassByTeacher(userProfile.value.id,classOfTeacher.value[index].classId);
    for (var f in classOfTeacher.value) {
      if (f.id != classId.id) {
        f.checked = false;
      } else {
        f.checked = true;
      }
    }
    classOfTeacher.refresh();
  }

  goToUpdateInfoUser() {
    Get.toNamed(Routes.updateInfoTeacher, arguments: userProfile.value);
  }

  void submitLogout() async {
    logout();
  }

  getScheduleTimeTableTeacher(idTeacher){
    _scheduleRepo.getScheduleTeacher(idTeacher).then((value) {
      if (value.state == Status.SUCCESS) {
        scheduleTeacher.value = value.object!;
        getAppointments();
      }
    });
    scheduleTeacher.refresh();
  }

  getAppointments() {
    timeTable.value.clear();
    for(int i =0; i<scheduleTeacher.value.length;i++){
      timeTable.value.add(Appointment(
          startTime: DateTime.fromMillisecondsSinceEpoch(scheduleTeacher.value[i].startTime!), // tg kết thúc
          endTime: DateTime.fromMillisecondsSinceEpoch(scheduleTeacher.value[i].endTime!), // tg bắt đầu
          subject: scheduleTeacher.value[i].subject!.subjectCategory!.name!,  // môn học
          notes: scheduleTeacher.
          value[i].subject?.clazz?.classCategory?.name!.toString(), // tên lớp
          resourceIds: scheduleTeacher.value[i].subject!.clazz!.students,
          recurrenceId: scheduleTeacher.value[i].subject?.teacher?.fullName!.toString(),
        // chi tiết học sinh
          color: Color.fromRGBO(249, 154, 81, 1)
      ));
    }
    scheduleToday.value = scheduleTeacher.value.where((element) => outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(element.startTime!))
        == outputDateFormat.format(DateTime.now())&&outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(element.endTime!))
        == outputDateFormat.format(DateTime.now())).toList();
    timeTable.refresh();
  }

  getStaticPage(type){
    _commonRepo.getListStaticPage(type).then((value){
      if (value.state == Status.SUCCESS) {
        staticPage.value = value.object!;
        goToStaticPage();
      }
    });
  }



  logout() async {
    _authRepo.logout().then((value) {
      goToLogin();
    });
    if (AppCache().isSaveInfoLogin) {
      String? usernameSaved = await AppCache().username;
      String? passwordSaved = await AppCache().password;
      Get.find<LoginController>().controllerUserName.text = usernameSaved ?? "";
      Get.find<LoginController>().controllerPassword.text = passwordSaved ?? "";
    }else{
      AppCache().deleteInfoLogin();
    }
    AppCache().resetCache();
  }

  goToStaticPage(){
    Get.toNamed(Routes.staticPage,arguments: staticPage.value);
  }


  void goToLogin() {
    Get.toNamed(Routes.login);
  }

  void goToStudentListPage() {
    Get.toNamed(Routes.studentListPage, arguments: currentClass.value);
  }
  void goToDetailAvatar(){
    Get.toNamed(Routes.getavatar, arguments: userProfile.value.image);
  }
  void goToChangePassPage(){
    Get.toNamed(Routes.changePass,arguments:userProfile.value.id);
  }
  goToSendNotificationPage(){
    Get.to(SendNotificationTeacherPage());
  }

  getType(type, subject){
    switch(type){
      case "HOME_ROOM_TEACHER":
        return "Giáo viên chủ nhiệm";
      case "SUBJECT_TEACHER":
        return "Giáo viên ${subject}";
      default:
        return "";
    }

  }

}
