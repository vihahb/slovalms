import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:task_manager/view/mobile/role/teacher/student_list/student_list_controller.dart';

import '../../../../../commom/utils/date_time_utils.dart';

class StudentListPage extends GetWidget<StudentListController> {
  final controller = Get.put(StudentListController());

  StudentListPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color.fromRGBO(239, 239, 239, 1),
        appBar: AppBar(
          actions: [
            IconButton(
              onPressed: () {
                Get.back();
              },
              icon: const Icon(Icons.home),
              color: Colors.white,
            )
          ],
          backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
          title: Text(
            'Danh Sách Học Sinh',
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.sp,
                fontFamily: 'static/Inter-Medium.ttf'),
          ),
        ),
        body: controller.isReady.value?Obx(
              () => RefreshIndicator(
              color: const Color.fromRGBO(248, 129, 37, 1),
              onRefresh: () async {
                await controller.onRefresh();
              },
              child: SingleChildScrollView(
                physics: const AlwaysScrollableScrollPhysics(),
                child: Center(
                  child: Column(
                    children: [
                      Container(
                          alignment: Alignment.centerLeft,
                          margin: EdgeInsets.only(
                              top: 16.h, left: 16.w, bottom: 8.h),
                          child: RichText(
                            text: TextSpan(children: [
                              TextSpan(
                                  text: 'Danh Sách Học Sinh ',
                                  style: TextStyle(
                                      color: const Color.fromRGBO(
                                          133, 133, 133, 1),
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w500)),
                              TextSpan(
                                  text: '${controller.currentClass.value.name}',
                                  style: TextStyle(
                                      color:
                                      const Color.fromRGBO(248, 129, 37, 1),
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w500))
                            ]),
                          )),
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: const Color.fromRGBO(255, 255, 255, 1)),
                        margin: EdgeInsets.only(left: 16.w, right: 16.w),
                        padding: EdgeInsets.symmetric(vertical: 8.h),
                        child: ListView.builder(
                            shrinkWrap: true,
                            physics: const ScrollPhysics(),
                            itemCount: controller.detailItem.value.length,
                            itemBuilder: (context, index) {
                              var showLine = index ==
                                  controller.detailItem.value.length - 1
                                  ? false
                                  : true;
                              return buildItemListStudent(index, showLine);
                            }),
                      ),
                    ],
                  ),
                ),
              )),
        ):const Center(
            child: CircularProgressIndicator()),
    );
  }

  buildItemListStudent(int index, bool showLine) {
    return Obx(() => GestureDetector(
          onTap: () {},
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 16.w, right: 16.w),
                    child: Text(
                      '${index + 1}',
                      style: TextStyle(
                          color: const Color.fromRGBO(26, 26, 26, 1),
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  SizedBox(
                    width: 48.w,
                    height: 48.w,
                    child: CircleAvatar(
                      backgroundColor: Colors.white,
                      backgroundImage: Image.network(
                        controller.detailItem.value[index].image ??
                            "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                        errorBuilder: (context, object, stackTrace) {
                          print(
                              "Image Error: ${controller.detailItem.value[index].image}");
                          return Image.asset(
                            "assets/images/img_Noavt.png",
                          );
                        },
                      ).image,
                    ),
                  ),
                  const SizedBox(
                    width: 16,
                  ),
                  Expanded(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RichText(
                          text: TextSpan(children: [
                        TextSpan(
                            text: 'Học Sinh: ',
                            style: TextStyle(
                                color: const Color.fromRGBO(248, 129, 37, 1),
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w400,
                                fontFamily: 'static/Inter-Regular.ttf')),
                        TextSpan(
                            text:
                                '${controller.detailItem.value[index].fullName}',
                            style: TextStyle(
                                color: const Color.fromRGBO(26, 26, 26, 1),
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w400,
                                fontFamily: 'static/Inter-Regular.ttf')),
                      ])),
                      Padding(padding: EdgeInsets.only(top: 4.h)),
                      Text(
                        controller.detailItem.value[index].birthday != null
                            ? DateTimeUtils.convertLongToStringTime(
                                controller.detailItem.value[index].birthday)
                            : "",
                        style: TextStyle(
                            color: const Color.fromRGBO(133, 133, 133, 1),
                            fontSize: 14.sp,
                            fontWeight: FontWeight.w400,
                            fontFamily: 'static/Inter-Regular.ttf'),
                      )
                    ],
                  ))
                ],
              ),
              (showLine) ? const Divider() : Container()
            ],
          ),
        ));
  }
}
