import 'package:get/get.dart';
import 'package:task_manager/commom/utils/app_utils.dart';
import 'package:task_manager/commom/utils/color_utils.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/data/model/common/list_student.dart';
import '../../../../../data/model/res/class/classTeacher.dart';
import '../../../../../data/repository/list_student/list_student_repo.dart';

class StudentListController extends GetxController{
  final ListStudentRepo _listStudent = ListStudentRepo();
  var listStudent = ListStudent().obs;
  RxList<DetailItem> detailItem = <DetailItem>[].obs;
  Rx<ClassId> currentClass = ClassId().obs;
  var isReady = false.obs;
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    var tmpClass = Get.arguments;
    if(tmpClass!=null){
      currentClass.value = tmpClass;
      getDetailListStudent();
    }
  }


    getDetailListStudent() {
    var classId = currentClass.value.classId;
    _listStudent.listStudentByClass(classId).then((value) {
      if (value.state == Status.SUCCESS) {
        detailItem.value = value.object!;
        AppUtils.shared.showToast("Lấy danh sách học sinh thành công!");
      }
    });
    isReady.value = true;
  }

  onRefresh() {
    getDetailListStudent();
  }

}