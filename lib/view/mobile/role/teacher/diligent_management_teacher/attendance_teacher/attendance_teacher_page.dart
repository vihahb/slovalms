import 'package:get/get_state_manager/src/simple/get_view.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../../../commom/utils/app_utils.dart';
import 'attendance_teacher_controller.dart';

class AttendanceTeacherPage extends GetView<AttendanceTeacherController> {
  final controller = Get.put(AttendanceTeacherController(), permanent: true);

  AttendanceTeacherPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
        child: Scaffold(
            body: controller.isReady.value
                ? Obx(() => Column(
                      children: [
                        Row(
                          children: [
                            Expanded(child: Container()),
                            Visibility(
                              visible: controller.isShowSwitchAttendance.value,
                                child: Container(
                              child:  controller.getStatusConfirmAttendance(
                                  controller.status.value)
                                  ? Container(
                                margin: EdgeInsets.only(
                                    right: 16.w, top: 8.h, bottom: 8.h),
                                child: Text(
                                    "Đã điểm danh ${controller.listStudentDiligence.value.length}/${controller.listStudentDiligence.value.length}"),
                              )
                                  : SizedBox(
                                height: 55,
                                width: 150,
                                child: SwitchListTile(
                                  contentPadding: EdgeInsets.zero,
                                  title: Transform.translate(
                                    offset: Offset(30, 0),
                                    child: Text(
                                      "Điểm danh",
                                      style: TextStyle(
                                          fontSize: 12.sp,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.black),
                                    ),
                                  ),
                                  activeColor:
                                  Color.fromRGBO(248, 129, 37, 1),
                                  value: controller.isAttendance.value,
                                  onChanged: (value) {
                                    controller.setStatusLockAttendance();

                                  },
                                ),
                              ),
                            ))
                          ],
                        ),
                        Expanded(
                            child:
                                controller.listStudentDiligence.value.isNotEmpty
                                    ? ListView.builder(
                                        itemCount: controller.listStudentDiligence.value.length,
                                        shrinkWrap: true,
                                        physics: const ScrollPhysics(),
                                        itemBuilder: (context, index) {
                                          return Container(
                                            margin: EdgeInsets.only(left: 16.w, right: 16.w, bottom: 8.h),
                                            padding: EdgeInsets.only(top: 12.h, bottom: 8.h, right: 8.w, left: 8.w),
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(6),
                                              color: Colors.white,
                                            ),
                                            child: Obx(() => controller.groupValue.value.length !=0?Column(
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.only(right: 8.w, left: 8.w),
                                                  child: Row(
                                                    children: [
                                                      SizedBox(
                                                        width: 32.w,
                                                        height: 32.w,
                                                        child: CircleAvatar(
                                                          backgroundColor:
                                                          Colors.white,
                                                          backgroundImage:
                                                          Image.network(controller.listStudentDiligence.value[index].student!.image ??
                                                                "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                                                            errorBuilder:
                                                                (context, object, stackTrace) {
                                                              return Image.asset(
                                                                "assets/images/img_Noavt.png",
                                                              );
                                                            },
                                                          ).image,
                                                        ),
                                                      ),
                                                      Padding(
                                                          padding:
                                                          EdgeInsets.only(
                                                              right: 8.w)),
                                                      Column(
                                                        crossAxisAlignment:
                                                        CrossAxisAlignment.start,
                                                        children: [
                                                          Text(
                                                            controller.listStudentDiligence.value[index].student!.fullName!,
                                                            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500, fontSize: 14.sp),
                                                          ),
                                                          Padding(
                                                              padding: EdgeInsets.only(top: 4.h)),
                                                          Text(
                                                            controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.listStudentDiligence.value[index].student!.birthday!)),
                                                            style: TextStyle(
                                                                color: const Color.fromRGBO(133, 133, 133, 1),
                                                                fontWeight: FontWeight.w400,
                                                                fontSize: 12.sp),
                                                          )
                                                        ],
                                                      ),
                                                      Expanded(
                                                          child: Container()),
                                                      Visibility(
                                                          visible: controller.getStatusConfirmAttendance(controller.status.value),
                                                          child: controller.isShowButtonEdit.value[index]
                                                              ? InkWell(
                                                            onTap: () {
                                                              controller.isShowButtonEdit.value[index] = false;
                                                              controller.colorItem(index);
                                                              controller.isShowButtonEdit.refresh();
                                                            },
                                                            child:
                                                            Container(
                                                              padding:
                                                              const EdgeInsets.all(8),
                                                              decoration: BoxDecoration(color: const Color.fromRGBO(248, 129, 37, 1),
                                                                  borderRadius:
                                                                  BorderRadius.circular(6.r)),
                                                              child: const Icon(
                                                                Icons.edit,
                                                                color: Colors.white,
                                                                size: 14,
                                                              ),
                                                            ),
                                                          )
                                                              : Row(
                                                            children: [
                                                              InkWell(
                                                                onTap:
                                                                    () {
                                                                  controller.isShowButtonEdit.value[index] = true;
                                                                  controller.colorItem(index);
                                                                  controller.groupValue.value[index] = controller.getStatusDiligence(controller.listStudentDiligence.value[index].statusDiligent);
                                                                  controller.isShowButtonEdit.refresh();
                                                                },
                                                                child:
                                                                Container(
                                                                  padding:
                                                                  const EdgeInsets.all(8),
                                                                  decoration: BoxDecoration(
                                                                      color:
                                                                      Colors.red,
                                                                      borderRadius: BorderRadius.circular(6.r)),
                                                                  child:
                                                                  const Icon(
                                                                    Icons
                                                                        .clear,
                                                                    color:
                                                                    Colors.white,
                                                                    size:
                                                                    14,
                                                                  ),
                                                                ),
                                                              ),
                                                              Padding(padding: EdgeInsets.only(right: 8.w)),
                                                             InkWell(
                                                               onTap: () {
                                                                 controller.updateAttendance(index,controller.groupValue.value[index]);
                                                                 controller.isShowButtonEdit.value[index] = true;
                                                                 controller.colorItem(index);
                                                                 controller.isShowButtonEdit.refresh();
                                                                 AppUtils.shared.showToast("Cập nhật thông tin điểm danh thành công!");
                                                               },
                                                               child:  Container(
                                                                 padding: const EdgeInsets.all(8),
                                                                 decoration: BoxDecoration(
                                                                     color: Colors.green,
                                                                     borderRadius: BorderRadius.circular(6.r)),
                                                                 child:
                                                                 const Icon(Icons.check, color: Colors.white,size: 14,
                                                                 ),
                                                               ),
                                                             )
                                                            ],
                                                          ))
                                                    ],
                                                  ),
                                                ),
                                                Padding(
                                                    padding: EdgeInsets.only(
                                                        top: 16.h)),
                                                Column(
                                                  children: [
                                                    Row(
                                                      mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .start,
                                                      children: [
                                                        Expanded(
                                                          flex: 1,
                                                          child: SizedBox(
                                                            height: 24.h,
                                                            child:
                                                            RadioListTile(
                                                                title: Transform
                                                                    .translate(
                                                                  offset: const Offset(
                                                                      -8,
                                                                      0),
                                                                  child:
                                                                  Text(
                                                                    "Đi học đúng giờ",
                                                                    style: TextStyle(
                                                                        fontSize: 14.sp,
                                                                        fontWeight: FontWeight.w400,
                                                                        color: controller.colorTextRadioListTile.value[index]),
                                                                  ),
                                                                ),
                                                                value:
                                                                1,
                                                                dense:
                                                                true,
                                                                visualDensity:
                                                                const VisualDensity(
                                                                  horizontal:
                                                                  VisualDensity.minimumDensity,
                                                                  vertical:
                                                                  VisualDensity.minimumDensity,
                                                                ),
                                                                contentPadding:
                                                                EdgeInsets
                                                                    .zero,
                                                                activeColor: const Color.fromRGBO(
                                                                    248,
                                                                    129,
                                                                    37,
                                                                    1),
                                                                groupValue:
                                                                controller.groupValue.value[index],
                                                                onChanged:
                                                                    (int?
                                                                value) {
                                                                  controller.onclickAttendance(
                                                                      index,
                                                                      value);
                                                                }),
                                                          ),
                                                        ),
                                                        Expanded(
                                                          flex: 1,
                                                          child: SizedBox(
                                                            height: 24.h,
                                                            child:
                                                            RadioListTile(
                                                                title: Transform
                                                                    .translate(
                                                                  offset: const Offset(
                                                                      -8,
                                                                      0),
                                                                  child:
                                                                  Text(
                                                                    "Đi học muộn",
                                                                    style: TextStyle(
                                                                        fontSize: 14.sp,
                                                                        fontWeight: FontWeight.w400,
                                                                        color: controller.colorTextRadioListTile.value[index]),
                                                                  ),
                                                                ),
                                                                value:
                                                                2,
                                                                dense:
                                                                true,
                                                                visualDensity:
                                                                const VisualDensity(
                                                                  horizontal:
                                                                  VisualDensity.minimumDensity,
                                                                  vertical:
                                                                  VisualDensity.minimumDensity,
                                                                ),
                                                                contentPadding:
                                                                EdgeInsets
                                                                    .zero,
                                                                activeColor: const Color.fromRGBO(
                                                                    248,
                                                                    129,
                                                                    37,
                                                                    1),
                                                                groupValue:
                                                                controller.groupValue.value[
                                                                index],
                                                                onChanged:
                                                                    (int?
                                                                value) {
                                                                  controller.onclickAttendance(
                                                                      index,
                                                                      value);
                                                                }),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Padding(
                                                        padding:
                                                        EdgeInsets.only(
                                                            top: 8.h)),
                                                    Row(
                                                      mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .start,
                                                      children: [
                                                        Expanded(
                                                          flex: 1,
                                                          child: SizedBox(
                                                            height: 24.h,
                                                            child:
                                                            RadioListTile(
                                                                title: Transform
                                                                    .translate(
                                                                  offset: Offset(
                                                                      -8,
                                                                      0),
                                                                  child:
                                                                  Text(
                                                                    "Nghỉ có phép",
                                                                    style: TextStyle(
                                                                        fontSize: 14.sp,
                                                                        fontWeight: FontWeight.w400,
                                                                        color: controller.colorTextRadioListTile.value[index]),
                                                                  ),
                                                                ),
                                                                value:
                                                                3,
                                                                dense:
                                                                true,
                                                                contentPadding:
                                                                EdgeInsets
                                                                    .zero,
                                                                visualDensity:
                                                                const VisualDensity(
                                                                  horizontal:
                                                                  VisualDensity.minimumDensity,
                                                                  vertical:
                                                                  VisualDensity.minimumDensity,
                                                                ),
                                                                activeColor: const Color.fromRGBO(
                                                                    248,
                                                                    129,
                                                                    37,
                                                                    1),
                                                                groupValue:
                                                                controller.groupValue.value[
                                                                index],
                                                                onChanged:
                                                                    (int?
                                                                value) {
                                                                  controller.onclickAttendance(
                                                                      index,
                                                                      value);
                                                                }),
                                                          ),
                                                        ),
                                                        Expanded(
                                                          flex: 1,
                                                          child: SizedBox(
                                                            height: 24.h,
                                                            child:
                                                            RadioListTile(
                                                                title: Transform
                                                                    .translate(
                                                                  offset: Offset(
                                                                      -8,
                                                                      0),
                                                                  child:
                                                                  Text(
                                                                    "Nghỉ không phép",
                                                                    style: TextStyle(
                                                                        fontSize: 14.sp,
                                                                        fontWeight: FontWeight.w400,
                                                                        color: controller.colorTextRadioListTile.value[index]),
                                                                  ),
                                                                ),
                                                                dense:
                                                                true,
                                                                contentPadding:
                                                                EdgeInsets
                                                                    .zero,
                                                                visualDensity:
                                                                const VisualDensity(
                                                                  horizontal:
                                                                  VisualDensity.minimumDensity,
                                                                  vertical:
                                                                  VisualDensity.minimumDensity,
                                                                ),
                                                                activeColor: const Color.fromRGBO(
                                                                    248,
                                                                    129,
                                                                    37,
                                                                    1),
                                                                value:
                                                                4,
                                                                groupValue:
                                                                controller.groupValue.value[
                                                                index],
                                                                onChanged:
                                                                    (int?
                                                                value) {
                                                                  controller.onclickAttendance(
                                                                      index,
                                                                      value);
                                                                }),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ):Container()),
                                          );
                                        })
                                    : Container())
                      ],
                    ))
                : const Center(
                    child: CircularProgressIndicator(),
                  )));
  }
}
