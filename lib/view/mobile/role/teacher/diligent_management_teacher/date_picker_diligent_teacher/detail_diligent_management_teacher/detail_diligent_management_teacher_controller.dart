import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../data/model/common/diligence.dart';
import '../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../../teacher_home_controller.dart';
import 'on_time/on_time_controller.dart';

class DetailDiligenceManagementTeacherController extends GetxController{
  DateTime now = new DateTime.now();
  RxInt indexClick = 0.obs;
  RxList<bool> click = <bool>[].obs;
  var countOnTime = 0.obs;
  var selectedDay = DateTime.now().obs;
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var diligenceClass = DiligenceClass().obs;
  var listStudentOntime = <Diligents>[].obs;
  var listStudentNotOnTime = <Diligents>[].obs;
  var listStudentExcusedAbsence = <Diligents>[].obs;
  var listStudentAbsentWithoutLeave = <Diligents>[].obs;
  var listStudent = <Diligents>[].obs;
  var student = StudentDiligent().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );
  var listStatus = <String>[].obs;


  @override
  void onInit() {
    super.onInit();
    listStatus.value = ["Đi học đúng giờ","Đi học muộn","Nghỉ có phép","Nghỉ không phép"];
    var tmpSelectedDay = Get.arguments;
    if(tmpSelectedDay!=null){
      selectedDay.value = tmpSelectedDay;
    }
    listStudent.value.clear();
    listStudentOntime.value.clear();
    listStudentNotOnTime.value.clear();
    listStudentExcusedAbsence.value.clear();
    listStudentAbsentWithoutLeave.value.clear();
    getListStudent();

    listStudent.refresh();
    listStudentOntime.refresh();
    listStudentNotOnTime.refresh();
    listStudentExcusedAbsence.refresh();
    listStudentAbsentWithoutLeave.refresh();

  }


  getListStudent(){
    var classId =Get.find<TeacherHomeController>().currentClass.value.classId;
    var fromdate = DateTime(selectedDay.value.year, selectedDay.value.month, selectedDay.value.day,00,00).millisecondsSinceEpoch;
    var todate = DateTime(selectedDay.value.year, selectedDay.value.month, selectedDay.value.day,23,59).millisecondsSinceEpoch;

    _diligenceRepo.getListStudentDiligence(classId, fromdate, todate, "").then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value = value.object!;
        listStudent.value = diligenceClass.value.studentDiligent!;
        listStudentOntime.value = listStudent.value.where((element) => element.statusDiligent == "ON_TIME").toList();
        listStudentNotOnTime.value = listStudent.value.where((element) => element.statusDiligent == "NOT_ON_TIME").toList();
        listStudentExcusedAbsence.value = listStudent.value.where((element) => element.statusDiligent == "EXCUSED_ABSENCE").toList();
        listStudentAbsentWithoutLeave.value = listStudent.value.where((element) => element.statusDiligent == "ABSENT_WITHOUT_LEAVE").toList();
      }
    });
  }

  getListStatus(index){
    switch(index){
      case 0:
        return listStudentOntime.value.length;
      case 1:
        return listStudentNotOnTime.value.length;
      case 2:
        return listStudentExcusedAbsence.value.length;
      case 3:
        return listStudentAbsentWithoutLeave.value.length;
    }
  }



  onPageViewChange(int page) {
    indexClick.value = page;
    if(page != 0) indexClick.value!-1;
    else indexClick.value = 0;

  }


  showColor(index) {
    click.value = [];
    for (int i = 0; i < 4; i++) {
      click.value.add(false);
    }
    click.value[index] = true;
  }


  setWeekDay(weekDay){
    switch(weekDay){
      case 8:
        return "Chủ Nhật";
      default:
        return "Thứ ${weekDay}";
    }
  }
}