import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'absent_without_leave_controller.dart';


class AbsentWithoutLeavePage extends GetView<AbsentWithoutLeaveController>{
  final controller = Get.put(AbsentWithoutLeaveController());
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(child: Obx(() => Container(
      margin: EdgeInsets.only(left: 16.w,right: 16.w),
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(6),color: Colors.white,),
      child:  ListView.builder(
          itemCount: controller.listStudentAbsentWithoutLeave.value.length,
          shrinkWrap: true,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: (){

              },
              child:  Column(
                children: [
                  Row(
                    children: [
                      Container(
                        margin:EdgeInsets.only(left: 16.w,right: 8.w),
                        child:Text('${index+1}', style: const TextStyle(color: Color.fromRGBO(26, 26, 26, 1),fontSize: 14),),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 8.h,bottom: 4.h),
                        child: Row(
                          children: [
                            SizedBox(
                              width: 48.w,
                              height: 48.w,
                              child: CircleAvatar(
                                backgroundColor: Colors.white,
                                backgroundImage: Image.network(
                                  controller.listStudentAbsentWithoutLeave.value[index].student!.image ??
                                      "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                                  errorBuilder:
                                      (context, object, stackTrace) {
                                    return Image.asset(
                                      "assets/images/img_Noavt.png",
                                    );
                                  },
                                ).image,
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(right: 4.w)),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                RichText(text:  TextSpan(
                                    children: [
                                      const TextSpan(text: 'Học Sinh: ',style:  TextStyle(color: Color.fromRGBO(248, 129, 37, 1),fontSize: 14)),
                                      TextSpan(text: '${controller.listStudentAbsentWithoutLeave.value[index].student?.fullName}' ,style:  TextStyle(color: Color.fromRGBO(26, 26, 26, 1),fontSize: 14)),
                                    ]
                                )),
                                Padding(padding: EdgeInsets.only(top: 4.h)),
                                Text(controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.listStudentAbsentWithoutLeave.value[index].student!.birthday!)),style: TextStyle(color: Color.fromRGBO(133, 133, 133, 1), fontSize: 14),)

                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  const Divider()
                ],
              ),
            );
          }),
    )));
  }

}