import 'package:get/get_state_manager/src/simple/get_view.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:task_manager/view/mobile/role/teacher/diligent_management_teacher/diligent_management_teacher_controller.dart';
import '../../../../../../../routes/app_pages.dart';
import '../../attendance_teacher/attendance_teacher_controller.dart';
import '../../diligent_management_teacher_page.dart';
import '../../list_leave_application_teacher/list_leave_application_teacher_controller.dart';
import '../../manager_leave_application_teacher/approved_leave_application_teacher/approved_teacher_leave_application_controller.dart';
import '../../manager_leave_application_teacher/cancel_leave_application_teacher/cancel_leave_application_teacher_controller.dart';
import '../../manager_leave_application_teacher/manager_leave_application_teacher_controller.dart';
import '../../manager_leave_application_teacher/pending_teacher/pending_teacher_controller.dart';
import 'absent_without_leave/absent_without_leave_controller.dart';
import 'absent_without_leave/absent_without_leave_page.dart';
import 'detail_diligent_management_teacher_controller.dart';

import 'excused_absence/excused_absence_controller.dart';
import 'excused_absence/excused_absence_page.dart';
import 'not_on_time/not_on_time_controller.dart';
import 'not_on_time/not_on_time_page.dart';
import 'on_time/on_time_controller.dart';
import 'on_time/on_time_page.dart';

class DetailDiligentManagementTeacherPage extends GetView<DetailDiligenceManagementTeacherController> {
  final controller = Get.put(DetailDiligenceManagementTeacherController());
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
        elevation: 0,
        actions: [
          InkWell(
            onTap: () {
              Get.offAllNamed(Routes.home);
              Get.delete<DiligenceManagementTeacherController>(force: true);
              Get.delete<ManagerLeaveApplicationTeacherController>(force: true);
              Get.delete<PendingTeacherController>(force: true);
              Get.delete<CancelLeaveApplicationTeacherController>(force: true);
              Get.delete<ApprovedLeaveApplicationTeacherController>(force: true);
              Get.delete<ListLeaveApplicationTeacherController>(force: true);
              Get.delete<AttendanceTeacherController>(force: true);
            },
            child: const Icon(
              Icons.home,
              color: Colors.white,
            ),
          ),
          Padding(padding: EdgeInsets.only(right: 16.w))
        ],
        title: const Text(
          'Chi Tiết Chuyên Cần',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: Obx(() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(top: 8.h, left: 8.w),
            child: Text(
              "${controller.setWeekDay(controller.selectedDay.value.weekday + 1)}, ${controller.selectedDay.value.day}/${controller.selectedDay.value.month}/${controller.selectedDay.value.year}",
              style:
              TextStyle(color: Colors.black, fontSize: 14.sp),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 16.w),
            height: 40,
            child: ListView.builder(
                itemCount: controller.listStatus.value.length,
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context,index){
                  return Obx(() => TextButton(
                      onPressed: () {
                        controller.indexClick.value = index;
                        controller.showColor(controller.indexClick.value);
                        controller.pageController.animateToPage(index,
                            duration: const Duration(seconds: 1),
                            curve: Curves.easeOutBack);
                      },
                      child: Text(
                        "${controller.listStatus.value[index]} (${controller.getListStatus(index) ?? 0})",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 12.sp,
                            color: controller.indexClick.value == index
                                ? const Color.fromRGBO(248, 129, 37, 1)
                                : const Color.fromRGBO(177, 177, 177, 1)),
                      )));
                }),
          ),
          Expanded(
              child: PageView(
                onPageChanged: (value) {
                  controller.onPageViewChange(value);
                },
                controller: controller.pageController,
                physics: const ScrollPhysics(),
                children: [OnTimePage(), NotOnTimePage(), ExcusedAbsencePage(),AbsentWithoutLeavePage()],
              )),
        ],
      )),
    ));
  }
}
