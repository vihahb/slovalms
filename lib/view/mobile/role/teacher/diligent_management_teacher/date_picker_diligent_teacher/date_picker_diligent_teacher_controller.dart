import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:table_calendar/table_calendar.dart';
import '../../../../../../data/base_service/api_response.dart';
import '../../../../../../data/model/common/diligence.dart';
import '../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../../../../../routes/app_pages.dart';
import '../../teacher_home_controller.dart';
import 'detail_diligent_management_teacher/absent_without_leave/absent_without_leave_controller.dart';
import 'detail_diligent_management_teacher/detail_diligent_management_teacher_controller.dart';
import 'detail_diligent_management_teacher/detail_diligent_management_teacher_page.dart';
import 'detail_diligent_management_teacher/excused_absence/excused_absence_controller.dart';
import 'detail_diligent_management_teacher/not_on_time/not_on_time_controller.dart';
import 'detail_diligent_management_teacher/on_time/on_time_controller.dart';


class DatePickerDiligentTeacherController extends GetxController{
  var format = CalendarFormat.month.obs;
  var focusedDay = DateTime.now().obs;
  var selectedDay = DateTime.now().obs;
  var cupertinoDatePicker = DateTime.now().obs;
  var timelineController = CalendarController().obs;
  var weekNumber = 1.obs;
  var heightOffset = (Get.width/5).obs;
  var monthController = DateRangePickerController().obs;



  @override
  void onInit() {

    super.onInit();
  }

  setWeekDay(weekDay){
    switch(weekDay){
      case 8:
        return "Chủ Nhật";
      default:
        return "Thứ ${weekDay}";
    }
  }





  selectDate(){
    Get.toNamed(Routes.detailDiligentManagementTeacherPage,arguments: selectedDay.value);
  }



}