import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:task_manager/commom/app_cache.dart';
import 'package:task_manager/commom/utils/color_utils.dart';
import 'package:task_manager/commom/widget/appointment_builder.dart';
import 'package:task_manager/commom/widget/hexColor.dart';
import 'package:task_manager/view/mobile/home/home_controller.dart';
import 'package:task_manager/view/mobile/schedule/schedule_controller.dart';
import '../../../../../../routes/app_pages.dart';
import 'date_picker_diligent_teacher_controller.dart';


class DatePickerDiligentTeacherPage
    extends GetView<DatePickerDiligentTeacherController> {
  var controller = Get.put(DatePickerDiligentTeacherController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Chi Tiết"),
        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
        elevation: 0,
        actions: [
          SizedBox(
            height: 20.w,
            width: 20.w,
            child: InkWell(
              onTap: () {
                showModalBottomSheet(
                    context: context,
                    builder: (context) {
                      return Wrap(
                        children: [
                          Row(
                            children: [
                              TextButton(
                                  onPressed: () {
                                    Get.back();
                                  },
                                  style: ElevatedButton.styleFrom(
                                      backgroundColor: Colors.white),
                                  child: const Text(
                                    "Hủy",
                                    style: TextStyle(
                                        color:
                                            Color.fromRGBO(123, 123, 123, 1)),
                                  )),
                              Expanded(child: Container()),
                              TextButton(
                                  onPressed: () {
                                    controller.monthController.value.selectedDate = controller.cupertinoDatePicker.value;
                                    controller.monthController.value.displayDate = controller.cupertinoDatePicker.value;
                                    controller.selectedDay.value = controller.cupertinoDatePicker.value;
                                    Get.back();
                                  },
                                  style: ElevatedButton.styleFrom(
                                      backgroundColor: Colors.white),
                                  child: const Text(
                                    "Xong",
                                    style: TextStyle(
                                        color: Color.fromRGBO(
                                            248, 129, 37, 1)),
                                  )),
                            ],
                          ),
                          SizedBox(
                            height: 300, // Just as an example
                            child: CupertinoDatePicker(
                              mode: CupertinoDatePickerMode.date,
                              initialDateTime: controller.selectedDay.value,
                              onDateTimeChanged: (DateTime dateTime) {
                                controller.cupertinoDatePicker.value = dateTime;
                              },
                            ),
                          ),
                        ],
                      );
                    });
              },
              child: Image.asset("assets/images/icon_date_picker_detail.png"),
            ),
          ),
          Padding(padding: EdgeInsets.only(right: 16.w)),
        ],
      ),
      body: Obx(() => RefreshIndicator(
          color: const Color.fromRGBO(248, 129, 37, 1),
          child: Column(
            children: [
              Expanded(
                  child: SingleChildScrollView(
                physics: const AlwaysScrollableScrollPhysics(),
                child: Container(
                  margin: const EdgeInsets.only(top: 16, right: 16, left: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 8.h, left: 8.w),
                        child: Text(
                          "${controller.setWeekDay(controller.selectedDay.value.weekday + 1)}, ${controller.selectedDay.value.day}/${controller.selectedDay.value.month}/${controller.selectedDay.value.year}",
                          style:
                              TextStyle(color: Colors.black, fontSize: 14.sp),
                        ),
                      ),
                      dateTimeSelectV2(),
                    ],
                  ),
                ),
              )),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: const EdgeInsets.all(16),
                color: Colors.white,
                child: SizedBox(
                  height: 46.h,
                  child: ElevatedButton(
                    onPressed: () {
                     controller.selectDate();
                    },
                    style: ElevatedButton.styleFrom(
                        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6))),
                    child: Text("Xem chi tiết",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16.sp,
                            fontWeight: FontWeight.w400)),
                  ),
                ),
              )
            ],
          ),
          onRefresh: () async {})),
    );
  }

  Card dateTimeSelectV2() {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(padding: EdgeInsets.only(top: 20)),
          SizedBox(
            height: (Get.width / 2),
            child: SfDateRangePicker(
              viewSpacing: 10,
              todayHighlightColor: const Color.fromRGBO(249, 154, 81, 1),
              controller: controller.monthController.value,
              view: DateRangePickerView.month,
              enablePastDates: true,
              selectionMode: DateRangePickerSelectionMode.single,
              showNavigationArrow: true,
              selectionRadius: 15,
              monthFormat: "MMM",
              headerHeight: 0,
              selectionColor: const Color.fromRGBO(249, 154, 81, 1),
              selectionShape: DateRangePickerSelectionShape.circle,
              selectionTextStyle: const TextStyle(
                  color: ColorUtils.COLOR_WHITE, fontWeight: FontWeight.w700),
              monthCellStyle: const DateRangePickerMonthCellStyle(
                todayTextStyle: TextStyle(
                    color: Color.fromRGBO(249, 154, 81, 1),
                    fontWeight: FontWeight.w700),
              ),
              headerStyle: DateRangePickerHeaderStyle(
                  textAlign: TextAlign.left,
                  textStyle: TextStyle(
                    fontStyle: FontStyle.normal,
                    fontSize: 12.sp,
                    color: Colors.black87,
                  )),

              monthViewSettings: DateRangePickerMonthViewSettings(
                  numberOfWeeksInView: 4,
                  firstDayOfWeek: 1,
                  showTrailingAndLeadingDates: true,
                  viewHeaderHeight: 16.h,
                  dayFormat: 'EEE',
                  viewHeaderStyle: DateRangePickerViewHeaderStyle(
                      textStyle: TextStyle(
                          color: HexColor("#B1B1B1"),
                          fontWeight: FontWeight.w600,
                          fontSize: 10.sp))),
              onViewChanged: (DateRangePickerViewChangedArgs args) {
                Future.delayed(const Duration(milliseconds: 400), () {
                  var visibleDates = args.visibleDateRange;
                  controller.focusedDay.value =
                      visibleDates.startDate ?? DateTime.now();
                });
              },
              onSelectionChanged: (DateRangePickerSelectionChangedArgs args) {
                var dateTime = args.value;
                if (args.value is PickerDateRange) {
                  final DateTime rangeStartDate = args.value.startDate;
                  final DateTime rangeEndDate = args.value.endDate;

                  controller.selectedDay.value = rangeStartDate;
                  controller.timelineController.value.displayDate =
                      rangeStartDate;
                } else if (args.value is DateTime) {
                  final DateTime selectedDate = args.value;
                  controller.timelineController.value.displayDate =
                      selectedDate;

                  controller.selectedDay.value = selectedDate;
                } else if (args.value is List<DateTime>) {
                  final List<DateTime> selectedDates = args.value;
                  controller.timelineController.value.displayDate =
                  selectedDates[0];
                  controller.selectedDay.value = selectedDates[0];
                } else {
                  final List<PickerDateRange> selectedRanges = args.value;
                }
              },
            ),
          )
        ],
      ),
    );
  }
}
