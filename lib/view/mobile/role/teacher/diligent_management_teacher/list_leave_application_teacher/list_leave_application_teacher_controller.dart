import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../data/model/common/leaving_application.dart';
import '../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../../../../../data/model/common/contacts.dart';
import '../../teacher_home_controller.dart';
import '../diligent_management_teacher_controller.dart';

class ListLeaveApplicationTeacherController extends GetxController{
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var leavingApplication = LeavingApplication().obs;
  var itemsLeavingApplications = <ItemsLeavingApplication>[].obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var selectedDay = DateTime.now().obs;
  var isHomeRoomTeacher = false.obs;
  var parents = <String>[].obs;
  @override
  void onInit() {
    selectedDay.value = DateTime(
        Get.find<DiligenceManagementTeacherController>()
            .selectedDate
            .value
            .year,
        Get.find<DiligenceManagementTeacherController>()
            .selectedDate
            .value
            .month,
        Get.find<DiligenceManagementTeacherController>()
            .selectedDate
            .value
            .day);
    getListLeavingApplicationTeacher();
    HomeRoomTeacher();
    super.onInit();

  }

  getListLeavingApplicationTeacher() async{
    itemsLeavingApplications.value.clear();
    var classId =Get.find<TeacherHomeController>().currentClass.value.classId;
    var fromdate = DateTime(selectedDay.value.year,selectedDay.value.month,selectedDay.value.day,00,00).millisecondsSinceEpoch;
    var todate = DateTime(selectedDay.value.year,selectedDay.value.month,selectedDay.value.day,23,59).millisecondsSinceEpoch;
    _diligenceRepo.getListLeavingApplicationTeacher(classId,fromdate,todate,"","","").then((value) {
      if (value.state == Status.SUCCESS) {
        itemsLeavingApplications.value = value.object!;
      }
    });
    itemsLeavingApplications.refresh();
  }


  getStatus(status){
    switch(status){
      case "CANCEL":
        return "Đã hủy";
      case "PENDING":
        return "Chờ duyệt";
      case "REFUSE":
        return "Từ chối";
      case "APPROVE":
        return "Đã duyệt";
    }
  }


  getColorTextStatus(status){
    switch(status){
      case "PENDING":
        return Color.fromRGBO(253, 185, 36, 1);
      case "APPROVE":
        return Color.fromRGBO(77, 197, 145, 1);
      case "REFUSE":
        return Color.fromRGBO(255, 69, 89, 1);
      case "CANCEL":
        return Color.fromRGBO(255, 69, 89, 1);
    }
  }

  getColorBackgroundStatus(status){
    switch(status){
      case "PENDING":
        return Color.fromRGBO(255, 243, 218, 1);
      case "APPROVE":
        return Color.fromRGBO(192, 242, 220, 1);
      case "REFUSE":
        return Color.fromRGBO(252, 211, 215, 1);
      case "CANCEL":
        return Color.fromRGBO(252, 211, 215, 1);
    }
  }




  HomeRoomTeacher(){
    if(Get.find<TeacherHomeController>().userProfile.value.classesOfHomeroomTeacher?.contains(Get.find<TeacherHomeController>().currentClass.value.classId) == true){
      isHomeRoomTeacher.value = true;
    }else{
      isHomeRoomTeacher.value = false;
    }
  }




}