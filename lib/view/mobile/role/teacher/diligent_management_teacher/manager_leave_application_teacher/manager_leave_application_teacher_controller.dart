import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:task_manager/view/mobile/role/teacher/diligent_management_teacher/manager_leave_application_teacher/pending_teacher/pending_teacher_controller.dart';

import '../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../data/model/common/diligence.dart';
import '../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../../../../../commom/utils/app_utils.dart';
import '../../../../../../data/model/common/contacts.dart';
import '../../../../../../data/model/common/leaving_application.dart';
import '../../../../../../data/model/common/list_student.dart';
import '../../../../../../data/repository/list_student/list_student_repo.dart';
import '../../teacher_home_controller.dart';
import '../diligent_management_teacher_controller.dart';
import 'approved_leave_application_teacher/approved_teacher_leave_application_controller.dart';
import 'cancel_leave_application_teacher/cancel_leave_application_teacher_controller.dart';

class ManagerLeaveApplicationTeacherController extends GetxController{
  DateTime now = new DateTime.now();
  var indexClick = 0;
  RxList<bool> click = <bool>[].obs;
  var countOnTime = 0.obs;

  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var diligenceClass = DiligenceClass().obs;
  var listStudent = <Diligents>[].obs;
  var student = StudentDiligent().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');

  var itemsLeavingApplications = <ItemsLeavingApplication>[].obs;
  var listPending = <ItemsLeavingApplication>[].obs;
  var listApprove = <ItemsLeavingApplication>[].obs;
  var listCancel = <ItemsLeavingApplication>[].obs;
  var listRefuse = <ItemsLeavingApplication>[].obs;
  var selectedDay = DateTime.now().obs;
  var controllerdateStart = TextEditingController().obs;
  var controllerdateEnd = TextEditingController().obs;
  var typeTimeLeaveApplication = "".obs;
  final ListStudentRepo _listStudent = ListStudentRepo();
  RxList<DetailItem> detailItem = <DetailItem>[].obs;
  var listStudentByClass = <Student>[].obs;
  var detailStudent = Student().obs;
  var selectStudent = "".obs;
  var studentId = "".obs;


  PageController pageController = PageController(
    initialPage: 0,
    keepPage: false,
  );
  var listStatus = <String>[].obs;



  void setUpdate(int index){
    indexClick= index;
    update();
  }


  @override
  void onInit() {
    super.onInit();
    selectStudent.value = "Tất cả";
    studentId.value = "";
    typeTimeLeaveApplication.value = "Thời gian tạo";
    listStatus.value= ["Chờ duyệt","Đã Duyệt","Đã Hủy"];
    controllerdateStart.value.text =   outputDateFormat.format(DateTime(findFirstDateOfTheWeek(selectedDay.value).year, findFirstDateOfTheWeek(selectedDay.value).month,findFirstDateOfTheWeek(selectedDay.value).day));
    controllerdateEnd.value.text = outputDateFormat.format(DateTime(findLastDateOfTheWeek(selectedDay.value).year, findLastDateOfTheWeek(selectedDay.value).month,findLastDateOfTheWeek(selectedDay.value).day));
    getListLeavingApplicationTeacher();
    getDetailListStudent();
  }

  getTypeTimeLeaveApplication(type){
    switch(type){
      case "Thời gian tạo":
        return "CREATEAT";
      case "Thời gian nghỉ":
        return "TIMELEAVE";
    }
  }

  getDetailListStudent() {
    listStudentByClass.value = [];
    listStudentByClass.value.add(Student(id: "",fullName: "Tất cả"));
    var classId = Get.find<TeacherHomeController>().currentClass.value.classId;
    _listStudent.listStudentByClass(classId).then((value) {
      if (value.state == Status.SUCCESS) {
        detailItem.value = value.object!;
        for(int i =0;i< detailItem.value.length ;i++){
          listStudentByClass.value.add(Student(id: "",fullName: ""));
          listStudentByClass.value[i+1].fullName = detailItem.value[i].fullName!;
          listStudentByClass.value[i+1].id = detailItem.value[i].id!;
        }
      }
    });
    listStudentByClass.refresh();
  }


  fillLeaveApplication(){
    if(controllerdateStart.value.text == ""){
      AppUtils().showToast("Vui lòng chọn ngày bắt đầu");
    }else{
      if(controllerdateEnd.value.text == ""){
        AppUtils().showToast("Vui lòng chọn ngày kết thúc");
      }else{
        if(typeTimeLeaveApplication.value == ""){
          AppUtils().showToast("Vui lòng chọn kiểu thời gian");
        }else{
          if(selectStudent.value == ""){
            AppUtils().showToast("Vui lòng chọn học sinh");
          }else{
            getListLeavingApplicationTeacher();
            Get.find<PendingTeacherController>().onInit();
            Get.find<CancelLeaveApplicationTeacherController>().onInit();
            Get.find<ApprovedLeaveApplicationTeacherController>().onInit();
          }
        }
      }
    }

  }


  getListLeavingApplicationTeacher() async{
    itemsLeavingApplications.value.clear();
    listPending.value.clear();
    listApprove.value.clear();
    listCancel.value.clear();
    listRefuse.value.clear();
    listApprove.value.clear();
    var classId =Get.find<TeacherHomeController>().currentClass.value.classId;
    var fromdate = DateTime(int.parse(controllerdateStart.value.text.substring(6,10)), int.parse(controllerdateStart.value.text.substring(3,5)),int.parse(controllerdateStart.value.text.substring(0,2),),00,00).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(controllerdateEnd.value.text.substring(6,10)), int.parse(controllerdateEnd.value.text.substring(3,5)),int.parse(controllerdateEnd.value.text.substring(0,2)),23,59).millisecondsSinceEpoch;
    _diligenceRepo.getListLeavingApplicationTeacher(classId,fromdate,todate,"",getTypeTimeLeaveApplication(typeTimeLeaveApplication.value),studentId.value).then((value) {
      if (value.state == Status.SUCCESS) {
        itemsLeavingApplications.value = value.object!;
        listPending.value = itemsLeavingApplications.value.where((element) => element.status == "PENDING").toList();
        listApprove.value = itemsLeavingApplications.value.where((element) => element.status == "APPROVE").toList();
        listCancel.value = itemsLeavingApplications.value.where((element) => element.status == "CANCEL").toList();
        listRefuse.value = itemsLeavingApplications.value.where((element) => element.status == "REFUSE").toList();
        listApprove.value.addAll(listRefuse.value);
      }
    });
    itemsLeavingApplications.refresh();
    listPending.refresh();
    listApprove.refresh();
    listCancel.refresh();
    listRefuse.refresh();
    listApprove.refresh();
    update();
  }


  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }

  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime
        .add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }

  getCountLeaveApplication(index){
    switch(index){
      case 0:
        return listPending.value.length;
      case 1:
        return listApprove.value.length;
      case 2:
        return listCancel.value.length;

    }
  }


  comePending() {
    pageController?.animateTo(0, duration: Duration(seconds: 1), curve: Curves.bounceIn);
  }


  comeApproved() {
    pageController?.animateTo(1, duration: Duration(seconds: 1), curve: Curves.bounceIn);
  }

  comeCancel() {
    pageController?.animateTo(2, duration: Duration(seconds: 1), curve: Curves.bounceIn);
  }

  onRefresh(){
    selectStudent.value = "Tất cả";
    studentId.value = "";
    controllerdateStart.value.text =   outputDateFormat.format(DateTime(findFirstDateOfTheWeek(selectedDay.value).year, findFirstDateOfTheWeek(selectedDay.value).month,findFirstDateOfTheWeek(selectedDay.value).day,00,00));
    controllerdateEnd.value.text = outputDateFormat.format(DateTime(findLastDateOfTheWeek(selectedDay.value).year, findLastDateOfTheWeek(selectedDay.value).month,findLastDateOfTheWeek(selectedDay.value).day,23,59));
    getListLeavingApplicationTeacher();
    getDetailListStudent();
    Get.find<PendingTeacherController>().onInit();
    Get.find<CancelLeaveApplicationTeacherController>().onInit();
    Get.find<ApprovedLeaveApplicationTeacherController>().onInit();
  }






  onPageViewChange(int page) {
    indexClick = page;
    if(page != 0) {
      indexClick!-1;
    } else {
      indexClick = 0;
    }

  }


  showColor(index) {
    click.value = [];
    for (int i = 0; i < 3; i++) {
      click.value.add(false);
    }
    click.value[index] = true;
  }
}



