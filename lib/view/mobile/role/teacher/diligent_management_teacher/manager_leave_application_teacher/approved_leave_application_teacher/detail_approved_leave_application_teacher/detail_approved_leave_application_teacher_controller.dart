import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/common/contacts.dart';
import '../../../../../../../../data/model/common/detail_notify_leaving_application.dart';
import '../../../../../../../../data/model/common/leaving_application.dart';
import '../../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../../../../../../../data/repository/notification_repo/notification_repo.dart';




class DetailApprovedTeacherController extends GetxController{
  var itemsLeavingApplication = ItemsLeavingApplication().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy hh:mm');

  var transId = "".obs;
  final NotificationRepo _notificationRepo = NotificationRepo();
  var deatilNotifyLeavingApplication = DetailNotifyLeavingApplication().obs;

  @override
  void onInit() {
    var tmpTransId = Get.arguments;
    if (tmpTransId != null) {
      transId.value = tmpTransId;
      getDetailNotify();
    }
    super.onInit();
  }


  getDetailNotify() async{
    _notificationRepo.getDetailNotifyLeavingApplication(transId.value).then((value) {
      if (value.state == Status.SUCCESS) {
        deatilNotifyLeavingApplication.value = value.object!;
      }
    });
  }


  getImageStatus(status){
    switch(status){
      case "PENDING":
        return "";
      case "APPROVE":
        return "assets/images/image_approved.png";
      case "REFUSE":
        return "assets/images/image_rejected.png";
    }
  }

  getColorTextStatus(status) {
    switch (status) {
      case "PENDING":
        return Color.fromRGBO(253, 185, 36, 1);
      case "APPROVE":
        return Color.fromRGBO(77, 197, 145, 1);
      case "REFUSE":
        return Color.fromRGBO(255, 69, 89, 1);
      case "CANCEL":
        return Color.fromRGBO(255, 69, 89, 1);
    }
  }

  getColorBackgroundStatus(status) {
    switch (status) {
      case "PENDING":
        return Color.fromRGBO(255, 243, 218, 1);
      case "APPROVE":
        return Color.fromRGBO(192, 242, 220, 1);
      case "REFUSE":
        return Color.fromRGBO(252, 211, 215, 1);
      case "CANCEL":
        return Color.fromRGBO(252, 211, 215, 1);
    }
  }

  getStatus(status) {
    switch (status) {
      case "CANCEL":
        return "Đã hủy";
      case "PENDING":
        return "Chờ duyệt";
      case "REFUSE":
        return "Từ chối";
      case "APPROVE":
        return "Đã duyệt";
    }
  }

}