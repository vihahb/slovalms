import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:intl/intl.dart';

import '../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../data/model/common/detail_notify_leaving_application.dart';
import '../../../../../../../data/model/common/leaving_application.dart';
import '../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../../../../../../data/repository/notification_repo/notification_repo.dart';
import '../../../teacher_home_controller.dart';
import '../../diligent_management_teacher_controller.dart';
import '../manager_leave_application_teacher_controller.dart';

class CancelLeaveApplicationTeacherController extends GetxController{

  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var leavingApplication = LeavingApplication().obs;
  var itemsLeavingApplications = <ItemsLeavingApplication>[].obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var selectedDay = DateTime.now().obs;
  var controllerdateStart = TextEditingController().obs;
  var controllerdateEnd = TextEditingController().obs;
  var studentId = "".obs;
  var typeTimeLeaveApplication = "Thời gian nghỉ".obs;
  @override
  void onInit() {
    if(Get.isRegistered<DiligenceManagementTeacherController>()){
      selectedDay.value = DateTime(
          Get.find<DiligenceManagementTeacherController>()
              .selectedDate
              .value
              .year,
          Get.find<DiligenceManagementTeacherController>()
              .selectedDate
              .value
              .month,
          Get.find<DiligenceManagementTeacherController>()
              .selectedDate
              .value
              .day);
    }
    controllerdateStart.value.text = Get.find<ManagerLeaveApplicationTeacherController>().controllerdateStart.value.text;
    controllerdateEnd.value.text = Get.find<ManagerLeaveApplicationTeacherController>().controllerdateEnd.value.text;
    studentId.value = Get.find<ManagerLeaveApplicationTeacherController>().studentId.value;
    typeTimeLeaveApplication.value = Get.find<ManagerLeaveApplicationTeacherController>().typeTimeLeaveApplication.value;
    getListLeavingApplicationCancelTeacher();
    super.onInit();
  }

  getListLeavingApplicationCancelTeacher() async{
    itemsLeavingApplications.value.clear();
    var fromdate = DateTime(int.parse(controllerdateStart.value.text.substring(6,10)), int.parse(controllerdateStart.value.text.substring(3,5)),int.parse(controllerdateStart.value.text.substring(0,2),),00,00).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(controllerdateEnd.value.text.substring(6,10)), int.parse(controllerdateEnd.value.text.substring(3,5)),int.parse(controllerdateEnd.value.text.substring(0,2)),23,59).millisecondsSinceEpoch;
    var classId =Get.find<TeacherHomeController>().currentClass.value.classId;
    _diligenceRepo.getListLeavingApplicationTeacher(classId,fromdate,todate,"CANCEL",getTypeTimeLeaveApplication(typeTimeLeaveApplication.value),studentId.value).then((value) {
      if (value.state == Status.SUCCESS) {
        itemsLeavingApplications.value = value.object!;
      }
    });
    itemsLeavingApplications.refresh();
  }
  getTypeTimeLeaveApplication(type){
    switch(type){
      case "Thời gian tạo":
        return "CREATEAT";
      case "Thời gian nghỉ":
        return "TIMELEAVE";
    }
  }


  getImageStatus(status){
    switch(status){
      case "PENDING":
        return "";
      case "APPROVE":
        return "assets/images/image_approved.png";
      case "REFUSE":
        return "assets/images/image_rejected.png";
    }
  }


  getColorTextStatus(status){
    switch(status){
      case "PENDING":
        return Color.fromRGBO(253, 185, 36, 1);
      case "APPROVE":
        return Color.fromRGBO(77, 197, 145, 1);
      case "REFUSE":
        return Color.fromRGBO(255, 69, 89, 1);
      case "CANCEL":
        return Color.fromRGBO(255, 69, 89, 1);
    }
  }

  getColorBackgroundStatus(status){
    switch(status){
      case "PENDING":
        return Color.fromRGBO(255, 243, 218, 1);
      case "APPROVE":
        return Color.fromRGBO(192, 242, 220, 1);
      case "REFUSE":
        return Color.fromRGBO(252, 211, 215, 1);
      case "CANCEL":
        return Color.fromRGBO(252, 211, 215, 1);
    }
  }


  getStatus(status){
    switch(status){
      case "CANCEL":
        return "Đã hủy";
      case "PENDING":
        return "Chờ duyệt";
      case "REFUSE":
        return "Từ chối";
      case "APPROVE":
        return "Đã duyệt";
    }
  }


}