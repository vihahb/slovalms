import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../../../../../commom/utils/global.dart';
import '../../../../../../../../commom/widget/text_field_custom.dart';
import '../../../../../../../routes/app_pages.dart';
import 'cancel_leave_application_teacher_controller.dart';



class CancelLeaveApplicationTeacherPage extends GetWidget<CancelLeaveApplicationTeacherController> {
  final controller = Get.put(CancelLeaveApplicationTeacherController(), permanent: true);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
      child: Obx(() => ListView.builder(
          itemCount: controller.itemsLeavingApplications.value.length,
          shrinkWrap: true,
          physics: const ScrollPhysics(),
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () {
              },
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 16.w, vertical: 4.h),
                padding:
                EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.h),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(6)),
                child: Column(
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          width: 32.w,
                          height: 32.w,
                          child: CircleAvatar(
                            backgroundColor: Colors.white,
                            backgroundImage: Image.network(
                              controller.itemsLeavingApplications.value[index].student!.image ??
                                  "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                              errorBuilder:
                                  (context, object, stackTrace) {
                                return Image.asset(
                                  "assets/images/img_Noavt.png",
                                );
                              },
                            ).image,
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(right: 8.w)),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "${controller.itemsLeavingApplications.value[index].student!.fullName}",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14.sp),
                            ),
                          ],
                        ),
                        Expanded(child: Container()),
                        InkWell(
                          onTap: () {
                            Get.toNamed(Routes.detailCancelLeaveApplicationTeacher,arguments: controller.itemsLeavingApplications.value[index].id);
                          },
                          child: Text(
                            "Chi Tiết",
                            style: TextStyle(
                                color: const Color.fromRGBO(26, 59, 112, 1),
                                fontWeight: FontWeight.w500,
                                fontSize: 14.sp),
                          ),
                        )
                      ],
                    ),
                    const Divider(),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: 80.w,
                          child: Text(
                            "Thời gian:",
                            style: TextStyle(
                                fontSize: 14.sp,
                                color: const Color.fromRGBO(133, 133, 133, 1),
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                        Flexible(
                            child: RichText(
                                text: TextSpan(children: [
                                  TextSpan(
                                    text: controller.outputDateFormat.format(
                                        DateTime.fromMillisecondsSinceEpoch(
                                            controller
                                                .itemsLeavingApplications
                                                .value[index]
                                                .fromDate!)),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14.sp),
                                  ),
                                  TextSpan(
                                    text: " đến ",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14.sp),
                                  ),
                                  TextSpan(
                                    text: controller.outputDateFormat.format(
                                        DateTime.fromMillisecondsSinceEpoch(
                                            controller
                                                .itemsLeavingApplications
                                                .value[index]
                                                .toDate!)),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14.sp),
                                  ),
                                ])))
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(top: 8.h)),
                    Row(crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: 80.w,
                            child: Text(
                              "Người gửi:",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  color: const Color.fromRGBO(133, 133, 133, 1),
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          Text(
                            "${controller.itemsLeavingApplications.value[index].parent?.fullName}",
                            style: TextStyle(
                                fontSize: 14.sp,
                                color: const Color.fromRGBO(51, 157, 255, 1),
                                fontWeight: FontWeight.w400),
                          ),
                        ]),
                    Padding(padding: EdgeInsets.only(top: 8.h)),
                    Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 80.w,
                            child: Text(
                              "Lý do:",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  color: const Color.fromRGBO(133, 133, 133, 1),
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          Flexible(
                            child: Text(
                              "${controller.itemsLeavingApplications.value[index].reason}",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400),
                            ),
                          )
                        ]),
                    Padding(padding: EdgeInsets.only(top: 8.h)),
                    Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          SizedBox(
                            width: 80.w,
                            child: Text(
                              "Trạng thái:",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  color: const Color.fromRGBO(
                                      133, 133, 133, 1),
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                            decoration: BoxDecoration(
                                color: controller.getColorBackgroundStatus(controller.itemsLeavingApplications.value[index].status),
                                borderRadius: BorderRadius.circular(6)
                            ),
                            child: Text(
                              "${controller.getStatus(controller.itemsLeavingApplications.value[index].status)}",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  color: controller.getColorTextStatus(controller.itemsLeavingApplications.value[index].status),
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ]),
                  ],
                ),
              ),
            );
          })),
    );
  }
}
