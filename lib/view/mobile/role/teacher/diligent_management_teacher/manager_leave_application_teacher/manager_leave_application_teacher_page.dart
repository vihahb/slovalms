import 'package:flutter_svg/svg.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:task_manager/view/mobile/role/teacher/diligent_management_teacher/diligent_management_teacher_controller.dart';
import 'package:task_manager/view/mobile/role/teacher/diligent_management_teacher/manager_leave_application_teacher/pending_teacher/pending_teacher_page.dart';
import '../../../../../../../routes/app_pages.dart';
import '../../../../../../commom/constants/date_format.dart';
import '../../../../../../commom/utils/app_utils.dart';
import '../../../../../../commom/utils/date_time_picker.dart';
import '../../../../../../commom/utils/time_utils.dart';
import '../../../../../../data/model/common/contacts.dart';
import 'approved_leave_application_teacher/approved_teacher_leave_application_page.dart';
import 'cancel_leave_application_teacher/cancel_leave_application_teacher_page.dart';
import 'manager_leave_application_teacher_controller.dart';

class ManagerLeaveApplicationTeacherPage
    extends GetView<ManagerLeaveApplicationTeacherController> {
  final controller =
      Get.put(ManagerLeaveApplicationTeacherController(), permanent: true);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
        elevation: 0,
        actions: [
          InkWell(
            onTap: () {
              Get.offAllNamed(Routes.home);
            },
            child: const Icon(
              Icons.home,
              color: Colors.white,
            ),
          ),
          Padding(padding: EdgeInsets.only(right: 16.w))
        ],
        leading: InkWell(
          onTap: () {
            Get.back();
          },
          child: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
        title: const Text(
          'Quản Lý đơn xin nghỉ',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: Obx(() => RefreshIndicator(
        color: const Color.fromRGBO(248, 129, 37, 1),
          child: Column(
            children: [
              SingleChildScrollView(
                physics: const AlwaysScrollableScrollPhysics(),
                child:  Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      decoration: const BoxDecoration(
                        color: Colors.white,
                      ),
                      padding: const EdgeInsets.all(8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 16.w),
                            height: 40,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(
                                    color: const Color.fromRGBO(192, 192, 192, 1)),
                                borderRadius: BorderRadius.circular(8)),
                            child: IntrinsicHeight(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  IntrinsicWidth(
                                      child: Theme(
                                        data: Theme.of(context).copyWith(
                                          canvasColor: Colors.white,
                                        ),
                                        child: DropdownButtonHideUnderline(
                                            child: DropdownButton(
                                              isExpanded: true,
                                              iconSize: 0,
                                              icon: const Visibility(
                                                  visible: false,
                                                  child: Icon(Icons.arrow_downward)),
                                              elevation: 0,
                                              borderRadius: BorderRadius.circular(10.r),
                                              hint:
                                              controller.typeTimeLeaveApplication.value ==
                                                  ""
                                                  ? Row(
                                                children: [
                                                  Text(
                                                    'chọn kiểu thời gian',
                                                    style: TextStyle(
                                                        fontSize: 14.sp,
                                                        fontWeight: FontWeight.w400,
                                                        color: const Color.fromRGBO(
                                                            248, 129, 37, 1)),
                                                  ),
                                                  const Icon(
                                                    Icons.keyboard_arrow_down,
                                                    color: Colors.black,
                                                    size: 18,
                                                  )
                                                ],
                                              )
                                                  : Row(
                                                children: [
                                                  Text(
                                                    '${controller.typeTimeLeaveApplication.value} ',
                                                    style: TextStyle(
                                                        fontSize: 14.sp,
                                                        fontWeight: FontWeight.w400,
                                                        color: Colors.black),
                                                  ),
                                                  const Icon(
                                                    Icons.keyboard_arrow_down,
                                                    color: Colors.black,
                                                    size: 18,
                                                  )
                                                ],
                                              ),
                                              items: ['Thời gian tạo', 'Thời gian nghỉ'].map(
                                                    (value) {
                                                  return DropdownMenuItem<String>(
                                                    value: value,
                                                    child: Text(
                                                      value,
                                                      style: TextStyle(
                                                          fontSize: 14.sp,
                                                          fontWeight: FontWeight.w400,
                                                          color: const Color.fromRGBO(
                                                              248, 129, 37, 1)),
                                                    ),
                                                  );
                                                },
                                              ).toList(),
                                              onChanged: (String? value) {
                                                controller.typeTimeLeaveApplication.value =
                                                value!;
                                                controller.fillLeaveApplication();
                                              },
                                            )),
                                      )),
                                  const SizedBox(
                                      height: 30,
                                      child: VerticalDivider(color: Colors.black)),
                                  InkWell(
                                    onTap: () {
                                      Get.dialog(Dialog(
                                        child: IntrinsicHeight(
                                            child: Container(
                                              margin: const EdgeInsets.symmetric(
                                                  horizontal: 16, vertical: 16),
                                              decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(6)),
                                              child: Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(bottom: 8.h),
                                                    child: Text(
                                                      "Vui lòng chọn ngày",
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight: FontWeight.w500,
                                                          fontSize: 12.sp),
                                                    ),
                                                  ),
                                                  Row(
                                                    children: [
                                                      Expanded(
                                                        child: Container(
                                                          alignment: Alignment.centerLeft,
                                                          padding:
                                                          EdgeInsets.only(left: 8.w),
                                                          decoration: BoxDecoration(
                                                              color: Colors.white,
                                                              borderRadius:
                                                              const BorderRadius.all(
                                                                  Radius.circular(
                                                                      6.0)),
                                                              border: Border.all(
                                                                width: 1,
                                                                style: BorderStyle.solid,
                                                                color:
                                                                const Color.fromRGBO(
                                                                    192, 192, 192, 1),
                                                              )),
                                                          child: TextFormField(
                                                            keyboardType:
                                                            TextInputType.multiline,
                                                            maxLines: null,
                                                            style: TextStyle(
                                                              fontSize: 10.0.sp,
                                                              color: const Color.fromRGBO(
                                                                  26, 26, 26, 1),
                                                            ),
                                                            onTap: () {
                                                              selectDateTimeStart(
                                                                  controller
                                                                      .controllerdateStart
                                                                      .value
                                                                      .text,
                                                                  DateTimeFormat
                                                                      .formatDateShort,
                                                                  context);

                                                            },
                                                            cursorColor:
                                                            const Color.fromRGBO(
                                                                248, 129, 37, 1),
                                                            controller: controller
                                                                .controllerdateStart
                                                                .value,
                                                            readOnly: true,
                                                            decoration: InputDecoration(
                                                              suffixIcon: SizedBox(
                                                                height: 1,
                                                                width: 1,
                                                                child: SvgPicture.asset(
                                                                  "assets/images/icon_date_picker.svg",
                                                                  fit: BoxFit.scaleDown,
                                                                ),
                                                              ),
                                                              labelText: "Từ ngày",
                                                              border: InputBorder.none,
                                                              labelStyle: TextStyle(
                                                                  color: const Color
                                                                      .fromRGBO(
                                                                      177, 177, 177, 1),
                                                                  fontSize: 12.sp,
                                                                  fontWeight:
                                                                  FontWeight.w500,
                                                                  fontFamily:
                                                                  'assets/font/static/Inter-Medium.ttf'),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      Padding(
                                                          padding: EdgeInsets.only(
                                                              right: 16.w)),
                                                      Expanded(
                                                        child: Container(
                                                          alignment: Alignment.centerLeft,
                                                          padding:
                                                          EdgeInsets.only(left: 8.w),
                                                          decoration: BoxDecoration(
                                                              color: Colors.white,
                                                              borderRadius:
                                                              const BorderRadius.all(
                                                                  Radius.circular(
                                                                      6.0)),
                                                              border: Border.all(
                                                                width: 1,
                                                                style: BorderStyle.solid,
                                                                color:
                                                                const Color.fromRGBO(
                                                                    192, 192, 192, 1),
                                                              )),
                                                          child: TextFormField(
                                                            keyboardType:
                                                            TextInputType.multiline,
                                                            maxLines: null,
                                                            style: TextStyle(
                                                              fontSize: 10.0.sp,
                                                              color: const Color.fromRGBO(
                                                                  26, 26, 26, 1),
                                                            ),
                                                            onTap: () {
                                                              selectDateTimeEnd(
                                                                  controller
                                                                      .controllerdateEnd
                                                                      .value
                                                                      .text,
                                                                  DateTimeFormat
                                                                      .formatDateShort,
                                                                  context);
                                                              FocusScope.of(context)
                                                                  .nextFocus();
                                                            },
                                                            readOnly: true,
                                                            cursorColor:
                                                            const Color.fromRGBO(
                                                                248, 129, 37, 1),
                                                            controller: controller
                                                                .controllerdateEnd.value,
                                                            decoration: InputDecoration(
                                                              suffixIcon: SizedBox(
                                                                height: 1,
                                                                width: 1,
                                                                child: SvgPicture.asset(
                                                                  "assets/images/icon_date_picker.svg",
                                                                  fit: BoxFit.scaleDown,
                                                                ),
                                                              ),
                                                              label:
                                                              const Text("Đến ngày"),
                                                              border: InputBorder.none,
                                                              labelStyle: TextStyle(
                                                                  color: const Color
                                                                      .fromRGBO(
                                                                      177, 177, 177, 1),
                                                                  fontSize: 12.sp,
                                                                  fontWeight:
                                                                  FontWeight.w500,
                                                                  fontFamily:
                                                                  'assets/font/static/Inter-Medium.ttf'),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  const Padding(
                                                      padding: EdgeInsets.only(top: 16)),
                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      Expanded(
                                                          child: SizedBox(
                                                            height: 24.h,
                                                            child: ElevatedButton(
                                                                style:
                                                                ElevatedButton.styleFrom(
                                                                    backgroundColor:
                                                                    const Color.fromRGBO(
                                                                        255,
                                                                        255,
                                                                        255,
                                                                        1),
                                                                    side:
                                                                    const BorderSide(
                                                                        color: Color
                                                                            .fromRGBO(
                                                                            248,
                                                                            129,
                                                                            37,
                                                                            1),
                                                                        width: 1),
                                                                    shape:
                                                                    RoundedRectangleBorder(
                                                                      borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                          6),
                                                                    )),
                                                                onPressed: () {
                                                                  Get.back();
                                                                },
                                                                child: Text(
                                                                  'Hủy',
                                                                  style: TextStyle(
                                                                      color: const Color
                                                                          .fromRGBO(
                                                                          248, 129, 37, 1),
                                                                      fontSize: 12.sp,
                                                                      fontWeight:
                                                                      FontWeight.w400,
                                                                      fontFamily:
                                                                      'assets/font/static/Inter-Regular.ttf'),
                                                                )),
                                                          )),
                                                      Padding(
                                                          padding: EdgeInsets.only(
                                                              right: 8.w)),
                                                      Expanded(
                                                          child: SizedBox(
                                                            height: 24.h,
                                                            child: ElevatedButton(
                                                                style:
                                                                ElevatedButton.styleFrom(
                                                                    backgroundColor:
                                                                    const Color.fromRGBO(
                                                                        248,
                                                                        129,
                                                                        37,
                                                                        1),
                                                                    side:
                                                                    const BorderSide(
                                                                        color: Color
                                                                            .fromRGBO(
                                                                            248,
                                                                            129,
                                                                            37,
                                                                            1),
                                                                        width: 1),
                                                                    shape:
                                                                    RoundedRectangleBorder(
                                                                      borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                          6),
                                                                    )),
                                                                onPressed: () {
                                                                  Get.back();
                                                                  controller.fillLeaveApplication();
                                                                },
                                                                child: Text(
                                                                  'Xác Nhận',
                                                                  style: TextStyle(
                                                                      color: const Color
                                                                          .fromRGBO(
                                                                          255, 255, 255, 1),
                                                                      fontSize: 12.sp,
                                                                      fontWeight:
                                                                      FontWeight.w400,
                                                                      fontFamily:
                                                                      'assets/font/static/Inter-Regular.ttf'),
                                                                )),
                                                          ))
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            )),
                                      ));
                                    },
                                    child: Row(
                                      children: [
                                        Text(
                                            "${controller.controllerdateStart.value.text.substring(0,10)} - ${controller.controllerdateEnd.value.text.substring(0,10)}",
                                            style: TextStyle(
                                                color: const Color.fromRGBO(
                                                    26, 26, 26, 1),
                                                fontSize: 12.sp,
                                                fontWeight: FontWeight.w500,
                                                fontFamily:
                                                'assets/font/static/Inter-Medium.ttf')),
                                        Padding(padding: EdgeInsets.only(right: 4.w)),
                                        SizedBox(
                                          child: Image.asset(
                                            "assets/images/icon_calenda.png",
                                            height: 16,
                                            width: 16,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(padding: EdgeInsets.only(top: 16.h)),
                         Obx(() =>  Container(
                           margin: EdgeInsets.symmetric(horizontal: 16.w),
                           padding: EdgeInsets.symmetric(horizontal: 16.w),
                           height: 40,
                           decoration: BoxDecoration(
                               color: Colors.white,
                               border: Border.all(
                                   color: const Color.fromRGBO(192, 192, 192, 1)),
                               borderRadius: BorderRadius.circular(8)),
                           child: Theme(
                             data: Theme.of(context).copyWith(
                               canvasColor: Colors.white,
                             ),
                             child: DropdownButtonHideUnderline(
                                 child: DropdownButton(
                                   isExpanded: true,
                                   iconSize: 0,
                                   icon: const Visibility(
                                       visible: false,
                                       child: Icon(Icons.arrow_downward)),
                                   elevation: 0,
                                   borderRadius: BorderRadius.circular(10.r),
                                   hint: controller.selectStudent.value != ""
                                       ? Row(
                                     children: [
                                       Text(
                                         '${controller.selectStudent.value}',
                                         style: TextStyle(
                                             fontSize: 14.sp,
                                             fontWeight: FontWeight.w400,
                                             color: const Color.fromRGBO(
                                                 248, 129, 37, 1)),
                                       ),
                                       Expanded(child: Container()),
                                       const Icon(
                                         Icons.keyboard_arrow_down,
                                         color: Colors.black,
                                         size: 18,
                                       )
                                     ],
                                   )
                                       : Row(
                                     children: [
                                       Text(
                                         'Chọn học sinh',
                                         style: TextStyle(
                                             fontSize: 14.sp,
                                             fontWeight: FontWeight.w400,
                                             color: Colors.black),
                                       ),
                                       Expanded(child: Container()),
                                       const Icon(
                                         Icons.keyboard_arrow_down,
                                         color: Colors.black,
                                         size: 18,
                                       )
                                     ],
                                   ),
                                   items: controller.listStudentByClass.value.map(
                                         (value) {
                                       return DropdownMenuItem<Student>(
                                         value: value,
                                         child: Text(
                                           value.fullName!,
                                           style: TextStyle(
                                               fontSize: 14.sp,
                                               fontWeight: FontWeight.w400,
                                               color: const Color.fromRGBO(
                                                   248, 129, 37, 1)),
                                         ),
                                       );
                                     },
                                   ).toList(),
                                   onChanged: (Student? value) {
                                     controller.selectStudent.value = value!.fullName!;
                                     controller.studentId.value = value.id!;
                                     controller.fillLeaveApplication();
                                   },
                                 )),
                           ),
                         ))
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 16.w),
                      height: 40,
                      child: ListView.builder(
                          itemCount: controller.listStatus.length,
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            return GetBuilder<ManagerLeaveApplicationTeacherController>(

                                builder: (controller) {
                                  return TextButton(
                                      onPressed: () {
                                        controller.setUpdate(index);
                                        controller.showColor(index);
                                        controller.pageController.animateToPage(index,
                                            duration:
                                            const Duration(milliseconds: 500),
                                            curve: Curves.linear);
                                      },
                                      child: Obx(() => Text(
                                        "${controller.listStatus.value[index]} (${controller.getCountLeaveApplication(index)})",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12.sp,
                                            color: controller.indexClick == index
                                                ? const Color.fromRGBO(
                                                248, 129, 37, 1)
                                                : const Color.fromRGBO(
                                                177, 177, 177, 1)),
                                      )));
                                });
                          }),
                    ),

                  ],
                ),
              ),
              Expanded(
                  child:  PageView(
                    physics: const NeverScrollableScrollPhysics(),
                    onPageChanged: (value) {
                      controller.onPageViewChange(value);
                    },
                    controller: controller.pageController,
                    children: [
                      PendingTeacherPage(),
                      ApprovedLeaveApplicationTeacherPage(),
                      CancelLeaveApplicationTeacherPage()
                    ],
                  )),
            ],
          ), onRefresh: () async{
          controller.onRefresh();
      })),
    ));
  }

  selectDateTimeStart(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
            initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      if (value != null) {
        date =
            TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
      }
    });

    if (date.isNotEmpty) {
      if (controller.controllerdateStart.value.text != "") {
        if (DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy')
                .parse(controller.controllerdateEnd.value.text)
                .millisecondsSinceEpoch) {
          controller.controllerdateStart.value.text = "$date";
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian bắt đầu nhỏ hơn thời gian bắt đầu");
        }
      } else {
        controller.controllerdateStart.value.text = "$date";
      }

    }
    FocusScope.of(context).requestFocus(FocusNode());
  }

  selectDateTimeEnd(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
            initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      if (value != null) {
        date =
            TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
      }
    });

    if (date.isNotEmpty) {
      if (controller.controllerdateStart.value.text != "") {
        if (DateFormat('dd/MM/yyyy')
            .parse(controller.controllerdateStart.value.text)
            .millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch) {
          controller.controllerdateEnd.value.text = "$date";
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian kết thúc lớn hơn thời gian bắt đầu");
        }
      } else {
        controller.controllerdateEnd.value.text = "$date";
      }

    }

    FocusScope.of(context).requestFocus(FocusNode());
  }
}
