import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:task_manager/view/mobile/role/parent/diligence_parent/approval_parent/cancel_leaving_application/cancel_leave_application_controller.dart';

import '../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../routes/app_pages.dart';
import '../approved_leave_application_teacher/approved_teacher_leave_application_controller.dart';
import '../cancel_leave_application_teacher/cancel_leave_application_teacher_controller.dart';
import '../manager_leave_application_teacher_controller.dart';
import 'detail_pending_teacher/detail_pending_teacher_page.dart';
import 'pending_teacher_controller.dart';

class PendingTeacherPage extends GetWidget<PendingTeacherController> {
  final controller = Get.put(PendingTeacherController(), permanent: true);

   PendingTeacherPage({super.key});

  _onDialogConfirm(status,index) {
    return StatefulBuilder(builder: (context,state){
      return Wrap(
        children: [
          Container(
            margin: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    height: 50,
                    padding:
                    EdgeInsets.only(top: 15.h, left: 10.w, right: 10.w, bottom: 15.h),
                    decoration: const ShapeDecoration(
                        color: Color.fromRGBO(248, 129, 37, 1),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(6),
                              topRight: Radius.circular(6)),
                        )),
                    child: SizedBox(
                      width: double.infinity,
                      height: 30,
                      child: Text("${controller.setTextDialog(status)}",
                          style: const TextStyle(color: Colors.white)),
                    )),

                Container(
                    color: Colors.white,
                    width: double.infinity,
                    alignment: Alignment.center,
                    child:  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Container(
                          margin: EdgeInsets.only(left: 8.w),
                          child: const Text("Nhập phản hồi cho đơn xin nghỉ học này!"),
                        ),
                        Container(
                          width: double.infinity,
                          margin: EdgeInsets.all(8.w),
                          padding: EdgeInsets.symmetric(
                              horizontal: 16.w, vertical: 4.h),
                          decoration: BoxDecoration(
                              border: Border.all(
                                color: const Color.fromRGBO(133, 133, 133, 1),
                              ),
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(6)),
                          child: TextFormField(
                            controller: controller.controllerFeedback.value,
                            cursorColor: const Color.fromRGBO(248, 129, 37, 1),
                            decoration: const InputDecoration(
                              hintText: "Nhập phản hồi",
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                      ],
                    )),

                Row(
                  children: [
                    Expanded(
                        child: SizedBox(
                          height: 50,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.grey,
                                shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(6)))),
                            onPressed: () {
                              controller.controllerFeedback.value.text = "";
                              Get.back();
                            },
                            child: const Text(
                              "Hủy",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        )),
                    Expanded(
                      child: SizedBox(
                        height: 50,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.red,
                              shape: const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      bottomRight: Radius.circular(6)))),
                          onPressed: ()  {
                            if(status == "REFUSE"){
                              if(controller.controllerFeedback.value.text.trim() == ""){
                                AppUtils.shared.showToast(
                                    "Vui lòng nhập phản hồi");
                              }else{
                                controller.refuseLeaveApplication(index);
                                controller.itemsLeavingApplications.refresh();
                                Future.delayed(const Duration(seconds: 1), () {
                                  Get.find<ManagerLeaveApplicationTeacherController>().getListLeavingApplicationTeacher();
                                  Get.find<PendingTeacherController>().getListLeavingApplicationPendingTeacher();
                                  Get.find<CancelLeaveApplicationTeacherController>().getListLeavingApplicationCancelTeacher();
                                  Get.find<ApprovedLeaveApplicationTeacherController>().getListLeavingApplicationApprovedTeacher();
                                  Get.back();
                                });
                              }
                            }
                            if(status == "APPROVE"){
                              controller.approveLeaveApplication(index);
                              controller.itemsLeavingApplications.refresh();
                              Future.delayed(const Duration(seconds: 1), () {
                                Get.find<ManagerLeaveApplicationTeacherController>().getListLeavingApplicationTeacher();
                                Get.find<PendingTeacherController>().getListLeavingApplicationPendingTeacher();
                                Get.find<CancelLeaveApplicationTeacherController>().getListLeavingApplicationCancelTeacher();
                                Get.find<ApprovedLeaveApplicationTeacherController>().getListLeavingApplicationApprovedTeacher();
                                Get.back();
                              });
                            }


                          },
                          child: const Text(
                            "Xác nhận",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      );
    });
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
        child: Obx(() => ListView.builder(
            itemCount: controller.itemsLeavingApplications.value.length,
            shrinkWrap: true,
            physics: const ScrollPhysics(),
            itemBuilder: (context, index) {
              return InkWell(
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 16.w, vertical: 4.h),
                  padding:
                  EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.h),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(6)),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          SizedBox(
                            width: 32.w,
                            height: 32.w,
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              backgroundImage: Image.network(
                                controller.itemsLeavingApplications.value[index].student!.image ??
                                    "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                                errorBuilder:
                                    (context, object, stackTrace) {
                                  return Image.asset(
                                    "assets/images/img_Noavt.png",
                                  );
                                },
                              ).image,
                            ),
                          ),
                          Padding(padding: EdgeInsets.only(right: 8.w)),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                controller.itemsLeavingApplications.value[index].student!.fullName!,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14.sp),
                              ),
                            ],
                          ),
                          Expanded(child: Container()),
                          InkWell(
                            onTap:() {
                              Get.toNamed(Routes.detailPendingLeaveApplicationTeacher,arguments: controller.itemsLeavingApplications.value[index].id);
                            },
                            child: Text(
                              "Chi Tiết",
                              style: TextStyle(
                                  color: const Color.fromRGBO(26, 59, 112, 1),
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14.sp),
                            ),
                          )
                        ],
                      ),
                      const Divider(),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 80.w,
                            child: Text(
                              "Thời gian:",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  color: const Color.fromRGBO(133, 133, 133, 1),
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          Flexible(
                              child: RichText(
                                  text: TextSpan(children: [
                                    TextSpan(
                                      text: controller.outputDateFormat.format(
                                          DateTime.fromMillisecondsSinceEpoch(
                                              controller
                                                  .itemsLeavingApplications
                                                  .value[index]
                                                  .fromDate??0)),
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 14.sp),
                                    ),
                                    TextSpan(
                                      text: " đến ",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 14.sp),
                                    ),
                                    TextSpan(
                                      text: controller.outputDateFormat.format(
                                          DateTime.fromMillisecondsSinceEpoch(
                                              controller
                                                  .itemsLeavingApplications
                                                  .value[index]
                                                  .toDate??0)),
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 14.sp),
                                    ),
                                  ])))
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 8.h)),
                      Row(crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: 80.w,
                              child: Text(
                                "Người Gửi:",
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                            Text(
                              controller.itemsLeavingApplications.value[index].parent?.fullName ?? "",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  color: const Color.fromRGBO(51, 157, 255, 1),
                                  fontWeight: FontWeight.w400),
                            ),
                          ]),
                      Padding(padding: EdgeInsets.only(top: 8.h)),
                      Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: 80.w,
                              child: Text(
                                "Lý Do:",
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                            Flexible(
                              child: Text(
                                "${controller.itemsLeavingApplications.value[index].reason}",
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w400),
                              ),
                            )
                          ]),
                      Padding(padding: EdgeInsets.only(top: 8.h)),
                      Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            SizedBox(
                              width: 80.w,
                              child: Text(
                                "Trạng thái:",
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    color: const Color.fromRGBO(
                                        133, 133, 133, 1),
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                              decoration: BoxDecoration(
                                  color: controller.getColorBackgroundStatus(controller.itemsLeavingApplications.value[index].status),
                                  borderRadius: BorderRadius.circular(6)
                              ),
                              child: Text(
                                "${controller.getStatus(controller.itemsLeavingApplications.value[index].status)}",
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    color: controller.getColorTextStatus(controller.itemsLeavingApplications.value[index].status),
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          ]),
                      Padding(padding: EdgeInsets.only(top: 8.h)),
                      Visibility(
                        visible: controller.isHomeRoomTeacher.value,
                          child: Row(
                        children: [
                          Expanded(child: Container()),
                          InkWell(
                            onTap: () {
                              Get.bottomSheet(_onDialogConfirm("REFUSE",index));

                            },
                            child:  Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 16.w, vertical: 6.h),
                              decoration: BoxDecoration(
                                  color: const Color.fromRGBO(255, 69, 89, 1),
                                  borderRadius: BorderRadius.circular(6.r)),
                              child: Row(
                                children: [
                                  Text(
                                    "Từ chối",
                                    style: TextStyle(
                                        fontSize: 12.sp,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white),
                                  ),
                                  Padding(padding: EdgeInsets.only(right: 4.w)),
                                  const Icon(Icons.close_outlined,color: Colors.white,size: 20,)
                                ],
                              ),
                            ),),
                          Padding(padding: EdgeInsets.only(right: 8.w)),
                          InkWell(
                            onTap: () {
                              Get.bottomSheet(_onDialogConfirm("APPROVE",index));
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 16.w, vertical: 6.h),
                              decoration: BoxDecoration(
                                  color: const Color.fromRGBO(248, 129, 37, 1),
                                  borderRadius: BorderRadius.circular(6.r)),
                              child: Row(
                                children: [
                                  Text(
                                    "Duyệt Đơn",
                                    style: TextStyle(
                                        fontSize: 12.sp,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white),
                                  ),
                                  Padding(padding: EdgeInsets.only(right: 4.w)),
                                  const Icon(Icons.check,color: Colors.white,size: 20,)
                                ],
                              ),
                            ),
                          ),
                        ],
                      ))
                    ],
                  ),
                ),
              );
            })));
  }
}
