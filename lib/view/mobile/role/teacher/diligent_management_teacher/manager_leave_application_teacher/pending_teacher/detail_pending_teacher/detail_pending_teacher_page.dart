import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../commom/utils/global.dart';
import '../../../../../../../../commom/widget/text_field_custom.dart';

import '../../../../../../../../routes/app_pages.dart';
import '../../../attendance_teacher/attendance_teacher_controller.dart';
import '../../../diligent_management_teacher_controller.dart';
import '../../../list_leave_application_teacher/list_leave_application_teacher_controller.dart';
import '../../approved_leave_application_teacher/approved_teacher_leave_application_controller.dart';
import '../../cancel_leave_application_teacher/cancel_leave_application_teacher_controller.dart';
import '../../manager_leave_application_teacher_controller.dart';
import '../pending_teacher_controller.dart';
import 'detail_pending_teacher_controller.dart';

class DetailPendingLeaveApplicationTeacherPage
    extends GetWidget<DetailPendingTeacherController> {
  final controller = Get.put(DetailPendingTeacherController());

  DetailPendingLeaveApplicationTeacherPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(child: SafeArea(
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
              elevation: 0,
              actions: [
                InkWell(
                  onTap: () {
                    Get.offAllNamed(Routes.home);
                    Get.delete<DiligenceManagementTeacherController>(
                        force: true);
                    Get.delete<ManagerLeaveApplicationTeacherController>(
                        force: true);
                    Get.delete<PendingTeacherController>(force: true);
                    Get.delete<CancelLeaveApplicationTeacherController>(
                        force: true);
                    Get.delete<ApprovedLeaveApplicationTeacherController>(
                        force: true);
                    Get.delete<ListLeaveApplicationTeacherController>(
                        force: true);
                    Get.delete<AttendanceTeacherController>(force: true);
                  },
                  child: const Icon(
                    Icons.home,
                    color: Colors.white,
                  ),
                ),
                Padding(padding: EdgeInsets.only(right: 16.w))
              ],
              leading: InkWell(
                onTap: () {
                  Get.back();
                  Get.find<ManagerLeaveApplicationTeacherController>().onRefresh();
                  Get.find<ManagerLeaveApplicationTeacherController>().indexClick = 0;
                  Get.find<ManagerLeaveApplicationTeacherController>().comePending();
                  Get.find<ManagerLeaveApplicationTeacherController>().update();
                },
                child: const Icon(
                  Icons.arrow_back,
                  color: Colors.white,
                ),
              ),
              title: const Text(
                'Chi Tiết Đơn Nghỉ Phép',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontFamily: 'static/Inter-Medium.ttf'),
              ),
            ),
            body: GestureDetector(
              onTap: () {
                dismissKeyboard();
              },
              child: Obx(() => Column(
                children: [
                  Expanded(
                      child: SingleChildScrollView(
                        physics: const BouncingScrollPhysics(),
                        child: Column(
                          children: [
                            Visibility(
                                visible: controller
                                    .deatilNotifyLeavingApplication
                                    .value
                                    .html !=
                                    null,
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                      BorderRadius.circular(6)),
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 8.w, vertical: 16.h),
                                  child: Html(
                                    data: controller
                                        .deatilNotifyLeavingApplication
                                        .value
                                        .html ??
                                        "",
                                  ),
                                )),
                          ],
                        ),
                      )),
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(6)),
                    padding: EdgeInsets.symmetric(vertical: 8.h),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        OutlineBorderTextFormField(
                          enable: true,
                          focusNode: controller.focusFeedback,
                          iconPrefix: "",
                          iconSuffix: "",
                          state: StateType.DEFAULT,
                          labelText: "Phản Hồi Của Giáo Viên",
                          autofocus: false,
                          controller: controller.controllerFeedback,
                          helperText: "",
                          showHelperText: false,
                          ishowIconPrefix: false,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          validation: (textToValidate) {
                            return controller
                                .getTempIFSCValidation(textToValidate);
                          },
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Container(
                          margin:
                          EdgeInsets.symmetric(horizontal: 16.w),
                          child: Row(children: [
                            SizedBox(
                              width: 80.w,
                              child: Text(
                                "Trạng thái:",
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    color: const Color.fromRGBO(
                                        133, 133, 133, 1),
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 16, vertical: 4),
                              decoration: BoxDecoration(
                                  color: controller
                                      .getColorBackgroundStatus(controller
                                      .deatilNotifyLeavingApplication
                                      .value
                                      .status),
                                  borderRadius:
                                  BorderRadius.circular(6)),
                              child: Text(
                                "${controller.getStatus(controller.deatilNotifyLeavingApplication.value.status) ?? ""}",
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    color: controller
                                        .getColorTextStatus(controller
                                        .deatilNotifyLeavingApplication
                                        .value
                                        .status),
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                            Expanded(child: Container()),
                          ]),
                        ),
                        Visibility(
                            visible: controller.isHomeRoomTeacher.value,
                            child: Container(
                              padding: EdgeInsets.all(16.h),
                              color: Colors.white,
                              child: Row(
                                children: [
                                  Expanded(
                                      flex: 1,
                                      child: InkWell(
                                        onTap: () {
                                          if (controller
                                              .controllerFeedback
                                              .value
                                              .text
                                              .trim() ==
                                              "") {
                                            AppUtils.shared.showToast(
                                                "Vui lòng nhập phản hồi");
                                          } else {
                                            controller
                                                .refuseLeaveApplication();
                                            Get.find<
                                                PendingTeacherController>()
                                                .refresh();
                                            Future.delayed(
                                                const Duration(
                                                    seconds: 1), () {
                                              Get.back();
                                              Get.find<
                                                  ManagerLeaveApplicationTeacherController>()
                                                  .onInit();
                                              Get.find<
                                                  PendingTeacherController>()
                                                  .getListLeavingApplicationPendingTeacher();
                                            });
                                          }
                                        },
                                        child: Container(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 16.w,
                                              vertical: 6.h),
                                          decoration: BoxDecoration(
                                              color:
                                              const Color.fromRGBO(
                                                  255, 69, 89, 1),
                                              borderRadius:
                                              BorderRadius.circular(
                                                  6.r)),
                                          child: Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment
                                                .center,
                                            children: [
                                              Text(
                                                "Từ chối",
                                                style: TextStyle(
                                                    fontSize: 12.sp,
                                                    fontWeight:
                                                    FontWeight.w400,
                                                    color:
                                                    Colors.white),
                                              ),
                                              Padding(
                                                  padding:
                                                  EdgeInsets.only(
                                                      right: 4.w)),
                                              const Icon(
                                                Icons.close_outlined,
                                                color: Colors.white,
                                                size: 20,
                                              )
                                            ],
                                          ),
                                        ),
                                      )),
                                  Padding(
                                      padding:
                                      EdgeInsets.only(right: 8.w)),
                                  Expanded(
                                    flex: 1,
                                    child: InkWell(
                                      onTap: () {
                                        controller
                                            .approveLeaveApplication();
                                        Get.find<
                                            PendingTeacherController>()
                                            .refresh();
                                        Future.delayed(
                                            const Duration(seconds: 1),
                                                () {
                                              Get.back();
                                              Get.find<
                                                  ManagerLeaveApplicationTeacherController>()
                                                  .onInit();
                                              Get.find<
                                                  PendingTeacherController>()
                                                  .getListLeavingApplicationPendingTeacher();
                                            });
                                      },
                                      child: Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 16.w,
                                            vertical: 6.h),
                                        decoration: BoxDecoration(
                                            color: const Color.fromRGBO(
                                                248, 129, 37, 1),
                                            borderRadius:
                                            BorderRadius.circular(
                                                6.r)),
                                        child: Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "Duyệt Đơn",
                                              style: TextStyle(
                                                  fontSize: 12.sp,
                                                  fontWeight:
                                                  FontWeight.w400,
                                                  color: Colors.white),
                                            ),
                                            Padding(
                                                padding:
                                                EdgeInsets.only(
                                                    right: 4.w)),
                                            const Icon(
                                              Icons.check,
                                              color: Colors.white,
                                              size: 20,
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )),
                      ],
                    ),
                  )
                ],
              )),
            )
        )
    ), onWillPop: () async{
      Get.back();
      Get.find<ManagerLeaveApplicationTeacherController>().onRefresh();
      Get.find<ManagerLeaveApplicationTeacherController>().indexClick = 0;
      Get.find<ManagerLeaveApplicationTeacherController>().comePending();
      return true;
    });
  }
}
