import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../commom/utils/app_utils.dart';
import '../../../../../data/base_service/api_response.dart';
import '../../../../../data/model/common/diligence.dart';
import '../../../../../data/model/common/leaving_application.dart';
import '../../../../../data/repository/diligence/diligence_repo.dart';
import '../teacher_home_controller.dart';

import 'attendance_teacher/attendance_teacher_controller.dart';

import 'list_leave_application_teacher/list_leave_application_teacher_controller.dart';
import 'manager_leave_application_teacher/approved_leave_application_teacher/approved_teacher_leave_application_controller.dart';
import 'manager_leave_application_teacher/cancel_leave_application_teacher/cancel_leave_application_teacher_controller.dart';
import 'manager_leave_application_teacher/manager_leave_application_teacher_controller.dart';
import 'manager_leave_application_teacher/pending_teacher/pending_teacher_controller.dart';


class DiligenceManagementTeacherController extends GetxController {
  DateTime now = DateTime.now();
  RxInt indexClick = 0.obs;
  RxList<bool> click = <bool>[].obs;
  var colorText = const Color.fromRGBO(177, 177, 177, 1).obs;
  var color = const Color.fromRGBO(246, 246, 246, 1).obs;
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var selectedDate = DateTime.now().obs;
  var cupertinoDatePicker = DateTime.now().obs;
  var offset = 0.0.obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );
  var itemsLeavingApplications = <ItemsLeavingApplication>[].obs;
  var diligenceClass = DiligenceClass().obs;
  var listStudentDiligence = <Diligents>[].obs;
  var listStatus = <String>[].obs;
  var isShowPopupMenu = false.obs;

  @override
  void onInit() {
    listStatus.value = ["Điểm danh","Danh sách đơn xin nghỉ"];
    if(DateTime.now().month<=12&&DateTime.now().month>9){
     selectedDate.value = DateTime(Get.find<TeacherHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
    }else{
     selectedDate.value = DateTime(Get.find<TeacherHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
    }
    pageController.addListener(() {
      if(indexClick.value == 0){
        Get.put(AttendanceTeacherController());
      }
    });
    getListStudentDiligence();
    getListLeavingApplicationTeacher();
    HomeRoomTeacher();
    indexClick.value = 0;
    super.onInit();
  }


  HomeRoomTeacher() {
    if (Get.find<TeacherHomeController>()
        .userProfile
        .value
        .classesOfHomeroomTeacher
        ?.contains(
        Get.find<TeacherHomeController>().currentClass.value.classId) ==
        true) {
      isShowPopupMenu.value = true;
    } else {
      isShowPopupMenu.value = false;
    }
  }


  onPageViewChange(int page) {
    indexClick.value = page;
    if (page != 0)
      indexClick.value! - 1;
    else
      indexClick.value = 0;
  }

  showColor(index) {
    click.value = [];
    for (int i = 0; i < 2; i++) {
      click.value.add(false);
    }
    click.value[index] = true;
  }

  setColorButtonAttendance(RxList<int> groupValue) {
    if (groupValue.value.contains(0) == true) {
      color.value = const Color.fromRGBO(246, 246, 246, 1);
    } else {
      color.value = const Color.fromRGBO(248, 129, 37, 1);
      if (Get.find<AttendanceTeacherController>().status == "CONFIRM") {
        color.value = const Color.fromRGBO(246, 246, 246, 1);
      }
    }
  }

  setColorTextButtonAttendance(RxList<int> groupValue) {
    if (groupValue.value.contains(0)) {
      colorText.value = const Color.fromRGBO(177, 177, 177, 1);
    } else {
      colorText.value = const Color.fromRGBO(255, 255, 255, 1);
      if (Get.find<AttendanceTeacherController>().status == "CONFIRM") {
        colorText.value = const Color.fromRGBO(177, 177, 177, 1);
      }
    }
  }

  confirmAttendance() {
    var id = Get.find<AttendanceTeacherController>().diligenceClass.value.id;
    if (Get.find<AttendanceTeacherController>().groupValue.value.contains(0)) {
    } else {
      if (Get.find<AttendanceTeacherController>().status != "CONFIRM") {
        _diligenceRepo.confirmAttendance(id).then((value) {
          if (value.state == Status.SUCCESS) {
            AppUtils.shared.showToast("Điểm danh thành công!");
            Get.find<AttendanceTeacherController>().closeAttendance();
            Get.find<AttendanceTeacherController>().colorAttendance(false);
            Get.find<AttendanceTeacherController>().isAttendance.value = false;
            Get.find<AttendanceTeacherController>().onInit();
            color.value = const Color.fromRGBO(246, 246, 246, 1);
            colorText.value = const Color.fromRGBO(177, 177, 177, 1);
          } else {
            AppUtils.shared.hideLoading();
            AppUtils.shared
                .snackbarError("Điểm danh thất bại", value.message ?? "");
          }
        });
      }
    }
  }

  getListStudentDiligence() {
    var classId = Get.find<TeacherHomeController>().currentClass.value.classId;
    var fromdate = DateTime(selectedDate.value.year, selectedDate.value.month,
        selectedDate.value.day, 00, 00)
        .millisecondsSinceEpoch;
    var todate = DateTime(selectedDate.value.year, selectedDate.value.month,
        selectedDate.value.day, 23, 59)
        .millisecondsSinceEpoch;

    _diligenceRepo
        .getListStudentDiligence(classId, fromdate, todate, "")
        .then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value = value.object!;
        listStudentDiligence.value = diligenceClass.value.studentDiligent!;
      }
    });

  }
  getListLeavingApplicationTeacher() async{
    itemsLeavingApplications.value.clear();
    var classId =Get.find<TeacherHomeController>().currentClass.value.classId;
    var fromdate = DateTime(selectedDate.value.year,selectedDate.value.month,selectedDate.value.day,00,00).millisecondsSinceEpoch;
    var todate = DateTime(selectedDate.value.year,selectedDate.value.month,selectedDate.value.day,23,59).millisecondsSinceEpoch;
    _diligenceRepo.getListLeavingApplicationTeacher(classId,fromdate,todate,"","","").then((value) {
      if (value.state == Status.SUCCESS) {
        itemsLeavingApplications.value = value.object!;

      }
    });
    itemsLeavingApplications.refresh();
  }



  getListStatus(index){
    switch(index){
      case 0:
        return  Get.find<AttendanceTeacherController>().listStudentDiligence.value.length;
      case 1:
        return Get.find<ListLeaveApplicationTeacherController>().itemsLeavingApplications.value.length;
    }
  }


  onSelectedDateDiligence(){
    Get.find<AttendanceTeacherController>().selectedDay.value = selectedDate.value;
    Get.find<ListLeaveApplicationTeacherController>().selectedDay.value = selectedDate.value;
    Get.find<AttendanceTeacherController>().onInit();
    Get.find<ListLeaveApplicationTeacherController>().onInit();
    if(selectedDate.value.year == DateTime.now().year&&selectedDate.value.month == DateTime.now().month&&selectedDate.value.day == DateTime.now().day){
      Get.find<AttendanceTeacherController>().isShowSwitchAttendance.value = true;
    }else{
      Get.find<AttendanceTeacherController>().isShowSwitchAttendance.value = false;
    }
    if(Get.isRegistered<ManagerLeaveApplicationTeacherController>()){
      Get.find<ManagerLeaveApplicationTeacherController>().onInit();
      Get.find<ManagerLeaveApplicationTeacherController>().update();
    }

    if(Get.isRegistered<CancelLeaveApplicationTeacherController>()){
      Get.find<CancelLeaveApplicationTeacherController>().onInit();
    }
    if(Get.isRegistered<PendingTeacherController>()){
      Get.find<PendingTeacherController>().onInit();
    }
    if(Get.isRegistered<ApprovedLeaveApplicationTeacherController>()){
      Get.find<ApprovedLeaveApplicationTeacherController>().onInit();
      Get.find<ApprovedLeaveApplicationTeacherController>().itemsLeavingApplications.refresh();
    }

    Get.find<AttendanceTeacherController>().groupValue.value.clear();
    Get.find<ListLeaveApplicationTeacherController>().itemsLeavingApplications.value.clear();


    Get.find<AttendanceTeacherController>().getListStudentDiligence();
    Get.find<ListLeaveApplicationTeacherController>().getListLeavingApplicationTeacher();

    Get.find<AttendanceTeacherController>().groupValue.refresh();
    Get.find<ListLeaveApplicationTeacherController>().itemsLeavingApplications.refresh();
    update();

  }


  comeBackAttendance() {
    //Move tab to first
    pageController?.animateTo(0, duration: Duration(seconds: 1), curve: Curves.bounceIn);
  }

}
