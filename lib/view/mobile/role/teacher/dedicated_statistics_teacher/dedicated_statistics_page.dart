import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:task_manager/commom/utils/date_time_picker.dart';
import 'package:task_manager/commom/utils/time_utils.dart';
import 'package:task_manager/view/mobile/role/teacher/dedicated_statistics_teacher/dedicated_statistics_controller.dart';

import '../../../../../commom/constants/date_format.dart';

class DedicatedStatisticsPage extends GetWidget<DedicatedStatisticsController>{
  final controller = Get.put(DedicatedStatisticsController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
          elevation: 0,
          title: Text(
            'Thống kê Chuyên Cần',
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.sp,
                fontFamily: 'static/Inter-Medium.ttf'),
          ),
        ),
        body:
        SingleChildScrollView(
          child:
          Obx(() =>  Container(
            color: Color.fromRGBO(245, 245, 245, 1),
            padding: EdgeInsets.all(16),
            child:
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Sĩ số: ${controller.dedicatedStastical.value.totalItems} học sinh", style: TextStyle(color: Color.fromRGBO(177, 177, 177, 1), fontSize: 15, fontWeight: FontWeight.w500),),
                Padding(padding: EdgeInsets.only(top: 12.h)),
                Container(
                  padding:
                  EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.h),
                  height: 60.h,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border:
                      Border.all(color: Color.fromRGBO(192, 192, 192, 1)),
                      borderRadius: BorderRadius.circular(8)),
                  child: Row(
                    children: [
                      Expanded(
                          child:
                          Container(
                            child:
                            TextFormField(
                              style: TextStyle(
                                fontSize: 12.0.sp,
                                color: const Color.fromRGBO(26, 26, 26, 1),
                              ),
                              onTap: () {
                                selectDateTimeStart(controller.controllerdateStart.value.text, DateTimeFormat.formatDateShort,context);
                              },
                              cursorColor: const Color.fromRGBO(248, 129, 37, 1),
                              controller:
                              controller.controllerdateStart.value,
                              readOnly: true,
                              decoration: InputDecoration(
                                suffixIcon: Container(
                                  child: Container(
                                    margin: EdgeInsets.zero,
                                    child: SvgPicture.asset(
                                      "assets/images/icon_date_picker.svg",
                                      fit: BoxFit.scaleDown,
                                    ),
                                  ),
                                ),
                                labelText: "Từ thời gian",
                                border: InputBorder.none,
                                labelStyle: TextStyle(
                                    color: const Color.fromRGBO(177, 177, 177, 1),
                                    fontSize: 10.sp,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'assets/font/static/Inter-Medium.ttf'
                                ),
                              ),
                            ),
                          )
                      ),
                      Image.asset("assets/images/icon_To.png", width: 16,height: 16,),
                      Expanded(
                        child:
                        Container(
                          margin: EdgeInsets.only(left: 16.w),
                          child:
                          TextFormField(
                            style: TextStyle(
                              fontSize: 12.0.sp,
                              color: const Color.fromRGBO(26, 26, 26, 1),
                            ),
                            onTap: () {
                              selectDateTimeEnd(controller.controllerdateEnd.value.text, DateTimeFormat.formatDateShort,context);
                            },
                            cursorColor: const Color.fromRGBO(248, 129, 37, 1),
                            controller:
                            controller.controllerdateEnd.value,
                            readOnly: true,
                            decoration: InputDecoration(
                              suffixIcon: Container(
                                child: Container(
                                  margin: EdgeInsets.zero,
                                  child: SvgPicture.asset(
                                    "assets/images/icon_date_picker.svg",
                                    fit: BoxFit.scaleDown,
                                  ),
                                ),
                              ),
                              labelText: "Đến thời gian",
                              border:InputBorder.none,
                              labelStyle: TextStyle(
                                  color: const Color.fromRGBO(177, 177, 177, 1),
                                  fontSize: 10.sp,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: 'assets/font/static/Inter-Medium.ttf'
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(left: 8.w)),
                    ],
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 16)),
                Container(
                  padding:
                  EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.h),
                  height: 40,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border:
                      Border.all(color: Color.fromRGBO(192, 192, 192, 1)),
                      borderRadius: BorderRadius.circular(8)),
                  child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                        isExpanded: true,
                        iconSize: 0,
                        icon: const Visibility(
                            visible: false,
                            child: Icon(Icons.arrow_downward)),
                        elevation: 0,
                        borderRadius: BorderRadius.circular(10.r),
                        hint: controller.category.value == ""
                            ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children:  [
                            Text(
                              'Học sinh',
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  fontWeight:
                                  FontWeight.w400,
                                  color: Colors.black),
                            ),
                            Expanded(child: Container()),
                            const Icon(
                              Icons.keyboard_arrow_down,
                              color: Colors.black,
                              size: 18,
                            )
                          ],
                        ) :
                        Row(
                          children:  [
                            Text(
                              '${controller.category.value} ',
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  fontWeight:
                                  FontWeight.w400,
                                  color: Colors.black),
                            ),
                            Expanded(child: Container()),
                            const Icon(
                              Icons.keyboard_arrow_down,
                              color: Colors.black,
                              size: 18,
                            )
                          ],
                        ),
                        items: [
                          'Nguyễn Văn A',
                          'Nguyễn Văn B'
                        ].map(
                              (value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(
                                value,
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.black),
                              ),
                            );
                          },
                        ).toList(),
                        onChanged: (String? value) {
                          controller.category.value = value!;
                        },
                      )
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 12.h)),
                Container(
                  child: ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: controller.items.value.length,
                      shrinkWrap: true,
                      itemBuilder: (context, index){
                        return
                          Container(
                            margin: EdgeInsets.only(top: 12.h),
                            padding: EdgeInsets.all(12),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                color: Color.fromRGBO(255, 255, 255, 1)
                            ),
                            child:
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('${controller.items.value[index].fullName}', style: TextStyle(color: Color.fromRGBO(51, 157, 255, 1), fontSize: 14, fontWeight: FontWeight.w500),),
                                Padding(padding: EdgeInsets.only(top: 12)),
                                Row(
                                  children: [
                                    Text("Đi học đúng giờ", style: TextStyle(color: Color.fromRGBO(133, 133, 133, 1), fontWeight: FontWeight.w600, fontSize: 12),),
                                    Expanded(child: Container()),
                                    Text("${controller.items.value[index].totalItemsOnTime} lần", style: TextStyle(color: Color.fromRGBO(26, 26, 26, 1),fontSize: 14, fontWeight: FontWeight.w400),)
                                  ],
                                ),
                                Padding(padding: EdgeInsets.only(top: 12)),
                                Row(
                                  children: [
                                    Text("Đi học muộn", style: TextStyle(color: Color.fromRGBO(133, 133, 133, 1), fontWeight: FontWeight.w600, fontSize: 12),),
                                    Expanded(child: Container()),
                                    Text("${controller.items.value[index].totalItemsNotOnTime} lần", style: TextStyle(color: Color.fromRGBO(26, 26, 26, 1),fontSize: 14, fontWeight: FontWeight.w400),)
                                  ],
                                ),
                                Padding(padding: EdgeInsets.only(top: 12)),
                                Row(
                                  children: [
                                    Text("Nghỉ học có phép", style: TextStyle(color: Color.fromRGBO(133, 133, 133, 1), fontWeight: FontWeight.w600, fontSize: 12),),
                                    Expanded(child: Container()),
                                    Text("${controller.items.value[index].totalStudentsExcusedAbsence} lần", style: TextStyle(color: Color.fromRGBO(26, 26, 26, 1),fontSize: 14, fontWeight: FontWeight.w400),)
                                  ],
                                ),
                                Padding(padding: EdgeInsets.only(top: 12)),
                                Row(
                                  children: [
                                    Text("Nghỉ học không phép", style: TextStyle(color: Color.fromRGBO(133, 133, 133, 1), fontWeight: FontWeight.w600, fontSize: 12),),
                                    Expanded(child: Container()),
                                    Text("${controller.items.value[index].totalStudentsAbsentWithoutLeave} lần", style: TextStyle(color: Color.fromRGBO(26, 26, 26, 1),fontSize: 14, fontWeight: FontWeight.w400),)
                                  ],
                                ),
                                Padding(padding: EdgeInsets.only(top: 12)),

                              ],
                            ),
                          );


                      }),
                )



              ],
            ),
          )),
        )
    );
  }
  selectDateTimeStart(stringTime, format,context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      if (value != null) {
        date =
            TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
      }
    });

    if (date.isNotEmpty) {
      controller.controllerdateStart.value.text = "$date";
      controller.dateStart.value = date;
      controller.getDiaryDiligenceTeacherClickDate(controller.dateStart.value, controller.dateEnd.value);

    }
    FocusScope.of(context).requestFocus(FocusNode());
  }

  selectDateTimeEnd(stringTime, format,context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      if (value != null) {
        date =
            TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
      }
    });

    if (date.isNotEmpty) {
      controller.controllerdateEnd.value.text = "$date";
      controller.dateEnd.value = date;
      controller.getDiaryDiligenceTeacherClickDate(controller.dateStart.value, controller.dateEnd.value);

    }
    FocusScope.of(context).requestFocus(FocusNode());
  }

}