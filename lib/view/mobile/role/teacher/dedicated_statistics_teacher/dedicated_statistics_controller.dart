import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/data/model/common/diligence.dart';
import 'package:task_manager/data/repository/diligence/diligence_repo.dart';
import 'package:task_manager/view/mobile/role/teacher/teacher_home_controller.dart';

import '../../../../../commom/utils/app_utils.dart';

class DedicatedStatisticsController extends GetxController{
  final category = ''.obs;
  var focusdateStart = FocusNode().obs;
  var controllerdateStart = TextEditingController().obs;
  var focusdateEnd = FocusNode().obs;
  var controllerdateEnd = TextEditingController().obs;
  var dateStart ="".obs;
  var dateEnd ="".obs;
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var dedicatedStastical = DedicatedStatisticalTeacher().obs;
  RxList<ItemsDedicatedStatistical> items = <ItemsDedicatedStatistical>[].obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var fromYearSelect ="".obs;
  var toYearSelect ="".obs;
  @override
  void onInit() {
    super.onInit();
    setDateSchoolYears();
    if(Get.find<TeacherHomeController>().fromYearPresent == DateTime.now().year || Get.find<TeacherHomeController>().toYearPresent == DateTime.now().year ){
      dateEnd.value =outputDateFormat.format(DateTime(findLastDateOfTheWeek(DateTime.now()).year, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
      dateStart.value =outputDateFormat.format(DateTime(findFirstDateOfTheWeek(DateTime.now()).year, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerdateStart.value.text =   outputDateFormat.format(DateTime(findFirstDateOfTheWeek(DateTime.now()).year, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerdateEnd.value.text = outputDateFormat.format(DateTime(findLastDateOfTheWeek(DateTime.now()).year, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));

    }else{
      dateEnd.value =outputDateFormat.format(DateTime(int.parse(toYearSelect.value.substring(0,4)), findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      dateStart.value =outputDateFormat.format(DateTime(int.parse(fromYearSelect.value.substring(0,4)), findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerdateStart.value.text = outputDateFormat.format(DateTime(int.parse(toYearSelect.value.substring(0,4)),findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerdateEnd.value.text = outputDateFormat.format(DateTime(int.parse(fromYearSelect.value.substring(0,4)), findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
    }

  }

  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }

  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime
        .add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }
  getDiaryDiligenceTeacherToday(){
    var classId = Get.find<TeacherHomeController>().currentClass.value.classId;
    var todate = DateTime(findLastDateOfTheWeek(DateTime.now()).year, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    var fromdate = DateTime(findFirstDateOfTheWeek(DateTime.now()).year, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    _diligenceRepo.getListDedicatedStatisticalTeacher(classId, fromdate, todate).then((value) {
      if (value.state == Status.SUCCESS) {
        dedicatedStastical.value= value.object!;
        items.value = dedicatedStastical.value.items!;
        AppUtils.shared.hideLoading();
        Future.delayed(Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }

  getDiaryDiligenceTeacherClickDate(fromdate,todate){
    var classId = Get.find<TeacherHomeController>().currentClass.value.classId;
    var fromdate = DateTime(int.parse(dateStart.value.substring(6,10)), int.parse(dateStart.value.substring(3,5)),int.parse(dateStart.value.substring(0,2))).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(dateEnd.value.substring(6,10)), int.parse(dateEnd.value.substring(3,5)),int.parse(dateEnd.value.substring(0,2))).millisecondsSinceEpoch;
    _diligenceRepo.getListDedicatedStatisticalTeacher(classId, fromdate, todate).then((value) {
      if (value.state == Status.SUCCESS) {
        dedicatedStastical.value= value.object!;
        items.value = dedicatedStastical.value.items!;
        AppUtils.shared.hideLoading();
        Future.delayed(Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }

  getDiaryDiligenceTeacherToYear(fromdate,todate){
    var classId = Get.find<TeacherHomeController>().currentClass.value.classId;
    var fromdate = DateTime(int.parse(toYearSelect.value.substring(0,4)), findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(fromYearSelect.value.substring(0,4)), findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    _diligenceRepo.getListDedicatedStatisticalTeacher(classId, fromdate, todate).then((value) {
      if (value.state == Status.SUCCESS) {
        dedicatedStastical.value= value.object!;
        items.value = dedicatedStastical.value.items!;
        AppUtils.shared.hideLoading();
        Future.delayed(Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }

  setDateSchoolYears() {
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (Get.find<TeacherHomeController>().fromYearPresent.value == DateTime.now().year) {
        getDiaryDiligenceTeacherToday();
        return;
      } else {
        fromYearSelect.value = "${Get.find<TeacherHomeController>().fromYearPresent}";
        toYearSelect.value = "${Get.find<TeacherHomeController>().toYearPresent}";
        getDiaryDiligenceTeacherToYear(fromYearSelect.value, toYearSelect.value);
      }
    } else {
      if (Get.find<TeacherHomeController>().toYearPresent.value == DateTime.now().year) {
        getDiaryDiligenceTeacherToday();
        return;
      } else {
        fromYearSelect.value = "${Get.find<TeacherHomeController>().toYearPresent}";
        toYearSelect.value = "${Get.find<TeacherHomeController>().fromYearPresent}";
        getDiaryDiligenceTeacherToYear(fromYearSelect.value, toYearSelect.value);
      }
    }
  }
}