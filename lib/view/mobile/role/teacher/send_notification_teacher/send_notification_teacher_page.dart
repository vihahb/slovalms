import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:task_manager/view/mobile/role/teacher/send_notification_teacher/send_notification_teacher_controller.dart';

class SendNotificationTeacherPage
    extends GetWidget<SendNotificationTeacherController> {
  final controller = Get.put(SendNotificationTeacherController());

  SendNotificationTeacherPage({super.key});
  @override
  Widget build(BuildContext context) {
    return Obx(() => SafeArea(
            child: Scaffold(
          appBar: AppBar(
            backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
            elevation: 0,
            title: Text(
              "Gửi Thông Báo",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w500),
            ),
            actions: [
              const Icon(
                Icons.home,
                color: Colors.white,
              ),
              Padding(padding: EdgeInsets.only(right: 16.w))
            ],
          ),
          body: Container(
            padding: const EdgeInsets.all(16),
            child: Column(
              children: [
                InkWell(
                  onTap: () {
                    controller.isExpandParents.value =
                        !controller.isExpandParents.value;
                  },
                  child: Card(
                    elevation: 1,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6)),
                    child: Container(
                      padding: const EdgeInsets.all(16),
                      child: Row(
                        children: [
                          Text(
                            "Cho Phụ Huynh",
                            style: TextStyle(
                                fontSize: 14.sp,
                                color: const Color.fromRGBO(26, 26, 26, 1),
                                fontWeight: FontWeight.w500),
                          ),
                          Expanded(child: Container()),
                          Visibility(
                            visible: controller.isExpandParents.value == false,
                            child: const Icon(
                              Icons.arrow_forward_ios,
                              color: Color.fromRGBO(248, 129, 37, 1),
                              size: 18,
                            ),
                          ),
                          Visibility(
                            visible: controller.isExpandParents.value == true,
                            child: const Icon(Icons.keyboard_arrow_up,
                                color: Color.fromRGBO(248, 129, 37, 1)),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Visibility(
                    visible: controller.isExpandParents.value == true,
                    child: Container(
                      padding: const EdgeInsets.all(8),
                      child: Column(
                        children: [
                          InkWell(
                            onTap: () {
                              controller.clickSMS.value = true;
                              controller.clickNotifyApp.value = false;
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  color: controller.clickSMS.value == true
                                      ? const Color.fromRGBO(248, 129, 37, 1)
                                      : const Color.fromRGBO(239, 239, 239, 1),
                                  borderRadius: BorderRadius.circular(12)),
                              padding: EdgeInsets.only(
                                  top: 8.h,
                                  left: 16.w,
                                  bottom: 8.h,
                                  right: 16.w),
                              child: Row(
                                children: [
                                  SvgPicture.asset(
                                    "assets/Images/icon_sms.svg",
                                    color: controller.clickSMS.value == true
                                        ? Colors.white
                                        : const Color.fromRGBO(90, 90, 90, 1),
                                  ),
                                  Text(
                                    "Gửi Thông Báo Qua SMS",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14.sp,
                                        color: controller.clickSMS.value == true
                                            ? Colors.white
                                            : const Color.fromRGBO(90, 90, 90, 1)),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          InkWell(
                            onTap: () {
                              controller.clickSMS.value = false;
                              controller.clickNotifyApp.value = true;
                              controller.goToListClasBySendNotificationInAppPage();
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  color: controller.clickNotifyApp.value == true
                                      ? const Color.fromRGBO(248, 129, 37, 1)
                                      : const Color.fromRGBO(239, 239, 239, 1),
                                  borderRadius: BorderRadius.circular(12)),
                              padding: EdgeInsets.only(
                                  top: 8.h,
                                  left: 16.w,
                                  bottom: 8.h,
                                  right: 16.w),
                              child: Row(
                                children: [
                                  SvgPicture.asset(
                                    "assets/Images/icon_send_notify_app.svg",
                                    color:
                                    controller.clickNotifyApp.value == true
                                            ? Colors.white
                                            : const Color.fromRGBO(90, 90, 90, 1),
                                  ),
                                  Text(
                                    "Gửi Thông Báo Trên Ứng Dụng",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14.sp,
                                        color: controller
                                                    .clickNotifyApp.value ==
                                                true
                                            ? Colors.white
                                            : const Color.fromRGBO(90, 90, 90, 1)),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    )),
                Padding(padding: EdgeInsets.only(top: 8.h)),
                InkWell(
                  onTap: () {},
                  child: Card(
                    elevation: 1,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6)),
                    child: Container(
                      padding: const EdgeInsets.all(16),
                      child: Row(
                        children: [
                          Text(
                            "Cho Học sinh",
                            style: TextStyle(
                                fontSize: 14.sp,
                                color: const Color.fromRGBO(26, 26, 26, 1),
                                fontWeight: FontWeight.w500),
                          ),
                          Expanded(child: Container()),
                          const Icon(
                            Icons.arrow_forward_ios,
                            color: Color.fromRGBO(248, 129, 37, 1),
                            size: 18,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 8.h)),
                InkWell(
                  onTap: () {},
                  child: Card(
                    elevation: 1,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6)),
                    child: Container(
                      padding: const EdgeInsets.all(16),
                      child: Row(
                        children: [
                          Text(
                            "Cho Phụ Huynh \& Học Sinh",
                            style: TextStyle(
                                fontSize: 14.sp,
                                color: const Color.fromRGBO(26, 26, 26, 1),
                                fontWeight: FontWeight.w500),
                          ),
                          Expanded(child: Container()),
                          const Icon(
                            Icons.arrow_forward_ios,
                            color: Color.fromRGBO(248, 129, 37, 1),
                            size: 18,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        )));
  }
}
