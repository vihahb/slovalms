
import 'package:get/get.dart';

import 'list_class_by_send_notification_in_app/list_class_by_send_notification_in_app_page.dart';

 class SendNotificationTeacherController extends GetxController{
 var isExpandParents = false.obs;
 var clickSMS = false.obs;
 var clickNotifyApp = false.obs;

 goToListClasBySendNotificationInAppPage(){
  Get.to(ListClasBySendNotificationInAppPage());
 }
 }