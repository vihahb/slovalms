import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'list_class_by_send_notification_in_app_controller.dart';


class ListClasBySendNotificationInAppPage extends GetWidget<ListClasBySendNotificationInAppController>{
  final controller = Get.put(ListClasBySendNotificationInAppController());
  @override
  Widget build(BuildContext context) {
  return  SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
          elevation: 0,
          title: Text(
            "Gửi Thông Báo Trên Ứng Dụng",
            style: TextStyle(
                color: Colors.white, fontSize: 16.sp, fontWeight: FontWeight.w500),
          ),
          actions: [
            const Icon(
              Icons.home,
              color: Colors.white,
            ),
            Padding(padding: EdgeInsets.only(right: 16.w))
          ],
        ),
        body: Container(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Danh Lớp Sách Nhận",
                style: TextStyle(
                    color: const Color.fromRGBO(177, 177, 177, 1),
                    fontSize: 12.sp,
                    fontWeight: FontWeight.w500),
              ),
              Padding(padding: EdgeInsets.only(top: 8.h)),
              Expanded(
                  child: ListView.builder(
                      itemCount: 3,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            controller.goToListClassBySendNotificationInAppPage();
                          },
                          child: Container(
                            margin: EdgeInsets.only(top: 12.h),
                            padding: EdgeInsets.zero,
                            child: Card(
                              margin: EdgeInsets.zero,
                              elevation: 1,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6)),
                              child: Container(
                                padding: const EdgeInsets.all(16),
                                child: Row(
                                  children: [
                                    Text(
                                      "Lớp 12A4",
                                      style: TextStyle(
                                          fontSize: 14.sp,
                                          color: const Color.fromRGBO(26, 26, 26, 1),
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Padding(padding: EdgeInsets.only(left: 8.w)),
                                    Visibility(
                                        visible: index == 0,
                                        child: const Icon(
                                          Icons.star,
                                          color: Color.fromRGBO(248, 129, 37, 1),
                                          size: 18,
                                        )),
                                    Expanded(child: Container()),
                                    Container(
                                      child:  Icon(
                                        Icons.arrow_forward_ios,
                                        color: Color.fromRGBO(248, 129, 37, 1),
                                        size: 18,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      }
                      )
              )
            ],
          ),
        ),
      ));
  }
}