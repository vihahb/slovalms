import 'package:get/get.dart';

import 'list_user_by_class_send_notification/list_user_by_class_send_notification_in_app_page.dart';


class ListClasBySendNotificationInAppController extends GetxController{
  void  goToListClassBySendNotificationInAppPage(){
    Get.to(ListUserByClassSendNotificationInAppPage());
  }

}