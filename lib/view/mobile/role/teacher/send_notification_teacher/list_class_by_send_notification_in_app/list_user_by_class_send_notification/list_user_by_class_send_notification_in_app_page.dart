
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'detail_send_notification_in_app_teacher/detail_send_notification_in_app_teacher_page.dart';
import 'list_user_by_class_send_notification_in_app_controller.dart';

class ListUserByClassSendNotificationInAppPage extends GetWidget<ListUserByClassSendNotificationInAppController>{
  final controller = Get.put(ListUserByClassSendNotificationInAppController());

  showDialogFilter(context) {
    showDialog(context: context, builder: (context){
      return 
       Obx(() =>  Dialog(
         insetPadding: EdgeInsets.only(bottom: 350.h, left: 70.w, right: 10.w),
         child: Container(
             padding: const EdgeInsets.all(12),
             decoration: BoxDecoration(
               color: Colors.white,
               borderRadius: BorderRadius.circular(6.r),
             ),
             child: IntrinsicHeight(
               child:Column(
                 crossAxisAlignment: CrossAxisAlignment.end,
                 children: [
                   Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     mainAxisSize: MainAxisSize.min,
                     children: [
                        Text(
                         "Lọc",
                         style: TextStyle(
                             color: Colors.black,
                             fontWeight: FontWeight.w500,
                             fontSize: 14.sp),
                       ),
                       Expanded(child: Container()),
                       InkWell(
                         onTap: () {
                           Get.back();
                         },
                         child: const Icon(
                           Icons.clear,
                           color: Colors.black,
                           size: 14,
                         ),
                       )
                     ],
                   ),
                   Padding(padding: EdgeInsets.only(top: 8.h)),
                   const Divider(),
                    Padding(padding: EdgeInsets.only(top: 8.h)),
                   Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: [
                       Container(
                         height: 30.h,
                         padding: EdgeInsets.fromLTRB(8.w, 4.h, 8.w, 4.h),
                         decoration: BoxDecoration(
                             border: Border.all(
                                 color: const Color.fromRGBO(
                                     192, 192, 192, 1)),
                             borderRadius: BorderRadius.circular(4.r)),
                         child: DropdownButtonHideUnderline(
                           child: DropdownButton(
                             iconSize: 0,
                             icon: const Visibility(
                                 visible: false,
                                 child: Icon(Icons.arrow_downward)),
                             elevation: 0,
                             borderRadius: BorderRadius.circular(10.r),
                             hint: controller.dropDownSort.value == null
                                 ? Row(
                               children:  [
                                 Text(
                                   'Sắp xếp theo  ',
                                   style: TextStyle(
                                       fontSize: 14.sp,
                                       fontWeight:
                                       FontWeight.w400,
                                       color: Colors.black),
                                 ),
                                 const Icon(
                                   Icons.keyboard_arrow_down,
                                   color: Colors.black,
                                   size: 18,
                                 )
                               ],
                             ) :
                             Row(
                               children:  [
                                 Text(
                                   '${controller.dropDownSort.value} ',
                                   style: TextStyle(
                                       fontSize: 14.sp,
                                       fontWeight:
                                       FontWeight.w400,
                                       color: Colors.black),
                                 ),
                                 const Icon(
                                   Icons.keyboard_arrow_down,
                                   color: Colors.black,
                                   size: 18,
                                 )
                               ],
                             ),
                             items: [
                               'Theo vần chữ cái',
                               'Ngẫu nhiên'
                             ].map(
                                   (value) {
                                 return DropdownMenuItem<String>(
                                   value: value,
                                   child: Text(
                                     value,
                                     style: TextStyle(
                                         fontSize: 14.sp,
                                         fontWeight: FontWeight.w400,
                                         color: Colors.black),
                                   ),
                                 );
                               },
                             ).toList(),
                             onChanged: (String? value) {
                               controller.dropDownSort.value = value!;
                             },
                           ),
                         ),
                       ),
                       Expanded(child: Container()),
                       Container(
                         height: 30,
                         padding: EdgeInsets.fromLTRB(8.w, 4.h, 8.w, 4.h),
                         decoration: BoxDecoration(
                             border: Border.all(
                                 color: const Color.fromRGBO(
                                     192, 192, 192, 1)),
                             borderRadius: BorderRadius.circular(4.r)),
                         child: DropdownButtonHideUnderline(
                           child: DropdownButton(
                             iconSize: 0,
                             elevation: 0,
                             borderRadius: BorderRadius.circular(10.r),
                             items: [controller.listSort.value, controller.listSort1.value].map(
                                   (list) {
                                 return DropdownMenuItem<
                                     List<String>>(
                                   value: list,
                                   child: Row(
                                     children: [
                                       Text(list[0],
                                           style: TextStyle(
                                               fontSize: 14.sp,
                                               fontWeight:
                                               FontWeight.w400,
                                               color: Colors.black)),
                                       Padding(
                                           padding: EdgeInsets.only(
                                               right: 4.w)),
                                       const Icon(
                                         Icons.arrow_forward,
                                         color: Color.fromRGBO(
                                             248, 129, 37, 1),
                                         size: 14,
                                       ),
                                       Padding(
                                           padding: EdgeInsets.only(
                                               left: 4.w)),
                                       Text(list[1],
                                           style: const TextStyle(
                                               fontSize: 14,
                                               fontWeight:
                                               FontWeight.w400,
                                               color: Colors.black)),
                                       Padding(
                                           padding: EdgeInsets.only(
                                               right: 8.w)),
                                       const Icon(
                                           Icons.keyboard_arrow_down,
                                           color: Colors.black,
                                           size: 18)
                                     ],
                                   ),
                                 );
                               },
                             ).toList(),
                             onChanged: (List<String>? value) {
                               controller.dropDownDetailSort.value = value!;
                             },
                             hint: controller.dropDownDetailSort.value  == null
                                 ?
                             Row(
                               children: [
                                 Row(
                                   children:  [
                                     Text("A",
                                         style: TextStyle(
                                             fontSize: 14.sp,
                                             fontWeight:
                                             FontWeight
                                                 .w400,
                                             color: Colors
                                                 .black)),
                                     Padding(
                                         padding:
                                         EdgeInsets.only(
                                             right: 4.w)),
                                     Icon(
                                       Icons.arrow_forward,
                                       color: const Color.fromRGBO(
                                           248, 129, 37, 1),
                                       size: 14.sp,
                                     ),
                                     Padding(
                                         padding:
                                         EdgeInsets.only(
                                             left: 4.w)),
                                     Text("Z",
                                         style: TextStyle(
                                             fontSize: 14.sp,
                                             fontWeight:
                                             FontWeight
                                                 .w400,
                                             color: Colors
                                                 .black)),
                                     Padding(
                                         padding:
                                         EdgeInsets.only(
                                             right: 8.w))
                                   ],
                                 ),
                                 const Icon(
                                     Icons.keyboard_arrow_down,
                                     color: Colors.black,
                                     size: 18)
                               ],
                             )
                                 : Row(
                               children: [
                                 Text(
                                    "A",
                                     style:  TextStyle(
                                         fontSize: 14.sp,
                                         fontWeight:
                                         FontWeight.w400,
                                         color: Colors.black)),
                                  Padding(
                                     padding: EdgeInsets.only(
                                         right: 4.w)),
                                 const Icon(
                                   Icons.arrow_forward,
                                   color: Color.fromRGBO(
                                       248, 129, 37, 1),
                                   size: 14,
                                 ),
                                  Padding(
                                     padding: EdgeInsets.only(
                                         left: 4.w)),
                                 Text(
                                     "Z",
                                     style:  TextStyle(
                                         fontSize: 14.sp,
                                         fontWeight:
                                         FontWeight.w400,
                                         color: Colors.black)),
                                 const Padding(
                                     padding: EdgeInsets.only(
                                         right: 8)),
                                 const Icon(
                                     Icons.keyboard_arrow_down,
                                     color: Colors.black,
                                     size: 18)
                               ],
                             ),
                           ),
                         ),
                       )
                     ],
                   ),
                    Padding(padding: EdgeInsets.only(top: 8.h)),
                   Row(
                     mainAxisAlignment: MainAxisAlignment.end,
                     children: [
                       Text(
                         "Làm Mới",
                         style: TextStyle(
                             color: const Color.fromRGBO(26, 59, 112, 1),
                             fontSize: 12.sp,
                             fontWeight: FontWeight.w400),
                       )
                     ],
                   )
                 ],
               ) ,
             )

         ),
       ));
    });

  }
  @override
  Widget build(BuildContext context) {
    return
     Obx(() =>  SafeArea(
         child: GestureDetector(
           onTap: (){
             FocusManager.instance.primaryFocus?.unfocus();
           },

           child: Scaffold(
             resizeToAvoidBottomInset: false,
             appBar: AppBar(
               backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
               elevation: 0,
               title: Text(
                 "Gửi Thông Báo Trên Ứng Dụng",
                 style: TextStyle(
                     color: Colors.white, fontSize: 16.sp, fontWeight: FontWeight.w500),
               ),
               actions: [
                 const Icon(
                   Icons.home,
                   color: Colors.white,
                 ),
                 Padding(padding: EdgeInsets.only(right: 16.w))
               ],
             ),
             body: Column(
               mainAxisAlignment: MainAxisAlignment.start,
               children: [
                 Container(
                   color: Colors.white,
                   padding: EdgeInsets.fromLTRB(16.w, 8.h, 16.w, 8.h),
                   child: Row(
                     children: [
                       Expanded(
                         child: SizedBox(
                           height: 40.h,
                           child: TextFormField(
                             decoration: InputDecoration(
                                 border: OutlineInputBorder(
                                     borderSide: const BorderSide(
                                         color: Color.fromRGBO(177, 177, 177, 1)),
                                     borderRadius: BorderRadius.circular(6)),
                                 prefixIcon: const Icon(
                                   Icons.search,
                                   color: Color.fromRGBO(177, 177, 177, 1),
                                 ),
                                 hintText: "Tìm kiếm",
                                 isCollapsed: true,
                                 hintStyle: TextStyle(
                                     fontSize: 14.sp,
                                     color: const Color.fromRGBO(177, 177, 177, 1)),
                                 focusedBorder: const OutlineInputBorder(
                                     borderSide: BorderSide(
                                         color: Color.fromRGBO(248, 129, 37, 1)))),
                             textAlignVertical: TextAlignVertical.center,
                             cursorColor: const Color.fromRGBO(248, 129, 37, 1),
                           ),
                         ),
                       ),
                       Padding(padding: EdgeInsets.only(left: 8.w)),
                       InkWell(
                         onTap: () {
                           Get.dialog(showDialogFilter(context));
                         },
                         child: Container(
                           height: 40,
                           padding: const EdgeInsets.all(10),
                           decoration: BoxDecoration(
                             borderRadius: BorderRadius.circular(6),
                             color: const Color.fromRGBO(248, 129, 37, 1),
                           ),
                           child: const Icon(
                             Icons.filter_alt,
                             color: Colors.white,
                           ),
                         ),
                       ),
                     ],
                   ),
                 ),
                 Container(
                   margin: EdgeInsets.only(left: 8.w),
                   child: Row(
                     children: [
                       TextButton(
                           onPressed: () {
                             controller.indexClick.value = 0;
                             controller.showColor(controller.indexClick.value);
                           },
                           child: Text(
                             "Tất Cả",
                             style: TextStyle(
                                 fontWeight: FontWeight.w500,
                                 fontSize: 14.sp,
                                 color:
                                 controller.indexClick.value == 0 ?
                                 const Color.fromRGBO(248, 129, 37, 1) :
                                 const Color.fromRGBO(177, 177, 177, 1)
                              ),
                           )),
                       Padding(padding: EdgeInsets.only(right: 16.w)),
                       TextButton(
                           onPressed: () {
                             controller.indexClick.value =1;
                             controller.showColor(controller.indexClick.value);
                           },
                           child: Text(
                             "Học Sinh",
                             style: TextStyle(
                                 fontWeight: FontWeight.w500,
                                 fontSize: 14.sp,
                                 color:
                                 controller.indexClick.value == 1 ?
                                 const Color.fromRGBO(248, 129, 37, 1) :
                                 const Color.fromRGBO(177, 177, 177, 1)),
                           )),
                       Padding(padding: EdgeInsets.only(right: 16.w)),
                       TextButton(
                           onPressed: () {
                             controller.indexClick.value =2;
                             controller.showColor(controller.indexClick.value);
                           },
                           child: Text(
                             "Phụ Huynh",
                             style: TextStyle(
                                 fontWeight: FontWeight.w500,
                                 fontSize: 14.sp,
                                 color:
                                 controller.indexClick.value == 2 ?
                                 const Color.fromRGBO(248, 129, 37, 1) :
                                 const Color.fromRGBO(177, 177, 177, 1)),
                           )),
                     ],
                   ),
                 ),
                 Expanded(
                   child: Container(
                     decoration: BoxDecoration(
                         color: Colors.white, borderRadius: BorderRadius.circular(6)),
                     padding: const EdgeInsets.all(16),
                     margin:  EdgeInsets.fromLTRB(16.w, 0, 16.w, 16.h),
                     child: Column(
                       children: [
                         Row(
                           children: [
                             InkWell(
                               onTap: () {
                                 controller.isCheckAll.value = !controller.isCheckAll.value;
                                 controller.checkAll();
                               },
                               child: Row(
                                 children: [
                                   Container(
                                       width: 20.w,
                                       height: 20.h,
                                       alignment: Alignment.center,
                                       padding: const EdgeInsets.all(2),
                                       decoration: ShapeDecoration(
                                         shape: CircleBorder(
                                             side: BorderSide(
                                                 color:
                                                 controller.isCheckAll.value?  const Color.fromRGBO(248, 129, 37, 1) : const Color.fromRGBO(235, 235, 235, 1)
                                             )),
                                       ),
                                       child:
                                       controller.isCheckAll.value?   const Icon(
                                         Icons.circle,
                                         size: 12,
                                         color: Color.fromRGBO(248, 129, 37, 1),
                                       ): null
                                   ),
                                   Padding(padding: EdgeInsets.only(left: 8.w)),
                                   Text(
                                     "Chọn hết",
                                     style: TextStyle(
                                         fontSize: 14.sp,
                                         fontWeight: FontWeight.w400,
                                         color: Colors.black),
                                   ),
                                 ],
                               ),
                             ),
                             Expanded(child: Container()),
                             Text(
                               "Đã chọn ${controller.selectedQuantity.value} /${controller.listUserStudent.length}",
                               style: TextStyle(
                                   color: const Color.fromRGBO(177, 177, 177, 1),
                                   fontSize: 12.sp,
                                   fontWeight: FontWeight.w500),
                             )
                           ],
                         ),
                         Padding(padding: EdgeInsets.only(top: 16.h)),
                         Expanded(
                           child: ListView.builder(
                               itemCount: controller.getList(controller.indexClick.value).length,
                               shrinkWrap: true,
                               physics: const ScrollPhysics(),
                               itemBuilder: (context, index) {
                                 return InkWell(
                                   onTap: () {
                                     controller.checkBoxListView(index);
                                       if(controller.listCheckBox.value.contains(false) == true){
                                         controller.isCheckAll.value = false;
                                       }else{
                                         controller.isCheckAll.value = true;
                                       }

                                   },
                                   child: Column(
                                     children: [
                                       Row(
                                         children: [
                                           Container(
                                               width: 20,
                                               height: 20,
                                               alignment: Alignment.center,
                                               padding: const EdgeInsets.all(2),
                                               decoration: ShapeDecoration(
                                                 shape: CircleBorder(
                                                     side: BorderSide(
                                                         color:
                                                         controller.listCheckBox.value[index] ?
                                                         const Color.fromRGBO(
                                                             248, 129, 37, 1) : const Color.fromRGBO(
                                                             235, 235, 235, 1)
                                                     )
                                                    ),
                                               ),
                                               child:
                                               controller.listCheckBox.value[index] ? const Icon(
                                                 Icons.circle,
                                                 size: 12,
                                                 color:
                                                 Color.fromRGBO(248, 129, 37, 1),
                                               ): null
                                           ),
                                           Padding(padding: EdgeInsets.only(left: 8.w)),
                                           Image.asset(controller.getAvatar(controller.indexClick.value,index)),
                                           Padding(padding: EdgeInsets.only(left: 8.w)),
                                           Column(
                                             crossAxisAlignment:
                                             CrossAxisAlignment.start,
                                             children: [
                                               Text(
                                                 "${controller.listUserStudent.value[index].name}",
                                                 style: TextStyle(
                                                     fontSize: 16.sp,
                                                     fontWeight: FontWeight.w500,
                                                     color: Colors.black),
                                               ),
                                               Row(
                                                 children: [
                                                   Text(
                                                     controller.getTitle(index),
                                                     style: TextStyle(
                                                         fontSize: 12.sp,
                                                         fontWeight: FontWeight.w400,
                                                         color: const Color.fromRGBO(
                                                             133, 133, 133, 1)),
                                                   ),
                                                   Text(
                                                     controller.getContent(index),
                                                     style: TextStyle(
                                                         fontSize: 12.sp,
                                                         fontWeight: FontWeight.w400,
                                                         color: Colors.black),
                                                   )
                                                 ],
                                               ),
                                             ],
                                           )
                                         ],
                                       ),
                                       const Divider(),
                                     ],
                                   ),
                                 );
                               }),
                         ),
                         Container(
                           margin: EdgeInsets.only(top: 8.h),
                           decoration: BoxDecoration(
                             border: Border.all(color: const Color.fromRGBO(239, 239, 239, 1),),
                           ),
                           height: 76.h,
                           child: ListView.builder(
                               itemCount: controller.getList(controller.indexClick.value).length,
                               physics: const ScrollPhysics(),
                               scrollDirection: Axis.horizontal,
                               itemBuilder: (context,index){
                                 return Container(
                                   margin: EdgeInsets.only(right: 16.w),
                                   child: Column(
                                     mainAxisAlignment: MainAxisAlignment.center,
                                     children: [
                                       Container(
                                         color: Colors.white,
                                         child: Stack(
                                           children: [
                                             Container(
                                               padding: const EdgeInsets.all(2),
                                               child: Image.asset("assets/images/avatar_recently_1.png",
                                               ),
                                             ),
                                             Positioned(
                                                 top: 0,
                                                 right: 0,
                                                 child: Container(
                                                   padding: EdgeInsets.zero,
                                                   decoration: const BoxDecoration(
                                                       color: Colors.white,
                                                       shape: BoxShape.circle
                                                   ),
                                                   child: SvgPicture.asset("assets/images/icon_dangerous.svg",fit: BoxFit.cover),
                                                 ))
                                           ],
                                         ) ,
                                       ),
                                       Container(
                                         margin: EdgeInsets.only(top: 8.h),
                                         child: const Text("Đặng Bình",style: TextStyle(color: Colors.black,fontWeight: FontWeight.w400,fontSize: 14),),
                                       )
                                     ],
                                   ),
                                 );
                               }),
                         )
                       ],
                     ),
                   ),),
                 Container(
                   width: MediaQuery.of(context).size.width,
                   padding: const EdgeInsets.all(16),
                   color: Colors.white,
                   child: SizedBox(
                     height: 46.h,
                     child: ElevatedButton(
                       onPressed: () {
                         Get.to(DetailSendInAppNotification());
                       },
                       style: ElevatedButton.styleFrom(
                           backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
                           shape: RoundedRectangleBorder(
                               borderRadius: BorderRadius.circular(6))),
                       child: Text("Tiếp Tục",
                           style: TextStyle(
                               color: Colors.white,
                               fontSize: 16.sp,
                               fontWeight: FontWeight.w400)),
                     ),
                   ),
                 )
               ],
             ),
           ),
         )));
  }

}