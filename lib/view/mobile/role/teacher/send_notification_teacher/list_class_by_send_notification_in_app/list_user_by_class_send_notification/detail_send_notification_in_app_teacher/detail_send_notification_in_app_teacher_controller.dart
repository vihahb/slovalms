import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class DetailSendNotificationInAppTeacherController extends GetxController{
  bool ischecked = false;
  var controllerTitle = TextEditingController().obs;
  var controllerContentNotification = TextEditingController().obs;
  var controllerTypeNotification = TextEditingController().obs;
  var focusTitle = FocusNode().obs;
  var focusContentNotification = FocusNode().obs;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  RxBool checkCorrectOrError = true.obs;
  RxBool enable = true.obs;
  RxBool setSubmit = false.obs;
  RxBool showHelptext = false.obs;
  RxString helperText = "".obs;
  RxBool labelText = false.obs;
  RxString dropdown = "".obs;
  String getTempIFSCValidation(String text) {
    return text.length > 7 ? "This line is helper text" : "";
  }
  final List<String> list_spn = ['Kế Hoạch Ôn Tập', 'Nộp Học Phí', 'Họp Phụ Huynh','Kiểm Điểm'];

}