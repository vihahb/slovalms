import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import '../../../../../../../../commom/utils/global.dart';
import '../../../../../../../../commom/widget/text_field_custom.dart';
import 'detail_send_notification_in_app_teacher_controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:dotted_border/dotted_border.dart';

class DetailSendInAppNotification
    extends GetWidget<DetailSendNotificationInAppTeacherController> {
  final controller = Get.put(DetailSendNotificationInAppTeacherController());
  bottomSheetTpyeNotification() {
    return Wrap(
      children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(32.r),
                topLeft: Radius.circular(32.r)),
            color: Colors.white,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6.r),
                    color: const Color.fromRGBO(210, 212, 216, 1)),
                height: 6.h,
                width: 48.w,
                alignment: Alignment.topCenter,
                margin: EdgeInsets.only(top: 16.h),
              ),
              Padding(padding: EdgeInsets.only(top: 12.h)),
              Container(
                margin: EdgeInsets.symmetric(vertical: 16.h, horizontal: 8.w),
                child: ListView.builder(
                    physics: const ScrollPhysics(),
                    itemCount: 4,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return InkWell(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 24.w),
                              child: Text(
                                "Kế hoạch nộp học phí",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(top: 16.h)),
                            Visibility(visible: index != 3, child: Divider()),
                          ],
                        ),
                        onTap: () {
                          controller.controllerTypeNotification.value.text =
                          "Kế hoạch nộp học phí";
                          Get.back();
                        },
                      );
                    }),
              ),
            ],
          ),
        )
      ],
    );
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        dismissKeyboard();
      },
      child: SafeArea(
          child: Scaffold(
              appBar: AppBar(
                backgroundColor: Color.fromRGBO(248, 129, 37, 1),
                elevation: 0,
                title: Text(
                  "Gửi Thông Báo Trên Ứng Dụng",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.sp,
                      fontWeight: FontWeight.w500),
                ),
                actions: [
                  const Icon(
                    Icons.home,
                    color: Colors.white,
                  ),
                  Padding(padding: EdgeInsets.only(right: 16.w))
                ],
              ),
              body: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    Expanded(
                        child: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(padding: EdgeInsets.only(top: 16.h)),
                              Container(
                                margin: EdgeInsets.only(left: 16.w),
                                child: Text(
                                  "Thông Báo",
                                  style: TextStyle(
                                      color: const Color.fromRGBO(177, 177, 177, 1),
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                              Padding(padding: EdgeInsets.only(top: 8.h)),
                              OutlineBorderTextFormField(
                                enable: true,
                                focusNode: controller.focusTitle.value,
                                iconPrefix: "",
                                iconSuffix: "",
                                state: StateType.DEFAULT,
                                labelText: "Tiêu Đề",
                                autofocus: false,
                                controller: controller.controllerTitle.value,
                                helperText: "",
                                showHelperText: false,
                                ishowIconPrefix: false,
                                textInputAction: TextInputAction.next,
                                keyboardType: TextInputType.text,
                                validation: (textToValidate) {
                                  return controller
                                      .getTempIFSCValidation(textToValidate);
                                },
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                padding: EdgeInsets.only(
                                    left: 16.w, bottom: 0.h, top: 0.h),
                                margin: EdgeInsets.symmetric(
                                    horizontal: 14.w, vertical: 12.h),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                    const BorderRadius.all(Radius.circular(6.0)),
                                    border: Border.all(
                                      width: 1,
                                      style: BorderStyle.solid,
                                      color: const Color.fromRGBO(192, 192, 192, 1),
                                    )),
                                child: TextFormField(
                                  onTap: () {
                                    Get.bottomSheet(bottomSheetTpyeNotification());
                                    FocusScope.of(context).nextFocus();
                                  },
                                  style: TextStyle(
                                    fontSize: 12.0.sp,
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                  ),
                                  controller:
                                  controller.controllerTypeNotification.value,
                                  decoration: InputDecoration(
                                    suffixIcon: const Icon(
                                      Icons.keyboard_arrow_down_outlined,
                                      color: Colors.black,
                                    ),
                                    label: const Text("Loại thông báo"),
                                    border: InputBorder.none,
                                    labelStyle: TextStyle(
                                      color: const Color.fromRGBO(192, 192, 192, 1),
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                padding: EdgeInsets.only(
                                    left: 16.w, bottom: 0.h, top: 0.h, right: 8.w),
                                margin: EdgeInsets.symmetric(horizontal: 14.w),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                    const BorderRadius.all(Radius.circular(6.0)),
                                    border: Border.all(
                                      width: 1,
                                      style: BorderStyle.solid,
                                      color: const Color.fromRGBO(192, 192, 192, 1),
                                    )),
                                child: TextFormField(
                                  keyboardType: TextInputType.multiline,
                                  maxLines: null,
                                  style: TextStyle(
                                    fontSize: 12.0.sp,
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                  ),
                                  controller:
                                  controller.controllerContentNotification.value,
                                  decoration: InputDecoration(
                                    suffixIcon: Container(
                                      margin: EdgeInsets.only(bottom: 4.h),
                                      child: SvgPicture.asset(
                                        "assets/images/icon_resizer.svg",
                                        height: 5,
                                        width: 5,
                                        fit: BoxFit.scaleDown,
                                        alignment: Alignment.bottomRight,
                                      ),
                                    ),
                                    label: const Text("Nội dung thông báo"),
                                    border: InputBorder.none,
                                    labelStyle: TextStyle(
                                      color: const Color.fromRGBO(192, 192, 192, 1),
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),
                              Padding(padding: EdgeInsets.only(top: 16.h)),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 16.w),
                                child: DottedBorder(
                                    dashPattern: [5, 5],
                                    radius: Radius.circular(6),
                                    borderType: BorderType.RRect,
                                    color: Color.fromRGBO(192, 192, 192, 1),
                                    child: Container(
                                      width: double.infinity,
                                      child: Column(
                                        children: [
                                          Padding(
                                              padding: EdgeInsets.only(top: 16.h)),
                                          const Icon(
                                            Icons.drive_folder_upload_rounded,
                                            color: Color.fromRGBO(248, 129, 37, 1),
                                            size: 40,
                                          ),
                                          Padding(padding: EdgeInsets.only(top: 4.h)),
                                          Text(
                                            "Tải lên Ảnh / Video",
                                            style: TextStyle(
                                                color: const Color.fromRGBO(
                                                    133, 133, 133, 1),
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          Padding(
                                              padding: EdgeInsets.only(top: 12.h)),
                                          Text(
                                            "File (Video, Ảnh, Zip,...) có dung lượng không quá 10Mb",
                                            style: TextStyle(
                                                color: const Color.fromRGBO(
                                                    133, 133, 133, 1),
                                                fontSize: 12.sp,
                                                fontWeight: FontWeight.w400),
                                          ),
                                          Padding(
                                              padding: EdgeInsets.only(bottom: 16.h)),
                                        ],
                                      ),
                                    )),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 16.w),
                                child: Row(
                                  children: [
                                    Text(
                                      "Danh Sách Người Nhận",
                                      style: TextStyle(
                                          color: const Color.fromRGBO(177, 177, 177, 1),
                                          fontSize: 12.sp,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Expanded(child: Container()),
                                    Card(
                                      shape: CircleBorder(
                                      ),
                                      elevation: 1,
                                      child: Container(
                                        padding: EdgeInsets.only(bottom: 12.h),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white,
                                        ),
                                        child:Icon(Icons.minimize,color: Color.fromRGBO(248, 129, 37, 1),),
                                      ),
                                    ),
                                    Card(
                                      shape: CircleBorder(
                                      ),
                                      elevation: 1,
                                      child: Container(

                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white,
                                        ),
                                        child:Icon(Icons.add_rounded,color: Color.fromRGBO(248, 129, 37, 1),),
                                      ),
                                    ),

                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 16.w),
                                child: ListView.builder(
                                    itemCount: 20,
                                    shrinkWrap: true,
                                    physics: const ScrollPhysics(),
                                    itemBuilder: (context, index) {
                                      return InkWell(
                                        child: Column(
                                          children: [
                                            Row(
                                              children: [
                                                Padding(padding: EdgeInsets.only(left: 8.w)),
                                                Image.asset("assets/images/avatar_person.png"),
                                                Padding(padding: EdgeInsets.only(left: 8.w)),
                                                Column(
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      "Đặng Văn Bình",
                                                      style: TextStyle(
                                                          fontSize: 16.sp,
                                                          fontWeight: FontWeight.w500,
                                                          color: Colors.black),
                                                    ),
                                                    Row(
                                                      children: [
                                                        Text(
                                                          "Phụ huynh:",
                                                          style: TextStyle(
                                                              fontSize: 12.sp,
                                                              fontWeight: FontWeight.w400,
                                                              color: const Color.fromRGBO(
                                                                  133, 133, 133, 1)),
                                                        ),
                                                        Padding(padding: EdgeInsets.only(top: 4.h)),
                                                        Text(
                                                          "Nguyễn Cuối Tuần",
                                                          style: TextStyle(
                                                              fontSize: 12.sp,
                                                              fontWeight: FontWeight.w400,
                                                              color: Colors.black),
                                                        )
                                                      ],
                                                    ),
                                                  ],
                                                )
                                              ],
                                            ),
                                            const Divider(),
                                          ],
                                        ),
                                      );
                                    }),
                              )
                            ],
                          ),
                        )),
                    Container(
                      color: const Color.fromRGBO(255, 255, 255, 1),
                      height: 78,
                      padding: const EdgeInsets.all(16),
                      child: SizedBox(
                        width: double.infinity,
                        height: 46,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor:
                              const Color.fromRGBO(248, 129, 37, 1),
                            ),
                            onPressed: () {},
                            child: const Text(
                              'Gửi',
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 1),
                                  fontSize: 16),
                            )),
                      ),
                    )
                  ],
                ),
              ))),
    );
  }


}
