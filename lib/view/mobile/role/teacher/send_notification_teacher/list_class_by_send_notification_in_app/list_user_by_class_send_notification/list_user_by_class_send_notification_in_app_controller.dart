import 'dart:ui';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../data/model/common/detail_item_list_student.dart';

class ListUserByClassSendNotificationInAppController  extends GetxController{
  var indexClick = 0.obs;
  var click = <bool>[].obs;
  var selectedQuantity = 0.obs;
 var  clickAll = <bool>[].obs;
var listCheckBox = <bool>[].obs;
  var isCheckAll = false.obs;
  var ischecker = false.obs;
  var dropDownSort =''.obs;
  var dropDownDetailSort= <String>[].obs;
var listSort = ["A", "Z"].obs;
 var listSort1 =["Z", "A"].obs;


  final listUserAll = <User>[
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.student),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.student),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.student),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.student),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.student),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
  ].obs;

  final listUserStudent = <User>[
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.student),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.student),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.student),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.student),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.student),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
  ].obs;

  final listUserParent = <User>[
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.student),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.student),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.student),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.student),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.student),
    User(name: "Đặng Văn Bình",phone: 0969804506,role: Role.parents),
  ].obs;
  getList(index){
    switch(index){
      case 0:
        return listUserAll.value;
      case 1:
        return listUserStudent.value;
      case 2:
        return listUserParent.value;
    }
  }

  getAvatar(index,indexItem){
    switch(index){
      case 1:
        return "assets/images/avatar_person.png";
      case 2:
        return "assets/images/imageProfile.png";
      case 0:
        switch(listUserAll[indexItem].role){
          case Role.student:
            return "assets/images/image_avatar_teacher.png";
          case Role.parents:
            return "assets/images/avatar_person.png";
          case Role.teacher:
            return "assets/images/imageProfile.png";
        }

    }
  }
  getTitle(index) {
    switch (getList(indexClick.value)[index].role) {
      case Role.parents:
        return "Phụ huynh hs: ";
      case Role.student:
        return "Phụ huynh: ";
      case Role.teacher:
        return "Giáo viên môn: ";
    }
  }
  getContent(index) {
    switch (getList(indexClick.value)[index].role) {
      case Role.parents:
        return "Đặng Văn Bình 1";
      case Role.student:
        return "Đặng Văn B";
      case Role.teacher:
        return "Toán";
    }
  }

@override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
      for (int i = 0; i < 3; i++) {
        click.value.add(false);
      }
      click.value[indexClick.value] = true;
      for (int i = 0; i < listUserStudent.length; i++) {
        listCheckBox.value.add(false);
      }
      print(listCheckBox.value[0]);

  }

  showColor(index) {
    for (int i = 0; i < 3; i++) {
      click.value.add(false);
    }
    click.value[index] = true;
  }

  checkBoxListView(index) {
    if (listCheckBox[index] == false) {
      listCheckBox[index] = true;
      selectedQuantity++;
    } else {
      listCheckBox[index] = false;
      selectedQuantity--;
    }
  }
  checkAll(){
    if(isCheckAll.value == true){
      listCheckBox.value = [];
      for (int i = 0; i < listUserStudent.length; i++) {
        listCheckBox.value.add(true);
      }
      selectedQuantity.value = listUserStudent.value.length;
    }else{
      listCheckBox.value = [];
      for (int i = 0; i < listUserStudent.value.length; i++) {
        listCheckBox.value.add(false);
      }
      selectedQuantity.value = 0;
    }
  }


}
enum Role { student, teacher, parents }