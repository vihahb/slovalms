import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:task_manager/commom/widget/appointment_builder_timelineday.dart';
import 'package:task_manager/commom/widget/meeting_data_source.dart';
import 'package:task_manager/view/mobile/role/teacher/teacher_home_controller.dart';
import '../../../../commom/app_cache.dart';
import '../../../../routes/app_pages.dart';
import '../../schedule/schedule_controller.dart';
import 'diligent_management_teacher/diligent_management_teacher_controller.dart';
import 'diligent_management_teacher/diligent_management_teacher_page.dart';

class TeacherHomePage extends GetWidget<TeacherHomeController> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final controller = Get.put(TeacherHomeController());

  TeacherHomePage({super.key});
  _onBottomSheetLogout() {
    return Wrap(
      children: [
        Container(
          margin: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  height: 50,
                  padding:
                      EdgeInsets.only(top: 15.h, left: 10.w, right: 10.w, bottom: 15.h),
                  decoration: const ShapeDecoration(
                      color: Color.fromRGBO(248, 129, 37, 1),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(6),
                            topRight: Radius.circular(6)),
                      )),
                  child: const SizedBox(
                    width: double.infinity,
                    child: Text("Đăng Xuất",
                        style: TextStyle(color: Colors.white)),
                  )),
              Container(
                  color: Colors.white,
                  width: double.infinity,
                  height: 57,
                  alignment: Alignment.center,
                  child: const Text("Bạn có muốn đăng xuất không?")),
              Row(
                children: [
                  Expanded(
                      child: SizedBox(
                    height: 50,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.grey,
                          shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(6)))),
                      onPressed: () {
                        Get.back();
                      },
                      child: const Text(
                        "Hủy",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  )),
                  Expanded(
                    child: SizedBox(
                      height: 50,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.red,
                            shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(6)))),
                        onPressed: () {
                          controller.logout();
                        },
                        child: const Text(
                          "Đăng Xuất",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      drawerEdgeDragWidth: 0,
      key: _scaffoldKey,
      drawer: Container(
        margin: const EdgeInsets.only(right: 28),
        width: MediaQuery.of(context).size.width,
        child: Obx(() => Drawer(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: const Color.fromRGBO(248, 129, 37, 1),
                        borderRadius: BorderRadius.circular(6),
                      ),
                      margin: const EdgeInsets.all(16),
                      padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                      child: Row(
                        children: [
                        InkWell(
                          onTap: (){
                            controller.goToDetailAvatar();
                          },
                          child:   SizedBox(
                            width: 40.w,
                            height: 40.w,
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              backgroundImage: Image.network(
                                controller.userProfile.value.image ??
                                    "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                                errorBuilder:
                                    (context, object, stackTrace) {
                                  return Image.asset(
                                    "assets/images/img_Noavt.png",
                                  );
                                },
                              ).image,
                            ),
                          ),
                        ),
                          const Padding(padding: EdgeInsets.only(left: 16)),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${controller.userProfile.value.fullName}",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w700),
                                ),
                                Text(
                                  "${controller.userProfile.value.email}",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 11.sp,
                                      fontWeight: FontWeight.w400),
                                )
                              ],
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              Get.back();
                            },
                            icon: const Icon(
                                Icons.keyboard_double_arrow_left_outlined),
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                    const Padding(padding: EdgeInsets.only(top: 16)),
                    Container(
                      margin: const EdgeInsets.only(left: 16, right: 16),
                      child: Row(
                        children: [
                          Text(
                            'Thông Tin Cá Nhân',
                            style: TextStyle(
                                color: const Color.fromRGBO(133, 133, 133, 1),
                                fontSize: 12.sp),
                          ),
                          Expanded(child: Container()),
                          InkWell(
                            onTap: () {
                              controller.goToUpdateInfoUser();
                              print("${controller.userProfile.value.fullName}");
                            },
                            child:
                        SizedBox(
                          height: 30.h,
                          width: 79.w,
                          child:     Row(
                            children: [
                              Text('Chỉnh sửa',
                                  style: TextStyle(
                                      color: const Color.fromRGBO(
                                          248, 129, 37, 1),
                                      fontSize: 12.sp,
                                      fontFamily:
                                      'static/Inter-Regular.ttf')),
                              const Icon(
                                Icons.mode_edit_outline_outlined,
                                color: Color.fromRGBO(248, 129, 37, 1),
                                size: 16,
                              )
                            ],
                          ),
                        ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin:
                          const EdgeInsets.only(left: 16, right: 16, top: 16),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              SizedBox(
                                width: 100.w,
                                child: Text(
                                  'Họ và tên: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14.sp,
                                      fontFamily: 'static/Inter-Medium.ttf'),
                                ),
                              ),
                              Text(
                                '${controller.userProfile.value.fullName}',
                                style: TextStyle(
                                    color:
                                        const Color.fromRGBO(248, 129, 37, 1),
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'static/Inter-Medium.ttf'),
                              )
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 10.h)),
                          Row(
                            children: [
                              SizedBox(
                                width: 100.w,
                                child: Text(
                                  'Số Điện Thoại: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14.sp,
                                      fontFamily: 'static/Inter-Medium.ttf'),
                                ),
                              ),
                              Text(
                                '${controller.userProfile.value.phone}',
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'static/Inter-Medium.ttf'),
                              )
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(top: 10)),
                          Row(
                            children: [
                              SizedBox(
                                width: 100.w,
                                child: Text(
                                  'Email: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14.sp,
                                      fontFamily: 'static/Inter-Medium.ttf'),
                                ),
                              ),
                              Text(
                                '${controller.userProfile.value.email}',
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'static/Inter-Medium.ttf'),
                              )
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(top: 10)),
                          Row(
                            children: [
                              SizedBox(
                                width: 100.w,
                                child: Text(
                                  'Trường : ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14.sp,
                                      fontFamily: 'static/Inter-Medium.ttf'),
                                ),
                              ),
                              Text(
                                '${controller.school.value.name}',
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'static/Inter-Medium.ttf'),
                              )
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(top: 10)),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: 100.w,
                                child: Text(
                                  'Chức vụ: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14.sp,
                                      fontFamily: 'static/Inter-Medium.ttf'),
                                ),
                              ),
                              Expanded(child:
                                ListView.builder(
                                  shrinkWrap: true,
                                    itemCount:  controller.userProfile.value.item!.teachers?.length,
                                    itemBuilder: (context, index){
                                      if(
                                      controller.userProfile.value.item!.teachers!.isNotEmpty
                                      ){
                                        controller.listclass.value = controller.userProfile.value.item!.teachers![index].clazz!;
                                      }
                                      return
                                        RichText(
                                          textAlign: TextAlign.start,
                                          text: TextSpan(
                                            text: "${controller.getType(controller.userProfile.value.item!.teachers?[index].type, controller.userProfile.value.item!.teachers?[index].subjectName)} ",
                                            style: TextStyle(
                                                color: const Color
                                                    .fromRGBO(26,
                                                    26, 26, 1),
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: 'static/Inter-Medium.ttf'),
                                            children:
                                            controller.listclass.value.map((e) {
                                              var index = controller.listclass.value.indexOf(e);
                                              var showSplit = ", ";
                                              if (index == controller.listclass.value.length - 1) {
                                                showSplit = "";
                                              }
                                              return TextSpan(
                                                  text: "${controller.listclass.value[index].name}$showSplit",
                                                  style: TextStyle(
                                                      color: const Color.fromRGBO(248, 129, 37, 1),
                                                      fontSize: 14.sp,
                                                      fontWeight: FontWeight.w500,
                                                      fontFamily: 'static/Inter-Medium.ttf'));
                                            }).toList(),
                                          ),
                                        );
                                    }

                                ),
                              ),
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(top: 10)),
                        ],
                      ),
                    ),
                    Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(
                              left: 16, right: 16, top: 24),
                          child: Row(
                            children: [
                              const Text(
                                'Cài Đặt',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Color.fromRGBO(133, 133, 133, 1),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'static/Inter-Medium.ttf'),
                              ),
                              Expanded(child: Container())
                            ],
                          ),
                        ),
                        const Padding(padding: EdgeInsets.only(top: 12)),
                        Container(
                          margin: const EdgeInsets.only(left: 16),
                          child: Column(
                            children: [
                             InkWell(
                               onTap: () {
                                 controller.getStaticPage("SUPPORT");
                               },
                               child:  Row(
                               children: [
                                 const Text(
                                   'Hỗ Trợ',
                                   style: TextStyle(
                                       color: Color.fromRGBO(24, 29, 39, 1),
                                       fontSize: 14,
                                       fontWeight: FontWeight.w500,
                                       fontFamily: 'static/Inter-Medium.ttf'),
                                 ),
                                 Expanded(child: Container()),
                                 IconButton(
                                     iconSize: 24,
                                     onPressed: () {},
                                     icon: const Icon(
                                       Icons.navigate_next,
                                       color: Colors.black,
                                     ))
                               ],
                             ),),
                             InkWell(
                               onTap: (){
                                 controller.goToChangePassPage();
                               },
                               child:  Row(
                                 children: [
                                   const Text(
                                     'Đổi Mật Khẩu ',
                                     style: TextStyle(
                                         color: Color.fromRGBO(24, 29, 39, 1),
                                         fontSize: 14,
                                         fontWeight: FontWeight.w500,
                                         fontFamily: 'static/Inter-Medium.ttf'),
                                   ),
                                   Expanded(child: Container()),
                                   IconButton(
                                       iconSize: 24,
                                       onPressed: () {},
                                       icon: const Icon(
                                         Icons.navigate_next,
                                         color: Colors.black,
                                       ))
                                 ],
                               ),
                             ),
                             InkWell(
                               onTap: () {
                                 controller.getStaticPage("RULES");
                               },
                               child:  Row(
                               children: [
                                 const Text(
                                   'Chính Sách Bảo Mật',
                                   style: TextStyle(
                                       color: Color.fromRGBO(24, 29, 39, 1),
                                       fontSize: 14,
                                       fontWeight: FontWeight.w500,
                                       fontFamily: 'static/Inter-Medium.ttf'),
                                 ),
                                 Expanded(child: Container()),
                                 IconButton(
                                     iconSize: 24,
                                     onPressed: () {},
                                     icon: const Icon(
                                       Icons.navigate_next,
                                       color: Colors.black,
                                     ))
                               ],
                             ),),
                             InkWell(
                               onTap: () {
                                 controller.getStaticPage("SECURITY");
                               },
                               child:  Row(
                               children: [
                                 const Text(
                                   'Điều Khoản Sử Dụng',
                                   style: TextStyle(
                                       color: Color.fromRGBO(24, 29, 39, 1),
                                       fontSize: 14,
                                       fontWeight: FontWeight.w500,
                                       fontFamily: 'static/Inter-Medium.ttf'),
                                 ),
                                 Expanded(child: Container()),
                                 IconButton(
                                     iconSize: 24,
                                     onPressed: () {},
                                     icon: const Icon(
                                       Icons.navigate_next,
                                       color: Colors.black,
                                     ))
                               ],
                             ),),
                              InkWell(
                                onTap: () {
                                  Get.bottomSheet(_onBottomSheetLogout());
                                },
                                child: Row(
                                  children: [
                                    const Text(
                                      'Đăng Xuất',
                                      style: TextStyle(
                                          color: Color.fromRGBO(24, 29, 39, 1),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          fontFamily:
                                              'static/Inter-Medium.ttf'),
                                    ),
                                    Expanded(child: Container()),
                                    IconButton(
                                        iconSize: 24,
                                        onPressed: () {},
                                        icon: const Icon(
                                          Icons.navigate_next,
                                          color: Colors.black,
                                        ))
                                  ],
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            )),
      ),

      body: Obx(() => (controller.isReady.value)?RefreshIndicator(
        color: const Color.fromRGBO(248, 129, 37, 1),
          child: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        child:  Column(
          children: [
            Container(
              color: Colors.white,
              padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
              child: Row(
                children: [
                  InkWell(
                    onTap: () {
                      _scaffoldKey.currentState?.openDrawer();
                    },
                    child: SizedBox(
                      height: 32.h,
                      width: 32.w,
                      child: Image.asset(
                          "assets/images/icon_more_teacher.png"),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(left: 8.w)),
                  buildClassOffice(),
                  Padding(padding: EdgeInsets.only(left: 8.w)),
                  Container(
                    height: 32.w,
                    width: 32.w,
                    padding: const EdgeInsets.all(4),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: const Color.fromRGBO(248, 129, 37, 1),
                    ),
                    child: const Icon(
                      Icons.filter_alt,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(padding: EdgeInsets.only(top: 16)),
                  Container(
                    margin: const EdgeInsets.only(left: 16),
                    child: const Text(
                      "Quản Lý Lớp Học",
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 20,
                          color: Colors.black),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Get.bottomSheet(
                          StatefulBuilder(builder: (context,state){
                            return  Wrap(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(32.r),
                                        topLeft: Radius.circular(32.r)),
                                    color: Colors.white,
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(6.r),
                                            color: const Color.fromRGBO(210, 212, 216, 1)),
                                        height: 6.h,
                                        width: 48.w,
                                        alignment: Alignment.topCenter,
                                        margin: EdgeInsets.only(top: 16.h),
                                      ),
                                      Padding(padding: EdgeInsets.only(top: 12.h)),
                                      Text("Năm học",style: TextStyle(color: const Color.fromRGBO(23, 32, 63, 1,),fontSize: 16.sp,fontWeight: FontWeight.w500),),
                                      controller.fromYearPresent.value != 0?Container(
                                        margin: EdgeInsets.symmetric(vertical: 16.h, horizontal: 8.w),
                                        child: ListView.builder(
                                            physics: const ScrollPhysics(),
                                            itemCount: controller.schoolYears.value.length,
                                            shrinkWrap: true,
                                            itemBuilder: (context,index) {
                                              return InkWell(
                                                child: Container(
                                                  color:  controller.clickSchoolYear.value[index] == true?Color.fromRGBO(254, 230, 211, 1):Colors.white,
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Padding(padding: EdgeInsets.only(top: 16.h)),
                                                      Row(
                                                        children: [
                                                          Text(
                                                            "Năm học ",
                                                            style: TextStyle(
                                                                color: controller.clickSchoolYear.value[index] == true?Color.fromRGBO(248, 129, 37, 1):Colors.black,
                                                                fontSize: 14.sp,
                                                                fontWeight: FontWeight.w500),
                                                          ),
                                                          Text(
                                                            "${controller.schoolYears.value[index].fromYear}",
                                                            style: TextStyle(
                                                                color: controller.clickSchoolYear.value[index] == true?Color.fromRGBO(248, 129, 37, 1):Colors.black,
                                                                fontSize: 14.sp,
                                                                fontWeight: FontWeight.w500),
                                                          ),
                                                          Text(
                                                            "-",
                                                            style: TextStyle(
                                                                color: controller.clickSchoolYear.value[index] == true?Color.fromRGBO(248, 129, 37, 1):Colors.black,
                                                                fontSize: 14.sp,
                                                                fontWeight: FontWeight.w500),
                                                          ),
                                                          Text(
                                                            "${controller.schoolYears.value[index].toYear}",
                                                            style: TextStyle(
                                                                color: controller.clickSchoolYear.value[index] == true?Color.fromRGBO(248, 129, 37, 1):Colors.black,
                                                                fontSize: 14.sp,
                                                                fontWeight: FontWeight.w500),
                                                          ),
                                                        ],
                                                      ),
                                                      Padding(padding: EdgeInsets.only(top: 16.h)),
                                                      Visibility(visible: index != 3, child: const Divider(
                                                        height: 0,
                                                        indent: 0,
                                                        thickness: 1,
                                                      )),
                                                    ],
                                                  ),
                                                ),
                                                onTap: () {
                                                  updated(state,index);
                                                  controller.onSelectedSchoolYears(index);

                                                },
                                              );
                                            }),
                                      ):Container()
                                    ],
                                  ),
                                )
                              ],
                            );
                          }));
                    },
                    child: Container(
                      margin: const EdgeInsets.only(left: 16),
                      child: Row(
                        children: [
                          Text(
                            "Năm học ",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w500),
                          ),
                          Text(
                            "${controller.fromYearPresent.value}",
                            style: TextStyle(
                                fontSize: 14.sp,
                                color: controller.clickSchoolYear.value.contains(true) == true ?Color.fromRGBO(248, 129, 37, 1):Colors.black,
                                fontWeight: FontWeight.w400),
                          ),
                          Text(
                            "-",
                            style: TextStyle(
                                fontSize: 14.sp,
                                color: controller.clickSchoolYear.value.contains(true) == true ?Color.fromRGBO(248, 129, 37, 1):Colors.black,
                                fontWeight: FontWeight.w400),
                          ),
                          Text(
                            "${controller.toYearPresent.value}",
                            style: TextStyle(
                                fontSize: 14.sp,
                                color: controller.clickSchoolYear.value.contains(true) == true ?Color.fromRGBO(248, 129, 37, 1):Colors.black,
                                fontWeight: FontWeight.w400),
                          ),
                          Padding(padding: EdgeInsets.only(left: 8.w)),
                          Icon(
                            Icons.keyboard_arrow_down_outlined,
                            size: 18.w,
                            color: Colors.black,
                          )
                        ],
                      ),
                    ),
                  ),
                  const Padding(padding: EdgeInsets.only(top: 16)),
                  Container(
                    margin: const EdgeInsets.only(left: 16, right: 16),
                    child: Row(
                      children: [
                        Expanded(
                            child: InkWell(
                              onTap: () {
                                Get.toNamed(Routes.diligentManagementTeacher);
                                if(Get.isRegistered<DiligenceManagementTeacherController>()){
                                  Get.find<DiligenceManagementTeacherController>().indexClick.value = 0;
                                  Get.find<DiligenceManagementTeacherController>().update();
                                }
                              },
                              child: Card(
                                color: Colors.white,
                                margin: EdgeInsets.zero,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(6)),
                                elevation: 3,
                                child: Container(
                                  padding: const EdgeInsets.all(8),
                                  child: Column(
                                    children: [
                                   SizedBox(
                                     width: 100.w,
                                     height: 100.h,
                                     child:    Image.asset(
                                         "assets/images/img_amicroteacher.png"),
                                   ),
                                      const Padding(
                                          padding: EdgeInsets.only(top: 8)),
                                      const Text("Quản Lý Chuyên Cần",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12,
                                              color: Colors.black))
                                    ],
                                  ),
                                ),
                              ),
                            )),
                        const Padding(padding: EdgeInsets.only(left: 16)),
                        Expanded(
                            child: InkWell(
                                onTap: () {},
                                child: Card(
                                  color: Colors.white,
                                  elevation: 3,
                                  margin: EdgeInsets.zero,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                      BorderRadius.circular(6)),
                                  child: Container(
                                    padding: const EdgeInsets.all(8),
                                    child: Column(
                                      children: [
                                        SizedBox(
                                          height: 100.h,
                                          width: 100.w,
                                          child:
                                          Image.asset(
                                              "assets/images/pana.png"),
                                        ),
                                        const Padding(
                                            padding:
                                            EdgeInsets.only(top: 8)),
                                        const Text("Quản Lý Học tập",
                                            style: TextStyle(
                                                fontWeight: FontWeight.w500,
                                                fontSize: 12,
                                                color: Colors.black))
                                      ],
                                    ),
                                  ),
                                ))),
                      ],
                    ),
                  ),
                  const Padding(padding: EdgeInsets.only(top: 16)),
                  Container(
                    margin: const EdgeInsets.only(left: 16, right: 16),
                    child: Row(
                      children: [
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              controller.goToSendNotificationPage();
                            },
                            child: Card(
                              color: Colors.white,
                              margin: EdgeInsets.zero,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6)),
                              elevation: 3,
                              child: Container(
                                padding: const EdgeInsets.all(8),
                                child: Column(
                                  children: [
                                   SizedBox(
                                     height: 100.h,
                                     width: 100.w,
                                     child:  Image.asset(
                                         "assets/images/img_sendnotify.png"),
                                   ),
                                    const Padding(
                                        padding: EdgeInsets.only(top: 8)),
                                    const Text("Gửi thông báo",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12,
                                            color: Colors.black))
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        const Padding(padding: EdgeInsets.only(left: 16)),
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              controller.goToStudentListPage();
                            },
                            child: Card(
                              color: Colors.white,
                              elevation: 3,
                              margin: EdgeInsets.zero,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6)),
                              child: Container(
                                padding: const EdgeInsets.all(8),
                                child: Column(
                                  children: [
                                   SizedBox(
                                     width: 100.w,
                                     height: 100.h,
                                     child:  Image.asset(
                                         "assets/images/img_list_student.png"),
                                   ),
                                    const Padding(
                                        padding: EdgeInsets.only(top: 8)),
                                    const Text("Danh Sách Học Sinh",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12,
                                            color: Colors.black))
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  const Padding(padding: EdgeInsets.only(bottom: 16)),
                ],
              ),
            ),
            const Padding(padding: EdgeInsets.only(top: 16)),
            Container(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(padding: EdgeInsets.only(top: 16)),
                  Container(
                    margin: const EdgeInsets.only(left: 16),
                    child: const Text(
                      "Thông tin từ nhà trường",
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 20,
                          color: Colors.black),
                    ),
                  ),
                  const Padding(padding: EdgeInsets.only(top: 16)),
                  Container(
                    margin: const EdgeInsets.only(left: 16, right: 16),
                    child: Row(
                      children: [
                        Expanded(
                          child: Card(
                            color: Colors.white,
                            margin: EdgeInsets.zero,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6)),
                            elevation: 3,
                            child: Container(
                              padding: const EdgeInsets.all(8),
                              child: Column(
                                children: [
                           SizedBox(
                             height: 100.h,
                             width: 100.w,
                             child:Image.asset(
                                 "assets/images/img_event.png"),
                           ),
                                  const Padding(
                                      padding: EdgeInsets.only(top: 8)),
                                  const Text("Tin tức sự kiện",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 12,
                                          color: Colors.black))
                                ],
                              ),
                            ),
                          ),
                        ),
                        const Padding(padding: EdgeInsets.only(left: 16)),
                        Expanded(
                          child: Card(
                            color: Colors.white,
                            elevation: 3,
                            margin: EdgeInsets.zero,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6)),
                            child: Container(
                              padding: const EdgeInsets.all(8),
                              child: Column(
                                children: [
                                  SizedBox(
                                    width: 100.w,
                                    height: 100.h,
                                    child: Image.asset(
                                        "assets/images/img_notify.png"),
                                  ),
                                  const Padding(
                                      padding: EdgeInsets.only(top: 8)),
                                  const Text("Thông Báo",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 12,
                                          color: Colors.black))
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  const Padding(padding: EdgeInsets.only(bottom: 16)),
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                color: Colors.white,
              ),
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.fromLTRB(16, 20, 16, 6),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          children: [
                            const SizedBox(
                              width: 200,
                              child: Text("Thời khóa biểu",
                                  style: TextStyle(
                                      fontSize: 20,
                                      color:
                                      Color.fromRGBO(26, 26, 26, 1))),
                            ),
                            const Padding(padding: EdgeInsets.only(top: 6)),
                        Visibility(
                          visible: controller.isSubjectToday.value,
                          child:
                        SizedBox(
                          width: 200,
                          child: Text(
                            "Hôm nay bạn có ${controller.scheduleToday.value.length} tiết dạy",
                            style: const TextStyle(
                                fontSize: 12,
                                color:
                                Color.fromRGBO(133, 133, 133, 1)),
                          ),
                        ),)
                          ],
                        ),
                        Expanded(child: Container()),
                        const Text(
                          "Xem Tất cả",
                          style: TextStyle(
                              fontSize: 12,
                              color: Color.fromRGBO(248, 129, 37, 1)),
                        )
                      ],
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 16.h)),
                  Card(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                    margin: const EdgeInsets.only(left: 16, right: 16, bottom: 20),
                    child: Obx(() => SfCalendar(
                      view: CalendarView.timelineDay,
                      firstDayOfWeek: 1,
                      onViewChanged: (viewChangedDetails) {

                      },
                      controller: controller.calendarController.value,
                      allowViewNavigation: false,
                      dataSource: MeetingDataSource(controller.timeTable.value),
                      todayHighlightColor: const Color.fromRGBO(249, 154, 81, 1),
                      appointmentBuilder: appointmentBuilderTimeLineDay,
                      timeSlotViewSettings: TimeSlotViewSettings(
                        timeFormat: "H a",
                        timeIntervalWidth: 80.w,
                        timelineAppointmentHeight: 50,
                      ),
                      scheduleViewSettings: const ScheduleViewSettings(
                          appointmentItemHeight: 20, hideEmptyScheduleWeek: true)
                      ,
                    )),
                  ),
                ],
              ),
            )
          ],
        ),
      ), onRefresh: () async{
        await controller.onRefresh();
      }):const Center(
      child: CircularProgressIndicator()),
    )));
  }
  Future<Null> updated(StateSetter updateState,int index) async {
    updateState(() {
      controller.onClickShoolYears(index);

    });
  }


  Expanded buildClassOffice() {
    return Expanded(
        child: Container(
          height: 42,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(6.r),
              border: Border.all(
                  color: const Color.fromRGBO(239, 239, 239, 1), width: 1)),
          padding: const EdgeInsets.all(8),
          child: ListView.builder(
              itemCount: controller.classOfTeacher.value.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return itemClassId(index);
              }),
        ));
  }

  itemClassId(int index) {
    return InkWell(
        onTap: () {
          controller.classUId.value = controller.classOfTeacher.value[index];
          controller.selectedClass(controller.classUId.value,index);
          controller.getTablePlan();
        },
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              width: 100,
              height: 26,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: controller.classOfTeacher.value[index].checked
                    ? const Color.fromRGBO(249, 154, 81, 1)
                    : const Color.fromRGBO(246, 246, 246, 1),
                borderRadius: BorderRadius.circular(6),
              ),
              margin: const EdgeInsets.only(right: 8),
              padding: const EdgeInsets.fromLTRB(12, 4, 12, 4),
              child: Text(
                "${controller.classOfTeacher.value[index].name}",
                style: TextStyle(
                    fontSize: 12,
                    color: controller.classOfTeacher.value[index].checked
                        ? Colors.white
                        : const Color.fromRGBO(90, 90, 90, 1)),
              ),
              ),
            (controller.classOfTeacher.value[index].homeroomClass == true)
                ? Positioned(
              right: 13,
              top: 4,
              child: Icon(
                Icons.star,
                size: 8,
                color: controller.classOfTeacher.value[index].checked
                    ? Colors.white
                    : const Color.fromRGBO(90, 90, 90, 1),
              ),
            )
                : Container()
          ],
        ));
  }

}
