import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:task_manager/view/mobile/schedule/schedule_controller.dart';
import '../../../../../../commom/app_cache.dart';
import '../../../../schedule/schedule_page.dart';
import '../../manager_home_controller.dart';
import 'list_class_in_block_manager_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class ListClassInBlockManagerPage extends GetView<ListClassInBlockManagerController>{
  final controller = Get.put(ListClassInBlockManagerController());
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(child: Scaffold(
      appBar: AppBar(
        title:Text("${controller.block.value.blockCategory?.name}"),
        leading: BackButton(
          color: Colors.white,
          onPressed: () {
            Get.back();
          },
        ),
        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
        elevation: 0,
      ),
      body: Obx(() => Container(
        margin: EdgeInsets.symmetric(horizontal: 16.w,vertical: 16.h),
        child: ListView.builder(
            itemCount: controller.classes.value.length,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return Obx(() => InkWell(
                onTap: () {
                  controller.clickClass.value = index;
                  Get.find<ScheduleController>().timeTable.value.clear();
                  Get.find<ScheduleController>().queryTimeTableByClass(controller.classes.value[index].id);
                  controller.classId.value = controller.classes.value[index].id!;
                  Get.find<ScheduleController>().timeTable.refresh();
                  Get.to(SchedulePage());
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: index ==
                          controller.clickClass.value
                          ? const Color.fromRGBO(
                          249, 154, 81, 1)
                          : Colors.white,
                      borderRadius: BorderRadius.circular(12)),
                  margin:
                  const EdgeInsets.only(top: 8, bottom: 8),
                  padding: const EdgeInsets.only(
                      left: 21, top: 13, bottom: 13),
                  child: Row(
                    children: [
                      Image.asset(
                        "assets/images/icon_class.png",
                        height: 18.h,
                        width: 18.h,
                        color: index ==
                            controller
                                .clickClass.value
                            ? Colors.white
                            : const Color.fromRGBO(
                            90, 90, 90, 1),
                      ),
                      const Padding(
                          padding: EdgeInsets.only(left: 8)),
                      Text(
                        "${controller.classes.value[index].classCategory?.name}",
                        style: TextStyle(
                            fontSize: 14,
                            color: index ==
                                controller
                                    .clickClass.value
                                ? Colors.white
                                : const Color.fromRGBO(
                                90, 90, 90, 1)),
                      ),
                    ],
                  ),
                ),
              ));
            }),
      )),
    ));
  }

}



