
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:task_manager/data/model/res/class/classTeacher.dart';

import '../../../../../../commom/app_cache.dart';
import '../../../../../../commom/utils/app_utils.dart';
import '../../../../../../data/base_service/api_response.dart';
import '../../../../../../data/model/res/class/block.dart';
import '../../../../../../data/repository/subject/class_office_repo.dart';


class ListClassInBlockManagerController extends GetxController {
  final ClassOfficeRepo _classOfficeRepo = ClassOfficeRepo();
  var classes = <Class>[].obs;
  RxInt clickClass = 0.obs;
  var block = Block().obs;
  var classId = "".obs;

  @override
  void onInit() {
    super.onInit();
    var data = Get.arguments;
    if(data != ""|| data!= null){
      block.value = data;
    }
    getListClassInBlock(AppCache().schoolYearId,block.value.id);
  }


  getListClassInBlock(schoolYearId,blockId){
    classes.value.clear();
    _classOfficeRepo.listClassInBlock(schoolYearId,blockId).then((value){
      if (value.state == Status.SUCCESS) {
        classes.value = value.object!;
      }
    });
    classes.refresh();
  }

}
