import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:task_manager/view/mobile/role/manager/schedule_manager/schedule_manager_individual/schedule_manager_individual_controller.dart';
import '../../../../../../commom/utils/color_utils.dart';
import '../../../../../../commom/widget/hexColor.dart';
import '../../../../../../commom/widget/text_field_custom.dart';
import '../../../../../../data/model/common/detail_item_list_student.dart';
import '../../../../home/home_controller.dart';

class ScheduleManagerIndividualPage extends GetView<ScheduleManagerIndividualController> {
  @override
  final controller = Get.put(ScheduleManagerIndividualController());

  ScheduleManagerIndividualPage({super.key});

  _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: controller.dateNow.value,
        firstDate: DateTime(1900),
        lastDate: DateTime(2200));
    if (picked != null && picked != controller.dateNow.value) {
      controller.dateNow.value = picked;
    }
  }

  Card dateTimeSelectV2() {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(top: 8.h, left: 8.w),
            child: Text(
              "${controller.selectedDay.value.year}",
              style: TextStyle(
                  color: const Color.fromRGBO(248, 129, 37, 1),
                  fontSize: 13.sp),
            ),
          ),
          Row(
            children: [
              Expanded(
                  child: Container(
                margin: EdgeInsets.only(top: 4.h, left: 8.w, bottom: 4.h),
                child: Text(
                  "Tháng ${controller.focusedDay.value.month}",
                  style: TextStyle(color: Colors.black87, fontSize: 14.sp),
                ),
              )),
              InkWell(
                child: (controller.weekNumber.value == 1)
                    ? const Icon(
                        Icons.expand_more,
                        size: 24,
                      )
                    : const Icon(
                        Icons.expand_less,
                        size: 24,
                      ),
                onTap: () {
                  controller.changeShowFull();
                },
              ),
              const SizedBox(
                width: 16,
              )
            ],
          ),
          SizedBox(
            height: controller.heightOffset.value,
            child: SfDateRangePicker(
              viewSpacing: 10,
              todayHighlightColor: const Color.fromRGBO(249, 154, 81, 1),
              controller: controller.weekController.value,
              view: DateRangePickerView.month,
              enablePastDates: true,
              selectionMode: DateRangePickerSelectionMode.single,
              showNavigationArrow: true,
              selectionRadius: 15,
              monthFormat: "MMM",
              headerHeight: 0,
              selectionColor: const Color.fromRGBO(249, 154, 81, 1),
              selectionShape: DateRangePickerSelectionShape.circle,
              selectionTextStyle: const TextStyle(
                  color: ColorUtils.COLOR_WHITE, fontWeight: FontWeight.w700),
              monthCellStyle: const DateRangePickerMonthCellStyle(
                todayTextStyle: TextStyle(
                    color: Color.fromRGBO(249, 154, 81, 1),
                    fontWeight: FontWeight.w700),
              ),
              headerStyle: DateRangePickerHeaderStyle(
                  textAlign: TextAlign.left,
                  textStyle: TextStyle(
                    fontStyle: FontStyle.normal,
                    fontSize: 12.sp,
                    color: Colors.black87,
                  )),
              monthViewSettings: DateRangePickerMonthViewSettings(
                  numberOfWeeksInView: controller.weekNumber.value,
                  firstDayOfWeek: 1,
                  showTrailingAndLeadingDates: true,
                  viewHeaderHeight: 16.h,
                  dayFormat: 'EEE',
                  viewHeaderStyle: DateRangePickerViewHeaderStyle(
                      textStyle: TextStyle(
                          color: HexColor("#B1B1B1"),
                          fontWeight: FontWeight.w600,
                          fontSize: 10.sp))),
              onViewChanged: (DateRangePickerViewChangedArgs args) {
                Future.delayed(const Duration(milliseconds: 400), () {
                  var visibleDates = args.visibleDateRange;
                  controller.focusedDay.value =
                      visibleDates.startDate ?? DateTime.now();
                });
              },
              onSelectionChanged: (DateRangePickerSelectionChangedArgs args) {
                // var dateTime = args.value
                if (args.value is PickerDateRange) {
                  final DateTime rangeStartDate = args.value.startDate;
                  final DateTime rangeEndDate = args.value.endDate;

                  controller.selectedDay.value = rangeStartDate;
                  controller.timelineController.value.displayDate =
                      rangeStartDate;
                } else if (args.value is DateTime) {
                  final DateTime selectedDate = args.value;
                  controller.timelineController.value.displayDate =
                      selectedDate;

                  controller.selectedDay.value = selectedDate;
                } else if (args.value is List<DateTime>) {
                  final List<DateTime> selectedDates = args.value;
                  controller.timelineController.value.displayDate =
                      selectedDates[0];
                  controller.selectedDay.value = selectedDates[0];
                } else {
                  final List<PickerDateRange> selectedRanges = args.value;
                }
              },
            ),
          )
        ],
      ),
    );
  }

  showCreateJob(context) {
    showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            child: Container(
              decoration: BoxDecoration(
                  color: const Color.fromRGBO(255, 255, 255, 1),
                  borderRadius: BorderRadius.circular(6)),
              margin: const EdgeInsets.all(16),
              child: IntrinsicHeight(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          'Thêm Công Việc',
                          style: TextStyle(
                              color: Color.fromRGBO(26, 26, 26, 1),
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                              fontFamily:
                                  'assets/font/static/Inter-Medium.ttf'),
                        ),
                        InkWell(
                          onTap: () {
                            controller.closeDialog();
                          },
                          child: const Icon(Icons.close),
                        ),
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(top: 16.h)),
                    SizedBox(
                      width: double.infinity,
                      child: OutlineBorderTextFormField(
                        enable: false,
                        focusNode: controller.focusName.value,
                        iconPrefix: null,
                        iconSuffix: "",
                        state: StateType.DEFAULT,
                        labelText: "Tên Công Việc",
                        autofocus: false,
                        controller: controller.controllerName.value,
                        helperText: "",
                        showHelperText: false,
                        textInputAction: TextInputAction.next,
                        ishowIconPrefix: false,
                        keyboardType: TextInputType.text,
                        validation: (textToValidate) {
                          return controller
                              .getTempIFSCValidation(textToValidate);
                        },
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(top: 21.h)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text('Thời Gian/ Ngày',
                            style: TextStyle(
                                color: Color.fromRGBO(26, 26, 26, 1),
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                fontFamily:
                                    'assets/font/static/Inter-Medium.ttf')),
                        Row(
                          children: [
                            Text('Chọn thời gian ',
                                style: TextStyle(
                                    color: const Color.fromRGBO(90, 90, 90, 1),
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14.sp,
                                    fontFamily:
                                        'assets/font/static/Inter-Medium.ttf')),
                            InkWell(
                              onTap: () {
                                _selectDate(context);
                              },
                              child: const Icon(
                                Icons.calendar_month_outlined,
                                size: 16,
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(top: 22.h)),
                    SizedBox(
                      height: 46.h,
                      width: double.infinity,
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6)),
                            backgroundColor:
                                const Color.fromRGBO(248, 129, 37, 1),
                          ),
                          onPressed: () {
                            controller.listJob.value.add(DetailJob(
                                content:
                                    controller.controllerName.value.text,
                                dateTime: '${DateTime.now()}'));
                            controller.closeDialog();
                          },
                          child: const Text(
                            'Thêm Công Việc',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w400),
                          )),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Công việc"),
        leading: BackButton(
          color: Colors.white,
          onPressed: () {
            Get.find<HomeController>().comeCalendar();
          },
        ),
        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
      ),
      body: Obx(
        () => SingleChildScrollView(
          physics: const ScrollPhysics(),
          child: Container(
            margin: EdgeInsets.only(right: 16.w, left: 16.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 16.h),
                  child: Text(
                    "Thời gian",
                    style: TextStyle(
                        color: const Color.fromRGBO(177, 177, 177, 1),
                        fontSize: 12.sp,
                        fontWeight: FontWeight.w400),
                  ),
                ),
                dateTimeSelectV2(),
                Padding(padding: EdgeInsets.only(top: 16.h)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Công Việc',
                      style: TextStyle(
                          color: const Color.fromRGBO(177, 177, 177, 1),
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w400),
                    ),
                    InkWell(
                      onTap: () {
                        showCreateJob(context);
                      },
                      child: Text(
                        'Thêm Công Việc +',
                        style: TextStyle(
                            color: const Color.fromRGBO(248, 129, 37, 1),
                            fontSize: 12.sp,
                            fontWeight: FontWeight.w400),
                      ),
                    )
                  ],
                ),
                Padding(padding: EdgeInsets.only(top: 16.h)),
                Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: const Color.fromRGBO(255, 255, 255, 1),
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: Column(
                    children: [
                      Padding(padding: EdgeInsets.only(top: 16.h)),
                      controller.isShow()
                          ? Container(
                              child: Column(
                              children: [
                                Text(
                                  "Bạn chưa có công việc nào được thêm",
                                  style: TextStyle(
                                      color: const Color.fromRGBO(26, 26, 26, 1),
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14.sp),
                                ),
                                Padding(padding: EdgeInsets.only(top: 8.h)),
                                SizedBox(
                                  height: 46.h,
                                  width: 174.h,
                                  child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(6)),
                                        backgroundColor: const Color.fromRGBO(
                                            248, 129, 37, 1),
                                      ),
                                      onPressed: () {
                                        showCreateJob(context);
                                      },
                                      child: Text(
                                        'Thêm Công Việc',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16.sp,
                                            fontWeight: FontWeight.w400),
                                      )),
                                ),
                              ],
                            ))
                          : Container(),
                      Obx(
                        () => ListView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: controller.listJob.value.length,
                            itemBuilder: (context, index) {
                              var showLine =
                                  index == controller.listJob.value.length - 1
                                      ? false
                                      : true;
                              return Obx(() => Container(
                                    margin: EdgeInsets.only(
                                        right: 16.h, left: 16.h, top: 16.h),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          '${controller.listJob.value[index].content}',
                                          style: TextStyle(
                                              color:
                                                  const Color.fromRGBO(26, 26, 26, 1),
                                              fontSize: 14.sp,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        Padding(
                                            padding: EdgeInsets.only(top: 5.h)),
                                        Row(
                                          children: [
                                            SvgPicture.asset(
                                                'assets/images/icon_diligence.svg'),
                                            Padding(
                                                padding: EdgeInsets.only(
                                                    right: 2.w)),
                                            Text(
                                              '${controller.listJob.value[index].dateTime}',
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      90, 90, 90, 1),
                                                  fontSize: 12.sp,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          ],
                                        ),
                                        (showLine)
                                            ? const Divider(
                                                indent: 10,
                                                endIndent: 10,
                                              )
                                            : Container(
                                                height: 16.h,
                                              )
                                      ],
                                    ),
                                  ));
                            }),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
