
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

import '../../../../../../data/model/common/detail_item_list_student.dart';

class ScheduleManagerIndividualController extends GetxController {
  final listJob =[
    DetailJob(content: 'Lý do nghỉ học', dateTime: '2/1/2023' ),
    DetailJob(content: 'Lý do nghỉ học', dateTime: '2/1/2023' ),
    DetailJob(content: 'Lý do nghỉ học', dateTime: '2/1/2023' ),
  ].obs;
  var format = CalendarFormat.month.obs;
  var focusedDay = DateTime.now().obs;
  var selectedDay = DateTime.now().obs;
  var focusName = FocusNode().obs;
  var controllerName = TextEditingController().obs;
  var isAddShowJob = false.obs;
  var dateNow = DateTime.now().obs;
  var showFull = false;

  var quarterTurns = 270.obs;
  var weekNumber = 1.obs;
  var heightOffset = (Get.width/5).obs;
  var weekController = DateRangePickerController().obs;

  var timelineController = CalendarController().obs;



  String getTempIFSCValidation(String text) {
    return text.length > 7 ? "This line is helper text" : "";
  }
  closeDialog(){
    Get.back();
  }

  isShow(){
    if(listJob.value.length ==0){
      isAddShowJob.value = true;
    }else {
      isAddShowJob.value = false;
    }
    return isAddShowJob.value;
  }
  void changeCalendar() {
    if(showFull){
      weekNumber.value = 4;
      heightOffset.value = (Get.width/2);
      quarterTurns.value = 90;
    } else {
      heightOffset.value = (Get.width/5);
      weekNumber.value = 1;
      quarterTurns.value = 270;
    }
  }
  void changeShowFull() {
    showFull = !showFull;
    changeCalendar();
  }


}
