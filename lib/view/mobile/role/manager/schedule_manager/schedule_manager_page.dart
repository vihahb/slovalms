import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:task_manager/view/mobile/role/manager/schedule_manager/schedule_manager_controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../../commom/app_cache.dart';
import '../../../../../routes/app_pages.dart';
import '../../../home/home_controller.dart';
import '../../../schedule/schedule_page.dart';
import 'list_class_in_block_manager/list_class_in_block_manager_page.dart';
class ScheduleManagerPage extends GetWidget<ScheduleManagerController>{
  final controller = Get.put(ScheduleManagerController());
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: const Text("Công việc"),
        leading: BackButton(
          color: Colors.white,
          onPressed: () {
            Get.find<HomeController>().comeBackHome();
          },
        ),
        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
        elevation: 0,
      ),
      body:Obx(() => ListView.builder(
          itemCount: controller.blocks.length,
          itemBuilder: (context,index){
            return Obx(() => InkWell(
              onTap: () {
                Get.toNamed(Routes.listClassInBlock,arguments: controller.blocks.value[index]);
              },
              child: Container(
                margin: EdgeInsets.fromLTRB(16.w, 4.h, 16.w, 4.h),
                child: Column(
                  children: [
                    Card(
                      elevation: 1,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      child: Container(
                        padding: const EdgeInsets.all(16),
                        child: Row(
                          children: [
                            Text(
                              "${controller.blocks.value[index].blockCategory?.name}",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                  fontWeight: FontWeight.bold),
                            ),
                            Expanded(child: Container()),
                            const Icon(
                              Icons.keyboard_arrow_right,
                              color: Color.fromRGBO(248, 129, 37, 1),
                              size: 28,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ));
          })),
    );
  }

}