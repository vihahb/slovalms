
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

import '../../../../../commom/app_cache.dart';
import '../../../../../commom/utils/app_utils.dart';
import '../../../../../data/base_service/api_response.dart';
import '../../../../../data/model/common/detail_item_list_student.dart';
import '../../../../../data/model/common/schedule.dart';
import '../../../../../data/model/res/class/block.dart';
import '../../../../../data/repository/schedule/schedule_repo.dart';
import '../../../../../data/repository/subject/class_office_repo.dart';

class ScheduleManagerController extends GetxController {
  final ClassOfficeRepo _classOfficeRepo = ClassOfficeRepo();
  var blocks= <Block>[].obs;
  var block = Block().obs;
  RxList<Appointment> timeTable = <Appointment>[].obs;
  RxList<Schedule> schedule = <Schedule>[].obs;

  @override
  void onInit() {
    super.onInit();
    getListBlock(AppCache().schoolYearId);
  }

  getListBlock(schoolYearId){
    blocks.value.clear();
    _classOfficeRepo.listBlock(schoolYearId).then((value){
      if (value.state == Status.SUCCESS) {
        blocks.value = value.object!;
      }
    });
    blocks.refresh();
  }


}
