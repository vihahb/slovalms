import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:task_manager/view/mobile/role/manager/diligent_management_manager/detail_list_class_by_manager/list_class_in_block_diligent_manager_controller.dart';

class ListClassInBlockDiligentManagerPage extends GetWidget<ListClassInBlockDiligentManagerController>{
  final controller = Get.put(ListClassInBlockDiligentManagerController());

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title:  Text("Chuyên cần ${controller.block.value.blockCategory?.name}"),
        leading: BackButton(
          color: Color.fromRGBO(245, 245, 245, 1),
          onPressed: () {
            Get.back();
          },
        ),
        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
        elevation: 0,
      ),
      body:Obx(() =>
          Container(
            margin: EdgeInsets.fromLTRB(16.w, 4.h, 16.w, 4.h),
            child:Column(
              children: [
                Container(
                  padding:EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    color: Colors.white,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(padding: EdgeInsets.only(top: 4.h)),
                      Text("Lớp học", style:  TextStyle(color: Color.fromRGBO(133, 133, 133, 1), fontWeight: FontWeight.w500, fontSize: 12.sp),),
                      Padding(padding: EdgeInsets.only(bottom: 4.h)),
                      ListView.builder(
                          itemCount: controller.classes.value.length,
                          shrinkWrap: true,
                          itemBuilder: (context,index){
                            return Obx(() => InkWell(
                              onTap: () {
                                controller.gotoDetailListStudentByClass(index);
                              },
                              child: Container(

                                child: Column(
                                  children: [
                                    Card(
                                      elevation: 1,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(6)),
                                      child: Container(
                                        padding: const EdgeInsets.all(16),
                                        child: Row(
                                          children: [
                                            Text(
                                              "${controller.classes.value[index].classCategory?.name}",
                                              style: TextStyle(
                                                  fontSize: 14.sp,
                                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Expanded(child: Container()),
                                            const Icon(
                                              Icons.keyboard_arrow_right,
                                             color: Colors.black,
                                              size: 24,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ));
                          }),
                      Padding(padding: EdgeInsets.only(top: 16.h)),
                    ],
                  ),
                )
              ],
            ),
          )
      ),
    );
  }
  
}