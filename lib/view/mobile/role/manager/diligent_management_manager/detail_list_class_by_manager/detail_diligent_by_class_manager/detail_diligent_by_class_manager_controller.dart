import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:task_manager/data/base_service/api_response.dart';


import 'package:task_manager/data/repository/diligence/diligence_repo.dart';
import 'package:task_manager/view/mobile/role/manager/manager_home_controller.dart';
import '../../../../../../../data/model/common/diligence.dart';
import '../../../../../../../data/model/res/class/block.dart';
import '../../../../../../../routes/app_pages.dart';
import '../../../manager_home_controller.dart';

class DetailDiligentByClassManagerController extends GetxController{
  DateTime now = new DateTime.now();
  RxInt indexClick = 0.obs;
  RxList<bool> click = <bool>[].obs;
  var cupertinoDatePicker = DateTime.now().obs;
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var diligenceClass = DiligenceClass().obs;
  var listStudent = <Diligents>[].obs;
  var student = StudentDiligent().obs;
  RxList<StudentDiligent> studentdiligence = <StudentDiligent>[].obs;
  var classcategoryid = "".obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var clazz = Class().obs;
  var listStudentOntime = <Diligents>[].obs;
  var listStudentNotOnTime = <Diligents>[].obs;
  var listStudentExcusedAbsence = <Diligents>[].obs;
  var listStudentAbsentWithoutLeave = <Diligents>[].obs;
  var selectedDay = DateTime.now().obs;
  var listStatus = <String>[].obs;
  var fromYearSelect ="".obs;
  var toYearSelect ="".obs;

  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );
  @override
  void onInit() {
    super.onInit();
    var data = Get.arguments;
    listStatus.value = ["Đi học đúng giờ","Đi học muộn","Nghỉ có phép","Nghỉ không phép"];
    if(data!= null){
      clazz.value = data;
    }

    if(DateTime.now().month<=12&&DateTime.now().month>9){
     selectedDay.value = DateTime( Get.find<ManagerHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
    }else{
      selectedDay.value = DateTime(Get.find<ManagerHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
    }

    listStudent.value.clear();
    listStudentOntime.value.clear();
    listStudentNotOnTime.value.clear();
    listStudentExcusedAbsence.value.clear();
    listStudentAbsentWithoutLeave.value.clear();
    setDateSchoolYears();
    listStudent.refresh();
    listStudentOntime.refresh();
    listStudentNotOnTime.refresh();
    listStudentExcusedAbsence.refresh();
    listStudentAbsentWithoutLeave.refresh();


  }

  getListStatus(index){
    switch(index){
      case 0:
        return listStudentOntime.value.length;
      case 1:
        return listStudentNotOnTime.value.length;
      case 2:
        return listStudentExcusedAbsence.value.length;
      case 3:
        return listStudentAbsentWithoutLeave.value.length;
    }
  }


  onPageViewChange(int page) {
    indexClick.value = page;
    if(page != 0) indexClick.value!-1;
    else indexClick.value = 0;

  }


  showColor(index) {
    click.value = [];
    for (int i = 0; i < 4; i++) {
      click.value.add(false);
    }
    click.value[index] = true;
  }
  getListStudent(){
    var classId = clazz.value.id;
    var fromdate = DateTime(selectedDay.value.year, selectedDay.value.month, selectedDay.value.day,00,00).millisecondsSinceEpoch;
    var todate = DateTime(selectedDay.value.year, selectedDay.value.month, selectedDay.value.day,23,59).millisecondsSinceEpoch;
    _diligenceRepo.getListStudentDiligence(classId, fromdate, todate, "").then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value = value.object!;
        listStudent.value = diligenceClass.value.studentDiligent!;
        listStudentOntime.value = listStudent.value.where((element) => element.statusDiligent == "ON_TIME").toList();
        listStudentNotOnTime.value = listStudent.value.where((element) => element.statusDiligent == "NOT_ON_TIME").toList();
        listStudentExcusedAbsence.value = listStudent.value.where((element) => element.statusDiligent == "EXCUSED_ABSENCE").toList();
        listStudentAbsentWithoutLeave.value = listStudent.value.where((element) => element.statusDiligent == "ABSENT_WITHOUT_LEAVE").toList();
      }
    });
  }
  getListStudentToYear(fromdate, todate){
    var classId = clazz.value.id;
    var fromdate = DateTime(int.parse(fromYearSelect.value.substring(0,4)), selectedDay.value.month,selectedDay.value.day).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(toYearSelect.value.substring(0,4)),selectedDay.value.month,selectedDay.value.day).millisecondsSinceEpoch;
    _diligenceRepo.getListStudentDiligence(classId, fromdate, todate, "").then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value = value.object!;
        listStudent.value = diligenceClass.value.studentDiligent!;
        listStudentOntime.value = listStudent.value.where((element) => element.statusDiligent == "ON_TIME").toList();
        listStudentNotOnTime.value = listStudent.value.where((element) => element.statusDiligent == "NOT_ON_TIME").toList();
        listStudentExcusedAbsence.value = listStudent.value.where((element) => element.statusDiligent == "EXCUSED_ABSENCE").toList();
        listStudentAbsentWithoutLeave.value = listStudent.value.where((element) => element.statusDiligent == "ABSENT_WITHOUT_LEAVE").toList();
      }
    });
  }
  setDateSchoolYears() {
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (Get.find<ManagerHomeController>().fromYearPresent.value == DateTime.now().year) {
        getListStudent();
        return;
      } else {
        fromYearSelect.value = "${Get.find<ManagerHomeController>().fromYearPresent}";
        toYearSelect.value = "${Get.find<ManagerHomeController>().toYearPresent}";
        getListStudentToYear(fromYearSelect.value, toYearSelect.value);
      }
    } else {
      if (Get.find<ManagerHomeController>().toYearPresent.value == DateTime.now().year) {
        getListStudent();
        return;
      } else {
        fromYearSelect.value = "${Get.find<ManagerHomeController>().fromYearPresent}";
        toYearSelect.value = "${Get.find<ManagerHomeController>().toYearPresent}";
        getListStudentToYear(fromYearSelect.value, toYearSelect.value);
      }
    }
  }

  getTextYear(){
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (Get.find<ManagerHomeController>().fromYearPresent.value == DateTime.now().year) {
        return DateTime.now().year.toString();
      } else {
        return Get.find<ManagerHomeController>().toYearPresent.toString();
      }
    } else {
      if (Get.find<ManagerHomeController>().toYearPresent.value == DateTime.now().year) {
        return DateTime.now().year.toString();
      } else {
        return Get.find<ManagerHomeController>().toYearPresent.toString();
      }
    }
  }


}