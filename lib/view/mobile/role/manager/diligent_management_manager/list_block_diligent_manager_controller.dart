import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:task_manager/data/model/common/detail_item_list_student.dart';
import 'package:task_manager/routes/app_pages.dart';
import 'package:task_manager/view/mobile/role/manager/diligent_management_manager/detail_list_class_by_manager/list_class_in_block_diligent_manager_page.dart';

import '../../../../../commom/app_cache.dart';
import '../../../../../data/base_service/api_response.dart';
import '../../../../../data/model/res/class/block.dart';
import '../../../../../data/repository/subject/class_office_repo.dart';

class ListBlockDiligentManagerController extends GetxController{
  final ClassOfficeRepo _classOfficeRepo = ClassOfficeRepo();
  var blocks= <Block>[].obs;

  @override
  void onInit() {
    getListBlock(AppCache().schoolYearId);
    super.onInit();
  }

  getListBlock(schoolYearId){
    blocks.value.clear();
    _classOfficeRepo.listBlock(schoolYearId).then((value){
      if (value.state == Status.SUCCESS) {
        blocks.value = value.object!;
      }
    });
    blocks.refresh();
  }



  gotoDetailListClass(index){
    Get.toNamed(Routes.listClassInBlockDiligentManager, arguments: blocks.value[index]);
  }
}