import 'package:get/get.dart';
import 'package:task_manager/commom/app_cache.dart';
import 'package:task_manager/commom/utils/app_utils.dart';
import 'package:task_manager/data/model/common/user_profile.dart';
import 'package:task_manager/data/repository/account/auth_repo.dart';
import 'package:task_manager/routes/app_pages.dart';
import 'package:task_manager/view/mobile/role/manager/diligent_management_manager/detail_list_class_by_manager/detail_diligent_by_class_manager/detail_diligent_by_class_manager_controller.dart';
import 'package:task_manager/view/mobile/role/manager/schedule_manager/schedule_manager_controller.dart';
import 'package:task_manager/view/mobile/role/student/personal_information_student/detail_avatar/detail_avatar_page.dart';

import '../../../../../data/base_service/api_response.dart';
import '../../../../data/model/common/Position.dart';
import '../../../../data/model/common/school_year.dart';
import '../../../../data/model/common/static_page.dart';
import '../../../../data/model/res/class/School.dart';
import '../../../../data/model/res/class/classTeacher.dart';
import '../../../../data/repository/common/common_repo.dart';
import '../../../../data/repository/person/personal_info_repo.dart';
import '../../../../data/repository/school_year/school_year_repo.dart';
import '../../../../data/repository/subject/class_office_repo.dart';
import '../../account/login/login_controller.dart';
import '../../phonebook/phone_book_controller.dart';
import '../../schedule/schedule_controller.dart';
import 'diligent_management_manager/detail_list_class_by_manager/detail_diligent_by_class_manager/detail_diligent_by_class_manager_controller.dart';

class ManagerHomeController extends GetxController {
  var indexClass = 0.obs;
  final AuthRepo _authRepo = AuthRepo();

  final PersonalInfoRepo _personalInfoRepo = PersonalInfoRepo();
  var userProfile = UserProfile().obs;
  var school = SchoolData().obs;
  var position = Position().obs;
  var staticPage = ItemsStaticPage().obs;
  final CommonRepo _commonRepo = CommonRepo();
  final SchoolYearRepo _schoolYearRepo = SchoolYearRepo();
  RxList<SchoolYear> schoolYears = <SchoolYear>[].obs;
  var clickSchoolYear = <bool>[].obs;
  var fromYearPresent = 0.obs;
  var toYearPresent = 0.obs;
  RxList<Clazzs> classByTeacher = <Clazzs>[].obs;
  var isReady = false.obs;
  Rx<ClassId> currentClass = ClassId().obs;
  final ClassOfficeRepo _classRepo = ClassOfficeRepo();
  RxList<ClassId> classOfManager = <ClassId>[].obs;
  @override
  void onInit() {
    super.onInit();
    getSchoolYearByUser();
  }

  onClickShoolYears(index) {
    clickSchoolYear.value = <bool>[].obs;
    for (int i = 0; i < schoolYears.value.length; i++) {
      clickSchoolYear.value.add(false);
    }
    clickSchoolYear.value[index] = true;
  }

  getSchoolYearByUser() async {
    _schoolYearRepo.getListSchoolYearByUser().then((value) {
      if (value.state == Status.SUCCESS) {
        schoolYears.value = value.object!;
        if (schoolYears.value.length != 0) {
          fromYearPresent.value = schoolYears.value[0].fromYear!;
          toYearPresent.value = schoolYears.value[0].toYear!;
          getDetailProfile();
          // Get.find<DetailDiligentByClassManagerController>().setDateSchoolYears();
        }
        for (int i = 0; i < schoolYears.value.length; i++) {
          clickSchoolYear.add(false);
        }
      }
    });
  }

  getListClassByManager() {
    var teacherId = userProfile.value.id;
    _classRepo.listClassByTeacher(teacherId).then((value) {
      if (value.state == Status.SUCCESS) {
        classOfManager.value = value.object!;
        for (var f in classOfManager.value) {
          f.checked = false;
        }
        classOfManager.value.first.checked = true;
        currentClass.value = classOfManager.value.first;
        classOfManager.refresh();
      }
    });
  }

  selectedClass(ClassId classId, index) {
    currentClass.value = classId;
    for (var f in classOfManager.value) {
      if (f.id != classId.id) {
        f.checked = false;
      } else {
        f.checked = true;
      }
    }
    classOfManager.refresh();
  }

  void submitLogout() async {
    logout();
  }

  logout() async {
    _authRepo.logout().then((value) {
      goToLogin();
      AppCache().resetCache();
    });
    if (AppCache().isSaveInfoLogin) {
      String? usernameSaved = await AppCache().username;
      String? passwordSaved = await AppCache().password;
      Get.find<LoginController>().controllerUserName.text = usernameSaved ?? "";
      Get.find<LoginController>().controllerPassword.text = passwordSaved ?? "";
    }else{
      AppCache().deleteInfoLogin();
    }
  }

  getDetailProfile() {
    _personalInfoRepo.detailUserProfile().then((value) {
      if (value.state == Status.SUCCESS) {
        userProfile.value = value.object!;
        if (userProfile.value.school == null ||
            userProfile.value.school == "") {
          school.value.name = "";
          school.value.id = "";
        } else {
          school.value = userProfile.value.school!;
        }
        if (userProfile.value.position == null ||
            userProfile.value.position == "") {
          position.value.name = "";
          position.value.id = "";
        } else {
          position.value = userProfile.value.position!;
        }
      }
      getListClassByManager();
    });
  }



  onSelectSchoolYears(index) {
    Get.find<ScheduleManagerController>().blocks.value.clear();
    AppCache().setSchoolYearId(schoolYears.value[index].id??"");
    fromYearPresent.value = schoolYears.value[index].fromYear!;
    toYearPresent.value = schoolYears.value[index].toYear!;
    Get.find<ScheduleController>().queryTimeCalendar(index, AppCache().userType);
    Get.find<PhoneBookController>().getListClassByStudent();
    getListClassByManager();
    Get.find<ScheduleManagerController>().getListBlock(schoolYears.value[index].id!);
    Get.find<ScheduleManagerController>().blocks.refresh();

    Get.back();
  }

  onRefresh() {
    getDetailProfile();
  }

  getStaticPage(type) {
    _commonRepo.getListStaticPage(type).then((value) {
      if (value.state == Status.SUCCESS) {
        staticPage.value = value.object!;
        goToStaticPage();
      }
    });
  }

  goToStaticPage() {
    Get.toNamed(Routes.staticPage, arguments: staticPage.value);
  }

  void goToStudentListPage() {
    Get.toNamed(Routes.studentListPage, arguments: currentClass.value);
  }

  void goToLogin() {
    Get.toNamed(Routes.login);
  }

  goToUpdateInfoUser() {
    Get.toNamed(Routes.updateInfoManager);
  }

  void goToDetailAvatar() {
    Get.toNamed(Routes.getavatar, arguments: userProfile.value.image);
  }

  void goToChangePassPage() {
    Get.toNamed(Routes.changePass, arguments: userProfile.value.id);
  }
}
