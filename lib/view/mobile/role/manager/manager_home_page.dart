import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:task_manager/view/mobile/phonebook/phone_book_controller.dart';
import 'package:task_manager/view/mobile/role/manager/manager_home_controller.dart';
import 'package:task_manager/view/mobile/role/manager/schedule_manager/schedule_manager_controller.dart';
import 'package:task_manager/view/mobile/role/manager/schedule_manager/schedule_manager_page.dart';

import '../../../../commom/app_cache.dart';
import '../../schedule/schedule_controller.dart';
import 'diligent_management_manager/list_block_diligent_manager_controller.dart';
import 'diligent_management_manager/list_block_diligent_manager_page.dart';

class ManagerHomePage extends GetWidget<ManagerHomeController> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final controller = Get.put(ManagerHomeController());

  ManagerHomePage({super.key});
  _onBottomSheetLogout() {
    return Wrap(
      children: [
        Container(
          margin: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  height: 50.h,
                  padding: EdgeInsets.only(
                      top: 15.h, left: 10.w, right: 10.w, bottom: 15.h),
                  decoration: ShapeDecoration(
                      color: const Color.fromRGBO(248, 129, 37, 1),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(6.r),
                            topRight: Radius.circular(6.r)),
                      )),
                  child: const SizedBox(
                    width: double.infinity,
                    child: Text("Đăng Xuất",
                        style: TextStyle(color: Colors.white)),
                  )),
              Container(
                  color: Colors.white,
                  width: double.infinity,
                  height: 57.h,
                  alignment: Alignment.center,
                  child: const Text("Bạn có muốn đăng xuất không?")),
              Row(
                children: [
                  Expanded(
                      child: SizedBox(
                    height: 50.h,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.grey,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(6.r)))),
                      onPressed: () {
                        Get.back();
                      },
                      child: const Text(
                        "Hủy",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  )),
                  Expanded(
                    child: SizedBox(
                      height: 50.h,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.red,
                            shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(6)))),
                        onPressed: () {
                          controller.logout();
                        },
                        child: const Text(
                          "Đăng Xuất",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      drawerEdgeDragWidth: 0,
      key: _scaffoldKey,
      drawer: Container(
        margin: EdgeInsets.only(right: 28.w),
        width: MediaQuery.of(context).size.width,
        child: Obx(() => Drawer(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: const Color.fromRGBO(248, 129, 37, 1),
                        borderRadius: BorderRadius.circular(6.r),
                      ),
                      margin: const EdgeInsets.all(16),
                      padding: EdgeInsets.fromLTRB(16.w, 8.h, 16.w, 8.h),
                      child: Row(
                        children: [
                          InkWell(
                            onTap: () {
                              controller.goToDetailAvatar();
                            },
                            child: SizedBox(
                              width: 40.w,
                              height: 40.w,
                              child: CircleAvatar(
                                backgroundColor: Colors.white,
                                backgroundImage: Image.network(
                                  controller.userProfile.value.image ??
                                      "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                                  errorBuilder: (context, object, stackTrace) {
                                    return Image.asset(
                                      "assets/images/img_Noavt.png",
                                    );
                                  },
                                ).image,
                              ),
                            ),
                          ),
                          const Padding(padding: EdgeInsets.only(left: 16)),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${controller.userProfile.value.fullName}",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w700),
                                ),
                                Text(
                                  "${controller.userProfile.value.email}",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 11.sp,
                                      fontWeight: FontWeight.w400),
                                )
                              ],
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              Get.back();
                            },
                            icon: const Icon(
                                Icons.keyboard_double_arrow_left_outlined),
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(top: 16.h)),
                    Container(
                      margin: EdgeInsets.only(left: 16.w, right: 16.w),
                      child: Row(
                        children: [
                          Text(
                            'Thông Tin Cá Nhân',
                            style: TextStyle(
                                color: const Color.fromRGBO(133, 133, 133, 1),
                                fontSize: 12.sp),
                          ),
                          Expanded(child: Container()),
                          InkWell(
                            onTap: () {
                              controller.goToUpdateInfoUser();
                            },
                            child: SizedBox(
                              height: 30.h,
                              width: 79.w,
                              child: Row(
                                children: [
                                  Text('Chỉnh sửa',
                                      style: TextStyle(
                                          color: const Color.fromRGBO(
                                              248, 129, 37, 1),
                                          fontSize: 12.sp,
                                          fontFamily:
                                              'static/Inter-Regular.ttf')),
                                  const Icon(
                                    Icons.mode_edit_outline_outlined,
                                    color: Color.fromRGBO(248, 129, 37, 1),
                                    size: 16,
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin:
                          EdgeInsets.only(left: 16.w, right: 16.w, top: 16.h),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              SizedBox(
                                width: 100.w,
                                child: Text(
                                  'Họ Và Tên: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14.sp,
                                      fontFamily: 'static/Inter-Medium.ttf'),
                                ),
                              ),
                              Text(
                                '${controller.userProfile.value.fullName}',
                                style: TextStyle(
                                    color:
                                        const Color.fromRGBO(248, 129, 37, 1),
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'static/Inter-Medium.ttf'),
                              )
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 10.h)),
                          Row(
                            children: [
                              SizedBox(
                                width: 100.w,
                                child: Text(
                                  'Số Điện Thoại: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14.sp,
                                      fontFamily: 'static/Inter-Medium.ttf'),
                                ),
                              ),
                              Text(
                                '${controller.userProfile.value.phone}',
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'static/Inter-Medium.ttf'),
                              )
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(top: 10)),
                          Row(
                            children: [
                              SizedBox(
                                width: 100.w,
                                child: Text(
                                  'Email: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14.sp,
                                      fontFamily: 'static/Inter-Medium.ttf'),
                                ),
                              ),
                              Text(
                                '${controller.userProfile.value.email}',
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'static/Inter-Medium.ttf'),
                              )
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 10.h)),
                          Row(
                            children: [
                              SizedBox(
                                width: 100.w,
                                child: const Text(
                                  'Trường : ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14,
                                      fontFamily: 'static/Inter-Medium.ttf'),
                                ),
                              ),
                              Text(
                                '${controller.school.value.name}',
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'static/Inter-Medium.ttf'),
                              )
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 10.h)),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: 100.w,
                                child: const Text(
                                  'Chức vụ: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14,
                                      fontFamily: 'static/Inter-Medium.ttf'),
                                ),
                              ),
                              Expanded(
                                  child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        '${controller.position!.value.name} ',
                                        style: TextStyle(
                                            color: const Color.fromRGBO(
                                                26, 26, 26, 1),
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.w500,
                                            fontFamily:
                                                'static/Inter-Medium.ttf'),
                                      ),
                                    ],
                                  ),
                                ],
                              ))
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 10.h)),
                        ],
                      ),
                    ),
                    Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(
                              left: 16, right: 16, top: 24),
                          child: Row(
                            children: [
                              const Text(
                                'Cài Đặt',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Color.fromRGBO(133, 133, 133, 1),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'static/Inter-Medium.ttf'),
                              ),
                              Expanded(child: Container())
                            ],
                          ),
                        ),
                        const Padding(padding: EdgeInsets.only(top: 12)),
                        Container(
                          margin: const EdgeInsets.only(left: 16),
                          child: Column(
                            children: [
                              InkWell(
                                onTap: () {
                                  controller.getStaticPage("SUPPORT");
                                },
                                child: Row(
                                  children: [
                                    const Text(
                                      'Hỗ Trợ',
                                      style: TextStyle(
                                          color: Color.fromRGBO(24, 29, 39, 1),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          fontFamily:
                                              'static/Inter-Medium.ttf'),
                                    ),
                                    Expanded(child: Container()),
                                    IconButton(
                                        iconSize: 24,
                                        onPressed: () {},
                                        icon: const Icon(
                                          Icons.navigate_next,
                                          color: Colors.black,
                                        ))
                                  ],
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  controller.goToChangePassPage();
                                },
                                child: Row(
                                  children: [
                                    const Text(
                                      'Đổi Mật Khẩu ',
                                      style: TextStyle(
                                          color: Color.fromRGBO(24, 29, 39, 1),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          fontFamily:
                                              'static/Inter-Medium.ttf'),
                                    ),
                                    Expanded(child: Container()),
                                    IconButton(
                                        iconSize: 24,
                                        onPressed: () {},
                                        icon: const Icon(
                                          Icons.navigate_next,
                                          color: Colors.black,
                                        ))
                                  ],
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  controller.getStaticPage("RULES");
                                },
                                child: Row(
                                  children: [
                                    const Text(
                                      'Chính Sách Bảo Mật',
                                      style: TextStyle(
                                          color: Color.fromRGBO(24, 29, 39, 1),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          fontFamily:
                                              'static/Inter-Medium.ttf'),
                                    ),
                                    Expanded(child: Container()),
                                    IconButton(
                                        iconSize: 24,
                                        onPressed: () {},
                                        icon: const Icon(
                                          Icons.navigate_next,
                                          color: Colors.black,
                                        ))
                                  ],
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  controller.getStaticPage("SECURITY");
                                },
                                child: Row(
                                  children: [
                                    const Text(
                                      'Điều Khoản Sử Dụng',
                                      style: TextStyle(
                                          color: Color.fromRGBO(24, 29, 39, 1),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          fontFamily:
                                              'static/Inter-Medium.ttf'),
                                    ),
                                    Expanded(child: Container()),
                                    IconButton(
                                        iconSize: 24,
                                        onPressed: () {},
                                        icon: const Icon(
                                          Icons.navigate_next,
                                          color: Colors.black,
                                        ))
                                  ],
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  Get.bottomSheet(_onBottomSheetLogout());
                                },
                                child: Row(
                                  children: [
                                    const Text(
                                      'Đăng Xuất',
                                      style: TextStyle(
                                          color: Color.fromRGBO(24, 29, 39, 1),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          fontFamily:
                                              'static/Inter-Medium.ttf'),
                                    ),
                                    Expanded(child: Container()),
                                    IconButton(
                                        iconSize: 24,
                                        onPressed: () {},
                                        icon: const Icon(
                                          Icons.navigate_next,
                                          color: Colors.black,
                                        ))
                                  ],
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            )),
      ),
      body: Obx(() => RefreshIndicator(
          color: const Color.fromRGBO(248, 129, 37, 1),
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(
              children: [
                Container(
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.fromLTRB(16.w, 8.h, 16.w, 8.h),
                        child: Row(
                          children: [
                            InkWell(
                              onTap: () {
                                _scaffoldKey.currentState?.openDrawer();
                              },
                              child: SizedBox(
                                height: 32.h,
                                width: 32.w,
                                child: Image.asset(
                                    "assets/images/icon_more_teacher.png"),
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(left: 8.w)),
                            Expanded(
                                child: Container(
                              height: 42.h,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(6.r),
                                  border: Border.all(
                                      color: const Color.fromRGBO(
                                          239, 239, 239, 1),
                                      width: 1)),
                              padding: const EdgeInsets.all(8),
                              child: ListView.builder(
                                  itemCount:
                                      controller.classOfManager.value.length,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (context, index) {
                                    return Obx(() => InkWell(
                                        onTap: () {
                                          var classUId = controller
                                              .classOfManager.value[index];
                                          controller.selectedClass(
                                              classUId, index);
                                        },
                                        child: Stack(
                                          alignment: Alignment.center,
                                          children: [
                                            Container(
                                              width: 100,
                                              height: 26,
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                color: controller.classOfManager
                                                        .value[index].checked
                                                    ? const Color.fromRGBO(
                                                        249, 154, 81, 1)
                                                    : const Color.fromRGBO(
                                                        246, 246, 246, 1),
                                                borderRadius:
                                                    BorderRadius.circular(6),
                                              ),
                                              margin: const EdgeInsets.only(
                                                  right: 8),
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      12, 4, 12, 4),
                                              child: Text(
                                                "${controller.classOfManager.value[index].name}",
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: controller
                                                            .classOfManager
                                                            .value[index]
                                                            .checked
                                                        ? Colors.white
                                                        : const Color.fromRGBO(
                                                            90, 90, 90, 1)),
                                              ),
                                            ),
                                            (controller
                                                        .classOfManager
                                                        .value[index]
                                                        .homeroomClass ==
                                                    true)
                                                ? Positioned(
                                                    right: 13,
                                                    top: 4,
                                                    child: Icon(
                                                      Icons.star,
                                                      size: 8,
                                                      color: controller
                                                              .classOfManager
                                                              .value[index]
                                                              .checked
                                                          ? Colors.white
                                                          : const Color
                                                                  .fromRGBO(
                                                              90, 90, 90, 1),
                                                    ),
                                                  )
                                                : Container()
                                          ],
                                        )));
                                  }),
                            )),
                            Padding(padding: EdgeInsets.only(left: 8.w)),
                            Container(
                              height: 32.h,
                              width: 32.h,
                              padding: const EdgeInsets.all(4),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6.r),
                                color: const Color.fromRGBO(248, 129, 37, 1),
                              ),
                              child: const Icon(
                                Icons.filter_alt,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 16.h)),
                      Container(
                        margin: EdgeInsets.only(left: 16.w),
                        child: Text(
                          "Quản Lý Lớp Học",
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 20.sp,
                              color: Colors.black),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 4.h)),
                      InkWell(
                        onTap: () {
                          Get.bottomSheet(
                              StatefulBuilder(builder: (context, state) {
                            return Wrap(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(32.r),
                                        topLeft: Radius.circular(32.r)),
                                    color: Colors.white,
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(6.r),
                                            color: const Color.fromRGBO(
                                                210, 212, 216, 1)),
                                        height: 6.h,
                                        width: 48.w,
                                        alignment: Alignment.topCenter,
                                        margin: EdgeInsets.only(top: 16.h),
                                      ),
                                      Padding(
                                          padding: EdgeInsets.only(top: 12.h)),
                                      Text(
                                        "Năm học",
                                        style: TextStyle(
                                            color: const Color.fromRGBO(
                                              23,
                                              32,
                                              63,
                                              1,
                                            ),
                                            fontSize: 16.sp,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      controller.fromYearPresent.value != 0
                                          ? Container(
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 16.h,
                                                  horizontal: 8.w),
                                              child: ListView.builder(
                                                  physics:
                                                      const ScrollPhysics(),
                                                  itemCount: controller
                                                      .schoolYears.value.length,
                                                  shrinkWrap: true,
                                                  itemBuilder:
                                                      (context, index) {
                                                    return InkWell(
                                                      child: Container(
                                                        color: controller
                                                                        .clickSchoolYear
                                                                        .value[
                                                                    index] ==
                                                                true
                                                            ? Color.fromRGBO(
                                                                254,
                                                                230,
                                                                211,
                                                                1)
                                                            : Colors.white,
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Padding(
                                                                padding: EdgeInsets
                                                                    .only(
                                                                        top: 16
                                                                            .h)),
                                                            Row(
                                                              children: [
                                                                Text(
                                                                  "Năm học ",
                                                                  style: TextStyle(
                                                                      color: controller.clickSchoolYear.value[index] ==
                                                                              true
                                                                          ? Color.fromRGBO(
                                                                              248,
                                                                              129,
                                                                              37,
                                                                              1)
                                                                          : Colors
                                                                              .black,
                                                                      fontSize:
                                                                          14.sp,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500),
                                                                ),
                                                                Text(
                                                                  "${controller.schoolYears.value[index].fromYear}",
                                                                  style: TextStyle(
                                                                      color: controller.clickSchoolYear.value[index] ==
                                                                              true
                                                                          ? Color.fromRGBO(
                                                                              248,
                                                                              129,
                                                                              37,
                                                                              1)
                                                                          : Colors
                                                                              .black,
                                                                      fontSize:
                                                                          14.sp,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500),
                                                                ),
                                                                Text(
                                                                  "-",
                                                                  style: TextStyle(
                                                                      color: controller.clickSchoolYear.value[index] ==
                                                                              true
                                                                          ? const Color.fromRGBO(
                                                                              248,
                                                                              129,
                                                                              37,
                                                                              1)
                                                                          : Colors
                                                                              .black,
                                                                      fontSize:
                                                                          14.sp,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500),
                                                                ),
                                                                Text(
                                                                  "${controller.schoolYears.value[index].toYear}",
                                                                  style: TextStyle(
                                                                      color: controller.clickSchoolYear.value[index] ==
                                                                              true
                                                                          ? const Color.fromRGBO(
                                                                              248,
                                                                              129,
                                                                              37,
                                                                              1)
                                                                          : Colors
                                                                              .black,
                                                                      fontSize:
                                                                          14.sp,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500),
                                                                ),
                                                              ],
                                                            ),
                                                            Padding(
                                                                padding: EdgeInsets
                                                                    .only(
                                                                        top: 16
                                                                            .h)),
                                                            Visibility(
                                                                visible:
                                                                    index != 3,
                                                                child:
                                                                    const Divider(
                                                                  height: 0,
                                                                  indent: 0,
                                                                  thickness: 1,
                                                                )),
                                                          ],
                                                        ),
                                                      ),
                                                      onTap: () {
                                                        updated(state, index);
                                                        controller.onSelectSchoolYears(index);
                                                      },
                                                    );
                                                  }),
                                            )
                                          : Container(),
                                    ],
                                  ),
                                )
                              ],
                            );
                          }));
                        },
                        child: Container(
                          margin: const EdgeInsets.only(left: 16),
                          child: Row(
                            children: [
                              Text(
                                "Năm học ",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                "${controller.fromYearPresent.value}",
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    color: controller.clickSchoolYear.value
                                                .contains(true) ==
                                            true
                                        ? Color.fromRGBO(248, 129, 37, 1)
                                        : Colors.black,
                                    fontWeight: FontWeight.w400),
                              ),
                              Text(
                                "-",
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    color: controller.clickSchoolYear.value
                                                .contains(true) ==
                                            true
                                        ? Color.fromRGBO(248, 129, 37, 1)
                                        : Colors.black,
                                    fontWeight: FontWeight.w400),
                              ),
                              Text(
                                "${controller.toYearPresent.value}",
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    color: controller.clickSchoolYear.value
                                                .contains(true) ==
                                            true
                                        ? Color.fromRGBO(248, 129, 37, 1)
                                        : Colors.black,
                                    fontWeight: FontWeight.w400),
                              ),
                              Padding(padding: EdgeInsets.only(left: 8.w)),
                              Icon(
                                Icons.keyboard_arrow_down_outlined,
                                size: 18.w,
                                color: Colors.black,
                              )
                            ],
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 8.h)),
                      Container(
                        margin: EdgeInsets.only(left: 16.w, right: 16.w),
                        child: Row(
                          children: [
                            Expanded(
                                child: InkWell(
                              onTap: () {
                                Get.to(ListBlockDiligentManagerPage());
                              },
                              child: Card(
                                color: Colors.white,
                                margin: EdgeInsets.zero,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(6.r)),
                                elevation: 3,
                                child: Container(
                                  padding: const EdgeInsets.all(8),
                                  child: Column(
                                    children: [
                                      SizedBox(
                                        width: 100.w,
                                        height: 100.h,
                                        child: Image.asset(
                                            "assets/images/img_amicroteacher.png"),
                                      ),
                                      Padding(
                                          padding: EdgeInsets.only(top: 8.h)),
                                      Text("Quản Lý Chuyên Cần",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12.sp,
                                              color: Colors.black))
                                    ],
                                  ),
                                ),
                              ),
                            )),
                            Padding(padding: EdgeInsets.only(left: 16.w)),
                            Expanded(
                                child: InkWell(
                                    onTap: () {
                                      controller.goToStudentListPage();
                                    },
                                    child: Card(
                                      color: Colors.white,
                                      elevation: 3,
                                      margin: EdgeInsets.zero,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(6.r)),
                                      child: Container(
                                        padding: const EdgeInsets.all(8),
                                        child: Column(
                                          children: [
                                            SizedBox(
                                              width: 100.w,
                                              height: 100.h,
                                              child: Image.asset(
                                                  "assets/images/img_list_student.png"),
                                            ),
                                            const Padding(
                                                padding:
                                                    EdgeInsets.only(top: 8)),
                                            const Text("Danh Sách Học Sinh",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 12,
                                                    color: Colors.black))
                                          ],
                                        ),
                                      ),
                                    ))),
                          ],
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 16.h)),
                      Container(
                        margin: EdgeInsets.only(left: 16.w, right: 16.w),
                        child: Row(
                          children: [
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  // Get.to(MySendNotification());
                                },
                                child: Card(
                                  color: Colors.white,
                                  margin: EdgeInsets.zero,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(6.r)),
                                  elevation: 3,
                                  child: Container(
                                    padding: const EdgeInsets.all(8),
                                    child: Column(
                                      children: [
                                        SizedBox(
                                          height: 100.h,
                                          width: 100.w,
                                          child: Image.asset(
                                              "assets/images/img_sendnotify.png"),
                                        ),
                                        Padding(
                                            padding: EdgeInsets.only(top: 8.h)),
                                        Text("Gửi thông báo",
                                            style: TextStyle(
                                                fontWeight: FontWeight.w500,
                                                fontSize: 12.sp,
                                                color: Colors.black))
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(left: 16.w)),
                            Expanded(
                              child: Container(),
                            )
                          ],
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(bottom: 16.h)),
                    ],
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 16.h)),
                Container(
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(padding: EdgeInsets.only(top: 16.h)),
                      Container(
                        margin: EdgeInsets.only(left: 16.w),
                        child: Text(
                          "Thông tin từ nhà trường",
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 20.sp,
                              color: Colors.black),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 16.h)),
                      Container(
                        margin: EdgeInsets.only(left: 16.w, right: 16.w),
                        child: Row(
                          children: [
                            Expanded(
                              child: Card(
                                color: Colors.white,
                                margin: EdgeInsets.zero,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(6.r)),
                                elevation: 3,
                                child: Container(
                                  padding: const EdgeInsets.all(8),
                                  child: Column(
                                    children: [
                                      SizedBox(
                                        height: 100.h,
                                        width: 100.w,
                                        child: Image.asset(
                                            "assets/images/img_event.png"),
                                      ),
                                      Padding(
                                          padding: EdgeInsets.only(top: 8.h)),
                                      Text("Tin tức sự kiện",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12.sp,
                                              color: Colors.black))
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(left: 16.w)),
                            Expanded(
                              child: Card(
                                color: Colors.white,
                                elevation: 3,
                                margin: EdgeInsets.zero,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(6.r)),
                                child: Container(
                                  padding: const EdgeInsets.all(8),
                                  child: Column(
                                    children: [
                                      SizedBox(
                                        height: 100.h,
                                        width: 100.w,
                                        child: Image.asset(
                                            "assets/images/img_sendnotify.png"),
                                      ),
                                      const Padding(
                                          padding: EdgeInsets.only(top: 8)),
                                      Text("Thông Báo",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12.sp,
                                              color: Colors.black))
                                    ],
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(bottom: 16.h)),
                    ],
                  ),
                ),
                Padding(padding: EdgeInsets.only(bottom: 16.h)),
              ],
            ),
          ),
          onRefresh: () async {
            await controller.onRefresh();
          })),
    ));
  }

  Future<Null> updated(StateSetter updateState, int index) async {
    updateState(() {
      controller.onClickShoolYears(index);
    });
  }
}
