import 'dart:ui';

import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:task_manager/commom/app_cache.dart';
import 'package:task_manager/commom/utils/app_utils.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/data/model/common/banner.dart';
import 'package:task_manager/data/model/common/contacts.dart';
import 'package:task_manager/data/model/common/subject.dart';
import 'package:task_manager/data/model/common/user_profile.dart';
import 'package:task_manager/data/model/res/class/classTeacher.dart';
import 'package:task_manager/data/repository/common/common_repo.dart';
import 'package:task_manager/data/repository/contact/contact_repo.dart';
import 'package:task_manager/data/repository/person/personal_info_repo.dart';
import 'package:task_manager/data/repository/subject/class_office_repo.dart';
import 'package:task_manager/view/mobile/role/parent/diligence_parent/diligence_parent_page.dart';
import 'package:task_manager/view/mobile/schedule/schedule_controller.dart';

import '../../../../commom/widget/hexColor.dart';
import '../../../../data/model/common/schedule.dart';
import '../../../../data/model/common/school_year.dart';
import '../../../../data/model/common/student_by_parent.dart';
import '../../../../data/repository/schedule/schedule_repo.dart';
import '../../../../data/repository/school_year/school_year_repo.dart';
import '../../../../routes/app_pages.dart';
import '../../phonebook/phone_book_controller.dart';


class ParentHomeController extends GetxController {
  var listStudent = <StudentByParent>[].obs;
  final CommonRepo _commonRepo = CommonRepo();
  final PersonalInfoRepo _personalInfoRepo = PersonalInfoRepo();
  final ClassOfficeRepo _subjectRepo = ClassOfficeRepo();
  final ScheduleRepo _scheduleRepo = ScheduleRepo();
  var isReady = false.obs;
  var userProfile = UserProfile().obs;
  var currentStudentProfile = StudentByParent().obs;
  RxList<Schedule> schedule = <Schedule>[].obs;
  RxList<SubjectItem> items = <SubjectItem>[].obs;
  RxList<Appointment> timeTable = <Appointment>[].obs;
  final SchoolYearRepo _schoolYearRepo = SchoolYearRepo();
  var clickSchoolYear = <bool>[].obs;
  RxList<Schedule> scheduleStudent = <Schedule>[].obs;
  RxList<SchoolYear> schoolYears = <SchoolYear>[].obs;
  RxList<Clazzs> clazzsOfSchool  = <Clazzs>[].obs;
  RxList<Clazzs> clazzsOfStudent  = <Clazzs>[].obs;
  var clazzInSchoolYear = Clazzs().obs;
  RxList<Clazzs> className =<Clazzs>[].obs;
  RxString studentName = "".obs;
  final ContactRepo _contactRepo = ContactRepo();
  var contacts = Contacts().obs;
  RxList<ItemContact>  itemContact = <ItemContact>[].obs;
  RxList<ClassId> classOfUser = <ClassId>[].obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  RxList<Schedule> scheduleToday = <Schedule>[].obs;
  var calendarController = CalendarController().obs;
  var fromYearPresent = 0.obs;
  var toYearPresent = 0.obs;
  @override
  void onInit() {
    super.onInit();
    getDetailProfile();
    getBanner();
    calendarController.value.displayDate = DateTime(DateTime.now().year,DateTime.now().month,DateTime.now().day,DateTime.now().hour-1);
  }

  getDetailProfile() async{
    _personalInfoRepo.detailUserProfile().then((value) {
      if (value.state == Status.SUCCESS) {
        userProfile.value = value.object!;
        isReady.value = true;
        if(userProfile.value.item?.students?.length !=0){
          getSchoolYearByStudent(userProfile.value.item?.students![0].id);
          studentName.value = userProfile.value.item!.students![0].fullName!;
        }
      }
    });
  }

  getListStudent()  {
    var userId = userProfile.value.id;
    _personalInfoRepo.listStudentByParent(userId).then((value) {
      if (value.state == Status.SUCCESS) {
        listStudent.value =value.object!;
        currentStudentProfile.value = listStudent.value[0];
        className.value = currentStudentProfile.value.clazz!;
        Get.find<ScheduleController>().queryTimeTableByStudent(currentStudentProfile.value.id);
        Get.find<PhoneBookController>().queryListClassByParent(currentStudentProfile.value.id);
      }
    });
  }

  getSchoolYearByStudent(userId) async {
    _schoolYearRepo.getListSchoolYearByStudent(userId).then((value) {
      if (value.state == Status.SUCCESS) {
        schoolYears.value = value.object!;
        for(int i = 0;i<schoolYears.value.length;i++){
          clickSchoolYear.add(false);
        }
        if(schoolYears.value.length !=0){
          fromYearPresent.value = schoolYears.value[0].fromYear!;
          toYearPresent.value = schoolYears.value[0].toYear!;
        }
      }
      getListStudent();
      if(userProfile.value.item?.students?.length !=0){
        getScheduleParent(userProfile.value.item?.students![0].id);
        getListSubject(userProfile.value.item?.students![0].id);
      }
    });
  }

  setTextSchoolYears(){
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      if(fromYearPresent.value == DateTime.now().year){
        return "nay";
      }else{
        return "${fromYearPresent.value} - ${toYearPresent.value}";
      }
    }else{
      if(toYearPresent.value == DateTime.now().year){
        return "nay";
      }else{
        return "${fromYearPresent.value} - ${toYearPresent.value}";
      }
    }
  }

  onClickShoolYears(index){
    clickSchoolYear.value = <bool>[].obs;
    for(int i = 0;i<schoolYears.value.length;i++){
      clickSchoolYear.value.add(false);
    }
    clickSchoolYear.value[index] = true;
  }


  setTimeCalendar(index){
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      calendarController.value.displayDate = DateTime(schoolYears.value[index].fromYear!.toInt(),DateTime.now().month,DateTime.now().day);
    }else{
      calendarController.value.displayDate = DateTime(schoolYears.value[index].toYear!.toInt(),DateTime.now().month,DateTime.now().day);
    }
  }

  toDetailPhonebook(type,index){
    Get.toNamed(Routes.detailPhonebook,arguments: [itemContact.value,type,classOfUser.value[index].name, classOfUser.value[index].id]);
  }

  getScheduleParent(studentId) async {
    _scheduleRepo.getScheduleStudent(studentId).then((value) {
      if (value.state == Status.SUCCESS) {
        scheduleStudent.value = value.object!;
        getAppointments();
      }
    });
  }

  getAppointments() {
    timeTable.clear();
    for(int i =0; i<scheduleStudent.value.length;i++){
      timeTable.value.add(Appointment(
        startTime: DateTime.fromMillisecondsSinceEpoch(scheduleStudent.value[i].startTime!),
        endTime: DateTime.fromMillisecondsSinceEpoch(scheduleStudent.value[i].endTime!),
        subject: scheduleStudent.value[i].subject!.subjectCategory!.name!,
        notes: scheduleStudent.value[i].subject?.clazz?.classCategory?.name!.toString(), // tên lớp
        resourceIds: scheduleStudent.value[i].subject!.clazz!.students,
          recurrenceId: scheduleStudent.value[i].subject?.teacher?.fullName!.toString(),
          color: Color.fromRGBO(249, 154, 81, 1)
      ));
    }
    scheduleToday.value = scheduleStudent.value.where((element) => outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(element.startTime!))
        == outputDateFormat.format(DateTime.now())&&outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(element.endTime!))
        == outputDateFormat.format(DateTime.now())).toList();
    timeTable.refresh();
  }


  var banner = BannerHomePage().obs;
  RxList<ItemBanner> itemBanner = <ItemBanner>[].obs;
  RxInt indexPageBanner = 0.obs;

  selectPageBanner(int index) {
    indexPageBanner.value = index;
    itemBanner.value.forEach((element) => element.selected = false);
    itemBanner.value[index].selected = true;
    if (index != 0) {
      indexPageBanner.value - 1;
    } else {
      indexPageBanner.value = 0;
    }
    itemBanner.refresh();
  }

  getBanner() async {
    _commonRepo.getBanner().then((value) {
      if (value.state == Status.SUCCESS) {
        banner.value = value.object!;
        itemBanner.value = banner.value.items!;
        itemBanner.value[0].selected = true;
        itemBanner.refresh();
      }
    });
  }




  getListSubject(studentId)  {
    items.clear();
    _subjectRepo.listSubject(studentId).then((value) {
      if (value.state == Status.SUCCESS) {
        items.value =value.object!;
      }
    });
    items.refresh();
  }


  onSelectUser(StudentByParent value) {
    timeTable.value.clear();
    currentStudentProfile.value = value;
    studentName.value = currentStudentProfile.value.fullName!;
    _schoolYearRepo.getListSchoolYearByStudent(currentStudentProfile.value.id).then((value) {
      if (value.state == Status.SUCCESS) {
        schoolYears.value = value.object!;
        for(int i = 0;i<schoolYears.value.length;i++){
          clickSchoolYear.add(false);
        }
      }
    });
    calendarController.value.displayDate = DateTime(DateTime.now().year,DateTime.now().month,DateTime.now().day,DateTime.now().hour-1);
    getScheduleParent(currentStudentProfile.value.id);
    getListSubject(currentStudentProfile.value.id);
    Get.find<ScheduleController>().queryTimeTableByStudent(currentStudentProfile.value.id);
    Get.find<PhoneBookController>().queryListClassByParent(currentStudentProfile.value.id);
    timeTable.refresh();
  }


  onRefresh() {
    getBanner();
    _personalInfoRepo.detailUserProfile().then((value) {
      if (value.state == Status.SUCCESS) {
        userProfile.value = value.object!;
      }
    });
    _personalInfoRepo.listStudentByParent(userProfile.value.id).then((value) {
      if (value.state == Status.SUCCESS) {
        listStudent.value =value.object!;
      }
    });
    getScheduleParent(currentStudentProfile.value.id);
    getListSubject(currentStudentProfile.value.id);
    _schoolYearRepo.getListSchoolYearByStudent(currentStudentProfile.value.id).then((value) {
      if (value.state == Status.SUCCESS) {
        schoolYears.value = value.object!;
        for(int i = 0;i<schoolYears.value.length;i++){
          clickSchoolYear.add(false);
        }
      }
    });
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      calendarController.value.displayDate = DateTime(fromYearPresent.value,DateTime.now().month,DateTime.now().day,DateTime.now().hour-1);
    }else{
      calendarController.value.displayDate = DateTime(toYearPresent.value,DateTime.now().month,DateTime.now().day,DateTime.now().hour-1);
    }
  }


  void gotoInformation(){
    var tmpList = <StudentByParent>[];
    tmpList.addAll(listStudent.value);
    Get.toNamed(Routes.personalInfoParent,arguments: tmpList);
  }
  goToDiligenceParent(){
    Get.toNamed(Routes.diligenceParentPage);
  }

  toSubjectPage() {
    Get.toNamed(Routes.subject, arguments: items.value);
  }

}
