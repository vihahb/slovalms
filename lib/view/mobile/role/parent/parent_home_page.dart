import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:task_manager/commom/utils/date_time_utils.dart';
import 'package:task_manager/commom/widget/appointment_builder_timelineday.dart';
import 'package:task_manager/commom/widget/meeting_data_source.dart';
import 'package:task_manager/data/model/common/user_profile.dart';
import 'package:task_manager/view/mobile/home/home_controller.dart';
import 'package:task_manager/view/mobile/role/parent/parent_home_controller.dart';

import '../../../../commom/app_cache.dart';
import '../../../../data/model/common/student_by_parent.dart';
import '../../phonebook/phone_book_controller.dart';
import '../../schedule/schedule_controller.dart';

class ParentHomePage extends GetWidget<ParentHomeController> {
  final controller = Get.put(ParentHomeController());
  @override
  Widget build(BuildContext context) {
    var isPhone = Get.context?.isPhone ?? true;
    return Scaffold(
        body: Obx(() =>
        (controller.isReady.value)
            ? RefreshIndicator(
            color: const Color.fromRGBO(248, 129, 37, 1),
            child: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          child: Stack(
            children: [
              buildBanner(),
              Container(
                margin: EdgeInsets.only(top: (isPhone) ? 180.h : 200.h),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    headerStudent(),
                    Padding(padding: EdgeInsets.only(top: 8.h)),
                    Container(
                      margin: const EdgeInsets.fromLTRB(16, 20, 16, 6),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(
                            children: [
                              const SizedBox(
                                width: 200,
                                child: Text("Môn Học",
                                    style: TextStyle(
                                        fontSize: 20,
                                        color: Color.fromRGBO(
                                            26, 26, 26, 1))),
                              ),
                              const Padding(
                                  padding: EdgeInsets.only(top: 6)),
                              SizedBox(
                                width: 200,
                                child: Text(
                                  "Năm ${controller.setTextSchoolYears()} bạn có ${controller.items.value.length} môn học",
                                  style: const TextStyle(
                                      fontSize: 12,
                                      color: Color.fromRGBO(
                                          133, 133, 133, 1)),
                                ),
                              ),
                            ],
                          ),
                          Flexible(child: Container()),
                          InkWell(
                            onTap: () {
                              controller.toSubjectPage();
                            },
                            child: const Text(
                              "Xem Tất cả",
                              style: TextStyle(
                                  fontSize: 12,
                                  color: Color.fromRGBO(248, 129, 37, 1)),
                            ),
                          )
                        ],
                      ),
                    ),
                    scheduleSubject(),
                    Container(
                      margin: const EdgeInsets.fromLTRB(16, 20, 16, 6),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(
                            children: [
                              const SizedBox(
                                width: 200,
                                child: Text("Thời khóa biểu",
                                    style: TextStyle(
                                        fontSize: 20,
                                        color: Color.fromRGBO(
                                            26, 26, 26, 1))),
                              ),
                            ],
                          ),
                          Expanded(child: Container()),
                          InkWell(
                            onTap: (){
                              Get.find<HomeController>().getSchedule();
                            },
                            child:  Text(
                              "Xem Tất cả",
                              style: TextStyle(
                                  fontSize: 12,
                                  color: Color.fromRGBO(248, 129, 37, 1)),
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(top: 8.h)),
                    Card(
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                      margin: const EdgeInsets.only(left: 16, right: 16, bottom: 20),
                      child: Obx(() => SfCalendar(
                        view: CalendarView.timelineDay,
                        firstDayOfWeek: 1,
                        allowViewNavigation: false,
                        controller: controller.calendarController.value,
                        dataSource: MeetingDataSource(controller.timeTable.value),
                        todayHighlightColor: const Color.fromRGBO(249, 154, 81, 1),
                        appointmentBuilder: appointmentBuilderTimeLineDay,
                        timeSlotViewSettings: TimeSlotViewSettings(
                          timeFormat: "H a",
                          timeIntervalWidth: 100.w,
                          timelineAppointmentHeight: 50,
                        ),
                        scheduleViewSettings: const ScheduleViewSettings(
                            appointmentItemHeight: 20, hideEmptyScheduleWeek: true),
                      )),
                    )
                  ],
                ),
              ),
            ],
          ),
        ), onRefresh: ()async{
              await controller.onRefresh();
        })
            : const Center(
                child: CircularProgressIndicator(),
              )));
  }

  buildIndicatorBanner() {
    return SizedBox(
      height: 6.h,
      child: ListView.builder(
          itemCount: controller.itemBanner.value.length,
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return controller.itemBanner.value[index].selected
                ? Container(
                    width: 12.w,
                    height: 10,
                    margin: EdgeInsets.only(left: 4.w, right: 4.w),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(6)),
                  )
                : SizedBox(
                    width: 10.w,
                    height: 10.h,
                    child: const CircleAvatar(
                      backgroundColor: Color.fromRGBO(252, 252, 254, 0.5),
                    ),
                  );
          }),
    );
  }

  buildBanner() {
    return Obx(() => SizedBox(
        height: 240.h,
        child: PageView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: controller.itemBanner.value.length,
            onPageChanged: (value) {
              controller.selectPageBanner(value);
            },
            itemBuilder: (context, index) {
              var url = "${controller.itemBanner.value[index].url}";
              return Image.network(
                url,
                width: double.infinity,
                fit: BoxFit.fitWidth,
              );
            })));
  }

  scheduleSubject() {
    return Obx(() => GridView.builder(
        padding: const EdgeInsets.all(10),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          crossAxisSpacing: 5,
          mainAxisSpacing: 5,
        ),
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        physics: const ClampingScrollPhysics(),
        itemCount: (controller.items.value.length > 6)
            ? 6
            : controller.items.value.length,
        itemBuilder: (context, index) {
          return SizedBox(
            height: 96.w,
            child: Card(
              elevation: 0.1,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 50.h,
                    width: 50.h,
                    child: Image.network(
                      "${controller.items.value[index].image}",
                      errorBuilder: (context, error, stackTrace) {
                        return Image.asset("assets/images/imageMath.png");
                      },
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 8.h)),
                  Text(
                    "${controller.items.value[index].name}",
                    style: TextStyle(
                        color: const Color.fromRGBO(26, 26, 26, 1),
                        fontSize: 12.sp),
                  )
                ],
              ),
            ),
          );
        }));
  }

  headerStudent() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8.w),
      child: Column(
        children: [
          buildIndicatorBanner(),
          Padding(padding: EdgeInsets.only(bottom: 8.h)),
          Container(
            decoration: BoxDecoration(
                color: const Color.fromRGBO(255, 255, 255, 1),
                borderRadius: BorderRadius.circular(8.r)),
            child: Column(
              children: [
                Padding(padding: EdgeInsets.only(bottom: 8.h)),
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 16.w, top: 16.w),
                    ),
                    Image.asset(
                      "assets/images/img_header.png", height: 48.h,width: 48.w,),
                    Padding(padding: EdgeInsets.only(left: 16.w)),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Obx(() => Text(
                              "Chào, ${controller.userProfile.value.fullName}",
                              style: TextStyle(
                                  fontSize: 16.sp,
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                  fontWeight: FontWeight.w400),
                            )),
                        InkWell(
                          onTap: () {
                            Get.bottomSheet(bottomSheetStudentProfile());
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              RichText(
                                textAlign: TextAlign.start,
                                text: TextSpan(
                                  text:
                                  '${controller.studentName.value} ',
                                  style: TextStyle(
                                      color: const Color.fromRGBO(
                                          26, 26, 26, 1),
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400,
                                      fontFamily:
                                      'static/Inter-Medium.ttf'),
                                  children: controller.className.value
                                      ?.map((e) {
                                    var index = controller.className.value
                                        ?.indexOf(e);
                                    var showSplit = ", ";
                                    if (index ==
                                        (controller.className.value.length)! -
                                            1) {
                                      showSplit = "";
                                    }
                                    return TextSpan(
                                        text: "${e.name}$showSplit",
                                        style: TextStyle(
                                            color: const Color.fromRGBO(
                                                248, 129, 37, 1),
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.w500,
                                            fontFamily:
                                            'static/Inter-Medium.ttf'));
                                  }).toList(),
                                ),
                              ),
                              Padding(padding: EdgeInsets.only(left: 8.w)),
                              Icon(
                                Icons.keyboard_arrow_down_outlined,
                                size: 18.w,
                                color: Colors.black,
                              )
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
                Padding(padding: EdgeInsets.only(top: 16.h)),
                Row(
                  children: [
                    Padding(padding: EdgeInsets.only(left: 16.w)),
                    Expanded(
                      flex: 2,
                      child: InkWell(
                        onTap: () {
                          controller.goToDiligenceParent();
                        },
                        child: Column(
                          children: [
                            Image.asset("assets/images/icon_diligence_std.png",width: 24.w,height: 24.h),
                            Padding(padding: EdgeInsets.only(top: 11.h)),
                            Text(
                              "Chuyên cần",
                              style: TextStyle(
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                  fontSize: 12.sp),
                            )
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: InkWell(
                        onTap: () {
                          //To Bảng điểm
                          // Get.to(Transcript());
                        },
                        child: Column(
                          children: [
                            Image.asset("assets/images/icon_bangdiem_std.png",width: 24.w,height: 24.h),
                            Padding(padding: EdgeInsets.only(top: 11.h)),
                            Text(
                              "Bảng điểm",
                              style: TextStyle(
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                  fontSize: 12.sp),
                            )
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: InkWell(
                        onTap: () {
                          Get.bottomSheet(
                              StatefulBuilder(builder: (context, state) {
                            return Wrap(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(32.r),
                                        topLeft: Radius.circular(32.r)),
                                    color: Colors.white,
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(6.r),
                                            color: const Color.fromRGBO(
                                                210, 212, 216, 1)),
                                        height: 6.h,
                                        width: 48.w,
                                        alignment: Alignment.topCenter,
                                        margin: EdgeInsets.only(top: 16.h),
                                      ),
                                      Padding(
                                          padding: EdgeInsets.only(top: 12.h)),
                                      Text(
                                        "Năm Học",
                                        style: TextStyle(
                                            fontSize: 16.sp,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      controller.schoolYears.value.isNotEmpty?Container(
                                        margin: EdgeInsets.symmetric(
                                            vertical: 16.h, horizontal: 8.w),
                                        child: ListView.builder(
                                            physics: const ScrollPhysics(),
                                            itemCount: controller
                                                .schoolYears.value.length,
                                            shrinkWrap: true,
                                            itemBuilder: (context, index) {
                                              if(controller.schoolYears.value.length!=0){
                                                controller.clazzsOfSchool.value = controller.schoolYears.value[index].clazz!;
                                              };
                                              return controller.schoolYears.value.length == 0?Container():InkWell(
                                                child: Column(
                                                  children: [
                                                    Container(
                                                      color:
                                                      controller.clickSchoolYear.value[
                                                      index] ==
                                                          true
                                                          ? const Color
                                                          .fromRGBO(
                                                          254,
                                                          230,
                                                          211,
                                                          1)
                                                          : Colors.white,
                                                      padding: const EdgeInsets
                                                          .fromLTRB(
                                                          0, 8, 0, 8),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                        crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                        children: [
                                                          SizedBox(
                                                            width: 40.w,
                                                            height: 40.w,
                                                            child: CircleAvatar(
                                                              backgroundColor: Colors.white,
                                                              backgroundImage:  Image.network(
                                                                controller
                                                                    .schoolYears[
                                                                index]
                                                                    .image ??
                                                                    "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                                                                errorBuilder:
                                                                    (context,
                                                                    object,
                                                                    stackTrace) {
                                                                  return Image
                                                                      .asset(
                                                                    "assets/images/avatar.png",
                                                                  );
                                                                },
                                                              ).image,
                                                            ),
                                                          ),

                                                          const Padding(
                                                              padding: EdgeInsets
                                                                  .only(
                                                                  left: 8)),
                                                          Expanded(child: Column(
                                                            mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                            crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                            children: [
                                                              RichText(
                                                                text: TextSpan(
                                                                    children: [
                                                                      TextSpan(
                                                                          text:
                                                                          'Học Sinh: ',
                                                                          style: TextStyle(
                                                                              color: Color.fromRGBO(248, 129, 37, 1),
                                                                              fontFamily: 'static/Inter-Regular.ttf',
                                                                              fontSize: 14.sp)),
                                                                      TextSpan(
                                                                          text:
                                                                          "${controller.schoolYears.value[index].fullName!}",
                                                                          style: TextStyle(
                                                                              color: Colors.black,
                                                                              fontFamily: 'static/Inter-Regular.ttf',
                                                                              fontSize: 14.sp)),
                                                                    ]),
                                                              ),
                                                              RichText(
                                                                textAlign: TextAlign.end,
                                                                text: TextSpan(
                                                                  text: 'Lớp: ',
                                                                  style: TextStyle(
                                                                      color: const Color.fromRGBO(26, 26, 26, 1),
                                                                      fontSize: 14.sp,
                                                                      fontWeight: FontWeight.w500,
                                                                      fontFamily: 'static/Inter-Medium.ttf'),

                                                                  children: controller.clazzsOfSchool.value.map((e) {
                                                                    var index = controller.clazzsOfSchool.value.indexOf(e);
                                                                    var showSplit = ", ";
                                                                    if (index == controller.clazzsOfSchool.value.length - 1) {
                                                                      showSplit = "";
                                                                    }
                                                                    return TextSpan(
                                                                        text: "${e.name}$showSplit",
                                                                        style: TextStyle(
                                                                            color: const Color.fromRGBO(248, 129, 37, 1),
                                                                            fontSize: 14.sp,
                                                                            fontWeight: FontWeight.w500,
                                                                            fontFamily: 'static/Inter-Medium.ttf'));
                                                                  }).toList(),
                                                                ),
                                                              )
                                                            ],
                                                          ),),
                                                          Container(
                                                            margin: EdgeInsets.only(right: 8.w),
                                                            child: Column(
                                                              crossAxisAlignment: CrossAxisAlignment.end,
                                                              children: [
                                                                Text(controller.currentStudentProfile.value.birthday != null
                                                                    ? DateTimeUtils.convertLongToStringTime(controller.currentStudentProfile.value.birthday)
                                                                    : ""),
                                                                Padding(padding: EdgeInsets.only(top: 4.h)),
                                                                Row(
                                                                  children: [
                                                                    Text(
                                                                      "Năm học:",
                                                                      style: TextStyle(
                                                                          color: Color.fromRGBO(133, 133, 133, 1),
                                                                          fontWeight:
                                                                          FontWeight
                                                                              .w400,
                                                                          fontSize:
                                                                          10.sp),
                                                                    ),
                                                                    Text(
                                                                      "${controller.schoolYears.value[index].fromYear}",
                                                                      style: TextStyle(
                                                                          color: Color.fromRGBO(133, 133, 133, 1),
                                                                          fontWeight:
                                                                          FontWeight
                                                                              .w400,
                                                                          fontSize:
                                                                          10.sp),
                                                                    ),
                                                                    Text(
                                                                      "-",
                                                                      style: TextStyle(
                                                                          color: const Color
                                                                              .fromRGBO(
                                                                              133,
                                                                              133,
                                                                              133,
                                                                              1),
                                                                          fontWeight:
                                                                          FontWeight
                                                                              .w400,
                                                                          fontSize:
                                                                          10.sp),
                                                                    ),
                                                                    Text(
                                                                      "${controller.schoolYears.value[index].toYear}",
                                                                      style: TextStyle(
                                                                          color: const Color
                                                                              .fromRGBO(
                                                                              133,
                                                                              133,
                                                                              133,
                                                                              1),
                                                                          fontWeight:
                                                                          FontWeight
                                                                              .w400,
                                                                          fontSize:
                                                                          10.sp),
                                                                    ),
                                                                  ],
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                onTap: () {
                                                  controller.timeTable.value.clear();
                                                  Get.find<ScheduleController>().timeTable.value.clear();
                                                  updated(state, index);
                                                  controller.className.value = controller.schoolYears.value[index].clazz!;
                                                  AppCache().setSchoolYearId(controller.schoolYears.value[index].id??"");
                                                  controller.getListSubject(controller.currentStudentProfile.value.id);
                                                  controller.getScheduleParent(controller.currentStudentProfile.value.id);
                                                  Get.find<ScheduleController>().queryTimeTableByStudent(controller.currentStudentProfile.value.id);
                                                  Get.find<PhoneBookController>().queryListClassByParent(controller.currentStudentProfile.value.id);
                                                  controller.fromYearPresent.value = controller.schoolYears.value[index].fromYear!;
                                                  controller.toYearPresent.value = controller.schoolYears.value[index].toYear!;
                                                  controller.timeTable.refresh();
                                                  controller.setTimeCalendar(index);
                                                  Get.find<ScheduleController>().queryTimeCalendar(index, AppCache().userType);
                                                  Get.find<ScheduleController>().timeTable.refresh();
                                                  Get.back();
                                                },
                                              );
                                            }),
                                      ):Container(),
                                      Padding(
                                          padding:
                                              EdgeInsets.only(bottom: 60.h))
                                    ],
                                  ),
                                )
                              ],
                            );
                          }));
                        },
                        child: Column(
                          children: [
                            Image.asset("assets/images/school_year_parent.png",width: 24.w,height: 24.h,),
                            Padding(padding: EdgeInsets.only(top: 11.h)),
                            Text(
                              "Năm Học",
                              style: TextStyle(
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                  fontSize: 12.sp),
                            )
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                        flex: 2,
                        child: InkWell(
                          onTap: () {
                            controller.gotoInformation();
                          },
                          child: Column(
                            children: [
                              Image.asset("assets/images/icon_profile.png",width: 24.w,height: 24.h,),
                              Padding(padding: EdgeInsets.only(top: 11.h)),
                              Text(
                                "Cá nhân",
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 12.sp),
                              )
                            ],
                          ),
                        )),
                    Padding(padding: EdgeInsets.only(right: 16.w)),
                  ],
                ),
                Padding(padding: EdgeInsets.only(bottom: 16.h)),
              ],
            ),
          ),
        ],
      ),
    );
  }

  bottomSheetStudentProfile() {
    return StatefulBuilder(builder: (context,state){
      return Wrap(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(32.r),
                  topLeft: Radius.circular(32.r)),
              color: Colors.white,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6.r),
                      color: const Color.fromRGBO(210, 212, 216, 1)),
                  height: 6.h,
                  width: 48.w,
                  alignment: Alignment.topCenter,
                  margin: EdgeInsets.only(top: 16.h),
                ),
                Padding(padding: EdgeInsets.only(top: 12.h)),
                Text(
                  "Học Sinh",
                  style: TextStyle(
                      fontSize: 16.sp,
                      color: Colors.black,
                      fontWeight: FontWeight.w500),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 16.h, horizontal: 8.w),
                  child: ListView.builder(
                      physics: const ScrollPhysics(),
                      itemCount: controller.listStudent.value.length,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        if(controller.listStudent.value[index].clazz!.length !=0){
                          controller.clazzsOfStudent.value = controller.listStudent.value[index].clazz!;
                        }
                        return InkWell(
                          child: Column(
                            children: [
                              Container(
                                padding: const EdgeInsets.fromLTRB(10, 8, 10, 0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      width: 40.w,
                                      height: 40.w,
                                      child: CircleAvatar(
                                        backgroundColor: Colors.white,
                                        backgroundImage: Image.network(
                                          controller.listStudent.value[index].image ??
                                              "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                                          errorBuilder: (context, object, stackTrace) {
                                            return Image.asset(
                                              "assets/images/avatar.png",
                                            );
                                          },
                                        ).image,
                                      ),
                                    ),

                                    Padding(padding: EdgeInsets.only(left: 8.w)),
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        RichText(
                                          text: TextSpan(children: [
                                            TextSpan(
                                                text: 'Học Sinh: ',
                                                style: TextStyle(
                                                    color: const Color.fromRGBO(248, 129, 37, 1),
                                                    fontFamily: 'static/Inter-Regular.ttf',
                                                    fontSize: 12.sp)),
                                            TextSpan(
                                                text: '${controller.listStudent.value[index].fullName ?? ""}',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontFamily: 'static/Inter-Regular.ttf',
                                                    fontSize: 14.sp)),
                                          ]),
                                        ),
                                        RichText(
                                          textAlign: TextAlign.end,
                                          text: TextSpan(
                                            text: 'Lớp: ',
                                            style: TextStyle(
                                                color: const Color.fromRGBO(248, 129, 37, 1),
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: 'static/Inter-Medium.ttf'),

                                            children: controller.clazzsOfStudent.value?.map((e) {
                                              var index = controller.clazzsOfStudent.value?.indexOf(e);
                                              var showSplit = ", ";
                                              if (index == (controller.clazzsOfStudent.value.length)! - 1) {
                                                showSplit = "";
                                              }
                                              return TextSpan(
                                                  text: "${e.name??""}$showSplit",
                                                  style: TextStyle(
                                                      color: const Color.fromRGBO(248, 129, 37, 1),
                                                      fontSize: 14.sp,
                                                      fontWeight: FontWeight.w500,
                                                      fontFamily: 'static/Inter-Medium.ttf'));
                                            }).toList(),
                                          ),
                                        )
                                      ],
                                    ),
                                    Expanded(child: Container()),
                                    Text(
                                      controller.listStudent.value[index].birthday != null
                                          ? DateTimeUtils.convertLongToStringTime(controller.listStudent.value[index].birthday)
                                          : "",
                                      style: TextStyle(
                                          color: const Color.fromRGBO(133, 133, 133, 1),
                                          fontWeight: FontWeight.w400,
                                          fontSize: 14.sp),
                                    ),
                                  ],
                                ),
                              ),
                              const Divider()
                            ],
                          ),
                          onTap: () {
                            controller.onSelectUser(controller.listStudent.value[index]);
                            controller.className.value = controller.listStudent.value[index].clazz!;
                            Get.back();
                          },
                        );
                      }),
                ),
                Padding(padding: EdgeInsets.only(bottom: 60.h))
              ],
            ),
          )
        ],
      );
    });
  }

  Future<Null> updated(StateSetter updateState, int index) async {
    updateState(() {
      for(int i = 0;i<controller.schoolYears.value.length;i++){
        controller.clickSchoolYear.add(false);
      }
    });
  }
}
