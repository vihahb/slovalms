import 'package:get/get.dart';
import 'package:task_manager/commom/app_cache.dart';
import 'package:task_manager/commom/utils/app_utils.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/data/model/common/user_profile.dart';
import 'package:task_manager/data/repository/account/auth_repo.dart';
import 'package:task_manager/data/repository/person/personal_info_repo.dart';
import 'package:task_manager/routes/app_pages.dart';
import 'package:task_manager/view/mobile/role/student/personal_information_student/detail_avatar/detail_avatar_page.dart';

import '../../../../../commom/widget/text_field_custom.dart';
import '../../../../../data/model/common/Position.dart';
import '../../../../../data/model/common/req_file.dart';
import '../../../../../data/model/common/static_page.dart';
import '../../../../../data/model/common/student_by_parent.dart';
import '../../../../../data/model/res/class/School.dart';
import '../../../../../data/model/res/file/response_file.dart';
import '../../../../../data/repository/common/common_repo.dart';
import '../../../../../data/repository/file/file_repo.dart';
import '../../../account/changePassword/change_pass_page.dart';
import 'dart:io';

import '../../../account/login/login_controller.dart';
class PersonalInformationParentController extends GetxController {
  Rx<StateType> stateInputUser = StateType.DEFAULT.obs;
  final AuthRepo _authRepo = AuthRepo();
  final PersonalInfoRepo _personalInfoRepo = PersonalInfoRepo();
  var userProfile = UserProfile().obs;
  var listStudent = <StudentByParent>[].obs;
  var files = <ReqFile>[].obs;
  final FileRepo fileRepo = FileRepo();
  var tmpUserProfile = UserProfile().obs;
  var position = Position().obs;
  var school = SchoolData().obs;
  var staticPage = ItemsStaticPage().obs;
  final CommonRepo _commonRepo = CommonRepo();
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    var data = Get.arguments;
    if (data != null) {
      listStudent.value = data;
    }

    getDetailProfile();

  }
  updateInfoUser(fullname,id) async {
    _personalInfoRepo.updateUserProfile(fullname,id).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Cập nhật thông tin cá nhân thành công");
        Future.delayed(Duration(seconds: 1), () {
          getDetailProfile();
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Cập nhật thông tin cá nhân thất bại", value.message ?? "");
      }
    });
  }
  /// Flow update infomation
  /// 1. get all person data
  /// 2. Check if file change (if not change => go to step 4)
  /// 3. upload file => get response
  /// 4. Update infomation with new url of avatar (if change)
  /// 5. Check response success => finish

  uploadFile(){
    var fileList = <File>[];
    files.forEach((element) {
      fileList.add(element.file!);
    });

    fileRepo.uploadFile(fileList).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Upload file thành công");
        var listResFile = value.object!;
        ResponseFileUpload updateAvatar = listResFile[0];
        if (updateAvatar!=null) {
          tmpUserProfile.value.img = updateAvatar;
          updateInfoUser(tmpUserProfile.value, tmpUserProfile.value.id);
        } else{
          AppUtils.shared.showToast("Upload file thất bại!");
        }
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Thông báo", value.message ?? "");
      }
    });
  }

  getDetailProfile() {
    _personalInfoRepo.detailUserProfile().then((value) {
      if (value.state == Status.SUCCESS) {
        userProfile.value = value.object!;
        tmpUserProfile.value = value.object!;
        if(userProfile.value.school == null || userProfile.value.school == ""){
          school.value.name = "";
          school.value.id = "";
        }else{
          school.value = userProfile.value.school!;
        }
        if(userProfile.value.position == null || userProfile.value.position == ""){
          position.value.name = "";
          position.value.id = "";
        }else{
          position.value = userProfile.value.position!;
        }
      }
    });
  }


  getStaticPage(type){
    _commonRepo.getListStaticPage(type).then((value){
      if (value.state == Status.SUCCESS) {
        staticPage.value = value.object!;
        goToStaticPage();
      }
    });
  }
  goToStaticPage(){
    Get.toNamed(Routes.staticPage,arguments: staticPage.value);
  }


  void submitLogout() async {
    logout();
  }

  logout() async {
    _authRepo.logout().then((value) {
      goToLogin();
      AppCache().resetCache();
    });
    if (AppCache().isSaveInfoLogin) {
      String? usernameSaved = await AppCache().username;
      String? passwordSaved = await AppCache().password;
      Get.find<LoginController>().controllerUserName.text = usernameSaved ?? "";
      Get.find<LoginController>().controllerPassword.text = passwordSaved ?? "";
    }else{
      AppCache().deleteInfoLogin();
    }
  }
  goToUpdateInfoUser(){
    Get.toNamed(Routes.updateInfoParent,arguments: [listStudent.value,userProfile.value]);
  }

  void goToLogin() {
    Get.offAllNamed(Routes.login);
  }
  void goToChangePassPage(){
    Get.toNamed(Routes.changePass,arguments:userProfile.value.id);
  }
  void goToDetailAvatar(){
    Get.toNamed(Routes.getavatar, arguments: userProfile.value.image);
  }


}
