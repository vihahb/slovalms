import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:task_manager/view/mobile/home/home_page.dart';
import 'package:task_manager/view/mobile/role/parent/diligence_parent/approval_parent/approval_parent_controller.dart';
import 'package:task_manager/view/mobile/role/parent/parent_home_controller.dart';
import '../../../../../../../commom/constants/date_format.dart';
import '../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../commom/utils/date_time_picker.dart';
import '../../../../../../../commom/utils/time_utils.dart';
import '../../../../../../../commom/widget/text_field_custom.dart';
import '../../../../../../../routes/app_pages.dart';
import '../../../../../home/home_controller.dart';
import 'create_a_leave_application_controller.dart';

class CreateALeavingApplicationPage
    extends GetWidget<CreateALeavingApplicationController> {
  final controller = Get.put(CreateALeavingApplicationController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color.fromRGBO(245, 245, 245, 1),
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              Get.toNamed(Routes.home);
            },
            icon: const Icon(Icons.home),
            color: Colors.white,
          )
        ],
        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
        title: Text(
          'Đơn xin nghỉ phép',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: Column(
        children: [
          Expanded(child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Container(
              margin: EdgeInsets.only(top: 16.h, left: 16.w, right: 16.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Đơn xin nghỉ phép',
                    style: TextStyle(
                        color: const Color.fromRGBO(177, 177, 177, 1),
                        fontFamily: 'assets/font/static/Inter-Medium.ttf',
                        fontSize: 12.sp,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 8.h),
                    padding: EdgeInsets.only(top: 8.h, bottom: 8.h),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: const Color.fromRGBO(255, 255, 255, 1)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(padding: EdgeInsets.only(bottom: 8.h)),
                        Obx(() => Container(
                          margin: EdgeInsets.symmetric(horizontal: 16.w),
                          alignment: Alignment.centerLeft,
                          padding: const EdgeInsets.all(2.0),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: const BorderRadius.all(
                                  Radius.circular(6.0)),
                              border: Border.all(
                                width: 1,
                                style: BorderStyle.solid,
                                color:
                                const Color.fromRGBO(192, 192, 192, 1),
                              )),
                          child: Container(
                            padding:
                            EdgeInsets.fromLTRB(16.w, 8.h, 16.w, 8.h),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Người nhận",
                                  style: TextStyle(
                                      fontSize: 10.0.sp,
                                      color: const Color.fromRGBO(
                                          177, 177, 177, 1),
                                      fontWeight: FontWeight.w500,
                                      fontFamily:
                                      'assets/font/static/Inter-Medium.ttf'),
                                ),
                                Padding(padding: EdgeInsets.only(top: 4.h)),
                                Text(
                                  controller.teacher.value.homeroomTeacher
                                      ?.fullName ??
                                      "",
                                  style: TextStyle(
                                    fontSize: 12.0.sp,
                                    color:
                                    const Color.fromRGBO(26, 26, 26, 1),
                                  ),
                                )
                              ],
                            ),
                          ),
                        )),
                        Padding(padding: EdgeInsets.only(top: 16.h)),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 16.w),
                          alignment: Alignment.centerLeft,
                          padding: const EdgeInsets.all(2.0),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                              const BorderRadius.all(Radius.circular(6.0)),
                              border: Border.all(
                                width: 1,
                                style: BorderStyle.solid,
                                color: const Color.fromRGBO(192, 192, 192, 1),
                              )),
                          child: Container(
                            padding: EdgeInsets.fromLTRB(16.w, 8.h, 16.w, 8.h),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Họ & tên học sinh",
                                  style: TextStyle(
                                      fontSize: 10.0.sp,
                                      color: const Color.fromRGBO(
                                          177, 177, 177, 1),
                                      fontWeight: FontWeight.w500,
                                      fontFamily:
                                      'assets/font/static/Inter-Medium.ttf'),
                                ),
                                Padding(padding: EdgeInsets.only(top: 4.h)),
                                Text(
                                  controller.studentName.value ?? "",
                                  style: TextStyle(
                                    fontSize: 12.0.sp,
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(top: 16.h)),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 2.w),
                          child: Row(
                            children: [
                              Expanded(
                                child: Container(
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(left: 8.w),
                                  margin:
                                  EdgeInsets.only(left: 14.w, right: 8.w),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(6.0)),
                                      border: Border.all(
                                        width: 1,
                                        style: BorderStyle.solid,
                                        color: const Color.fromRGBO(
                                            192, 192, 192, 1),
                                      )),
                                  child: TextFormField(
                                    keyboardType: TextInputType.multiline,
                                    maxLines: null,
                                    style: TextStyle(
                                      fontSize: 12.0.sp,
                                      color:
                                      const Color.fromRGBO(26, 26, 26, 1),
                                    ),
                                    onTap: () {
                                      selectDateTimeStart(
                                          controller
                                              .controllerdateStart.value.text,
                                          DateTimeFormat.formatDateShort,
                                          context);
                                    },
                                    cursorColor:
                                    const Color.fromRGBO(248, 129, 37, 1),
                                    controller: controller.controllerdateStart,
                                    readOnly: true,
                                    decoration: InputDecoration(
                                      suffixIcon: Container(
                                        margin: EdgeInsets.only(bottom: 4.h),
                                        child: SizedBox(
                                          height: 14,
                                          width: 14,
                                          child: Container(
                                            margin: EdgeInsets.zero,
                                            child: SvgPicture.asset(
                                              "assets/images/icon_date_picker.svg",
                                              fit: BoxFit.scaleDown,
                                            ),
                                          ),
                                        ),
                                      ),
                                      label: const Text("Từ thời gian"),
                                      border: InputBorder.none,
                                      labelStyle: TextStyle(
                                          color: const Color.fromRGBO(
                                              177, 177, 177, 1),
                                          fontSize: 12.sp,
                                          fontWeight: FontWeight.w500,
                                          fontFamily:
                                          'assets/font/static/Inter-Medium.ttf'),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(left: 8.w),
                                  margin:
                                  EdgeInsets.only(left: 8.w, right: 14.w),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(6.0)),
                                      border: Border.all(
                                        width: 1,
                                        style: BorderStyle.solid,
                                        color: const Color.fromRGBO(
                                            192, 192, 192, 1),
                                      )),
                                  child: TextFormField(
                                    keyboardType: TextInputType.multiline,
                                    maxLines: null,
                                    style: TextStyle(
                                      fontSize: 12.0.sp,
                                      color:
                                      const Color.fromRGBO(26, 26, 26, 1),
                                    ),
                                    onTap: () {
                                      selectDateTimeEnd(
                                          controller
                                              .controllerdateEnd.value.text,
                                          DateTimeFormat.formatDateShort,
                                          context);
                                    },
                                    readOnly: true,
                                    cursorColor:
                                    const Color.fromRGBO(248, 129, 37, 1),
                                    controller: controller.controllerdateEnd,
                                    decoration: InputDecoration(
                                      suffixIcon: Container(
                                        margin: EdgeInsets.only(bottom: 4.h),
                                        child: SizedBox(
                                          height: 14,
                                          width: 14,
                                          child: Container(
                                            margin: EdgeInsets.zero,
                                            child: SvgPicture.asset(
                                              "assets/images/icon_date_picker.svg",
                                              fit: BoxFit.scaleDown,
                                            ),
                                          ),
                                        ),
                                      ),
                                      label: const Text("Đến thời gian"),
                                      border: InputBorder.none,
                                      labelStyle: TextStyle(
                                          color: const Color.fromRGBO(
                                              177, 177, 177, 1),
                                          fontSize: 12.sp,
                                          fontWeight: FontWeight.w500,
                                          fontFamily:
                                          'assets/font/static/Inter-Medium.ttf'),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Obx(() => Visibility(
                          visible: controller.isShowListDate.value,
                          child: Container(
                            margin: EdgeInsets.only(left: 16.w),
                            child: ListView.builder(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: controller
                                    .listLeavingDetail.value.length,
                                itemBuilder: (context, index) {
                                  return Obx(() => Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          SizedBox(
                                            width: 150,
                                            child: Text(
                                                "${controller.setWeekDay(controller
                                                    .listDayLeavingApplication
                                                    .value[index].weekday+1)} ${controller.outputDateFormat
                                                    .format(controller
                                                    .listDayLeavingApplication
                                                    .value[index])}"),
                                          ),
                                          TextButton(
                                              onPressed: () {
                                                controller.checkBoxMorning(index);
                                              },
                                              child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  children: [
                                                    SizedBox(
                                                        height: 24.0,
                                                        width: 24.0,
                                                        child: Checkbox(
                                                            value:  controller.listLeavingDetail.value[index].sessionDay?.contains("AM") ==true,
                                                            activeColor: const Color.fromRGBO(248, 129, 37, 1),
                                                            onChanged: (value){
                                                              controller.checkBoxMorning(index);
                                                            }
                                                        )
                                                    ),
                                                    SizedBox(width: 10.0),
                                                    Text("Sáng",style: TextStyle(color: controller.listLeavingDetail.value[index].sessionDay?.contains("AM") == true?Color.fromRGBO(248, 129, 37, 1):Colors.black45,fontWeight: FontWeight.w500),)
                                                  ]
                                              )
                                          ),
                                          Container(
                                              child: TextButton(
                                                  onPressed: () {
                                                    controller.checkBoxAfternoon(index);
                                                  },
                                                  child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: [
                                                        SizedBox(
                                                            height: 24.0,
                                                            width: 24.0,
                                                            child: Checkbox(
                                                                activeColor: Color.fromRGBO(248, 129, 37, 1),
                                                                value:  controller.listLeavingDetail.value[index].sessionDay?.contains("PM"),
                                                                onChanged: (value){
                                                                  controller.checkBoxAfternoon(index);
                                                                }
                                                            )
                                                        ),
                                                        // You can play with the width to adjust your
                                                        // desired spacing
                                                        SizedBox(width: 10.0),
                                                        Text("Chiều",style: TextStyle(color:  controller.listLeavingDetail.value[index].sessionDay?.contains("PM")==true?Color.fromRGBO(248, 129, 37, 1):Colors.black45,fontWeight: FontWeight.w500))
                                                      ]
                                                  )
                                              )
                                          )
                                        ],
                                      )
                                    ],
                                  ));
                                }),
                          ),
                        )),
                        Padding(padding: EdgeInsets.only(top: 16.h)),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 4),
                          child: MyOutlineBorderTextFormFieldBinh(
                            enable: true,
                            focusNode: controller.focusReason,
                            iconPrefix: null,
                            iconSuffix: "assets/images/icon_resize.svg",
                            state: StateType.DEFAULT,
                            labelText: "Lý do làm đơn (*)",
                            autofocus: false,
                            controller: controller.controllerReason,
                            helperText: "",
                            showHelperText: false,
                            textInputAction: TextInputAction.next,
                            ishowIconPrefix: false,
                            keyboardType: TextInputType.text,
                            validation: (textToValidate) {
                              return controller
                                  .getTempIFSCValidation(textToValidate);
                            },
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(top: 16.h)),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 4),
                          child: MyOutlineBorderTextFormFieldBinh(
                            enable: true,
                            focusNode: controller.focusNote,
                            iconPrefix: null,
                            iconSuffix: "assets/images/icon_resize.svg",
                            state: StateType.DEFAULT,
                            labelText: "Ghi chú",
                            autofocus: false,
                            controller: controller.controllerNote,
                            helperText: "",
                            showHelperText: false,
                            textInputAction: TextInputAction.next,
                            ishowIconPrefix: false,
                            keyboardType: TextInputType.text,
                            validation: (textToValidate) {
                              return controller
                                  .getTempIFSCValidation(textToValidate);
                            },
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(top: 16.h)),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 4),
                          child: MyOutlineBorderTextFormFieldBinh(
                            enable: true,
                            focusNode: controller.focusAssure,
                            iconPrefix: null,
                            iconSuffix: "assets/images/icon_resize.svg",
                            state: StateType.DEFAULT,
                            labelText: "Cam đoan (*)",
                            autofocus: false,
                            controller: controller.controllerAssure,
                            helperText: "",
                            showHelperText: false,
                            textInputAction: TextInputAction.next,
                            ishowIconPrefix: false,
                            keyboardType: TextInputType.text,
                            validation: (textToValidate) {
                              return controller
                                  .getTempIFSCValidation(textToValidate);
                            },
                          ),
                        ),
                        Padding(
                          // this is new
                            padding: EdgeInsets.only(
                                bottom:
                                MediaQuery.of(context).viewInsets.bottom)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )),
          Container(
            color: const Color.fromRGBO(255, 255, 255, 1),
            height: 78,
            padding: const EdgeInsets.all(16),
            child: Row(
              children: [
                Expanded(
                    child: SizedBox(
                  height: 48,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor:
                              const Color.fromRGBO(255, 69, 89, 1)),
                      onPressed: () {
                        Navigator.of(Get.context!).pop();
                        controller.onInit();
                      },
                      child: Text(
                        'Hủy',
                        style: TextStyle(
                            color: const Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 16.sp,
                            fontWeight: FontWeight.w400,
                            fontFamily: 'assets/font/static/Inter-Regular.ttf'),
                      )),
                )),
                Padding(padding: EdgeInsets.only(right: 8.w)),
                Expanded(
                    child: SizedBox(
                  height: 48,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
                      ),
                      onPressed: () {
                        if (controller.controllerdateStart.value.text == "") {
                          AppUtils.shared
                              .showToast("Vui lòng nhập thời gian bắt đầu!");
                        } else {
                          if (controller.controllerdateEnd.value.text == "") {
                            AppUtils.shared
                                .showToast("Vui lòng nhập thời gian kết thúc!");
                          } else {
                            if (controller.controllerReason.value.text.trim() == "") {
                              AppUtils.shared.showToast("Vui lòng chọn lý do!");
                            } else {
                              if (DateFormat('dd/MM/yyyy')
                                      .parse(controller
                                          .controllerdateStart.value.text)
                                      .millisecondsSinceEpoch >
                                  DateFormat('dd/MM/yyyy')
                                      .parse(controller
                                          .controllerdateEnd.value.text)
                                      .millisecondsSinceEpoch) {
                                AppUtils.shared.showToast(
                                    "Vui lòng chọn thời gian kết thúc lớn hơn thời gian bắt đầu");
                              } else {
                                if (controller.controllerAssure.value.text.trim()  ==
                                    "") {
                                  AppUtils.shared
                                      .showToast("Vui lòng viết cam đoan!");
                                } else {
                                  controller.createALeaveApplication();

                                }
                              }
                            }
                          }
                        }
                      },
                      child: Text(
                        'Nộp Đơn',
                        style: TextStyle(
                            color: const Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 16.sp,
                            fontWeight: FontWeight.w400,
                            fontFamily: 'assets/font/static/Inter-Regular.ttf'),
                      )),
                ))
              ],
            ),
          ),
        ],
      ),
    );
  }

  selectDateTimeStart(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
            initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      if (value != null) {
        date =
            TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
      }
    });

    if (date.isNotEmpty) {
      if (controller.controllerdateStart.text != "") {
        if (DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy')
                .parse(controller.controllerdateEnd.value.text)
                .millisecondsSinceEpoch) {
          controller.controllerdateStart.text = "$date";
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian bắt đầu nhỏ hơn thời gian bắt đầu");
        }
      } else {
        controller.controllerdateStart.text = "$date";
      }
      controller.showListDate();
      controller.listLeavingDetail.value.clear();
      controller.getListDayLeavingApplication();
      controller.listLeavingDetail.refresh();

    }
    FocusScope.of(context).requestFocus(FocusNode());
  }

  selectDateTimeEnd(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
            initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      if (value != null) {
        date =
            TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
      }
    });

    if (date.isNotEmpty) {
      if (controller.controllerdateStart.text != "") {
        if (DateFormat('dd/MM/yyyy')
                .parse(controller.controllerdateStart.value.text)
                .millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch) {
          controller.controllerdateEnd.text = "$date";
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian kết thúc lớn hơn thời gian bắt đầu");
        }
      } else {
        controller.controllerdateEnd.text = "$date";
      }
      controller.showListDate();
      controller.listLeavingDetail.value.clear();
      controller.getListDayLeavingApplication();
      controller.listLeavingDetail.refresh();
    }
    FocusScope.of(context).requestFocus(FocusNode());
  }
}
