import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:intl/intl.dart';

import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/common/contacts.dart';
import '../../../../../../../../data/model/common/detail_notify_leaving_application.dart';
import '../../../../../../../../data/model/common/leaving_application.dart';
import '../../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../../../../../../../data/repository/notification_repo/notification_repo.dart';
class DetailLeaveApplicationParentController extends GetxController {
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var isHomeRoomTeacher = false.obs;
  var transId = "".obs;
  final NotificationRepo _notificationRepo = NotificationRepo();
  var deatilNotifyLeavingApplication = DetailNotifyLeavingApplication().obs;

  @override
  void onInit() {
    var tmpTransId = Get.arguments;
    if (tmpTransId != null) {
      transId.value = tmpTransId;
      getDetailNotify();
    }
    super.onInit();
  }


  getDetailNotify() {
    _notificationRepo.getDetailNotifyLeavingApplication(transId.value).then((
        value) {
      if (value.state == Status.SUCCESS) {
        deatilNotifyLeavingApplication.value = value.object!;
      }
    });
  }


  getStatus(status){
    switch(status){
      case "CANCEL":
        return "Đã hủy";
      case "PENDING":
        return "Chờ duyệt";
      case "REFUSE":
        return "Từ chối";
      case "APPROVE":
        return "Đã duyệt";
    }
  }

  getTextFeedback(status){
    switch(status){
      case "CANCEL":
        return "Lý do hủy đơn :";
      case "PENDING":
        return "";
      case "REFUSE":
        return "Phản hồi :";
      case "APPROVE":
        return "Phản hồi :";
    }
  }

  getColorTextStatus(status){
    switch(status){
      case "PENDING":
        return Color.fromRGBO(253, 185, 36, 1);
      case "APPROVE":
        return Color.fromRGBO(77, 197, 145, 1);
      case "REFUSE":
        return Color.fromRGBO(255, 69, 89, 1);
      case "CANCEL":
        return Color.fromRGBO(255, 69, 89, 1);
    }
  }

  getColorBackgroundStatus(status){
    switch(status){
      case "PENDING":
        return Color.fromRGBO(255, 243, 218, 1);
      case "APPROVE":
        return Color.fromRGBO(192, 242, 220, 1);
      case "REFUSE":
        return Color.fromRGBO(252, 211, 215, 1);
      case "CANCEL":
        return Color.fromRGBO(252, 211, 215, 1);
    }
  }

}