import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:task_manager/view/mobile/role/parent/diligence_parent/approval_parent/approval_parent_controller.dart';
import 'package:task_manager/view/mobile/role/parent/parent_home_controller.dart';

import '../../../../../../commom/constants/date_format.dart';
import '../../../../../../commom/utils/date_time_picker.dart';
import '../../../../../../commom/utils/time_utils.dart';
import '../../../../../../commom/widget/text_field_custom.dart';
import '../../../../../../routes/app_pages.dart';

class ApprovalParentPage extends GetWidget<ApprovalParentController> {
  final controller = Get.put(ApprovalParentController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color.fromRGBO(245, 245, 245, 1),
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              Get.toNamed(Routes.home);
            },
            icon: const Icon(Icons.home),
            color: Colors.white,
          )
        ],
        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
        title: Text(
          'Danh sách đơn xin nghỉ phép',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: Obx(() => RefreshIndicator(
        color:  const Color.fromRGBO(248, 129, 37, 1),
          child: SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            child: Column(
              children: [
                Padding(padding: EdgeInsets.only(top: 16.h)),
                Row(
                  children: [
                    Expanded(child: Container()),
                    InkWell(
                      onTap: () {
                        Get.toNamed(Routes.createALeavingApplication);
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: 16.w, vertical: 8.h),
                        decoration: BoxDecoration(
                            color: const Color.fromRGBO(248, 129, 37, 1),
                            borderRadius: BorderRadius.circular(6.r)),
                        child: Text(
                          "Tạo Đơn",
                          style: TextStyle(
                              fontSize: 12.sp,
                              fontWeight: FontWeight.w400,
                              color: Colors.white),
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(right: 16.w))
                  ],
                ),
                Padding(padding: EdgeInsets.only(top: 16.w)),
                ListView.builder(
                    itemCount: controller.itemsLeavingApplications.value.length,
                    shrinkWrap: true,
                    physics: const ScrollPhysics(),
                    itemBuilder: (context, index) {
                      return Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: 16.w, vertical: 4.h),
                        padding: EdgeInsets.symmetric(
                            horizontal: 16.w, vertical: 8.h),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(6)),
                        child: Column(
                          children: [
                            Padding(padding: EdgeInsets.only(top: 16.h)),
                            Visibility(
                                visible: controller.isShowEditAndDeleteLeavingApplication(controller.itemsLeavingApplications.value[index].status),
                                child: Row(
                                  children: [
                                    Expanded(child: Container()),
                                    InkWell(
                                      onTap: () {
                                        Get.toNamed(Routes.editLeavingApplication,arguments: controller.itemsLeavingApplications.value[index]);
                                      },
                                      child: Text(
                                        "Chỉnh sửa",
                                        style: TextStyle(
                                            color: const Color.fromRGBO(
                                                26, 59, 112, 1),
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16.sp),
                                      ),
                                    ),
                                    Padding(padding: EdgeInsets.only(right: 16.w)),
                                    InkWell(
                                      onTap: () {
                                        Get.toNamed(Routes.cancelLeavingApplication,arguments: controller.itemsLeavingApplications.value[index].id);
                                      },
                                      child: Text(
                                        "Hủy",
                                        style: TextStyle(
                                            color: Colors.red,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16.sp),
                                      ),
                                    ),
                                  ],
                                )),
                            Visibility(
                                visible: controller.isShowEditAndDeleteLeavingApplication(controller.itemsLeavingApplications.value[index].status),
                                child:  const Divider()),
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.detailLeaveApplicationParentPage,arguments: controller.itemsLeavingApplications.value[index].id);
                              },
                              child: Column(
                                children: [
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        width: 80.w,
                                        child: Text(
                                          "Thời gian:",
                                          style: TextStyle(
                                              fontSize: 14.sp,
                                              color: const Color.fromRGBO(
                                                  133, 133, 133, 1),
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                      Flexible(
                                          child: RichText(
                                              text: TextSpan(children: [
                                                TextSpan(
                                                  text: controller.setTimeStartLeavingApplication(index),
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontWeight: FontWeight.w400,
                                                      fontSize: 14.sp),
                                                ),
                                                TextSpan(
                                                  text: controller.setTimeLeavingApplication(index),
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontWeight: FontWeight.w400,
                                                      fontSize: 14.sp),
                                                ),
                                                TextSpan(
                                                  text: controller.setTimeEndLeavingApplication(index),
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontWeight: FontWeight.w400,
                                                      fontSize: 14.sp),
                                                ),
                                              ])))
                                    ],
                                  ),
                                  Padding(padding: EdgeInsets.only(top: 8.w)),
                                  Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        SizedBox(
                                          width: 80.w,
                                          child: Text(
                                            "Giáo viên:",
                                            style: TextStyle(
                                                fontSize: 14.sp,
                                                color: const Color.fromRGBO(
                                                    133, 133, 133, 1),
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Text(
                                          "${controller.itemsLeavingApplications.value[index].teacher?.fullName}",
                                          style: TextStyle(
                                              fontSize: 14.sp,
                                              color: const Color.fromRGBO(
                                                  51, 157, 255, 1),
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ]),
                                  Padding(padding: EdgeInsets.only(top: 8.w)),
                                  Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        SizedBox(
                                          width: 80.w,
                                          child: Text(
                                            "Học sinh:",
                                            style: TextStyle(
                                                fontSize: 14.sp,
                                                color: const Color.fromRGBO(
                                                    133, 133, 133, 1),
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Text(
                                          "${controller.itemsLeavingApplications.value[index].student?.fullName}",
                                          style: TextStyle(
                                              fontSize: 14.sp,
                                              color: const Color.fromRGBO(
                                                  51, 157, 255, 1),
                                              fontWeight: FontWeight.w400),
                                        ),
                                        Expanded(child: Container()),
                                      ]),
                                  Padding(padding: EdgeInsets.only(top: 8.w)),
                                  Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          width: 80.w,
                                          child: Text(
                                            "Lý do:",
                                            style: TextStyle(
                                                fontSize: 14.sp,
                                                color: const Color.fromRGBO(
                                                    133, 133, 133, 1),
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Flexible(
                                          child: Text(
                                            "${controller.itemsLeavingApplications.value[index].reason}",
                                            style: TextStyle(
                                                fontSize: 14.sp,
                                                color: Colors.black,
                                                fontWeight: FontWeight.w400),
                                          ),
                                        )
                                      ]),
                                  Padding(padding: EdgeInsets.only(top: 8.w)),
                                  Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        SizedBox(
                                          width: 80.w,
                                          child: Text(
                                            "Trạng thái:",
                                            style: TextStyle(
                                                fontSize: 14.sp,
                                                color: const Color.fromRGBO(
                                                    133, 133, 133, 1),
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                                          decoration: BoxDecoration(
                                              color: controller.getColorBackgroundStatus(controller.itemsLeavingApplications.value[index].status),
                                              borderRadius: BorderRadius.circular(6)
                                          ),
                                          child: Text(
                                            "${controller.getStatus(controller.itemsLeavingApplications.value[index].status)}",
                                            style: TextStyle(
                                                fontSize: 14.sp,
                                                color: controller.getColorTextStatus(controller.itemsLeavingApplications.value[index].status),
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Expanded(child: Container()),
                                      ]),
                                ],
                              ),
                            )

                          ],
                        ),
                      );
                    }),
                Padding(padding: EdgeInsets.only(top: 16.h)),
              ],
            ),
          ),
          onRefresh: () async {
            controller.getListLeavingApplicationParent();
          })),
    );
  }
}
