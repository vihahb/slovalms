import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:task_manager/view/mobile/role/parent/parent_home_controller.dart';

import '../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../data/model/common/contacts.dart';
import '../../../../../../../data/model/common/leaving_application.dart';
import '../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../approval_parent_controller.dart';
import '../create_a_leave_application/create_a_leave_application_controller.dart';


class EditLeavingApplicationController extends GetxController {
  String dropdown = '';
  var focusReason = FocusNode();
  var controllerReason = TextEditingController();
  var focusAssure = FocusNode();
  var controllerAssure = TextEditingController();

  var focusNote = FocusNode();
  var controllerNote = TextEditingController();

  var focusdateStart = FocusNode();
  var controllerdateStart = TextEditingController();

  var focusdateEnd = FocusNode();
  var controllerdateEnd = TextEditingController();
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var teacher = Teacher().obs;
  var studentName = "".obs;
  String getTempIFSCValidation(String text) {
    return text.length > 7 ? "This line is helper text" : "";
  }
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var itemsLeavingApplications = ItemsLeavingApplication().obs;
  var isShowListDate = false.obs;
  var listDayLeavingApplication = <DateTime>[].obs;
  var listLeavingDetail = <LeaveDetail>[].obs;
  var leaveDetail = LeaveDetail().obs;
  var isConfirmEdit = <bool>[].obs;
  var sessionDay = <String>[].obs;
  var indexClick = 0.obs;
  void onInit() {
    var data = Get.arguments;

    if (data != null) {
      itemsLeavingApplications.value = data;
      teacher.value = itemsLeavingApplications.value.teacher!;
      studentName.value = itemsLeavingApplications.value.student!.fullName!;
      controllerdateEnd.text =  outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(itemsLeavingApplications.value.toDate!));
      controllerdateStart.text =  outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(itemsLeavingApplications.value.fromDate!));
      controllerReason.text = itemsLeavingApplications.value.reason!;
      controllerAssure.text = itemsLeavingApplications.value.assure!;
      controllerNote.text = itemsLeavingApplications.value.note!;
      var startDate = DateFormat("dd/MM/yyyy").parse(controllerdateStart.text);
      var endDate = DateFormat("dd/MM/yyyy").parse(controllerdateEnd.text);
      for (int i = 0; i <= endDate.difference(startDate).inDays; i++){
        listDayLeavingApplication.value.add(startDate.add(Duration(days: i)));
        listLeavingDetail.value.add(LeaveDetail(date: 0,sessionDay: "AM,PM"));
        listLeavingDetail.value[i].date = listDayLeavingApplication.value[i].millisecondsSinceEpoch;
        listLeavingDetail.value[i].sessionDay = itemsLeavingApplications.value.leaveDetail![i].sessionDay;
      }
    }
    super.onInit();
  }



  getStatus(status){
    switch(status){
      case true:
        return "TRUE";
      case false:
        return "FALSE";
    }
  }

  setStatus(status){
    switch(status){
      case "TRUE":
        return true;
      case "FALSE" :
        return false;
    }
  }



  getListDayLeavingApplication() {
    if (controllerdateStart.text != "" && controllerdateEnd.text != "") {
      var startDate = DateFormat("dd/MM/yyyy").parse(controllerdateStart.text);
      var endDate = DateFormat("dd/MM/yyyy").parse(controllerdateEnd.text);
      listDayLeavingApplication.value = [];
      listLeavingDetail.value = [];
      for (int i = 0; i <= endDate.difference(startDate).inDays; i++) {
        listDayLeavingApplication.value.add(startDate.add(Duration(days: i)));
        listLeavingDetail.value.add(LeaveDetail(date: 0,sessionDay: "AM,PM"));
        listLeavingDetail.value[i].date = listDayLeavingApplication.value[i].millisecondsSinceEpoch;
        listLeavingDetail.value[i].sessionDay = "AM,PM";
      }
      listLeavingDetail.refresh();
      listDayLeavingApplication.refresh();
    }
  }

  checkBoxMorning(index) {
    indexClick.value = index;
    if(listLeavingDetail.value[indexClick.value].sessionDay?.contains("AM") == true){
      if(listLeavingDetail.value[indexClick.value].sessionDay?.contains("PM") == true){
        listLeavingDetail.value[indexClick.value].sessionDay = "PM";
      }else{
        listLeavingDetail.value[indexClick.value].sessionDay = "";
      }

    }else{
      if(listLeavingDetail.value[indexClick.value].sessionDay?.contains("PM") == true){
        listLeavingDetail.value[indexClick.value].sessionDay = "AM,PM";
      }else{
        listLeavingDetail.value[indexClick.value].sessionDay = "AM";
      }
    }
    listLeavingDetail.refresh();
  }

  checkBoxAfternoon(index) {
    indexClick.value = index;
    if(listLeavingDetail.value[indexClick.value].sessionDay?.contains("PM") == true){
      if(listLeavingDetail.value[indexClick.value].sessionDay?.contains("AM") == true){
        listLeavingDetail.value[indexClick.value].sessionDay = "AM";
      }else{
        listLeavingDetail.value[indexClick.value].sessionDay = "";
      }

    }else{
      if(listLeavingDetail.value[indexClick.value].sessionDay?.contains("AM") == true){
        listLeavingDetail.value[indexClick.value].sessionDay = "AM,PM";
      }else{
        listLeavingDetail.value[indexClick.value].sessionDay = "PM";
      }
    }
    listLeavingDetail.refresh();
  }



  editLeaveApplication(){
    var startDate = DateFormat("dd/MM/yyyy").parse(controllerdateStart.text);
    var endDate = DateFormat("dd/MM/yyyy").parse(controllerdateEnd.text);
    for (int i = 0; i <= endDate.difference(startDate).inDays; i++) {
      if (listLeavingDetail.value[i].sessionDay == "") {
        isConfirmEdit.value.add(false);
        AppUtils.shared.showToast("Vui lòng chọn đầy đủ thời gian");
      } else {
        isConfirmEdit.value.add(true);
      }
    }
    if(isConfirmEdit.value.contains(false) == false){
      _diligenceRepo.editLeaveApplication(itemsLeavingApplications.value.id,
          itemsLeavingApplications.value.teacher!.id!,
          itemsLeavingApplications.value.student!.id!,
          DateFormat('dd/MM/yyyy hh:mm').parse("${controllerdateStart.value.text} 00:00").millisecondsSinceEpoch,
          DateFormat('dd/MM/yyyy hh:mm').parse("${controllerdateEnd.value.text} 23:59").millisecondsSinceEpoch,
          controllerReason.text.trim(),
          controllerNote.text.trim(),
          controllerAssure.text.trim(),
          listLeavingDetail.value
      ).then((value) {
        if (value.state == Status.SUCCESS) {
          AppUtils.shared.showToast("Chỉnh sửa đơn thành công!"); Get.back();
          Future.delayed(const Duration(seconds: 1), () {
            Get.find<ApprovalParentController>().onInit();
          });
        }else {
          AppUtils.shared.hideLoading();
          AppUtils.shared
              .snackbarError("Chỉnh sửa đơn thất bại", value.message ?? "");
        }
      });
    }
  }






}