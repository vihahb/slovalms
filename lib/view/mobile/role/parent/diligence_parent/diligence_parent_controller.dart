
import 'package:get/get.dart';
import 'package:task_manager/view/mobile/role/parent/diligence_parent/approval_parent/approval_parent_page.dart';
import 'package:task_manager/view/mobile/role/parent/diligence_parent/diary_parent/diary_parent_page.dart';
import 'package:task_manager/view/mobile/role/parent/diligence_parent/statistical_parent/statistical_parent_page.dart';

import '../../../../../routes/app_pages.dart';



class DiligenceParentController extends GetxController{
 goToStatisticalPage(){
   Get.to(StatisticalParentPage());
 }
 goToDiaryPage(){
   Get.to(DiaryParentPage());
 }
 goToApprovalPage(){
   Get.toNamed(Routes.approvalParent);
 }

}