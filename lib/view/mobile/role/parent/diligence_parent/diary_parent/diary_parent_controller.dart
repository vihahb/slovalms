import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/data/model/common/diligence.dart';
import 'package:task_manager/data/repository/diligence/diligence_repo.dart';
import 'package:task_manager/routes/app_pages.dart';
import 'package:task_manager/view/mobile/role/parent/parent_home_controller.dart';
import 'package:task_manager/commom/utils/app_utils.dart';




class DiaryParentController extends GetxController{
  var controllerdate = TextEditingController().obs;
  var ontime ="Đi Học Đúng giờ".obs;
  var backOnTime= "Về Đúng Giờ".obs;
  var latetime ="Đi muộn".obs;
  var noAllowwed ="Nghỉ Học Không phép".obs;
  var Allowwed= "Nghỉ Học Có Phép".obs;
  var comeBackSoon= "Về Sớm".obs;
  var showhiden =<bool>[].obs;
  var indexHiden = 0.obs;
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var diligence = Diligence().obs;
  RxList<Items> item = <Items>[].obs;
  var focusdateStart = FocusNode().obs;
  var controllerdateStart = TextEditingController().obs;
  var focusdateEnd = FocusNode().obs;
  var controllerdateEnd = TextEditingController().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var outputFormat = DateFormat('HH:mm:ss');
  var dateStart ="".obs;
  var dateEnd ="".obs;
  var fromYearSelect ="".obs;
  var toYearSelect ="".obs;
  @override
  void onInit() {
    for(int i=0;i<7;i++){
      showhiden.value.add(false);
    }
    showhiden.value[indexHiden.value]= true;

    setDateSchoolYears();
    if(Get.find<ParentHomeController>().fromYearPresent == DateTime.now().year || Get.find<ParentHomeController>().toYearPresent == DateTime.now().year ){
      dateEnd.value =outputDateFormat.format(DateTime(findLastDateOfTheWeek(DateTime.now()).year, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
      dateStart.value =outputDateFormat.format(DateTime(findFirstDateOfTheWeek(DateTime.now()).year, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerdateStart.value.text =   outputDateFormat.format(DateTime(findFirstDateOfTheWeek(DateTime.now()).year, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerdateEnd.value.text = outputDateFormat.format(DateTime(findLastDateOfTheWeek(DateTime.now()).year, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));

    }else{
      dateEnd.value =outputDateFormat.format(DateTime(int.parse(toYearSelect.value.substring(0,4)), findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      dateStart.value =outputDateFormat.format(DateTime(int.parse(fromYearSelect.value.substring(0,4)), findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerdateStart.value.text = outputDateFormat.format(DateTime(int.parse(toYearSelect.value.substring(0,4)),findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerdateEnd.value.text = outputDateFormat.format(DateTime(int.parse(fromYearSelect.value.substring(0,4)), findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));

    }


  }
  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }

  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime
        .add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }

  showBorder(index) {
    if(showhiden.value[index]= true){
    }else {
      indexHiden = index;
      showhiden.value[indexHiden.value]=! showhiden.value[indexHiden.value];
      showhiden.value =[];
      for(int i=0;i<7;i++){
        showhiden.value.add(false);
      }
      showhiden.value[indexHiden.value]= true;

    }
  }
  getListDiligence(){
    var userId = Get.find<ParentHomeController>().currentStudentProfile.value.id;
    var todate = DateTime(findLastDateOfTheWeek(DateTime.now()).year, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    var fromdate = DateTime(findFirstDateOfTheWeek(DateTime.now()).year, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    _diligenceRepo.listDiligence(userId, fromdate, todate,"").then((value) {
      if (value.state == Status.SUCCESS) {
        diligence.value = value.object!;
        item.value = diligence.value.items!;
        AppUtils.shared.hideLoading();
        Future.delayed(Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }
  getListDiligenceAll(fromdate,todate){
    var userId = Get.find<ParentHomeController>().currentStudentProfile.value.id;
    var fromdate = DateTime(int.parse(dateStart.value.substring(6,10)), int.parse(dateStart.value.substring(3,5)),int.parse(dateStart.value.substring(0,2))).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(dateEnd.value.substring(6,10)), int.parse(dateEnd.value.substring(3,5)),int.parse(dateEnd.value.substring(0,2))).millisecondsSinceEpoch;
    _diligenceRepo.listDiligence(userId, fromdate, todate,"").then((value) {
      if (value.state == Status.SUCCESS) {
        diligence.value = value.object!;
        item.value = diligence.value.items!;
        AppUtils.shared.hideLoading();
        Future.delayed(Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }
  onRefresh() {
    getListDiligenceAll(DateTime(int.parse(dateStart.value.substring(6,10)), int.parse(dateStart.value.substring(4,5)),int.parse(dateStart.value.substring(0,2))).millisecondsSinceEpoch,
        DateTime(int.parse(dateEnd.value.substring(6,10)), int.parse(dateEnd.value.substring(4,5)),int.parse(dateEnd.value.substring(0,2))).millisecondsSinceEpoch);
  }
  getListDiligenceOnTime(fromdate,todate){
    var userId = Get.find<ParentHomeController>().currentStudentProfile.value.id;
    var fromdate = DateTime(int.parse(dateStart.value.substring(6,10)), int.parse(dateStart.value.substring(3,5)),int.parse(dateStart.value.substring(0,2))).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(dateEnd.value.substring(6,10)), int.parse(dateEnd.value.substring(3,5)),int.parse(dateEnd.value.substring(0,2))).millisecondsSinceEpoch;
    _diligenceRepo.listDiligence(userId, fromdate, todate,"ON_TIME").then((value) {
      if (value.state == Status.SUCCESS) {
        diligence.value = value.object!;
        item.value = diligence.value.items!;
        AppUtils.shared.hideLoading();
        Future.delayed(Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }
  getListDiligenceNotOnTime(fromdate,todate){
    var userId = Get.find<ParentHomeController>().currentStudentProfile.value.id;
    var fromdate = DateTime(int.parse(dateStart.value.substring(6,10)), int.parse(dateStart.value.substring(4,5)),int.parse(dateStart.value.substring(0,2))).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(dateEnd.value.substring(6,10)), int.parse(dateEnd.value.substring(4,5)),int.parse(dateEnd.value.substring(0,2))).millisecondsSinceEpoch;
    _diligenceRepo.listDiligence(userId, fromdate, todate,"NOT_ON_TIME").then((value) {
      if (value.state == Status.SUCCESS) {
        diligence.value = value.object!;
        item.value = diligence.value.items!;
        AppUtils.shared.hideLoading();
        Future.delayed(Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }
  getListDiligenceAbsuebWithout(fromdate,todate){
    var userId = Get.find<ParentHomeController>().currentStudentProfile.value.id;
    var fromdate = DateTime(int.parse(dateStart.value.substring(6,10)), int.parse(dateStart.value.substring(4,5)),int.parse(dateStart.value.substring(0,2))).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(dateEnd.value.substring(6,10)), int.parse(dateEnd.value.substring(4,5)),int.parse(dateEnd.value.substring(0,2))).millisecondsSinceEpoch;
    _diligenceRepo.listDiligence(userId, fromdate, todate,"ABSENT_WITHOUT_LEAVE").then((value) {
      if (value.state == Status.SUCCESS) {
        diligence.value = value.object!;
        item.value = diligence.value.items!;
        AppUtils.shared.hideLoading();
        Future.delayed(Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }

  getListDiligenceExecud(fromdate,todate){
    var userId = Get.find<ParentHomeController>().currentStudentProfile.value.id;
    var fromdate = DateTime(int.parse(dateStart.value.substring(6,10)), int.parse(dateStart.value.substring(3,5)),int.parse(dateStart.value.substring(0,2))).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(dateEnd.value.substring(6,10)), int.parse(dateEnd.value.substring(3,5)),int.parse(dateEnd.value.substring(0,2))).millisecondsSinceEpoch;
    _diligenceRepo.listDiligence(userId, fromdate, todate,"EXCUSED_ABSENCE").then((value) {
      if (value.state == Status.SUCCESS) {
        diligence.value = value.object!;
        item.value = diligence.value.items!;
        AppUtils.shared.hideLoading();
        Future.delayed(Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }
  setDateSchoolYears() {
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (Get.find<ParentHomeController>().fromYearPresent.value == DateTime.now().year) {
        getListDiligence();
        return;
      } else {
        fromYearSelect.value = "${Get.find<ParentHomeController>().fromYearPresent}";
        toYearSelect.value = "${Get.find<ParentHomeController>().toYearPresent}";
        getListDiligenceToSelectYear(fromYearSelect.value, toYearSelect.value);
      }
    } else {
      if (Get.find<ParentHomeController>().toYearPresent.value == DateTime.now().year) {
        getListDiligence();
        return;
      } else {
        fromYearSelect.value = "${Get.find<ParentHomeController>().toYearPresent}";
        toYearSelect.value = "${Get.find<ParentHomeController>().fromYearPresent}";
        getListDiligenceToSelectYear(fromYearSelect.value, toYearSelect.value);
      }
    }
  }
  getListDiligenceToSelectYear(fromdate, todate){
    var userId = Get.find<ParentHomeController>().currentStudentProfile.value.id;
    var fromdate = DateTime(int.parse(toYearSelect.value.substring(0,4)), findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(fromYearSelect.value.substring(0,4)), findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    _diligenceRepo.listDiligence(userId, fromdate, todate,"").then((value) {
      if (value.state == Status.SUCCESS) {
        diligence.value = value.object!;
        item.value = diligence.value.items!;
        AppUtils.shared.hideLoading();
        Future.delayed(Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }

  goToDetailStatisticalParentOnSchoolPage(index){
    Get.toNamed(Routes.detailstatisticalparentpage, arguments:[item.value[index].date, item.value[index].statusDiligent,item.value[index].updatedAt] );
  }



  getColorTextStatus(state){
    switch(state){
      case "ON_TIME":
        return Color.fromRGBO(77,197,145, 1);
      case "ON_TIME":
        return Color.fromRGBO(77,197,145, 1);
      case "NOT_ON_TIME":
        return Color.fromRGBO(253,185,36, 1);
      case "NOT_ON_TIME":
        return Color.fromRGBO(253,185,36, 1);
      case "ABSENT_WITHOUT_LEAVE":
        return Color.fromRGBO(255,69,89,1);
      case "EXCUSED_ABSENCE":
        return Color.fromRGBO(255,69,89,1);
      default:
        return Color.fromRGBO(255,255,255,1);
    }
  }
  getTextStatus(state){
    switch(state){
      case "ON_TIME":
        return ontime;
      case "NOT_ON_TIME":
        return latetime;
      case "ABSENT_WITHOUT_LEAVE":
        return noAllowwed;
      case "EXCUSED_ABSENCE":
        return Allowwed;
      default:
        return('');
    }
  }



}
enum status {
  ON_TIME,
  ABSENT_WITHOUT_LEAVE,
  NOT_ON_TIME,
  EXCUSED_ABSENCE
}