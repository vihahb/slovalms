import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:task_manager/view/mobile/role/parent/diligence_parent/diligence_parent_controller.dart';

class DiligenceParentPage extends GetWidget<DiligenceParentController> {

  final controller = Get.put(DiligenceParentController());

  DiligenceParentPage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(245, 245, 245, 1),
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
        title: Text(
          'Chuyên Cần',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: Container(
        margin: EdgeInsets.only(top: 16.h, right: 16.h, left: 16.h),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Chuyên Cần',
              style: TextStyle(
                  color: const Color.fromRGBO(177, 177, 177, 1),
                  fontWeight: FontWeight.w500,
                  fontFamily: 'static/Inter-Medium.ttf',
                  fontSize: 12.sp),
            ),
            Padding(padding: EdgeInsets.only(top: 8.h)),
            Container(
              padding: EdgeInsets.all(16.h),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6.r),
                  color: const Color.fromRGBO(255, 255, 255, 1)),
              width: double.infinity,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      controller.goToApprovalPage();
                    },
                    child: Column(
                      children: [
                        Image.asset('assets/images/approval.png',width: 24.w,height: 24.h,),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Text(
                          'Đơn xin nghỉ phép',
                          style: TextStyle(
                              color: const Color.fromRGBO(26, 26, 26, 1),
                              fontSize: 12.sp,
                              fontWeight: FontWeight.w500,
                              fontFamily:
                                  'assets/font/static/Inter-Medium.ttf'),
                        )
                      ],
                    ),
                  ),

                  InkWell(
                    onTap: () {
                      controller.goToStatisticalPage();
                    },
                    child: Column(
                      children: [
                        Image.asset('assets/images/thongke.png',width: 24.w,height: 24.h,),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Text(
                          'Thống Kê',
                          style: TextStyle(
                              color: const Color.fromRGBO(26, 26, 26, 1),
                              fontSize: 12.sp,
                              fontWeight: FontWeight.w500,
                              fontFamily:
                                  'assets/font/static/Inter-Medium.ttf'),
                        )
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      controller.goToDiaryPage();
                    },
                    child: Column(
                      children: [
                        Image.asset('assets/images/diary.png',width: 24.w,height: 24.h,),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Text(
                          'Nhật Ký',
                          style: TextStyle(
                              color: const Color.fromRGBO(26, 26, 26, 1),
                              fontSize: 12.sp,
                              fontWeight: FontWeight.w500,
                              fontFamily:
                                  'assets/font/static/Inter-Medium.ttf'),
                        )
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
