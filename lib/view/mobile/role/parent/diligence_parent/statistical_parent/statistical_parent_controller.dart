

import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:task_manager/data/model/common/detail_item_list_student.dart';
import 'package:task_manager/data/model/common/diligence.dart';
import 'package:task_manager/data/model/common/user_profile.dart';
import 'package:task_manager/data/repository/diligence/diligence_repo.dart';
import 'package:task_manager/routes/app_pages.dart';
import 'package:task_manager/view/mobile/role/parent/diligence_parent/statistical_parent/detail_statistical_parent/detail_statistical_parent_page.dart';
import 'package:task_manager/commom/utils/app_utils.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/view/mobile/role/parent/parent_home_controller.dart';


class StatisticalParentController extends GetxController{
  var controllerdate = TextEditingController().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var outputDateToDateFormat = DateFormat('dd/MM/yyyy');
  var outputDate = DateFormat('EEEE,dd/MM/yyyy');
  var outputTimeFormat = DateFormat('HH:mm:ss ');
  var aDayGotoSchool = false.obs;
  var dayOff= false.obs;
  var dayOffExcused= false.obs;
  var textDate = "".obs;
  var text_color= false.obs;
  var lateForSchool = false.obs;
  var dateNow = DateFormat.yMd().format(DateTime.now()).obs;
  var timeNow = DateFormat.Hms().format(DateTime.now()).obs;
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var focusedDay = DateTime.now().obs;
  var selectedDay = DateTime.now().obs;
  var userProfile = UserProfile().obs;
  var diligence = Diligence().obs;
  var studentOnTime = StudentOnSchool().obs;
  var studentNotOnTime = StudentNotOnTime().obs;
  var studentExcused = StudentExcusedAbsence().obs;
  var studentAbsend = StudentAbsentWithoutLeave().obs;
  var totalDay = 0.obs;
  RxList<StatusDetailDiligence> dayonShool = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayonShoolOntime = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayNotontime = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayAbsent = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayExcused = <StatusDetailDiligence>[].obs;
  var detailDiligence = DetailDiligence().obs;
  var totalDayOnTime = 0.obs;
  var totalDayNotOnTime = 0.obs;
  var totalDayAbsend = 0.obs;
  var totalDayExsecud = 0.obs;
  var selectedDate ="".obs;
  var focusdateStart = FocusNode().obs;
  var controllerdateStart = TextEditingController().obs;
  var focusdateEnd = FocusNode().obs;
  var controllerdateEnd = TextEditingController().obs;
  var dateStart ="".obs;
  var dateEnd ="".obs;
  var fromYearSelect ="".obs;
  var toYearSelect ="".obs;

  @override
  void onInit() {
    setDateSchoolYears();
    // controllerdateEnd.value.text =outputDateFormat.format(DateTime(DateTime.now().year,DateTime.now().month, DateTime.now().day));
    // controllerdateStart.value.text =outputDateFormat.format(DateTime(DateTime.now().year,DateTime.now().month, DateTime.now().day-7));
    if(Get.find<ParentHomeController>().fromYearPresent == DateTime.now().year || Get.find<ParentHomeController>().toYearPresent == DateTime.now().year ){
      controllerdateStart.value.text =   outputDateToDateFormat.format(DateTime(findFirstDateOfTheWeek(DateTime.now()).year, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerdateEnd.value.text = outputDateToDateFormat.format(DateTime(findLastDateOfTheWeek(DateTime.now()).year, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
      dateEnd.value =outputDateFormat.format(DateTime(findLastDateOfTheWeek(DateTime.now()).year, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
      dateStart.value =outputDateFormat.format(DateTime(findFirstDateOfTheWeek(DateTime.now()).year, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));

    }else{
      controllerdateStart.value.text = outputDateToDateFormat.format(DateTime(int.parse(fromYearSelect.value.substring(0,4)),findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerdateEnd.value.text = outputDateToDateFormat.format(DateTime(int.parse(toYearSelect.value.substring(0,4)), findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
      dateEnd.value =outputDateFormat.format(DateTime(int.parse(toYearSelect.value.substring(0,4)), DateTime.now().month));
      dateStart.value =outputDateFormat.format(DateTime(int.parse(fromYearSelect.value.substring(0,4)), DateTime.now().month+1).subtract(Duration(days: 1)));

    }
 }



  getColorTextStatus(state){
    switch(state){
      case "ON_TIME":
        return Color.fromRGBO(77,197,145, 1);
      case "NOT_ON_TIME":
        return Color.fromRGBO(253,185,36, 1);
      case "EXCUSED_ABSENCE":
        return Color.fromRGBO(255,69,89,1);
      default:
        return Color.fromRGBO(255,69,89,1);
    }
  }
  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }

  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime
        .add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }

  getListDiligence(fromdate,todate){
    var userId = Get.find<ParentHomeController>().currentStudentProfile.value.id;
    var fromdate = DateTime(int.parse(dateStart.value.substring(6,10)), int.parse(dateStart.value.substring(3,5)),int.parse(dateStart.value.substring(0,2))).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(dateEnd.value.substring(6,10)), int.parse(dateEnd.value.substring(3,5)),int.parse(dateEnd.value.substring(0,2))).millisecondsSinceEpoch;
    _diligenceRepo.DetailListDiligence(userId, fromdate, todate,"").then((value) {
      if (value.state == Status.SUCCESS) {
          detailDiligence.value = value.object!;
          dayonShool.value = detailDiligence.value.studentOnSchool!.dayOnSchool! ;
          dayonShoolOntime.value = dayonShool.value.where((element) => element.statusDiligent== "ON_TIME").toList();
          dayExcused.value = detailDiligence.value.studentExcusedAbsence!.dayExcusedAbsence!;
          dayAbsent.value = detailDiligence.value.studentAbsentWithoutLeave!.dayAbsentWithoutLeave!;
          dayNotontime.value = detailDiligence.value.studentNotOnTime!.dayNotOnTime!;
          if(detailDiligence.value.studentOnSchool != null){
            totalDayOnTime.value = detailDiligence.value.studentOnSchool!.totalDaysOnSchool!;
          }
          if(detailDiligence.value.studentNotOnTime != null){
            totalDayNotOnTime.value = detailDiligence.value.studentNotOnTime!.totalDaysNotOnTime!;
          }
          if(detailDiligence.value.studentAbsentWithoutLeave != null){
            totalDayAbsend.value = detailDiligence.value.studentAbsentWithoutLeave!.totalDaysAbsentWithoutLeave!;
          }
          if(detailDiligence.value.studentExcusedAbsence != null){
            totalDayExsecud.value = detailDiligence.value.studentExcusedAbsence!.totalDaysExcusedAbsence!;
          }
          AppUtils.shared.hideLoading();
          Future.delayed(Duration(seconds: 1), () {
          });

      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }
  getTotalDay(){
    if(totalDayExsecud.value == 0){
      return totalDayAbsend.value;
    }else if(totalDayAbsend.value ==0){
      return totalDayExsecud.value;
    }else{
      return totalDayExsecud.value + totalDayAbsend.value;

    }
  }
  checkUpdateAt(index){
    if(dayNotontime.value[index].updatedAt==null || dayNotontime.value[index].updatedAt==""){
      return "";
    }else{
      return "${outputTimeFormat.format(DateTime.fromMillisecondsSinceEpoch(dayNotontime.value[index].updatedAt!))}";
    }

  }
  getListDiligenceToday(){
    var userId = Get.find<ParentHomeController>().currentStudentProfile.value.id;
    var todate = DateTime(findLastDateOfTheWeek(DateTime.now()).year, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    var fromdate = DateTime(findFirstDateOfTheWeek(DateTime.now()).year, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    _diligenceRepo.DetailListDiligence(userId, fromdate, todate,"").then((value) {
      if (value.state == Status.SUCCESS) {
        detailDiligence.value = value.object!;
        dayonShool.value = detailDiligence.value.studentOnSchool!.dayOnSchool!;
        dayExcused.value = detailDiligence.value.studentExcusedAbsence!.dayExcusedAbsence!;
        dayAbsent.value = detailDiligence.value.studentAbsentWithoutLeave!.dayAbsentWithoutLeave!;
        dayNotontime.value = detailDiligence.value.studentNotOnTime!.dayNotOnTime!;
        if(detailDiligence.value.studentOnSchool != null){
          totalDayOnTime.value = detailDiligence.value.studentOnSchool!.totalDaysOnSchool!;
        }
        if(detailDiligence.value.studentNotOnTime != null){
          totalDayNotOnTime.value = detailDiligence.value.studentNotOnTime!.totalDaysNotOnTime!;
        }
        if(detailDiligence.value.studentAbsentWithoutLeave != null){
          totalDayAbsend.value = detailDiligence.value.studentAbsentWithoutLeave!.totalDaysAbsentWithoutLeave!;
        }
        if(detailDiligence.value.studentExcusedAbsence != null){
          totalDayExsecud.value = detailDiligence.value.studentExcusedAbsence!.totalDaysExcusedAbsence!;
        }
        AppUtils.shared.hideLoading();
        Future.delayed(Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }
  getListDiligenceToSelectYear(fromdate, todate){
    var userId = Get.find<ParentHomeController>().currentStudentProfile.value.id;
    var fromdate = DateTime(int.parse(fromYearSelect.value.substring(0,4)), findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(toYearSelect.value.substring(0,4)), findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    _diligenceRepo.DetailListDiligence(userId, fromdate, todate,"").then((value) {
      if (value.state == Status.SUCCESS) {
        detailDiligence.value = value.object!;
        dayonShool.value = detailDiligence.value.studentOnSchool!.dayOnSchool! ;
        dayExcused.value = detailDiligence.value.studentExcusedAbsence!.dayExcusedAbsence!;
        dayAbsent.value = detailDiligence.value.studentAbsentWithoutLeave!.dayAbsentWithoutLeave!;
        dayNotontime.value = detailDiligence.value.studentNotOnTime!.dayNotOnTime!;
        if(detailDiligence.value.studentOnSchool != null){
          totalDayOnTime.value = detailDiligence.value.studentOnSchool!.totalDaysOnSchool!;
        }
        if(detailDiligence.value.studentNotOnTime != null){
          totalDayNotOnTime.value = detailDiligence.value.studentNotOnTime!.totalDaysNotOnTime!;
        }
        if(detailDiligence.value.studentAbsentWithoutLeave != null){
          totalDayAbsend.value = detailDiligence.value.studentAbsentWithoutLeave!.totalDaysAbsentWithoutLeave!;
        }
        if(detailDiligence.value.studentExcusedAbsence != null){
          totalDayExsecud.value = detailDiligence.value.studentExcusedAbsence!.totalDaysExcusedAbsence!;
        }
        AppUtils.shared.hideLoading();
        Future.delayed(Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }
  setDateSchoolYears() {
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (Get.find<ParentHomeController>().fromYearPresent.value == DateTime.now().year) {
        getListDiligenceToday();
        return;
      } else {
        fromYearSelect.value = "${Get.find<ParentHomeController>().fromYearPresent}";
        toYearSelect.value = "${Get.find<ParentHomeController>().toYearPresent}";
        getListDiligenceToSelectYear(fromYearSelect.value, toYearSelect.value);
      }
    } else {
      if (Get.find<ParentHomeController>().toYearPresent.value == DateTime.now().year) {
        getListDiligenceToday();
        return;
      } else {
        fromYearSelect.value = "${Get.find<ParentHomeController>().fromYearPresent}";
        toYearSelect.value = "${Get.find<ParentHomeController>().toYearPresent}";
        getListDiligenceToSelectYear(fromYearSelect.value, toYearSelect.value);
      }
    }
  }

  getConvertDateNotOn(index){
    var  dayNotOn = "${outputDate.format(DateTime.fromMillisecondsSinceEpoch(dayNotontime.value[index].date!))}";
    if(dayNotOn.substring(0,6)=="Monday"){
      return "Thứ 2, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayNotontime.value[index].date!))} ";
    }else if(dayNotOn.substring(0,7)=="Tuesday"){
      return "Thứ 3, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayNotontime.value[index].date!))} ";
    }else if(dayNotOn.substring(0,9)=="Wednesday"){
      return "Thứ 4, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayNotontime.value[index].date!))} ";
    }else if(dayNotOn.substring(0,8)=="Thursday"){
      return "Thứ 5, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayNotontime.value[index].date!))} ";
    }else if(dayNotOn.substring(0,6)=="Friday"){
      return "Thứ 6, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayNotontime.value[index].date!))} ";
    }else if(dayNotOn.substring(0,8)=="Saturday"){
      return "Thứ 7, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayNotontime.value[index].date!))} ";
    }else if(dayNotOn.substring(0,6)=="Sunday"){
      return "Chủ nhật, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayNotontime.value[index].date!))} ";
    }else{
      return "";
    }
  }

  getConvertDateOnTime(index){
    var  dayOnTime = "${outputDate.format(DateTime.fromMillisecondsSinceEpoch(dayonShool.value[index].date!))}";
    if(dayOnTime.substring(0,6)=="Monday"){
      return "Thứ 2, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayonShool.value[index].date!))} ";
    }else if(dayOnTime.substring(0,7)=="Tuesday"){
      return "Thứ 3, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayonShool.value[index].date!))} ";
    }else if(dayOnTime.substring(0,9)=="Wednesday"){
      return "Thứ 4, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayonShool.value[index].date!))} ";
    }else if(dayOnTime.substring(0,8)=="Thursday"){
      return "Thứ 5, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayonShool.value[index].date!))} ";
    }else if(dayOnTime.substring(0,6)=="Friday"){
      return "Thứ 6, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayonShool.value[index].date!))} ";
    }else if(dayOnTime.substring(0,8)=="Saturday"){
      return "Thứ 7, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayonShool.value[index].date!))} ";
    }else if(dayOnTime.substring(0,6)=="Sunday"){
      return "Chủ nhật, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayonShool.value[index].date!))} ";
    }else{
      return "";
    }
  }
  getConvertDateAbsend(index){
    var  dayAbsend = "${outputDate.format(DateTime.fromMillisecondsSinceEpoch(dayAbsent.value[index].date!))}";
    if(dayAbsend.substring(0,6)=="Monday"){
      return "Thứ 2, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsent.value[index].date!))} ";
    }else if(dayAbsend.substring(0,7)=="Tuesday"){
      return "Thứ 3, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsent.value[index].date!))} ";
    }else if(dayAbsend.substring(0,9)=="Wednesday"){
      return "Thứ 4, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsent.value[index].date!))} ";
    }else if(dayAbsend.substring(0,8)=="Thursday"){
      return "Thứ 5, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsent.value[index].date!))} ";
    }else if(dayAbsend.substring(0,6)=="Friday"){
      return "Thứ 6, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsent.value[index].date!))} ";
    }else if(dayAbsend.substring(0,8)=="Saturday"){
      return "Thứ 7, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsent.value[index].date!))} ";
    }else if(dayAbsend.substring(0,6)=="Sunday"){
      return "Chủ nhật, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsent.value[index].date!))} ";
    }else{
      return "";
    }
  }

  getConvertDateExcused(index){
    var  dayexcused = "${outputDate.format(DateTime.fromMillisecondsSinceEpoch(dayExcused.value[index].date!))}";
    if(dayexcused.substring(0,6)=="Monday"){
      return "Thứ 2, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayExcused.value[index].date!))} ";
    }else if(dayexcused.substring(0,7)=="Tuesday"){
      return "Thứ 3, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayExcused.value[index].date!))} ";
    }else if(dayexcused.substring(0,9)=="Wednesday"){
      return "Thứ 4, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayExcused.value[index].date!))} ";
    }else if(dayexcused.substring(0,8)=="Thursday"){
      return "Thứ 5, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayExcused.value[index].date!))} ";
    }else if(dayexcused.substring(0,6)=="Friday"){
      return "Thứ 6, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayExcused.value[index].date!))} ";
    }else if(dayexcused.substring(0,8)=="Saturday"){
      return "Thứ 7, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayExcused.value[index].date!))} ";
    }else if(dayexcused.substring(0,6)=="Sunday"){
      return "Chủ nhật, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayExcused.value[index].date!))} ";
    }else{
      return "";
    }
  }
  goToDetailStatisticalParent(){
    Get.toNamed(Routes.detailAllStasticalPageParent, arguments: [dateStart.value.toString(), dateEnd.value.toString()]);

  }

}
enum TimeStatus{
  ON_TIME,
  ABSENT_WITHOUT_LEAVE,
  NOT_ON_TIME,
  EXCUSED_ABSENCE
}
