import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_neat_and_clean_calendar/flutter_neat_and_clean_calendar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:task_manager/commom/utils/app_utils.dart';
import 'package:task_manager/commom/utils/date_time_picker.dart';
import 'package:task_manager/commom/utils/time_utils.dart';
import 'package:task_manager/view/mobile/role/parent/diligence_parent/statistical_parent/statistical_parent_controller.dart';
import 'package:task_manager/view/mobile/role/parent/parent_home_controller.dart';
import 'package:task_manager/view/mobile/role/parent/parent_home_page.dart';
import 'package:task_manager/view/mobile/role/teacher/diligent_management_teacher/date_picker_diligent_teacher/date_picker_diligent_teacher_page.dart';

import '../../../../../../commom/constants/date_format.dart';

class StatisticalParentPage extends GetWidget<StatisticalParentController> {

  final controller = Get.put(StatisticalParentController());

  StatisticalParentPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
          appBar: AppBar(
            actions: [
              IconButton(
                onPressed: () {
                  Get.to(ParentHomePage());
                },
                icon: const Icon(Icons.home),
                color: Colors.white,
              )
            ],
            backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
            title: Text(
              'Thống Kê',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.sp,
                  fontFamily: 'static/Inter-Medium.ttf'),
            ),
          ),
          body: Column(
            children: [
              HeaderPage(),
              Container(
                margin: EdgeInsets.only(
                    top: 16.h, bottom: 8.h, right: 16.w, left: 16.w),
                child: Row(
                  children: [
                    Text(
                      'Nhật Ký Chuyên Cần',
                      style: TextStyle(
                          color: const Color.fromRGBO(26, 26, 26, 1),
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'assets/font/static/Inter-Medium.ttf'),
                    ),
                 Expanded(child: Container()),
              InkWell(
                onTap: (){
                  Get.dialog(
                      Dialog(
                        child: IntrinsicHeight(
                            child:
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 16,vertical: 16),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(6)
                              ),
                              child:
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(

                                    margin: EdgeInsets.only( bottom: 8.h),
                                    child:
                                    Text("Vui lòng chọn ngày", style:  TextStyle(color: Colors.black, fontWeight: FontWeight.w500, fontSize: 12.sp),),),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Container(
                                          alignment: Alignment.centerLeft,
                                          padding: EdgeInsets.only(
                                              left: 8.w),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                              const BorderRadius.all(Radius.circular(6.0)),
                                              border: Border.all(
                                                width: 1,
                                                style: BorderStyle.solid,
                                                color: const Color.fromRGBO(192, 192, 192, 1),
                                              )),
                                          child:
                                          TextFormField(
                                            keyboardType: TextInputType.multiline,
                                            maxLines: null,
                                            style: TextStyle(
                                              fontSize: 10.0.sp,
                                              color: const Color.fromRGBO(26, 26, 26, 1),
                                            ),
                                            onTap: () {
                                              selectDateTimeStart(controller.controllerdateStart.value.text, DateTimeFormat.formatDateShort,context);
                                            },
                                            cursorColor: const Color.fromRGBO(248, 129, 37, 1),
                                            controller:
                                            controller.controllerdateStart.value,
                                            readOnly: true,
                                            decoration: InputDecoration(
                                              suffixIcon: Container(
                                                  child:SizedBox(
                                                    height: 1,
                                                    width: 1,
                                                    child: SvgPicture.asset(
                                                      "assets/images/icon_date_picker.svg",
                                                      fit: BoxFit.scaleDown,
                                                    ),
                                                  )
                                              ),
                                              labelText: "Từ ngày",
                                              border: InputBorder.none,
                                              labelStyle: TextStyle(
                                                  color: const Color.fromRGBO(177, 177, 177, 1),
                                                  fontSize: 12.sp,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily: 'assets/font/static/Inter-Medium.ttf'
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(padding: EdgeInsets.only(right: 16.w)),
                                      Expanded(
                                        child: Container(
                                          alignment: Alignment.centerLeft,
                                          padding: EdgeInsets.only(
                                              left: 8.w),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                              const BorderRadius.all(Radius.circular(6.0)),
                                              border: Border.all(
                                                width: 1,
                                                style: BorderStyle.solid,
                                                color: const Color.fromRGBO(192, 192, 192, 1),
                                              )),
                                          child: TextFormField(
                                            keyboardType: TextInputType.multiline,
                                            maxLines: null,
                                            style: TextStyle(
                                              fontSize: 10.0.sp,
                                              color: const Color.fromRGBO(26, 26, 26, 1),
                                            ),
                                            onTap: () {

                                              selectDateTimeEnd(controller.controllerdateEnd.value.text, DateTimeFormat.formatDateShort,context);
                                              FocusScope.of(context).nextFocus();
                                            },
                                            readOnly: true,
                                            cursorColor: const Color.fromRGBO(248, 129, 37, 1),
                                            controller:
                                            controller.controllerdateEnd.value,
                                            decoration: InputDecoration(
                                              suffixIcon:
                                              SizedBox(
                                                height: 1,
                                                width: 1,
                                                child: SvgPicture.asset(
                                                  "assets/images/icon_date_picker.svg",
                                                  fit: BoxFit.scaleDown,
                                                ),
                                              ),
                                              label: const Text("Đến ngày"),
                                              border: InputBorder.none,
                                              labelStyle: TextStyle(
                                                  color: const Color.fromRGBO(177, 177, 177, 1),
                                                  fontSize: 12.sp,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily: 'assets/font/static/Inter-Medium.ttf'
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Padding(padding: EdgeInsets.only(top: 16)),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(child:
                                      SizedBox(
                                        height: 24.h,
                                        child:  ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                                backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
                                                side: const BorderSide(
                                                    color: Color.fromRGBO(248, 129, 37, 1),width: 1
                                                ),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(6),
                                                )
                                            ),
                                            onPressed: (){
                                              Get.back();
                                            },
                                            child: Text('Hủy', style: TextStyle(color: const Color.fromRGBO(248, 129, 37, 1), fontSize: 12.sp, fontWeight: FontWeight.w400, fontFamily: 'assets/font/static/Inter-Regular.ttf'),)),
                                      )),
                                      Padding(padding: EdgeInsets.only(right: 8.w)),
                                      Expanded(child:SizedBox(
                                        height: 24.h,
                                        child: ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                                backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
                                                side: const BorderSide(
                                                    color: Color.fromRGBO(248, 129, 37, 1),width: 1
                                                ),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(6),
                                                )
                                            ),
                                            onPressed: (){
                                              controller.dayAbsent.value.clear();
                                              controller.dayExcused.value.clear();
                                              controller.dayonShool.value.clear();
                                              controller.dayNotontime.value.clear();
                                              controller.getListDiligence(
                                                  DateTime(int.parse(controller.dateStart.value.substring(6,10)), int.parse(controller.dateStart.value.substring(4,5)),int.parse(controller.dateStart.value.substring(1,2))).millisecondsSinceEpoch,
                                                  DateTime(int.parse(controller.dateEnd.value.substring(6,10)), int.parse(controller.dateEnd.value.substring(4,5)),int.parse(controller.dateEnd.value.substring(0,2))).millisecondsSinceEpoch
                                              );

                                              controller.dayonShool.refresh();
                                              controller.dayNotontime.refresh();
                                              controller.dayAbsent.refresh();
                                              controller.dayExcused.refresh();

                                              Get.back();
                                            },
                                            child: Text('Xác Nhận', style: TextStyle(color: const Color.fromRGBO(255, 255, 255, 1), fontSize: 12.sp, fontWeight: FontWeight.w400, fontFamily: 'assets/font/static/Inter-Regular.ttf'),)),
                                      ))
                                    ],
                                  ),

                                ],
                              ),
                            )
                        ),
                      )
                  );
                },
                child:  Row(
                  children: [
                    Text("${controller.controllerdateStart.value.text} - ${controller.controllerdateEnd.value.text}", style: TextStyle(  color: const Color.fromRGBO(26, 26, 26, 1),
                        fontSize: 12.sp,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'assets/font/static/Inter-Medium.ttf')),
                    Padding(padding: EdgeInsets.only(right: 4.w)),

                    SizedBox(
                      child: Image.asset("assets/images/icon_calenda.png",height: 16, width: 16,),
                    ),
                  ],
                ),
              )

                  ],
                ),
              ),
              BodyPage()
            ],
          ),
        ));
  }

  ListView detail_list_item_Not_On_Time() {
    return ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: controller.dayNotontime.value.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {

            },
            child:
            Container(
              color: const Color.fromRGBO(245, 245, 245, 1),
              margin: EdgeInsets.only(top: 8.h),
              child: Row(
                children: [
                  Text("${controller.getConvertDateNotOn(index)} lúc ",
                    style: styleContent(),
                  ),
                  Text(
                    '${controller.outputTimeFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.dayNotontime.value[index].updatedAt!))}',
                    style: TextStyle(
                        fontSize: 12.sp,
                        color: controller.getColorTextStatus(controller
                            .dayNotontime.value[index].statusDiligent)),
                  )
                ],
              ),
            )
          );
        });
  }

  ListView detail_list_item_On_Time() {
    return 
      ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: controller.dayonShool.value.length,
        itemBuilder: (context, index) {
          return 
           Obx(() =>  GestureDetector(
               onTap: () {
               },
               child:
               Container(
                 margin: EdgeInsets.only(top: 8.h),
                 child:
                 Row(
                   children: [
                     Text("${controller.getConvertDateOnTime(index)}",
                       style: styleContent(),
                     ),
                     Expanded(child: Container()),
                   ],
                 ),
               )
           ));
        });
  }

  ListView detail_list_item_ExcusedAbsence() {
    return ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: controller.dayExcused.value.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
            },
            child:
            Container(
              color: const Color.fromRGBO(245, 245, 245, 1),
              margin: EdgeInsets.only(top: 8.h),
              child: Row(
                children: [
                  Text("${controller.getConvertDateExcused(index)} lúc ",
                    style: styleContent(),
                  ),
                  Text(
                    '${controller.outputTimeFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.dayExcused.value[index].updatedAt!))}',
                    style: TextStyle(
                        fontSize: 12,
                        color: controller.getColorTextStatus(
                            controller.dayExcused.value[index].statusDiligent)),
                  )
                ],
              ),
            )
          );
        });
  }
  ListView detail_list_item_AbsentWithoutLeave() {
    return ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: controller.dayAbsent.value.length,
        itemBuilder: (context, index) {
          return GestureDetector(
              onTap: () {
              },
              child:
              Container(
                color: const Color.fromRGBO(245, 245, 245, 1),
                margin: EdgeInsets.only(top: 8.h),
                child: Row(
                  children: [
                    Text("${controller.getConvertDateAbsend(index)} lúc ",
                      style: styleContent(),
                    ),
                    Text(
                      '${controller.outputTimeFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.dayAbsent.value[index].updatedAt!))}',
                      style: TextStyle(
                          fontSize: 12,
                          color: controller.getColorTextStatus(
                              controller.dayAbsent.value[index].statusDiligent)),
                    )
                  ],
                ),
              )
          );
        });
  }

  styleTitle() {
    return TextStyle(
        color: const Color.fromRGBO(26, 26, 26, 1),
        fontSize: 12.sp,
        fontWeight: FontWeight.w400,
        fontFamily: 'assets/font/static/Inter-Regular.ttf');
  }

  styleContent() {
    return TextStyle(
        color: const Color.fromRGBO(90, 90, 90, 1),
        fontSize: 12.sp,
        fontWeight: FontWeight.w400,
        fontFamily: 'assets/font/static/Inter-Regular.ttf');
  }

  HeaderPage() {
    return Container(
      padding: EdgeInsets.all(16.h),
      color: const Color.fromRGBO(255, 255, 255, 1),
      height: 65.h,
      child: Row(
        children: [
          CircleAvatar(
            backgroundColor: Colors.white,
            backgroundImage: Image.network(
              Get.find<ParentHomeController>().currentStudentProfile.value.image??
                  "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
              errorBuilder:
                  (context, object, stackTrace) {
                return Image.asset(
                  "assets/images/img_Noavt.png",
                );
              },
            ).image,
          ),
          Container(
            margin: EdgeInsets.only(left: 8.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("${Get.find<ParentHomeController>().currentStudentProfile.value.fullName}",
                  style: TextStyle(
                      color: const Color.fromRGBO(26, 26, 26, 1), fontSize: 12.sp),
                ),
                Text(
                  '${Get.find<ParentHomeController>().currentStudentProfile.value.school!.name}',
                  style: TextStyle(
                      color: const Color.fromRGBO(177, 177, 177, 1), fontSize: 12.sp),
                ),
              ],
            ),
          ),

          Expanded(child: Container()),
         InkWell(
           onTap: (){
             controller.goToDetailStatisticalParent();
           },
           child:
           Container(
             alignment: Alignment.center,
             decoration: BoxDecoration(
                 borderRadius: BorderRadius.circular(6),
                 color: const Color.fromRGBO(254, 230, 211, 1)),
             width: 80.w,
             height: 18.h,
             child: Text(
               "Chi tiết",
               style: TextStyle(
                   color: const Color.fromRGBO(248, 129, 37, 1),
                   fontSize: 12.sp,
                   fontFamily: 'static/Inter-Medium.ttf'),
             ),
           ),
         )
        ],
      ),
    );
  }

  BodyPage() {
    return 
  Obx(() =>     Container(
    margin: EdgeInsets.only(left: 16.w, right: 16.w),
    padding:
    EdgeInsets.only(top: 11.h, bottom: 11.h, right: 16.w, left: 16.w),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6.r),
        color: const Color.fromRGBO(255, 255, 255, 1)),
    child: Column(
      children: [
        Column(
          children: [
            InkWell(
              onTap: () {
                controller.aDayGotoSchool.value =
                !controller.aDayGotoSchool.value;
              },
              child:
              Row(
                children: [
                  Text(
                    'Ngày đi học:',
                    style: controller.text_color.value
                        ? styleTitle()
                        : TextStyle(
                        color: const Color.fromRGBO(133, 133, 133, 1),
                        fontSize: 12.sp),
                  ),
                  Expanded(child: Container()),
                  Row(
                    children: [
                      Text("${controller.totalDayOnTime.value} ngày",
                        style: TextStyle(
                            color: const Color.fromRGBO(133, 133, 133, 1),
                            fontSize: 14.sp),
                      ),
                      Padding(padding: EdgeInsets.only(right: 9.w)),
                      Icon(controller.aDayGotoSchool.value
                          ? Icons.keyboard_arrow_up
                          : Icons.keyboard_arrow_down_outlined),
                    ],
                  )
                ],
              ),
            ),
            Visibility(
                visible: controller.aDayGotoSchool.value,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    color: const Color.fromRGBO(245, 245, 245, 1),
                  ),
                  padding: const EdgeInsets.all(4),
                  margin: EdgeInsets.only(top: 11.h),
                  child: detail_list_item_On_Time(),
                ))
          ],
        ),
        Padding(padding: EdgeInsets.only(top: 22.h)),
        Column(
          children: [
            InkWell(
              onTap: () {
                controller.dayOff.value =
                !controller.dayOff.value;
              },
              child: Row(
                children: [
                  Text(
                    'Ngày nghỉ có phép',
                    style: controller.text_color.value
                        ? styleTitle()
                        : TextStyle(
                        color: const Color.fromRGBO(133, 133, 133, 1),
                        fontSize: 12.sp),
                  ),
                  Expanded(child: Container()),
                  Row(
                    children: [
                      Text("${controller.totalDayExsecud.value} ngày",
                        // '${controller.diligence.value.studentNotOnTime!.totalDaysNotOnTime} ngày',
                        style: TextStyle(
                            color: const Color.fromRGBO(133, 133, 133, 1),
                            fontSize: 14.sp),
                      ),
                      Padding(padding: EdgeInsets.only(right: 9.w)),
                      Icon(controller.dayOff.value
                          ? Icons.keyboard_arrow_up
                          : Icons.keyboard_arrow_down_outlined),
                    ],
                  )
                ],
              ),
            ),
            Visibility(
                visible: controller.dayOff.value,
                child:     Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: const Color.fromRGBO(245, 245, 245, 1),
                    ),
                    padding: const EdgeInsets.all(4),
                    margin: const EdgeInsets.only(top: 11),
                    child: Column(
                      children: [
                        detail_list_item_ExcusedAbsence(),
                      ],
                    )
                ),
            )
          ],
        ),
        Padding(padding: EdgeInsets.only(top: 22.h)),
        Column(
          children: [
            InkWell(
              onTap: () {
                controller.dayOffExcused.value =
                !controller.dayOffExcused.value;
              },
              child: Row(
                children: [
                  Text(
                    'Ngày nghỉ không phép:',
                    style: controller.text_color.value
                        ? styleTitle()
                        : TextStyle(
                        color: const Color.fromRGBO(133, 133, 133, 1),
                        fontSize: 12.sp),
                  ),
                  Expanded(child: Container()),
                  Row(
                    children: [
                      Text("${controller.totalDayAbsend.value} ngày",
                        // '${controller.diligence.value.studentNotOnTime!.totalDaysNotOnTime} ngày',
                        style: TextStyle(
                            color: const Color.fromRGBO(133, 133, 133, 1),
                            fontSize: 14.sp),
                      ),
                      Padding(padding: EdgeInsets.only(right: 9.w)),
                      Icon(controller.dayOffExcused.value
                          ? Icons.keyboard_arrow_up
                          : Icons.keyboard_arrow_down_outlined),
                    ],
                  )
                ],
              ),
            ),
            Visibility(
              visible: controller.dayOffExcused.value,
              child:     Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    color: const Color.fromRGBO(245, 245, 245, 1),
                  ),
                  padding: const EdgeInsets.all(4),
                  margin: const EdgeInsets.only(top: 11),
                  child: Column(
                    children: [
                      detail_list_item_AbsentWithoutLeave()
                    ],
                  )
              ),
            )
          ],
        ),
        Padding(padding: EdgeInsets.only(top: 22.h)),
        Column(
          children: [
            InkWell(
              onTap: () {
                controller.lateForSchool.value =
                !controller.lateForSchool.value;
              },
              child: Row(
                children: [
                  Text(
                    'Đến muộn:',
                    style: controller.text_color.value
                        ? styleTitle()
                        : TextStyle(
                        color: const Color.fromRGBO(133, 133, 133, 1),
                        fontSize: 12.sp),
                  ),
                  Expanded(child: Container()),
                  Row(
                    children: [
                      Text("${controller.totalDayNotOnTime.value} ngày",

                        style: TextStyle(
                            color: const Color.fromRGBO(133, 133, 133, 1),
                            fontSize: 14.sp),
                      ),
                      Padding(padding: EdgeInsets.only(right: 9.w)),
                      Icon(controller.lateForSchool.value
                          ? Icons.keyboard_arrow_up
                          : Icons.keyboard_arrow_down_outlined),
                    ],
                  )
                ],
              ),
            ),
            Visibility(
                visible: controller.lateForSchool.value,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    color: const Color.fromRGBO(245, 245, 245, 1),
                  ),
                  padding: const EdgeInsets.all(4),
                  margin: EdgeInsets.only(top: 11.h),
                  child: detail_list_item_Not_On_Time(),
                ),

            )
          ],
        ),
      ],
    ),
  ));
  }
  selectDateTime(stringTime, format,context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(
        Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {

      if (value != null) {
        date = TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
      }
    });

    if (date.isNotEmpty) {
      controller.textDate.value= "$date";
      controller.selectedDate.value = date;
      controller.dayAbsent.value.clear();
      controller.dayExcused.value.clear();
      controller.dayonShool.value.clear();
      controller.dayNotontime.value.clear();
      controller.dayonShool.refresh();
      controller.dayNotontime.refresh();
      controller.dayAbsent.refresh();
      controller.dayExcused.refresh();

    }
    FocusScope.of(context).requestFocus(FocusNode());
  }
  selectDateTimeStart(stringTime, format,context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      if (value != null) {
        date =
            TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
      }
    });

    if (date.isNotEmpty) {
      controller.controllerdateStart.value.text = "$date";
      controller.dateStart.value = date;
      // controller.getListDiligence(
      //     DateTime(int.parse(controller.dateStart.value.substring(6,10)), int.parse(controller.dateStart.value.substring(3,5)),int.parse(controller.dateStart.value.substring(0,2))).millisecondsSinceEpoch,
      //     DateTime(int.parse(controller.dateEnd.value.substring(6,10)), int.parse(controller.dateEnd.value.substring(3,5)),int.parse(controller.dateEnd.value.substring(0,2))).millisecondsSinceEpoch
      // );
    }
    FocusScope.of(context).requestFocus(FocusNode());
  }

  selectDateTimeEnd(stringTime, format,context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      if (value != null) {
        date =
            TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
      }
    });

    if (date.isNotEmpty) {
      controller.controllerdateEnd.value.text = "$date";
      controller.dateEnd.value = date;

    }
    FocusScope.of(context).requestFocus(FocusNode());
  }



}
