import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neat_and_clean_calendar/flutter_neat_and_clean_calendar.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:task_manager/commom/utils/app_utils.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/data/model/common/diligence.dart';
import 'package:task_manager/data/repository/diligence/diligence_repo.dart';
import 'package:task_manager/view/mobile/role/parent/parent_home_controller.dart';
class DetailStatisticalParentController extends GetxController{
  var format = CalendarFormat.month.obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var outputFormat = DateFormat('HH:mm:ss');
  var focusedDay = DateTime.now().obs;
  var selectedDay = DateTime.now().obs;
  var focusName = FocusNode().obs;
  var controllerName = TextEditingController().obs;
  var timeNow = DateFormat.Hms().format(DateTime.now()).obs;
  var isAddShowJob = false.obs;
  var dateNow = "".obs;
  var showDetailDiligence = true.obs;
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var detailDiligence = DetailDiligence().obs; 
  RxList<Items> item = <Items>[].obs;
  RxInt date= 0.obs;
  RxInt time= 0.obs;
  var status = ''.obs;
  var quarterTurns = 270.obs;
  var weekNumber = 1.obs;
  var heightOffset = (Get.width/5).obs;
  var weekController = DateRangePickerController().obs;
  var timelineController = CalendarController().obs;
  String ON_TIME_TEXT = "Đúng giờ";
  String NOT_ON_TIME_TEXT= "Đi muộn";
  String ABSENT_WITHOUT_LEAVE_TEXT = "Không phép";
  String EXCUSED_ABSENCE_TEXT = "Có phép";
  var allStatus = <StatusDetailDiligence>[].obs;
  var eventList = <NeatCleanCalendarEvent>[].obs;


  @override
  void onInit() {
    // TODO: implement onInit
    var data = Get.arguments;
    if (data != null) {
      date.value = data[0];
      status.value = data[1];
      time.value = data[2];
    }
    dateNow.value=outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(date.value!));
    focusedDay.value= DateTime(int.parse(dateNow.value.substring(6,10)),int.parse(dateNow.value.substring(3,5)),int.parse(dateNow.value.substring(0,2)));
  }
  getColorTextStatus(state){
    switch(state){
      case "ON_TIME":
        return Color.fromRGBO(77,197,145, 1);
      case "NOT_ON_TIME":
        return Color.fromRGBO(253,185,36, 1);
      case "EXCUSED_ABSENCE":
        return Color.fromRGBO(255,69,89,1);
      default:
        return Color.fromRGBO(255,69,89,1);
    }
  }
  getColorStatus(state) {
    switch (state) {
      case "ON_TIME":
        return Color.fromRGBO(77,197,145, 1);
      case "NOT_ON_TIME":
        return Color.fromRGBO(253, 185, 36, 1);
      case "ABSENT_WITHOUT_LEAVE":
        return Color.fromRGBO(255,69,89,1);
      default:
        return Color.fromRGBO(255,69,89,1);
    }
  }
  getTextStatus(state) {
    switch (state) {
      case "ON_TIME":
        return ON_TIME_TEXT;
      case "NOT_ON_TIME":
        return NOT_ON_TIME_TEXT;
      case "ABSENT_WITHOUT_LEAVE":
        return ABSENT_WITHOUT_LEAVE_TEXT;
      default:
        return EXCUSED_ABSENCE_TEXT;
    }
  }
  getStatusAll(fromdate,todate){
    for(int i=0; i<allStatus.value.length; i++){
      eventList.value.add(
        NeatCleanCalendarEvent('Chi tiết',
          location: "${outputFormat.format(DateTime.fromMillisecondsSinceEpoch(allStatus.value[i].updatedAt!))}",
          description:"${getTextStatus(allStatus.value[i].statusDiligent)}",
          startTime: DateTime.fromMillisecondsSinceEpoch(allStatus.value[i].date!),
          endTime:DateTime.fromMillisecondsSinceEpoch(allStatus.value[i].date!),
          color:getColorTextStatus(allStatus.value[i].statusDiligent),
        ),
      );
    }
  }
  }




