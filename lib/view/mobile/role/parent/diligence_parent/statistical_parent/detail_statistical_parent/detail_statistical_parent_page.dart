import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_neat_and_clean_calendar/flutter_neat_and_clean_calendar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:task_manager/commom/constants/date_format.dart';
import 'package:task_manager/view/mobile/role/parent/diligence_parent/statistical_parent/detail_statistical_parent/detail_statistical_parent_controller.dart';
import 'package:task_manager/view/mobile/role/parent/parent_home_controller.dart';
import 'package:task_manager/view/mobile/role/parent/parent_home_page.dart';
import '../../../../../../../commom/utils/color_utils.dart';

class DetailStatisticalParentPage extends GetWidget<DetailStatisticalParentController> {
  final controller = Get.put(DetailStatisticalParentController());
  @override
  Widget build(BuildContext context) {
    var  eventList = [
      NeatCleanCalendarEvent('Trạng thái',
          isMultiDay: false,
          isDone: true,
          description: '${controller.getTextStatus(controller.status.value)}',
          startTime: DateTime.fromMillisecondsSinceEpoch(controller.date.value),
          endTime: DateTime.fromMillisecondsSinceEpoch(controller.date.value),
          color: controller.getColorStatus(controller.status.value),
         ),
    ].obs;
    return 
     Obx(() =>  Scaffold(
         appBar: AppBar(
           actions: [
             IconButton(
               onPressed: () {
                 Get.to(ParentHomePage());
               },
               icon: Icon(Icons.home),
               color: Colors.white,
             )
           ],
           backgroundColor: Color.fromRGBO(248, 129, 37, 1),
           title: Text(
             'Chi Tiết',
             style: TextStyle(
                 color: Colors.white,
                 fontSize: 16,
                 fontFamily: 'static/Inter-Medium.ttf'),
           ),
         ),

         body:
         SingleChildScrollView(
           physics: ScrollPhysics(),
           child:  Container(
             color: Color.fromRGBO(245, 245, 245, 1),
             child:
             Column(
               crossAxisAlignment: CrossAxisAlignment.start,
               children: [
                 Container(
                   padding: EdgeInsets.all(16),
                   color: Color.fromRGBO(255, 255, 255, 1),
                   height: 65.h,
                   child:  Row(
                     children: [
                       CircleAvatar(
                         backgroundColor: Colors.white,
                         backgroundImage: Image.network(
                           Get.find<ParentHomeController>().currentStudentProfile.value.image??
                               "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                           errorBuilder:
                               (context, object, stackTrace) {
                             return Image.asset(
                               "assets/images/img_Noavt.png",
                             );
                           },
                         ).image,
                       ),
                       // SizedBox(
                       //   width: 32.w,
                       //   height: 32.h,
                       //   child: Image.network('${Get.find<ParentHomeController>().currentStudentProfile.value.image}'),
                       // ),
                       Container(
                         margin: EdgeInsets.only(left:8 ),
                         child: Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: [
                             Text('${Get.find<ParentHomeController>().currentStudentProfile.value.fullName}',style: TextStyle(color:Color.fromRGBO(26, 26, 26, 1), fontSize: 12),
                             ),
                             Padding(padding: EdgeInsets.only(top: 4.h)),
                             Text('${Get.find<ParentHomeController>().currentStudentProfile.value.school!.name}',style: TextStyle(color:Color.fromRGBO(177, 177, 177, 1), fontSize: 12),),
                           ],
                         ),
                       ) ,
                       // Expanded(child: Container()),
                       // Container(
                       //   alignment: Alignment.center,
                       //   decoration: BoxDecoration(
                       //       borderRadius: BorderRadius.circular(6),
                       //       color: Color.fromRGBO(254, 230, 211, 1)
                       //   ),
                       //   width: 90.w,
                       //   height: 18.h,
                       //   child: Text("${controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.date.value))}", style: TextStyle(color:Color.fromRGBO(248, 129, 37, 1), fontSize: 12, fontFamily:'static/Inter-Medium.ttf' ),),
                       // )

                     ],
                   ),
                 ),
                 Container(
                   height: 300.h,
                   child:  Calendar(
                     eventListBuilder: (BuildContext context,
                         List<NeatCleanCalendarEvent> _selectesdEvents) {
                       return new Text("");
                     },
                     startOnMonday: true,
                     weekDays: ['Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7', 'CN'],
                     eventsList: eventList,
                     isExpandable: false,
                     eventDoneColor: Colors.green,
                     selectedColor: Colors.orange,
                     selectedTodayColor: Colors.orange,
                     todayColor: Colors.orange,
                     eventColor: null,
                     locale: 'vi',
                     initialDate:controller.focusedDay.value,
                     onDateSelected: (value) {
                       for(int i =0; i<eventList.value.length; i++){
                         if(eventList.value[i].startTime == value){
                           controller.showDetailDiligence.value = true;
                           return;
                         }else{
                           controller.showDetailDiligence.value = false;
                         }
                       }
                     },

                     todayButtonText: 'Ngày hôm nay',
                     multiDayEndText: 'Kết thúc',
                     isExpanded: true,
                     onEventSelected: (value) {
                     },
                     onEventLongPressed: (value) {
                     },
                     hideTodayIcon: true,
                     hideBottomBar: true,
                     expandableDateFormat: 'EEEE, dd/MM/yyyy',
                     datePickerType: DatePickerType.hidden,
                     dayOfWeekStyle: TextStyle(
                         color: Colors.orange, fontWeight: FontWeight.w800, fontSize: 10),
                   ),
                 ),
                 // dateTimeSelectV2(),
                 Container(
                   padding: EdgeInsets.only(top: 16, left: 16, right: 16),
                   color: Color.fromRGBO(255, 255, 255, 1),
                   height:80,
                   margin: EdgeInsets.only(top: 16),
                   child: Row(
                     crossAxisAlignment: CrossAxisAlignment.start,
                     children: [

                       Text('Trạng thái', style:  TextStyle(color: Color.fromRGBO(26, 26, 26, 1), fontSize: 14, fontWeight: FontWeight.w400,fontFamily: 'assets/font/static/Inter-Regular.ttf'),),
                       Expanded(child: Container()),
                       Container(
                         child: Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: [
                             Row(
                               children: [
                                 Image.asset('assets/images/circle_green.png',width: 10.w,height: 10.h,),
                                 Padding(padding: EdgeInsets.only(right: 8)),
                                 Text('Đúng Giờ', style: TextStyle(color: Color.fromRGBO(133, 133, 133, 1), fontSize: 12,fontWeight: FontWeight.w400, fontFamily:'assets/font/static/Inter-Regular.ttf' ),)
                               ],
                             ),
                             Padding(padding: EdgeInsets.only(top: 8)),
                             Row(
                               children: [
                                 Image.asset('assets/images/circle_red.png',width: 10.w,height: 10.h,),
                                 Padding(padding: EdgeInsets.only(right: 8)),
                                 Text('Nghỉ Học', style: TextStyle(color: Color.fromRGBO(133, 133, 133, 1), fontSize: 12,fontWeight: FontWeight.w400, fontFamily:'assets/font/static/Inter-Regular.ttf' ),)
                               ],
                             ),
                             Padding(padding: EdgeInsets.only(top: 8)),
                             Row(
                               children: [
                                 Image.asset('assets/images/circle_yellow.png',width: 10.w,height: 10.h,),
                                 Padding(padding: EdgeInsets.only(right: 8)),
                                 Text('Đi Muộn', style: TextStyle(color: Color.fromRGBO(133, 133, 133, 1), fontSize: 12,fontWeight: FontWeight.w400, fontFamily:'assets/font/static/Inter-Regular.ttf' ),)
                               ],
                             ),

                           ],
                         ),)
                     ],
                   ),
                 ),
                 Container(
                   height: 10.h,
                   color: Color.fromRGBO(245, 245, 245, 1),
                 ),
                 controller.showDetailDiligence.value?
                 Container(
                   color: Color.fromRGBO(255, 255, 255, 1),
                   child:
                   Column(
                     children: [
                       ListTile(
                         title : Column(
                           children: [
                             Padding(padding: EdgeInsets.only(top: 8.h)),
                             Container(
                                 width: double.infinity,
                                 child: Row(
                                   children: [
                                     Image.asset('assets/images/icon_diligence_student.png', width: 12.w,height: 12.h,),
                                     Padding(padding: EdgeInsets.only(right: 8.r)),
                                     Text('Điểm danh lúc ${controller.outputFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.time.value))}',style: TextStyle(color: Colors.black, fontSize: 12.sp, fontWeight: FontWeight.w400),)
                                   ],
                                 )
                             ),
                             Container(
                                 margin: EdgeInsets.only(left:10.w, top: 4.h),
                                 child:   Column(
                                   crossAxisAlignment: CrossAxisAlignment.start,
                                   children: [
                                     Padding(padding: EdgeInsets.only(top:6.h)),
                                     Row(
                                       children: [
                                         Icon(
                                           Icons.location_on,
                                           color: Color.fromRGBO(177, 177, 177, 1),
                                           size: 12,
                                         ),
                                         Container(
                                           height: 30.h,
                                           width:300.w,
                                           margin: EdgeInsets.only(left: 6),
                                           child: Text(
                                             '${Get.find<ParentHomeController>().currentStudentProfile.value.school!.name},${Get.find<ParentHomeController>().currentStudentProfile.value.school!.address}',
                                             style: TextStyle(
                                                 color: Color.fromRGBO(177, 177, 177, 1),
                                                 fontSize: 10.sp),
                                           ),
                                         )
                                       ],
                                     ),
                                     Padding(padding: EdgeInsets.only(top: 8.h)),
                                     Container(
                                       height: 20.h,
                                       padding: EdgeInsets.only(top: 3,bottom: 3,right: 12,left: 12),
                                       decoration: BoxDecoration(
                                         borderRadius: BorderRadius.circular(6),
                                         color: controller.getColorStatus(controller.status.value),
                                       ),
                                       child:
                                       Text('${controller.getTextStatus(controller.status.value)}', style:  TextStyle(color: Color.fromRGBO(255, 255, 255, 1), fontSize: 10),),),
                                   ],
                                 )
                             ),
                           ],
                         ),
                       ),
                     ],
                   ),

                 ):Container()
               ],
             ),
           ),
         )
     ));
  }
}
