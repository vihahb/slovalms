import 'package:flutter/cupertino.dart';
import 'package:flutter_neat_and_clean_calendar/flutter_neat_and_clean_calendar.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:task_manager/commom/utils/app_utils.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/data/model/common/diligence.dart';
import 'package:task_manager/data/model/common/school_year.dart';
import 'package:task_manager/data/model/common/user_profile.dart';
import 'package:task_manager/data/repository/diligence/diligence_repo.dart';
import 'package:task_manager/view/mobile/role/parent/parent_home_controller.dart';
import 'package:task_manager/view/mobile/role/student/student_home_controller.dart';
class DetailAllStatisticalParentController extends GetxController{
  var format = CalendarFormat.month.obs;
  var focusedDay = DateTime.now().obs;
  var selectedDay = DateTime.now().obs;
  var focusName = FocusNode().obs;
  var controllerName = TextEditingController().obs;
  var isAddShowJob = false.obs;
  var dateNow = "".obs;
  var dayon ="".obs;
  var ShowDatailDiligence = true.obs;
  String ON_TIME_TEXT = "Đúng giờ";
  String NOT_ON_TIME_TEXT= "Đi muộn";
  String ABSENT_WITHOUT_LEAVE_TEXT = "Không phép";
  var outputFormat = DateFormat('HH:mm:ss');
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  String EXCUSED_ABSENCE_TEXT = "Có phép";
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var userProfile = UserProfile().obs;
  RxList<SchoolYear> dateTime = <SchoolYear>[].obs;
  var diligence = Diligence().obs;
  var outputMonthFormat = DateFormat('MM/yyyy');
  var isvisibility = false.obs;
  var isvisibility_icon = false.obs;
  var isvisibility_latetime = false.obs;
  var text_color = false.obs;
  RxList<String> listStatus = <String>[].obs;
  RxList<Items> item = <Items>[].obs;
  RxList<StatusDetailDiligence> dayonShool = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayAbsent = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayExcused = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayNotontime = <StatusDetailDiligence>[].obs;
  var detailDiligence = DetailDiligence().obs;
  var studentOnTime = StudentOnSchool().obs;
  var studentNotOnTime = StudentNotOnTime().obs;
  var studentExcused = StudentExcusedAbsence().obs;
  var studentAbsend = StudentAbsentWithoutLeave().obs;
  var totalDayOnTime = 0.obs;
  var totalDayNotOnTime = 0.obs;
  var totalDayAbsend = 0.obs;
  var totalDayExsecud = 0.obs;
  var selectDate = "".obs;
  var allStatus = <StatusDetailDiligence>[].obs;
  var eventList = <NeatCleanCalendarEvent>[].obs;
  var eventTest = <NeatCleanCalendarEvent>[].obs;
  var objectEvent = NeatCleanCalendarEvent("", startTime: DateTime.now(), endTime: DateTime.now()).obs;
  var dateStart ="".obs;
  var dateEnd ="".obs;
  var outputDateToDateFormat = DateFormat('dd/MM/yyyy ');
  var fromYearSelect ="".obs;
  var toYearSelect ="".obs;

  @override
  void onInit() {
    var data = Get.arguments;
    if(data != null){
      dateStart.value = data[0];
      dateEnd.value = data[1];
    }
    getStatusAll(dateStart.value, dateEnd.value);
    setDateSchoolYears();
    if(Get.find<ParentHomeController>().fromYearPresent== DateTime.now().year || Get.find<ParentHomeController>().toYearPresent== DateTime.now().year){
    }else{
      dateStart.value= outputDateToDateFormat.format(DateTime(int.parse(fromYearSelect.value.substring(0,4)), DateTime.now().month));
      dateEnd.value = outputDateToDateFormat.format(DateTime(int.parse(toYearSelect.value.substring(0,4)), DateTime.now().month+1).subtract(Duration(days: 1)));
    }
    ShowDatailDiligence.value = false;


  }


  getColorStatus(state) {
    switch (state) {
      case "ON_TIME":
        return Color.fromRGBO(221, 246, 235, 1);
      case "NOT_ON_TIME":
        return Color.fromRGBO(255, 239, 203, 1);
      case "ABSENT_WITHOUT_LEAVE":
        return Color.fromRGBO(255, 200, 206, 1);
      default:
        return Color.fromRGBO(255, 255, 255, 1);
    }
  }

  getColorTextStatus(state) {
    switch (state) {
      case "ON_TIME":
        return Color.fromRGBO(77, 197, 145, 1);
      case "NOT_ON_TIME":
        return Color.fromRGBO(253, 185, 36, 1);
      case "ABSENT_WITHOUT_LEAVE":
        return Color.fromRGBO(255, 69, 89, 1);
      default:
        return Color.fromRGBO(255, 69, 89, 1);
    }
  }

  getTextStatus(state) {
    switch (state) {
      case "ON_TIME":
        return ON_TIME_TEXT;
      case "NOT_ON_TIME":
        return NOT_ON_TIME_TEXT;
      case "ABSENT_WITHOUT_LEAVE":
        return ABSENT_WITHOUT_LEAVE_TEXT;
      default:
        return EXCUSED_ABSENCE_TEXT;
    }
  }
  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }

  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime
        .add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }


  getListDiligence(){
    var userId = Get.find<ParentHomeController>().currentStudentProfile.value.id;
    var todate = DateTime(findLastDateOfTheWeek(DateTime.now()).year, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    var fromdate = DateTime(findFirstDateOfTheWeek(DateTime.now()).year, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    _diligenceRepo.DetailListDiligence(userId, fromdate, todate,"").then((value) {
      if (value.state == Status.SUCCESS) {
        detailDiligence.value = value.object!;
        dayonShool.value = detailDiligence.value.studentOnSchool!.dayOnSchool! ;
        dayExcused.value = detailDiligence.value.studentExcusedAbsence!.dayExcusedAbsence!;
        dayAbsent.value = detailDiligence.value.studentAbsentWithoutLeave!.dayAbsentWithoutLeave!;
        dayNotontime.value = detailDiligence.value.studentNotOnTime!.dayNotOnTime!;
        allStatus.value.addAll(dayonShool.value);
        allStatus.value.addAll(dayNotontime.value);
        allStatus.value.addAll(dayAbsent.value);
        allStatus.value.addAll(dayExcused.value);
        getStatusAll(dateStart.value, dateEnd.value);
        if(detailDiligence.value.studentOnSchool != null){
          totalDayOnTime.value = detailDiligence.value.studentOnSchool!.totalDaysOnSchool!;
        }
        if(detailDiligence.value.studentNotOnTime != null){
          totalDayNotOnTime.value = detailDiligence.value.studentNotOnTime!.totalDaysNotOnTime!;
        }
        if(detailDiligence.value.studentAbsentWithoutLeave != null){
          totalDayAbsend.value = detailDiligence.value.studentAbsentWithoutLeave!.totalDaysAbsentWithoutLeave!;
        }
        if(detailDiligence.value.studentExcusedAbsence != null){
          totalDayExsecud.value = detailDiligence.value.studentExcusedAbsence!.totalDaysExcusedAbsence!;
        }
        AppUtils.shared.hideLoading();
        Future.delayed(Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }
  getListDiligenceToSelectYear(fromdate, todate){
    var userId = Get.find<ParentHomeController>().currentStudentProfile.value.id;
    var fromdate = DateTime(int.parse(toYearSelect.value.substring(0,4)), DateTime.now().month).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(fromYearSelect.value.substring(0,4)), DateTime.now().month+1).subtract(Duration(days: 1)).millisecondsSinceEpoch;
    _diligenceRepo.DetailListDiligence(userId, fromdate, todate,"").then((value) {
      if (value.state == Status.SUCCESS) {
        detailDiligence.value = value.object!;
        dayonShool.value = detailDiligence.value.studentOnSchool!.dayOnSchool! ;
        dayExcused.value = detailDiligence.value.studentExcusedAbsence!.dayExcusedAbsence!;
        dayAbsent.value = detailDiligence.value.studentAbsentWithoutLeave!.dayAbsentWithoutLeave!;
        dayNotontime.value = detailDiligence.value.studentNotOnTime!.dayNotOnTime!;
        allStatus.value.addAll(dayonShool.value);
        allStatus.value.addAll(dayNotontime.value);
        allStatus.value.addAll(dayAbsent.value);
        allStatus.value.addAll(dayExcused.value);
        getStatusAll(dateStart.value, dateEnd.value);
        if(detailDiligence.value.studentOnSchool != null){
          totalDayOnTime.value = detailDiligence.value.studentOnSchool!.totalDaysOnSchool!;
        }
        if(detailDiligence.value.studentNotOnTime != null){
          totalDayNotOnTime.value = detailDiligence.value.studentNotOnTime!.totalDaysNotOnTime!;
        }
        if(detailDiligence.value.studentAbsentWithoutLeave != null){
          totalDayAbsend.value = detailDiligence.value.studentAbsentWithoutLeave!.totalDaysAbsentWithoutLeave!;
        }
        if(detailDiligence.value.studentExcusedAbsence != null){
          totalDayExsecud.value = detailDiligence.value.studentExcusedAbsence!.totalDaysExcusedAbsence!;
        }
        AppUtils.shared.hideLoading();
        Future.delayed(Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }


  setDateSchoolYears() {
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (Get.find<ParentHomeController>().fromYearPresent.value == DateTime.now().year) {
        getListDiligence();
        getStatusAll(dateStart.value, dateEnd.value);
        return;
      } else {
        fromYearSelect.value = "${Get.find<ParentHomeController>().fromYearPresent}";
        toYearSelect.value = "${Get.find<ParentHomeController>().toYearPresent}";
        getListDiligenceToSelectYear(fromYearSelect.value, toYearSelect.value);
        getStatusAll(dateStart.value, dateEnd.value);

        ShowDatailDiligence.value = true;
      }
    } else {
      if (Get.find<ParentHomeController>().toYearPresent.value == DateTime.now().year) {
        getListDiligence();
        getStatusAll(dateStart.value, dateEnd.value);
        return;
      } else {
        fromYearSelect.value = "${Get.find<ParentHomeController>().fromYearPresent}";
        toYearSelect.value = "${Get.find<ParentHomeController>().toYearPresent}";
        getListDiligenceToSelectYear(fromYearSelect.value, toYearSelect.value);
        getStatusAll(dateStart.value, dateEnd.value);

      }
    }
  }

  getStatusAll(fromdate,todate){
    for(int i=0; i<allStatus.value.length; i++){
      eventList.value.add(
        NeatCleanCalendarEvent('Chi tiết',
          location: "${outputFormat.format(DateTime.fromMillisecondsSinceEpoch(allStatus.value[i].updatedAt!))}",
          description:"${getTextStatus(allStatus.value[i].statusDiligent)}",
          startTime: DateTime.fromMillisecondsSinceEpoch(allStatus.value[i].date!),
          endTime: DateTime.fromMillisecondsSinceEpoch(allStatus.value[i].date!),
          color:getColorTextStatus(allStatus.value[i].statusDiligent),
        ),
      );
      eventTest.value = eventList.value.where((element) =>
      element.startTime.year == DateTime.now().year
          && element.startTime.month == DateTime.now().month).toList();
      print("dropTest${eventTest.value.length}");
      objectEvent.value = eventTest.value[0];
      ShowDatailDiligence.value = true;
      dateNow.value= outputDateFormat.format(eventList.value[i].startTime);
      focusedDay.value= DateTime(int.parse(dateNow.value.substring(6,10)),int.parse(dateNow.value.substring(3,5)),int.parse(dateNow.value.substring(0,2)));
    }



  }




}