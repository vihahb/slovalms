import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:task_manager/view/mobile/staticPage/static_page_controller.dart';
import 'package:webview_flutter/webview_flutter.dart';
class StaticPage extends GetWidget<StaticPageController> {
  final controller = Get.put(StaticPageController());
  StaticPage({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
          elevation: 0,
          title: Text("${controller.itemsStaticPage.value.title}",style: const TextStyle(color: Colors.white)),
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: const Icon(
                Icons.arrow_back_outlined,
                color: Colors.white,
              )),
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 8.w,vertical: 16.h),
            child: Html(data: controller.itemsStaticPage.value.content),
          ),
        ),
      ),
    );
  }
}
