import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:task_manager/commom/app_cache.dart';
import 'package:task_manager/view/mobile/schedule/teaching_schedule/teaching_schedule_controller.dart';

class TeachingSchedulePage extends GetView<TeachingScheduleController> {
  var controller = Get.put(TeachingScheduleController());


  TeachingSchedulePage({super.key});

  @override
  Widget build(BuildContext context) {
    return
     Scaffold(
       appBar: AppBar(
         backgroundColor:  Color.fromRGBO(248, 129, 37, 1),
         elevation: 0,
         title:  Text(
           "${controller.getTitleDetailSchedule(AppCache().userType)}",
           style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w500),
         ),
       ),
       body: SingleChildScrollView(
         child: Container(
           margin: const EdgeInsets.all(16),
           child: Column(
             crossAxisAlignment: CrossAxisAlignment.start,
             children: [
               Text(
                 "Môn ${controller.appoinment.value.subject}",
                 style: TextStyle(
                     fontWeight: FontWeight.w500,
                     fontSize: 12.sp,
                     color: Color.fromRGBO(133, 133, 133, 1)),
               ),
               const Padding(padding: EdgeInsets.only(top: 8)),
               Container(
                 decoration: BoxDecoration(
                     borderRadius: BorderRadius.circular(6),
                     color: Colors.white),
                 padding: const EdgeInsets.all(16),
                 child: Column(
                   children: [
                     Row(
                       children: [
                         Text(
                           "Môn học:",
                           style: TextStyle(
                               fontSize: 14.sp,
                               color: Color.fromRGBO(26, 26, 26, 1),
                               fontWeight: FontWeight.w400),
                         ),
                         Expanded(child: Container()),
                         Text("${controller.appoinment.value.subject}",
                             style: TextStyle(
                                 fontSize: 14.sp,
                                 color: Color.fromRGBO(26, 26, 26, 1),
                                 fontWeight: FontWeight.w500))
                       ],
                     ),
                     const Padding(padding: EdgeInsets.only(top: 16)),
                     Row(
                       children: [
                         Text(
                           "Lớp:",
                           style: TextStyle(
                               fontSize: 14.sp,
                               color: Color.fromRGBO(26, 26, 26, 1),
                               fontWeight: FontWeight.w400),
                         ),
                         Expanded(child: Container()),
                         Text("${controller.appoinment.value.notes}",
                             style: TextStyle(
                                 fontSize: 14.sp,
                                 color: Color.fromRGBO(248, 129, 37, 1),
                                 fontWeight: FontWeight.w500))
                       ],
                     ),
                     const Padding(padding: EdgeInsets.only(top: 16)),
                     Row(
                       children: [
                         Text(
                           "Thời gian:",
                           style: TextStyle(
                               fontSize: 14.sp,
                               color: Color.fromRGBO(26, 26, 26, 1),
                               fontWeight: FontWeight.w400),
                         ),
                         Expanded(child: Container()),
                         Text("${controller.outputFormat.format(controller.appoinment.value.startTime)} - ${controller.outputFormat.format(controller.appoinment.value.endTime)}",
                             style: TextStyle(
                                 fontSize: 14.sp,
                                 color: Color.fromRGBO(26, 26, 26, 1),
                                 fontWeight: FontWeight.w500))
                       ],
                     ),
                     const Padding(padding: EdgeInsets.only(top: 16)),
                     Row(
                       children: [
                          Text(
                           "Ngày:",
                           style: TextStyle(
                               fontSize: 14.sp,
                               color: Color.fromRGBO(26, 26, 26, 1),
                               fontWeight: FontWeight.w400),
                         ),
                         Expanded(child: Container()),
                         Text("${controller.outputDateFormat.format(controller.appoinment.value.startTime)}",
                             style: TextStyle(
                                 fontSize: 14.sp,
                                 color: Color.fromRGBO(26, 26, 26, 1),
                                 fontWeight: FontWeight.w500))
                       ],
                     ),
                     const Padding(padding: EdgeInsets.only(top: 16)),

                  AppCache().userType != "TEACHER"? Row(
                      children: [
                         Text(
                          "Giáo viên: ",
                          style: TextStyle(
                              fontSize: 14.sp,
                              color: Color.fromRGBO(26, 26, 26, 1),
                              fontWeight: FontWeight.w400),
                        ),
                        Expanded(child: Container()),
                        Text("${controller.appoinment.value.recurrenceId}",
                            style: TextStyle(
                                fontSize: 14.sp,
                                color: Color.fromRGBO(26, 26, 26, 1),
                                fontWeight: FontWeight.w500))
                      ],
                    )
                        : Container(),
                   ],
                 ),
               ),
               const Padding(padding: EdgeInsets.only(top: 16)),
               Row(
                 children:  [
                   Text(
                     "Danh Sách Học Sinh ",
                     style: TextStyle(
                         fontSize: 14.sp,
                         color: Color.fromRGBO(26, 26, 26, 1),
                         fontWeight: FontWeight.w400),
                   ),
                   Text("${controller.appoinment.value.notes}",
                       style: TextStyle(
                           fontSize: 14.sp,
                           color: Color.fromRGBO(248, 129, 37, 1),
                           fontWeight: FontWeight.w500))
                 ],
               ),
               Container(
                   child: ListView.builder(
                       shrinkWrap: true,
                       physics: const NeverScrollableScrollPhysics(),
                       itemCount: controller.students.value.length,
                       itemBuilder: (context, index) {
                         return GestureDetector(
                           onTap: () {
                           },
                           child: Column(
                             children: [
                               Row(
                                 children: [
                                   Container(
                                     margin: const EdgeInsets.only(
                                         left: 16, right: 24),
                                     child: Text(
                                       '${index+1}',
                                       style: const TextStyle(
                                           color: Color.fromRGBO(26, 26, 26, 1),
                                           fontSize: 14),
                                     ),
                                   ),
                                   Container(
                                     margin: const EdgeInsets.only(
                                         top: 8, bottom: 8),
                                     child: Row(
                                       children: [

                                         SizedBox(
                                           width: 48.w,
                                           height: 48.w,
                                           child: CircleAvatar(
                                             backgroundColor: Colors.white,
                                             backgroundImage: Image.network(
                                               controller.students.value[index].image ??
                                                   'https://files.lms.xteldev.com/files/n1/static/img',
                                               errorBuilder: (context, object, stackTrace) {
                                                 return Image.asset(
                                                   "assets/images/img_Noavt.png",
                                                 );
                                               },
                                             ).image,
                                           ),
                                         ),
                                         const Padding(
                                             padding: EdgeInsets.only(right: 8)),
                                         Column(
                                           crossAxisAlignment: CrossAxisAlignment.start,
                                           children: [
                                             SizedBox(
                                               child: RichText(
                                                   text: TextSpan(children: [
                                                     const TextSpan(
                                                         text: 'Học Sinh: ',
                                                         style: TextStyle(
                                                             color: Color.fromRGBO(
                                                                 248, 129, 37, 1),
                                                             fontSize: 14)),
                                                     TextSpan(
                                                         text:
                                                         '${controller.students.value[index].fullName}',
                                                         style: const TextStyle(
                                                             color: Color.fromRGBO(
                                                                 26, 26, 26, 1),
                                                             fontSize: 14)),
                                                   ])),
                                             ),
                                             const Padding(
                                                 padding:
                                                 EdgeInsets.only(top: 4)),
                                             SizedBox(
                                               width: 200,

                                               child: Text(
                                                 '${controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.students.value[index].birthday))}',
                                                 style: const TextStyle(
                                                     color: Color.fromRGBO(
                                                         133, 133, 133, 1),
                                                     fontSize: 14),
                                               ),
                                             )
                                           ],
                                         )
                                       ],
                                     ),
                                   )
                                 ],
                               ),
                               const Divider()
                             ],
                           ),
                         );
                       }))
             ],
           ),
         ),
       ),
     );
  }
}
