import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';


import '../../../../data/model/common/schedule.dart';
import '../../../../data/model/common/student_by_parent.dart';
import '../../../../data/model/common/user_profile.dart';
import '../../../../data/repository/schedule/schedule_repo.dart';


class TeachingScheduleController extends GetxController {
  var userProfile = UserProfile().obs;
  var studentByParent = StudentByParent().obs;
  var subject = Subject().obs;
  final ScheduleRepo _scheduleRepo = ScheduleRepo();
  RxList<Schedule> scheduleTeacher = <Schedule>[].obs;
  var  listStudent = Students().obs;
  var appoinment = Appointment(startTime: DateTime.now(), endTime: DateTime.now()).obs;
  var outputFormat = DateFormat('HH:mm');
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var useprofile = UserProfile().obs;
  RxList<Students> students = <Students>[].obs;
  @override
  void onInit() {
    var tmpAppoinment = Get.arguments;
    if(tmpAppoinment != null){
      appoinment.value = tmpAppoinment;
      students.value = appoinment.value.resourceIds as List<Students>;
    }
    super.onInit();
  }

  @override
  void dispose() {
    super.dispose();
  }
  getRole(role){
    switch(role){
      case "STUDENT":
        return "STUDENT";
      case "TEACHER":
        return "TEACHER";
      case "PARENT":
        return "PARENT";
      case "MANAGER":
        return "MANAGER";
      default:
        return "";

    }

  }
  getTitleDetailSchedule(type){
    switch(type){
      case "STUDENT":
        return "Chi tiết thời khóa biểu";
      case "PARENT":{
        return "Chi tiết thời khóa biểu";
      }
      case "TEACHER":{
        return "Lịch dạy";
      }
      case "MANAGER":{
        return "Chi tiết thời khóa biểu";
      }
    }

  }



}
