import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:task_manager/commom/app_cache.dart';
import 'package:task_manager/commom/utils/color_utils.dart';
import 'package:task_manager/commom/widget/appointment_builder.dart';
import 'package:task_manager/commom/widget/hexColor.dart';
import 'package:task_manager/view/mobile/home/home_controller.dart';
import 'package:task_manager/view/mobile/schedule/schedule_controller.dart';

import '../../../commom/widget/meeting_data_source.dart';
import '../role/manager/manager_home_controller.dart';
import '../role/parent/parent_home_controller.dart';
import '../role/student/student_home_controller.dart';
import '../role/teacher/teacher_home_controller.dart';

class SchedulePage extends GetView<ScheduleController> {
  var controller = Get.find<ScheduleController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${controller.getTitleSchedule(AppCache().userType)}"),
        leading: BackButton(
          color: Colors.white,
          onPressed: () {
            if (AppCache().userType == "MANAGER") {
              Get.back();
              controller.timeTable.value.clear();
              controller.selectedDay.refresh();
            } else {
              Get.find<HomeController>().comeBackHome();
            }
          },
        ),
        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
      ),
      body: Obx(
        () => (controller.isReady.value)
            ? RefreshIndicator(
            color: const Color.fromRGBO(248, 129, 37, 1),
                child: Column(
                  children: [
                    SingleChildScrollView(
                      physics: const AlwaysScrollableScrollPhysics(),
                      child: Container(
                        margin: const EdgeInsets.only(right: 16, left: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: const EdgeInsets.only(top: 16),
                              child: const Text(
                                "Thời gian",
                                style: TextStyle(
                                    color: Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 12),
                              ),
                            ),
                            dateTimeSelectV2(),
                            Container(
                              margin: const EdgeInsets.only(top: 16),
                              child: const Text(
                                "Thời khóa biểu",
                                style: TextStyle(
                                    color: Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 12),
                              ),
                            ),
                            
                          ],
                        ),
                      ),
                    ),
                    Expanded(child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 16.w),
                      child: timelineView(),
                    ))
                  ],
                ),
                onRefresh: () async {
                  await controller.onRefresh(AppCache().userType);
                })
            : const Center(
                child: CircularProgressIndicator(
                  color: ColorUtils.PRIMARY_COLOR,
                ),
              ),
      ),
    );
  }

  Card timelineView() {
    return Card(
      child: SfCalendar(
        headerHeight: 0,
        timeSlotViewSettings: const TimeSlotViewSettings(
          timeFormat: "h:mm aa",
          numberOfDaysInView: 1,
          startHour: 0,
          endHour: 24,
          nonWorkingDays: <int>[DateTime.sunday, DateTime.saturday],
          timeIntervalHeight: 56,
        ),
        controller: controller.timelineController.value,
        onViewChanged: (ViewChangedDetails details) {},
        view: CalendarView.day,
        selectionDecoration: const BoxDecoration(),
        weekNumberStyle: const WeekNumberStyle(
          backgroundColor: ColorUtils.PRIMARY_COLOR,
          textStyle: TextStyle(color: Colors.white, fontSize: 15),
        ),
        viewNavigationMode: ViewNavigationMode.none,
        allowViewNavigation: false,
        showCurrentTimeIndicator: true,
        viewHeaderStyle: const ViewHeaderStyle(
            dateTextStyle: TextStyle(color: Color.fromRGBO(26, 26, 26, 1))),
        dataSource: MeetingDataSource(controller.timeTable.value),
        todayHighlightColor: const Color.fromRGBO(249, 154, 81, 1),
        appointmentBuilder: appointmentBuilder,
      ),
    );
  }

  Card dateTimeSelect() {
    return Card(
      child: TableCalendar(
        firstDay: DateTime(2000),
        focusedDay: controller.focusedDay.value,
        lastDay: DateTime(2030),
        calendarFormat: controller.format.value,
        onFormatChanged: (CalendarFormat _format) {
          controller.format.value = _format;
        },
        startingDayOfWeek: StartingDayOfWeek.monday,
        onDaySelected: (DateTime selectDay, DateTime focusDay) {
          controller.selectedDay.value = selectDay;
          controller.focusedDay.value = focusDay;
          controller.timelineController.value.displayDate = selectDay;
        },
        selectedDayPredicate: (DateTime date) {
          return isSameDay(controller.selectedDay.value, date);
        },
        calendarStyle: const CalendarStyle(
          isTodayHighlighted: true,
          selectedDecoration: BoxDecoration(
              color: Color.fromRGBO(248, 129, 37, 1), shape: BoxShape.circle),
          selectedTextStyle:
              TextStyle(color: Color.fromRGBO(26, 26, 26, 1), fontSize: 14),
          todayDecoration: ShapeDecoration(
              shape: CircleBorder(
                  side: BorderSide(color: Color.fromRGBO(248, 129, 37, 1)))),
          todayTextStyle:
              TextStyle(color: Color.fromRGBO(26, 26, 26, 1), fontSize: 14),
        ),
      ),
    );
  }

  Card dateTimeSelectV2() {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(top: 8.h, left: 8.w),
            child: Text(
              "${controller.selectedDay.value.year}",
              style: TextStyle(
                  color: const Color.fromRGBO(248, 129, 37, 1),
                  fontSize: 13.sp),
            ),
          ),
          Row(
            children: [
              Expanded(
                  child: Container(
                    margin: EdgeInsets.only(top: 4.h, left: 8.w, bottom: 4.h),
                    child: Text(
                      "Tháng ${controller.focusedDay.value.month}",
                      style: TextStyle(color: Colors.black87, fontSize: 14.sp),
                    ),
                  )),
              InkWell(
                child: (controller.weekNumber.value == 1)
                    ? const Icon(
                  Icons.expand_more,
                  size: 24,
                )
                    : const Icon(
                  Icons.expand_less,
                  size: 24,
                ),
                onTap: () {
                  controller.changeShowFull();
                },
              ),
              const SizedBox(
                width: 16,
              )
            ],
          ),
          SizedBox(
            height: controller.heightOffset.value,
            child: SfDateRangePicker(
              viewSpacing: 10,
              todayHighlightColor: const Color.fromRGBO(249, 154, 81, 1),
              controller: controller.weekController.value,
              view: DateRangePickerView.month,
              enablePastDates: true,
              selectionMode: DateRangePickerSelectionMode.single,
              showNavigationArrow: true,
              selectionRadius: 15,
              monthFormat: "MMM",
              headerHeight: 0,
              selectionColor: const Color.fromRGBO(249, 154, 81, 1),
              selectionShape: DateRangePickerSelectionShape.circle,
              selectionTextStyle: const TextStyle(
                  color: ColorUtils.COLOR_WHITE, fontWeight: FontWeight.w700),
              monthCellStyle: const DateRangePickerMonthCellStyle(
                todayTextStyle: TextStyle(
                    color: Color.fromRGBO(249, 154, 81, 1),
                    fontWeight: FontWeight.w700),
              ),
              headerStyle: DateRangePickerHeaderStyle(
                  textAlign: TextAlign.left,
                  textStyle: TextStyle(
                    fontStyle: FontStyle.normal,
                    fontSize: 12.sp,
                    color: Colors.black87,
                  )),
              monthViewSettings: DateRangePickerMonthViewSettings(
                  numberOfWeeksInView: controller.weekNumber.value,
                  firstDayOfWeek: 1,
                  showTrailingAndLeadingDates: true,
                  viewHeaderHeight: 16.h,
                  dayFormat: 'EEE',
                  viewHeaderStyle: DateRangePickerViewHeaderStyle(
                      textStyle: TextStyle(
                          color: HexColor("#B1B1B1"),
                          fontWeight: FontWeight.w600,
                          fontSize: 10.sp))),
              onViewChanged: (DateRangePickerViewChangedArgs args) {
                Future.delayed(const Duration(milliseconds: 400), () {
                  var visibleDates = args.visibleDateRange;
                  controller.focusedDay.value =
                      visibleDates.startDate ?? DateTime.now();
                });
              },
              onSelectionChanged: (DateRangePickerSelectionChangedArgs args) {
                // var dateTime = args.value
                if (args.value is PickerDateRange) {
                  final DateTime rangeStartDate = args.value.startDate;
                  final DateTime rangeEndDate = args.value.endDate;

                  controller.selectedDay.value = rangeStartDate;
                  controller.timelineController.value.displayDate =
                      rangeStartDate;
                } else if (args.value is DateTime) {
                  final DateTime selectedDate = args.value;
                  controller.timelineController.value.displayDate =
                      selectedDate;

                  controller.selectedDay.value = selectedDate;
                } else if (args.value is List<DateTime>) {
                  final List<DateTime> selectedDates = args.value;
                  controller.timelineController.value.displayDate =
                  selectedDates[0];
                  controller.selectedDay.value = selectedDates[0];
                } else {
                  final List<PickerDateRange> selectedRanges = args.value;
                }
                switch(AppCache().userType){
                  case "STUDENT":
                    if(DateTime.now().month<=12&&DateTime.now().month>9){
                      controller.timelineController.value.displayDate = DateTime(Get.find<StudentHomeController>().fromYearPresent.value!,controller.selectedDay.value.month,controller.selectedDay.value.day,DateTime.now().hour-3);
                    }else{
                      controller. timelineController.value.displayDate = DateTime(Get.find<StudentHomeController>().toYearPresent.value!,controller.selectedDay.value.month,controller.selectedDay.value.day,DateTime.now().hour-3);
                    };
                    break;
                  case "PARENT":
                    if(DateTime.now().month<=12&&DateTime.now().month>9){
                      controller.timelineController.value.displayDate = DateTime(Get.find<ParentHomeController>().fromYearPresent.value!,controller.selectedDay.value.month,controller.selectedDay.value.day,DateTime.now().hour-3);
                    }else{
                      controller.timelineController.value.displayDate = DateTime(Get.find<ParentHomeController>().toYearPresent.value!,controller.selectedDay.value.month,controller.selectedDay.value.day,DateTime.now().hour-3);
                    };
                    break;
                  case "TEACHER":
                    if(DateTime.now().month<=12&&DateTime.now().month>9){
                      controller.timelineController.value.displayDate = DateTime(Get.find<TeacherHomeController>().fromYearPresent.value!,controller.selectedDay.value.month,controller.selectedDay.value.day,DateTime.now().hour-3);
                    }else{
                      controller.timelineController.value.displayDate = DateTime(Get.find<TeacherHomeController>().toYearPresent.value!,controller.selectedDay.value.month,controller.selectedDay.value.day,DateTime.now().hour-3);
                    }
                    break;
                  case "MANAGER":
                    if(DateTime.now().month<=12&&DateTime.now().month>9){
                      controller.timelineController.value.displayDate = DateTime(Get.find<ManagerHomeController>().fromYearPresent.value!,controller.selectedDay.value.month,controller.selectedDay.value.day,DateTime.now().hour-3);
                    }else{
                      controller.timelineController.value.displayDate = DateTime(Get.find<ManagerHomeController>().toYearPresent.value!,controller.selectedDay.value.month,controller.selectedDay.value.day,DateTime.now().hour-3);
                    };
                    break;
                }
              },
            ),
          )
        ],
      ),
    );
  }
}
