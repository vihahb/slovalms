import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:task_manager/view/mobile/splash/splash_controller.dart';

class SplashPage extends StatelessWidget {
  final controller = Get.put(SplashController());

  @override
  Widget build(BuildContext context) {
    return Obx(() => SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: true,
          body: controller.isReady.value ? SingleChildScrollView(
            child: Container(
              margin:
              EdgeInsets.only(top: 54.h, left: 24.w, right: 24.w, bottom: 24.h),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: double.infinity,
                    child: Image.asset('assets/images/img_Splat.png'),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 64),
                    height: 90,
                    width: double.infinity,
                    child: const Text(
                      'Đồng Hành Và Phát Triển Giáo Dục',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: 36.5,
                          color: Color.fromRGBO(26, 26, 26, 1),
                          fontFamily: 'assets/font/static/Inter-Medium.ttf',
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 8),
                    height: 35,
                    width: double.infinity,
                    child: const Text(
                      'Hỗ trợ và giảm thiểu thời gian, công sức trong việc quá trình học tập của Học Sinh cho Giáo Viên, Phụ Huynh',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: Color.fromRGBO(133, 133, 133, 1),
                          fontSize: 12.5,
                          fontFamily: 'static/Inter-Regular.ttf'),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 102),
                    width: double.infinity,
                    height: 46,
                    child: ElevatedButton(
                        onPressed: () {
                          controller.pushToLogin();
                        },
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6)),
                            backgroundColor: const Color.fromRGBO(248, 129, 37, 1)),
                        child: const Text(
                          'Đăng Nhập',
                          style: TextStyle(fontSize: 16),
                        )),
                  ),
                ],
              ),
            ),
          ):Center(
              child: CircularProgressIndicator(
                color: Color.fromRGBO(248, 129, 37, 1),
              )),
        )));
  }
}
