import 'package:get/get.dart';
import 'package:task_manager/routes/app_pages.dart';

import '../../../commom/app_cache.dart';
import '../../../commom/utils/app_utils.dart';
import '../../../data/base_service/api_response.dart';
import '../../../data/repository/account/auth_repo.dart';

class SplashController extends GetxController {
  final AuthRepo _loginRepo = AuthRepo();
  var isReady = false.obs;


  @override
  void onInit() async {
    checkLogged();
    super.onInit();
  }

  checkLogged() async {

    String? userName = await AppCache().username ??"";
    String? password = await AppCache().password ??"";
    if (userName != "" && password != "") {
      final fcmKey = AppCache().fcmToken ??"";
      _loginRepo.requestToken(userName, password, true, fcmKey).then((value) {
        if (value.state == Status.SUCCESS) {
          var token = value.object?.accessToken ?? "";
          var userType = value.object?.userType ?? "";
          AppCache().setToken(token);
          AppCache().setUserType(userType);
          AppUtils.shared.showToast("Đăng nhập thành công!");
          pushToHome();
        } else {
          AppUtils.shared.hideLoading();
          AppUtils.shared
              .snackbarError("Đăng nhập thất bại", value.message ?? "");
        }
      });
    }
    if(userName == ""|| password == ""){
      isReady.value = true;
    }

  }




  pushToHome() {
    Get.offAllNamed(Routes.home);
  }

  pushToLogin() {
    // AppCache().userName = userNameController.text;
    Get.toNamed(Routes.login);
  }


}



