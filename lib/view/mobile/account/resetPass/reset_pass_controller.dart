import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:task_manager/commom/utils/app_utils.dart';
import 'package:task_manager/commom/widget/text_field_custom.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/data/repository/account/auth_repo.dart';
import 'package:task_manager/routes/app_pages.dart';

import '../../../../commom/utils/encrypt_aes_password.dart';
import '../../../../commom/utils/global.dart';
import '../../../../data/model/common/encrypt_password.dart';

class ResetPassController extends GetxController {
  final AuthRepo _loginRepo = AuthRepo();

  var controllerPassword = TextEditingController().obs;
  var controllerRePassword = TextEditingController().obs;
  Rx<StateType> statePassword = StateType.DEFAULT.obs;
  Rx<StateType> stateRePassword = StateType.DEFAULT.obs;
  RxBool enableSubmit = true.obs;
  String transId = "";
  var _cryptoSecret = cryptoSecret().obs;

  @override
  void onInit() async {
    super.onInit();
    var tmpTransId = Get.arguments[0];
    if (tmpTransId != null && tmpTransId.isNotEmpty) {
      transId = tmpTransId;
    }
    getSecret();
  }

  getSecret() async {
    _loginRepo.geSecret().then((
        value ) {
      if (value.state == Status.SUCCESS) {
        _cryptoSecret.value = value.object!;
      }
    });
  }

  void submitChangePass() async {
    dismissKeyboard();
    statePassword.value = StateType.DEFAULT;
    if (controllerPassword.value.text == controllerRePassword.value.text &&
        controllerPassword.value.text.length > 7 &&
        controllerRePassword.value.text.length > 7) {
      var newPassword = encryptAESCryptoJS(controllerPassword.value.text,"${_cryptoSecret.value.secret}");
      _changePassResponse(transId, newPassword);
    } else {
      checkValidateNewPassword();
      checkValidateRePassword();
    }
  }
  checkValidateNewPassword(){
    if (controllerPassword.value.text.length > 7){
      if( statePassword.value == StateType.ERROR){
        if(RegExp(r"\s").hasMatch(controllerPassword.value.text)){
          return " (Mật khẩu không hợp lệ)";
        }
        return "(Mật khẩu mới phải khác mật khẩu cũ)";
      }
    } else if(controllerPassword.value.text.isEmpty){
      return "(Mật khẩu không được để trống)";
    }else {
      return "(Mật khẩu có ít nhất 8 kí tự)";
    }
  }

  checkValidateRePassword(){
    if(controllerPassword.value.text.length>7){
      statePassword.value = StateType.DEFAULT;
      if(controllerRePassword.value.text.length<=7&&controllerRePassword.value.text.isNotEmpty){
        stateRePassword.value = StateType.ERROR;
        return " (Mật khẩu có ít nhất 8 kí tự)";
      }else if(controllerRePassword.value.text.isEmpty){
        return " (Mật khẩu không hợp lệ)";}
      else{
        if(controllerPassword.value.text == controllerRePassword.value.text){
          statePassword.value = StateType.SUCCESS;
          stateRePassword.value = StateType.SUCCESS;
        }else{
          stateRePassword.value = StateType.ERROR;
          if(RegExp(r"\s").hasMatch(controllerRePassword.value.text)){
            return " (Mật khẩu không hợp lệ)";
          }
          return " (Mật khẩu không khớp)";
        }
      }
    }else{
      statePassword.value = StateType.ERROR;
      if(controllerRePassword.value.text.length<=7&&controllerRePassword.value.text.isNotEmpty){
        stateRePassword.value = StateType.ERROR;
        return "(Mật khẩu có ít nhất 8 kí tự)";
      }else if(controllerRePassword.value.text.isEmpty){
        return "(Mật khẩu không được để trống)";}
      else{
          stateRePassword.value = StateType.ERROR;
          if(RegExp(r"\s").hasMatch(controllerRePassword.value.text)){
            return "(Mật khẩu không được chứa dấu cách)";
          }
          return "(Mật khẩu không khớp)";
      }
    }
  }

  _changePassResponse(transId, newPass) async {
    _loginRepo.changePassRespone(transId, newPass).then((value) {
      if (value.state == Status.SUCCESS) {
        statePassword.value = StateType.SUCCESS;
        stateRePassword.value = StateType.SUCCESS;
        AppUtils.shared.showToast("Đã thay đổi mật khẩu của bạn.");
        Future.delayed(Duration(seconds: 1), () {
          goToLogin();
        });
      } else {
        statePassword.value = StateType.ERROR;
        stateRePassword.value = StateType.ERROR;
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Đặt lại mật khẩu thất bại", value.message ?? "");
      }
    });
  }

  void goToLogin() {
    Get.toNamed(Routes.login);
  }
}
