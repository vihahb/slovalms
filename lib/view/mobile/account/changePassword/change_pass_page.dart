import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:task_manager/commom/utils/global.dart';
import 'package:task_manager/commom/widget/text_field_custom.dart';
import 'package:task_manager/view/mobile/account/changePassword/change_pass_controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class ChangePassPage extends GetWidget<ChangePassController> {
  @override
  final contronller = Get.put(ChangePassController());
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        dismissKeyboard();
      },
      child:
      SafeArea(
        child:
        Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              leading: IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: const Icon(
                    Icons.arrow_back_outlined,
                    color: Colors.black,
                  )),
            ),
            body: SingleChildScrollView(
              reverse: true,
              physics: const BouncingScrollPhysics(),
              child:     Obx(() => Center(
                  child: Container(
                    margin: const EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Column(
                          children: [
                            Container(
                              alignment: Alignment.center,
                              width: 300,
                              height: 44,
                              child: const Text(
                                'Đổi Mật Khẩu',
                                style: TextStyle(
                                    fontSize: 32,
                                    color: Color.fromRGBO(26, 26, 26, 1),
                                    fontFamily: 'static/Inter-Medium.ttf',
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                            Container(
                                margin: EdgeInsets.only(top: 8.h),
                                width: 280,
                                height: 15,
                                child: const Text(
                                  'Mật khẩu mới phải khác so với mật khẩu cũ',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Color.fromRGBO(133, 133, 133, 1),
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      fontFamily: 'static/Inter-Regular.ttf'),
                                ))
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 32.h),
                          child: Column(
                            children: [
                              LabelOutSideTextFormField(
                                state: contronller.stateInputOldPass.value,
                                hintText: "Nhập Mật Khẩu Cũ",
                                controller: contronller.controllerOldPassword,
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                autofocus: false,
                                validation: (textToValidate) {
                                  // return getTempIFSCValidation(textToValidate);
                                },
                                enable: true,
                                showHelperText: true,
                                iconPrefix: "assets/images/icon_password.png",
                                iconSuffix:
                                getIconSuffix(contronller.stateInputOldPass.value),
                                helperText: "",
                                obscureText: true,
                                errorText: contronller.checvalidateOldpass()??"",
                                labelText: "Mật Khẩu Cũ",
                                showIconHideShow: true,
                              ),
                            ],
                          ),
                        ),
                        Column(
                          children: [
                            LabelOutSideTextFormField(
                              state: contronller.stateInputNewpass.value,
                              hintText: "Nhập Mật Khẩu Mới",
                              controller: contronller.controllerNewPassword,
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                              autofocus: false,
                              validation: (textToValidate) {
                                // return getTempIFSCValidation(textToValidate);
                              },
                              enable: true,
                              showHelperText: true,
                              iconPrefix: "assets/images/icon_password.png",
                              iconSuffix:
                              getIconSuffix(contronller.stateInputNewpass.value),
                              helperText: "",
                              obscureText: true,
                              errorText: contronller.checkvalidateNewPass(),
                              labelText: "Mật Khẩu Mới",
                              showIconHideShow: true,
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            LabelOutSideTextFormField(
                              state: contronller.stateInputRepass.value,
                              hintText: "Nhập Lại Mật Khẩu",
                              controller: contronller.controllerRePassword,
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                              autofocus: false,
                              validation: (textToValidate) {
                                // return getTempIFSCValidation(textToValidate);
                              },
                              enable: true,
                              showHelperText: true,
                              iconPrefix: "assets/images/icon_password.png",
                              iconSuffix:
                              getIconSuffix(contronller.stateInputRepass.value),
                              helperText: "Mật khẩu có ít nhất 8 kí tự",
                              obscureText: true,
                              errorText: contronller.checkvalidateRepass() ?? "Mật khẩu không hợp lệ",
                              labelText: "Nhập Lại Mật Khẩu",
                              showIconHideShow: true,
                            ),
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(top: 95.h)),
                        SizedBox(
                          width: 360,
                          height: 46,
                          child: ElevatedButton(
                              onPressed: (contronller.enableSubmit.value)
                                  ? () {
                                contronller.submitUpdatePass();
                              }
                                  : null,
                              style: ElevatedButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(6)),
                                  backgroundColor: const Color.fromRGBO(248, 129, 37, 1)),
                              child: const Text(
                                'Xác Nhận',
                                style: TextStyle(fontSize: 16),
                              )),
                        ),
                      ],
                    ),
                  ))),
            )
        )
      ),
    );
  }
}
