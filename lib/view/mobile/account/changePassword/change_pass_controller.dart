import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:task_manager/commom/utils/app_utils.dart';
import 'package:task_manager/commom/widget/text_field_custom.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/data/repository/account/auth_repo.dart';
import 'package:task_manager/routes/app_pages.dart';
import 'package:encrypt/encrypt.dart';
import 'package:encrypt/encrypt.dart' as encrypt;
import '../../../../commom/utils/encrypt_aes_password.dart';
import '../../../../commom/utils/global.dart';
import 'package:flutter/material.dart' hide Key;

import '../../../../data/model/common/encrypt_password.dart';

class ChangePassController extends GetxController {
  final AuthRepo _loginRepo = AuthRepo();

  TextEditingController controllerOldPassword = TextEditingController();
  TextEditingController controllerNewPassword = TextEditingController();
  TextEditingController controllerRePassword = TextEditingController();
  Rx<StateType> stateInputOldPass = StateType.DEFAULT.obs;
  Rx<StateType> stateInputNewpass = StateType.DEFAULT.obs;
  Rx<StateType> stateInputRepass = StateType.DEFAULT.obs;
  RxBool enableSubmit = true.obs;
  String userId = "";
  final _cryptoSecret = cryptoSecret().obs;
  @override
  void onInit() {
    // TODO: implement onInit
    var tmpusedId = Get.arguments;
    if (tmpusedId != null && tmpusedId.isNotEmpty) {
      userId = tmpusedId;
    }
    getSecret();
  }



  getSecret() async {

    _loginRepo.geSecret().then((
        value ) {
      if (value.state == Status.SUCCESS) {
        _cryptoSecret.value = value.object!;
      }
    });
  }




  void submitUpdatePass() async {
    dismissKeyboard();
    stateInputOldPass.value = StateType.DEFAULT;
    stateInputNewpass.value = StateType.DEFAULT;
    stateInputRepass.value = StateType.DEFAULT;
    if (controllerNewPassword.text == controllerRePassword.text &&
        controllerNewPassword.text != controllerOldPassword.text &&
        controllerNewPassword.text.length > 7 &&
        controllerRePassword.text.length > 7 &&
        controllerOldPassword.text.length > 7) {
      stateInputRepass.value = StateType.SUCCESS;
      stateInputNewpass.value = StateType.SUCCESS;
      stateInputOldPass.value = StateType.SUCCESS;
      var oldPassword = encryptAESCryptoJS("${controllerOldPassword.text}","${_cryptoSecret.value.secret}");
      var newPassword = encryptAESCryptoJS("${controllerNewPassword.text}","${_cryptoSecret.value.secret}");

      _changePassResponse(
          oldPassword, newPassword, userId);
    } else {
      checvalidateOldpass();
      checkvalidateNewPass();
      checkvalidateRepass();
    }



  }
  checvalidateOldpass() {
    if (controllerOldPassword.value.text.length > 7 ) {
      stateInputOldPass.value = StateType.SUCCESS;
    } else if (controllerOldPassword.value.text.isEmpty) {
      return "(Mật khẩu không được để trống)";
    } else if(controllerOldPassword.value.text.isNotEmpty){
      return "(Mật khẩu phải có ít nhất 8 ký tự)";
    }
    else{
      return "(Mật Khẩu Cũ Không Đúng)";
    }
  }

  checkvalidateNewPass() {
    if (controllerOldPassword.value.text.length > 7) {
      stateInputOldPass.value = StateType.SUCCESS;
      if(controllerNewPassword.value.text.length <=7 && controllerNewPassword.value.text.isNotEmpty){
        stateInputNewpass.value = StateType.ERROR;
        return"(Mật khẩu phải có ít nhất 8 ký tự)";
      }else if(controllerNewPassword.value.text.isEmpty){
        return "(Mật khẩu không được để trống)";
      } else if(controllerNewPassword.value.text.contains(" ")){
        return "(Mật khẩu không được có khoảng trắng)";
      }else {
        // trường hợp khi nhập 2 trường mkc và mkm và ấn submit
        if(controllerNewPassword.value.text != controllerOldPassword.value.text){
          stateInputOldPass.value = StateType.SUCCESS;
          stateInputNewpass.value = StateType.SUCCESS;
        if(controllerRePassword.value.text == controllerNewPassword.value.text){
          stateInputRepass.value = StateType.SUCCESS;
        }else{
          stateInputRepass.value = StateType.ERROR;
        }
        if(controllerNewPassword.value.text.contains(" ")){
          stateInputNewpass.value = StateType.ERROR;
        }
        }else {
          stateInputNewpass.value = StateType.ERROR;
          stateInputRepass.value = StateType.ERROR;
          return "(Mật khẩu mới phải khác mật khẩu cũ)";
        }
      }
    } else {
      if(controllerNewPassword.value.text.length <=7 && controllerNewPassword.value.text.isNotEmpty){
        stateInputNewpass.value = StateType.ERROR;
        stateInputOldPass.value=StateType.ERROR;
        return"(Mật khẩu phải có ít nhất 8 ký tự)";
      }else if(controllerNewPassword.value.text.isEmpty){
        return "(Mật khẩu không được để trống)";
      } else if(controllerNewPassword.value.text.contains(" ")) {
        return "(Mật khẩu không được có khoảng trắng)";
      }else {
          stateInputNewpass.value = StateType.ERROR;
          return "(Mật khẩu mới phải khác mật khẩu cũ)";
      }
    }
  }
  checkvalidateRepass(){
    if(controllerNewPassword.value.text.length>7 && controllerOldPassword.value.text.length >7){
      if(controllerRePassword.value.text.length <= 7 && controllerRePassword.value.text.isNotEmpty){
        stateInputRepass.value = StateType.ERROR;
        return"(Mật khẩu phải có ít nhất 8 ký tự)";
      }else if(controllerRePassword.value.text.isEmpty){
        return "(Mật khẩu không được để trống)";
      }else if(controllerRePassword.value.text == " ") {
        return "(Mật khẩu không được có khoảng trắng)";
      }else {
        if(controllerRePassword.value.text == controllerNewPassword.value.text && controllerNewPassword.value.text != controllerOldPassword.value.text){
          if(controllerNewPassword.value.text.contains(" ")){
            stateInputNewpass.value = StateType.ERROR;
            stateInputRepass.value = StateType.ERROR;
          }else{
            stateInputNewpass.value = StateType.SUCCESS;
            stateInputRepass.value = StateType.SUCCESS;
          }

          stateInputOldPass.value = StateType.SUCCESS;
        }else {
          if(controllerNewPassword.value.text == controllerOldPassword.value.text || controllerRePassword.value.text.length <= 7){
            stateInputRepass.value = StateType.ERROR;
            return "(Mật khẩu không hợp lệ)";
          } else {
            stateInputRepass.value = StateType.ERROR;
            return "(Mật khẩu không khớp)";
          }

        }
      }
    } else {
      stateInputNewpass.value = StateType.ERROR;
      stateInputRepass.value = StateType.ERROR;
      if(controllerOldPassword.value.text.length >7){
        stateInputOldPass.value =StateType.SUCCESS;
      }else{
        stateInputOldPass.value= StateType.ERROR;
      }
      if(controllerRePassword.value.text.length <= 7 && controllerRePassword.value.text.isNotEmpty){
        stateInputRepass.value = StateType.ERROR;
        return"(Mật khẩu phải có ít nhất 8 ký tự)";
      }else if(controllerRePassword.value.text.isEmpty){
        return "(Mật khẩu không được để trống)";
      }else if(controllerRePassword.value.text == " ") {
        return "(Mật khẩu không được có khoảng trắng)";
      }else {
          stateInputRepass.value = StateType.ERROR;
          return "(Mật khẩu không khớp)";
      }
    }
  }


  _changePassResponse(oldPassword, newPassword, userId) async {

    _loginRepo.UpdatePassResponse(oldPassword, newPassword, userId).then((
        value ) {
      if (value.state == Status.SUCCESS) {
         AppUtils.shared.showToast("Cập nhật mật khẩu thành công");
         Future.delayed(Duration(seconds: 1), () {
           goToLogin();
         });
      } else {

        AppUtils.shared.hideLoading();
        stateInputOldPass.value= StateType.ERROR;
        AppUtils.shared.snackbarError(
            "Cập nhật mật khẩu thất bại", value.message == "SERVER_ERROR"?"Mật khẩu cũ không chính xác":value.message);
      }
    });
  }


  void goToLogin() {
    Get.toNamed(Routes.login);
  }
}
