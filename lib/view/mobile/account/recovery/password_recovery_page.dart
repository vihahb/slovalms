import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:task_manager/commom/utils/global.dart';
import 'package:task_manager/commom/widget/text_field_custom.dart';
import 'package:task_manager/view/mobile/account/recovery/password_recovery_controller.dart';

class PaswordRecoveryPage extends StatelessWidget {
  var controller = Get.put(PaswordRecoveryController());

  PaswordRecoveryPage({super.key});

  void onPressSubmit() {
    controller.submitOTP();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        dismissKeyboard();
      },
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: const Icon(
                  Icons.arrow_back_outlined,
                  color: Colors.black,
                )),
          ),
          body: Obx(() => Center(
                  child: Container(
                margin: const EdgeInsets.all(10),
                child: Column(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                            height: 44,
                            margin: const EdgeInsets.only(right: 16, left: 16),
                            child: const Text(
                              'Lấy Lại Mật Khẩu',
                              style: TextStyle(
                                fontSize: 36,
                                color: Color.fromRGBO(26, 26, 26, 1),
                                fontFamily: 'static/Inter-Medium.ttf',
                                fontWeight: FontWeight.w500,
                              ),
                            )),
                        Container(
                            padding: const EdgeInsets.only(left: 48, right: 48),
                            margin: const EdgeInsets.only(
                                top: 8, right: 16, left: 16),
                            child: const Text(
                              'Nhập Email hoặc số điện thoại để Slova giúp bạn lấy lại mật khẩu nhé',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Color.fromRGBO(133, 133, 133, 1),
                                  fontSize: 12,
                                  fontFamily: 'static/Inter-Regular.ttf'),
                            )),
                        Container(
                          margin: const EdgeInsets.only(top: 48),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              LabelOutSideTextFormField(
                                state: controller.stateInputUser.value,
                                hintText: "Nhập Email hoặc số điện thoại",
                                controller: controller.controllerUserName,
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                autofocus: false,
                                validation: (textToValidate) {},
                                showHelperText: true,
                                iconPrefix: "assets/images/icon_username.png",
                                iconSuffix: getIconSuffix(
                                    controller.stateInputUser.value),
                                helperText: "",
                                obscureText: false,
                                errorText: "(Email không khả dụng)",
                                labelText: "Email/ số điện thoại đăng ký",
                                showIconHideShow: false,
                                enable: true,
                              ),
                              Container(
                                width: 500,
                                margin:
                                    const EdgeInsets.only(left: 16, right: 16),
                                height: 46,
                                child: ElevatedButton(
                                    onPressed: controller.enableSubmit.value
                                        ? onPressSubmit
                                        : null,
                                    style: ElevatedButton.styleFrom(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(6)),
                                        backgroundColor: const Color.fromRGBO(
                                            248, 129, 37, 1)),
                                    child: const Text(
                                      'Gửi Mã',
                                      style: TextStyle(fontSize: 16),
                                    )),
                              ),
                              Container(
                                  alignment: Alignment.center,
                                  margin: const EdgeInsets.only(
                                      top: 22, left: 16, right: 16),
                                  child: RichText(
                                    text: const TextSpan(children: [
                                      TextSpan(
                                          text: 'Slova sẽ gửi đến ',
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  177, 177, 177, 1),
                                              fontFamily:
                                                  'static/Inter-Regular.ttf',
                                              fontSize: 12)),
                                      TextSpan(
                                          text: 'Email/số điện thoại ',
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  248, 129, 37, 1),
                                              fontFamily:
                                                  'static/Inter-Regular.ttf',
                                              fontSize: 12)),
                                      TextSpan(
                                          text:
                                              'của bạn 1 đoạn mã để đặt lại mật khẩu',
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  177, 177, 177, 1),
                                              fontFamily:
                                                  'static/Inter-Regular.ttf',
                                              fontSize: 12)),
                                    ]),
                                  )),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Expanded(child: Container())
                  ],
                ),
              ))),
        ),
      ),
    );
  }
}
