import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:task_manager/commom/utils/app_utils.dart';
import 'package:task_manager/commom/utils/global.dart';
import 'package:task_manager/commom/utils/validate_utils.dart';
import 'package:task_manager/commom/widget/text_field_custom.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/data/repository/account/auth_repo.dart';
import 'package:task_manager/routes/app_pages.dart';

class PaswordRecoveryController extends GetxController {
  final AuthRepo _loginRepo = AuthRepo();

  TextEditingController controllerUserName = TextEditingController();
  Rx<StateType> stateInputUser = StateType.DEFAULT.obs;
  RxBool enableSubmit = false.obs;

  @override
  void onInit() async {
    super.onInit();
    controllerUserName.addListener(() {
      if (!ValidateUtils.isValidUsername(controllerUserName.text.trim())) {
        stateInputUser.value = StateType.DEFAULT;
        enableSubmit.value = false;
      } else {
        stateInputUser.value = StateType.SUCCESS;
        enableSubmit.value = true;
      }
    });
  }

  void submitOTP() async {
    dismissKeyboard();
    stateInputUser.value = StateType.DEFAULT;
    if (controllerUserName.text.trim().isEmpty) {
      stateInputUser.value = StateType.ERROR;
      return;
    }
    requestOTP(controllerUserName.text);
  }

  requestOTP(username) async {
    _loginRepo.requestOTP(username).then((value) {
      if (value.state == Status.SUCCESS) {
        var transId = value.object?.transId ?? "";
        stateInputUser.value = StateType.SUCCESS;
        AppUtils.shared.showToast("Đã gửi mã OTP");
        toConfirmCode(transId);
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError(
            "Email hoặc số điện thoại không hợp lệ", value.message ?? "");
      }
    });
  }

  void toConfirmCode(String transId) {
    print("Receive TransID: $transId");
    Get.toNamed(Routes.retrievingCode,
        arguments: [controllerUserName.text, transId]);
  }
}
