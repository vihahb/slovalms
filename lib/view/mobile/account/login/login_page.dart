import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:task_manager/commom/utils/global.dart';
import 'package:task_manager/commom/widget/LabeledCheckbox.dart';
import 'package:task_manager/commom/widget/text_field_custom.dart';
import 'package:task_manager/view/mobile/account/login/login_controller.dart';

import '../../../../routes/app_pages.dart';
import '../../splash/splash_controller.dart';
import '../../splash/splash_page.dart';

class LoginPage extends StatelessWidget {
  final controller = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    Size size = Get.size;
    return WillPopScope(
        child: GestureDetector(
          onTap: () {
            dismissKeyboard();
          },
          child: SafeArea(
            child: Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.transparent,
                elevation: 0,
                leading: IconButton(
                    onPressed: () {
                      Get.find<SplashController>().onInit();
                      Get.toNamed(Routes.splash);
                    },
                    icon: const Icon(
                      Icons.arrow_back_outlined,
                      color: Colors.black,
                    )),
              ),
              body: Obx(() => Container(
                    margin: const EdgeInsets.all(10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              alignment: Alignment.center,
                              height: 44,
                              child: const Text(
                                'Đăng Nhập',
                                style: TextStyle(
                                    fontSize: 32,
                                    color: Color.fromRGBO(26, 26, 26, 1),
                                    fontFamily: 'static/Inter-Medium.ttf',
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                            Container(
                                alignment: Alignment.center,
                                margin: const EdgeInsets.only(top: 8),
                                height: 15,
                                child: const Text(
                                  'Vui lòng nhập tài khoản và mật khẩu ở bên dưới',
                                  style: TextStyle(
                                      color: Color.fromRGBO(133, 133, 133, 1),
                                      fontSize: 12,
                                      fontFamily: 'static/Inter-Regular.ttf',
                                      fontWeight: FontWeight.w400),
                                )),
                            Container(
                              margin: const EdgeInsets.only(
                                  top: 32, left: 0, right: 0, bottom: 0),
                              child: LabelOutSideTextFormField(
                                state: controller.stateInputUser.value,
                                hintText: "Nhập Email hoặc số điện thoại",
                                controller: controller.controllerUserName,
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                autofocus: false,
                                validation: (textToValidate) {
                                  // return getTempIFSCValidation(textToValidate);
                                },
                                enable: true,
                                showHelperText: true,
                                iconPrefix: "assets/images/icon_username.png",
                                iconSuffix: getIconSuffix(
                                    controller.stateInputUser.value),
                                helperText: "",
                                obscureText: false,
                                errorText: controller.submitErrorUserName(),
                                labelText: "Email hoặc số điện thoại ",
                                showIconHideShow: false,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.zero,
                              child: Column(
                                children: [
                                  LabelOutSideTextFormField(
                                    state: controller.stateInputPassword.value,
                                    hintText: "Nhập mật khẩu",
                                    controller: controller.controllerPassword,
                                    keyboardType: TextInputType.text,
                                    textInputAction: TextInputAction.next,
                                    autofocus: false,
                                    validation: (textToValidate) {
                                      // return getTempIFSCValidation(textToValidate);
                                    },
                                    enable: true,
                                    showHelperText: true,
                                    iconPrefix:
                                        "assets/images/icon_password.png",
                                    iconSuffix: getIconSuffix(
                                        controller.stateInputPassword.value),
                                    helperText: "Mật khẩu có ít nhất 8 kí tự",
                                    obscureText: controller.obscureText.value,
                                    errorText: controller.submitErrorPassword(),
                                    labelText: "Mật khẩu ",
                                    showIconHideShow: true,
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin:
                                  const EdgeInsets.only(left: 10, right: 10),
                              child: Row(
                                children: [
                                  Expanded(
                                      child: LabeledCheckbox(
                                    activeColor:
                                        const Color.fromRGBO(248, 129, 37, 1),
                                    value: controller.ischecked.value,
                                    onTap: (value) {
                                      if (value != null) {
                                        controller.ischecked.value = value;
                                      }
                                    },
                                    label: 'Nhớ mật khẩu',
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 2.w),
                                    fontSize: 14.sp,
                                  )),
                                  TextButton(
                                      onPressed: () {
                                        controller.toRecoveryPassword();
                                      },
                                      child: const Text(
                                        'Quên mật khẩu?',
                                        style: TextStyle(
                                            fontSize: 12,
                                            color:
                                                Color.fromRGBO(248, 129, 37, 1),
                                            fontFamily:
                                                'static/Inter-Regular.ttf',
                                            fontWeight: FontWeight.w400),
                                      ))
                                ],
                              ),
                            )
                          ],
                        ),
                        Flexible(
                            child: Center(
                          child: SizedBox(
                            width: Get.size.width - 48.w,
                            height: 46,
                            child: ElevatedButton(
                                onPressed: () {
                                  controller.submitLogin();
                                  print("login");
                                },
                                style: ElevatedButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(6)),
                                    backgroundColor:
                                        const Color.fromRGBO(248, 129, 37, 1)),
                                child: const Text(
                                  'Đăng Nhập',
                                  style: TextStyle(fontSize: 16),
                                )),
                          ),
                        )),
                      ],
                    ),
                  )),
            ),
          ),
        ),
        onWillPop: () async {
          Get.to(SplashPage());
          return false;
        });
  }
}
