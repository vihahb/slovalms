import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:task_manager/commom/app_cache.dart';
import 'package:task_manager/commom/utils/app_utils.dart';
import 'package:task_manager/commom/utils/global.dart';
import 'package:task_manager/commom/utils/validate_utils.dart';
import 'package:task_manager/commom/widget/text_field_custom.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/data/repository/account/auth_repo.dart';
import 'package:task_manager/routes/app_pages.dart';

class LoginController extends GetxController {
  final AuthRepo _loginRepo = AuthRepo();

  TextEditingController controllerUserName = TextEditingController();
  TextEditingController controllerPassword = TextEditingController();
  RxBool obscureText = true.obs;

  Rx<StateType> stateInputUser = StateType.DEFAULT.obs;
  Rx<StateType> stateInputPassword = StateType.DEFAULT.obs;
  RxBool ischecked = false.obs;

  String language = 'vi';
  var supportState = (_SupportState.unknown).obs;

  @override
  void onInit() async {
    getInfoLoginSaved();
    super.onInit();
  }

  void toggle() {
    obscureText.value = !obscureText.value;
    update();
  }

  getInfoLoginSaved() async {
    ischecked.value = AppCache().isSaveInfoLogin;
    if (ischecked.value) {
      String? usernameSaved = await AppCache().username;
      String? passwordSaved = await AppCache().password;
      controllerUserName.text = usernameSaved ?? "";
      controllerPassword.text = passwordSaved ?? "";
    }else{
      controllerUserName.text = "";
      controllerPassword.text = "";
    }
  }

  void saveInfoLogin() {
    if (ischecked.value) {
      AppCache().saveInfoLogin(ischecked.value);
      AppCache().saveLogin(controllerUserName.text, controllerPassword.text);
    } else {
      if (!AppCache().isSaveLoginAuthen) {

      }
      AppCache().saveInfoLogin(ischecked.value);
    }
  }


   submitErrorUserName(){
    if(controllerUserName.value.text.length == 0){
      return "(Vui lòng nhập Email/SĐT)";
    }else{
      return "(Sai Email/ Số điện thoại)";
    }

  }
  submitErrorPassword(){
    if(controllerPassword.value.text.length ==0){
      return "(Vui lòng nhập mật khẩu)";
    }else{
      return "(Mật khẩu không hợp lệ)";
    };
  }

  void submitLogin() {
    dismissKeyboard();
    stateInputUser.value = StateType.DEFAULT;
    stateInputPassword.value = StateType.DEFAULT;

    if (controllerUserName.text.trim().isEmpty) {
      //User name empty
      stateInputUser.value = StateType.ERROR;
      return;
    }
    if (!ValidateUtils.isValidUsername(controllerUserName.text.trim())) {
      //User name empty
      stateInputUser.value = StateType.ERROR;
      return;
    } else {
      stateInputUser.value = StateType.DEFAULT;
    }
    if (controllerPassword.text.trim().isEmpty) {
      //Password empty
      stateInputPassword.value = StateType.ERROR;
      return;
    }
    if (ValidateUtils.isPassword(controllerPassword.text)) {
      //Not valid password
      stateInputPassword.value = StateType.ERROR;
      return;
    } else {
      stateInputPassword.value = StateType.DEFAULT;
    }
    _requestToken(
        controllerUserName.text, controllerPassword.text, ischecked.value);
    AppCache().saveLogin(controllerUserName.text, controllerPassword.text);
  }

  _requestToken(username, password, isCheck) async {
    final fcmKey = AppCache().fcmToken ??"";
    _loginRepo.requestToken(username, password, isCheck, fcmKey).then((value) {
      if (value.state == Status.SUCCESS) {
        var token = value.object?.accessToken ?? "";
        var userType = value.object?.userType ?? "";
        AppCache().setToken(token);
        AppCache().setUserType(userType);
        stateInputUser.value = StateType.SUCCESS;
        stateInputPassword.value = StateType.SUCCESS;
        AppUtils.shared.showToast("Đăng nhập thành công!");
        saveInfoLogin();
        pushToHome();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared
            .snackbarError("Đăng nhập thất bại", value.message ?? "");
      }
    });

  }

  pushToHome() {
    Get.offAllNamed(Routes.home);
  }

  void toRecoveryPassword() {
    Get.toNamed(Routes.recoveryPass);
  }
}

enum _SupportState {
  unknown,
  supported,
  unsupported,
}
