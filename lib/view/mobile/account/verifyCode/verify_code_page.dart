import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:task_manager/view/mobile/account/verifyCode/verify_code_controller.dart';

import '../recovery/password_recovery_controller.dart';

class VerifyCodePage extends StatelessWidget {
 VerifyCodePage({super.key});
  final controller = Get.put(VerifyCodeController());



  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: const Icon(
                Icons.arrow_back_outlined,
                color: Colors.black,
              )),
        ),
        body: Obx(() => Center(
              child: (controller.ready.value)
                  ? Container(
                      margin: const EdgeInsets.symmetric(
                          vertical: 16, horizontal: 24),
                      child: Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(top: 25),
                            child: Column(
                              children: [
                                Container(
                                  alignment: Alignment.center,
                                  width: double.infinity,
                                  height: 88,
                                  child: const Text(
                                    'Vui Lòng Kiểm Tra Email',
                                    style: TextStyle(
                                        fontSize: 36,
                                        color: Color.fromRGBO(26, 26, 26, 1),
                                        fontFamily: 'static/Inter-Medium.ttf',
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                                Container(
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.only(top: 8),
                                    width: double.infinity,
                                    height: 45.h,
                                    child: Text(
                                      'Slova đã gửi e-mail xác nhận và đặt lại mật khẩu tới ${controller.userName}. Vui lòng kiểm tra Hộp thư đến hoặc Hộp thư Spam.',
                                      style: const TextStyle(
                                          color:
                                              Color.fromRGBO(133, 133, 133, 1),
                                          fontSize: 12,
                                          fontFamily:
                                              'static/Inter-Regular.ttf'),
                                    ))
                              ],
                            ),
                          ),
                          Container(
                              margin:
                                  const EdgeInsets.only(top: 30, bottom: 30),
                              width: 300,
                              child: Form(
                                child: controller.ShowPinPut()
                                // FocusScope(
                                //     autofocus: true,
                                //     node: controller.node,
                                //     child: Row(
                                //       mainAxisAlignment:
                                //           MainAxisAlignment.spaceBetween,
                                //       children: controller.getPinView(),
                                //     )),
                              )),
                          SizedBox(
                            width: double.infinity,
                            height: 46,
                            child: ElevatedButton(
                                onPressed: (controller.enableSubmit.value)
                                    ? () {
                                        //To reset password
                                        controller.confirmOTP();
                                      }
                                    : null,
                                style: ElevatedButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(6)),
                                    backgroundColor:
                                        const Color.fromRGBO(248, 129, 37, 1)),
                                child: const Text(
                                  'Nhập Mã Xác Nhận',
                                  style: TextStyle(fontSize: 16),
                                )),
                          ),
                          Container(
                            width: double.infinity,
                            margin: const EdgeInsets.only(top: 22),
                            alignment: Alignment.centerLeft,
                            child: Row(
                              children: [
                                const Text(
                                    'Bạn chưa nhận được Email?',
                                    style: TextStyle(
                                        color: Color.fromRGBO(177, 177, 177, 1),
                                        fontSize: 12,
                                        fontFamily:
                                            'static/Inter-Regular.ttf')),
                                InkWell(
                                  onTap: () {
                                    Get.find<PaswordRecoveryController>().requestOTP(controller.userName.value);
                                  },
                                  child: const Text(
                                   ' Gửi lại mã',
                                    style: TextStyle(
                                        color: Color.fromRGBO(248, 129, 37, 1),
                                        fontSize: 12,
                                        fontFamily:
                                        'static/Inter-Regular.ttf'),
                                )),
                              ]),
                            ),
                          Expanded(flex: 1, child: Container()),
                        ],
                      ),
                    )
                  : Container(),
            )),
      ),
    );
  }
}
