import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:task_manager/data/model/common/custom_icons.dart';
import 'package:task_manager/view/mobile/message/message_controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'message_content/message_content_page.dart';
class MessagePage extends GetView<MessageController> {
  TabController? _tabController;

  MessagePage({super.key});

  @override
  Widget build(BuildContext context) {
    var controller = Get.put(MessageController());
    return Obx(() => DefaultTabController(
        length: 2,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
            title: Text(
              "Tin Nhắn",
              style: TextStyle(color: Colors.white, fontSize: 16.sp),
            ),
            actions: [
              IconButton(
                icon: Image.asset("assets/images/icon_edit_message.png"),
                onPressed: () {
                  // Get.to(MySendNewMessage());
                },
              )
            ],
          ),
          body: GestureDetector(
            onTap: () {
              FocusScope.of(context).unfocus();
            },
            child: Column(
              children: [
                Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6)),
                    color: const Color.fromRGBO(246, 246, 246, 1),
                    margin: EdgeInsets.only(top: 16.h, left: 16.w, right: 16.w),
                    child: TextFormField(
                      onTap: () {
                        // Get.to(MySearchMessage(),arguments: allMessage.listAllMessage);
                        // SearchFocus.unfocus();
                        // print(allMessage.listAllMessage.length);
                      },
                      // focusNode: SearchFocus,
                      decoration: InputDecoration(
                        contentPadding:  EdgeInsets.symmetric(
                            vertical: 7.h, horizontal: 16.w),
                        hintText: "Tìm tin nhắn...",
                        hintStyle: TextStyle(
                            fontSize: 14.sp,
                            color: const Color.fromRGBO(177, 177, 177, 1)),
                        prefixIcon: const Icon(
                          Icons.search,
                          color: Color.fromRGBO(177, 177, 177, 1),
                        ),
                        border: InputBorder.none,
                      ),
                      textAlignVertical: TextAlignVertical.center,
                      cursorColor: const Color.fromRGBO(248, 129, 37, 1),
                    )),
                TabBar(
                    physics: const NeverScrollableScrollPhysics(),
                    indicatorColor: const Color.fromRGBO(248, 129, 37, 1),
                    indicatorWeight: 2,
                    controller: _tabController,
                    labelColor: Colors.black,
                    unselectedLabelColor: Colors.grey,
                    onTap: (index) {
                      controller.selectedPageIndex.value = index;
                    },
                    tabs: [
                      Container(
                        height: 60,
                        padding: EdgeInsets.only(top: 20.h),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Tất cả  ",
                              style: TextStyle(
                                  color:
                                      controller.selectedPageIndex.value == 0
                                          ? const Color.fromRGBO(248, 129, 37, 1)
                                          : const Color.fromRGBO(26, 26, 26, 1),
                                  fontSize: 14),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(16),
                                  color: controller.selectedPageIndex.value == 0
                                      ? const Color.fromRGBO(248, 129, 37, 1)
                                      : const Color.fromRGBO(177, 177, 177, 1)),
                              padding: EdgeInsets.fromLTRB(8.w, 4.h, 8.w, 4.h),
                              child: Text(
                                "02",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 10.sp),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 20.h),
                        height: 60.h,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Chưa đọc  ",
                              style: TextStyle(
                                  color:
                                      controller.selectedPageIndex.value == 1
                                          ? const Color.fromRGBO(248, 129, 37, 1)
                                          : const Color.fromRGBO(26, 26, 26, 1),
                                  fontSize: 14),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(16),
                                  color:
                                      controller.selectedPageIndex.value == 1
                                          ? const Color.fromRGBO(248, 129, 37, 1)
                                          : const Color.fromRGBO(177, 177, 177, 1)),
                              padding: EdgeInsets.fromLTRB(8.w, 4.h, 8.w, 4.h),
                              child: Text(
                                "03",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 10.sp),
                              ),
                            )
                          ],
                        ),
                      ),
                    ]),
                Expanded(
                  child: IndexedStack(
                    index: controller.selectedPageIndex.value,
                    children: <Widget>[
                      Visibility(
                        maintainState: true,
                        visible: controller.selectedPageIndex.value == 0,
                        child: const AllMessage(),
                      ),
                      Visibility(
                        maintainState: true,
                        visible: controller.selectedPageIndex.value == 1,
                        child: const SizedBox(
                          child: Center(
                            child: Unread(),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        )));
  }
}

enum Actions { more, delete, notification }

class AllMessage extends GetView<MessageController> {
  const AllMessage({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: controller.listAllMessage.value.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              controller.message = controller.listAllMessage[index];
              Get.to(MessageContent(),arguments: controller.message);
              print("avatar: ${controller.message.avatar}");
            },
            child: Slidable(
                endActionPane: ActionPane(
                  motion: const ScrollMotion(),
                  children: [
                    SlidableAction(
                      // An action can be bigger than the others.
                      flex: 1,
                      onPressed: (context) =>
                          _onClickSlidable(index, Actions.more),
                      backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
                      foregroundColor: Colors.white,
                      icon: CustomIcons.more,
                    ),
                    SlidableAction(
                      flex: 1,
                      onPressed: (context) =>
                          _onClickSlidable(index, Actions.notification),
                      backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
                      foregroundColor: Colors.white,
                      icon: CustomIcons.notifications,
                    ),
                    SlidableAction(
                      flex: 1,
                      onPressed: (context) =>
                          _onClickSlidable(index, Actions.delete),
                      backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
                      foregroundColor: Colors.white,
                      icon: CustomIcons.delete_conversation,
                    ),
                  ],
                ),
                child: Container(
                  margin: const EdgeInsets.all(16),
                  child: Row(
                    children: [
                      Image.asset(
                          "${controller.listAllMessage.value[index].avatar}"),
                      Padding(padding: EdgeInsets.only(left: 16.w)),
                      Column(
                        children: [
                          SizedBox(
                            width: 200,
                            child: Text(
                              "${controller.listAllMessage.value[index].name}",
                              style: TextStyle(
                                  fontSize: 16.sp,
                                  color: const Color.fromRGBO(0, 0, 0, 1)),
                            ),
                          ),
                          SizedBox(
                            width: 200,
                            child: Text(
                              "${controller.listAllMessage.value[index].message} \. ${controller.focusedDay.value}",
                              style: TextStyle(
                                  color:
                                      controller.listAllMessage[index].status ==
                                              true
                                          ? const Color.fromRGBO(26, 26, 26, 1)
                                          : const Color.fromRGBO(133, 133, 133, 1),
                                  fontSize: 14),
                            ),
                          )
                        ],
                      ),
                      Expanded(child: Container()),
                      Icon(
                        Icons.circle,
                        color: controller.listAllMessage[index].status == true
                            ? const Color.fromRGBO(248, 129, 37, 1)
                            : Colors.white,
                        size: 18,
                      )
                    ],
                  ),
                )),
          );
        });
  }
}

class Unread extends GetView<MessageController> {
  const Unread({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: controller.listUnreadMessage.value.length,
        itemBuilder: (context, index) {
          return Slidable(
              endActionPane: ActionPane(
                motion: const ScrollMotion(),
                children: [
                  SlidableAction(
                    // An action can be bigger than the others.
                    flex: 1,
                    onPressed: (context) =>
                        _onClickSlidable(index, Actions.more),
                    backgroundColor: const Color(0xFF7BC043),
                    foregroundColor: Colors.white,
                    icon: Icons.archive,
                  ),
                  SlidableAction(
                    flex: 1,
                    onPressed: (context) =>
                        _onClickSlidable(index, Actions.notification),
                    backgroundColor: const Color(0xFF0392CF),
                    foregroundColor: Colors.white,
                    icon: Icons.notifications,
                  ),
                  SlidableAction(
                      flex: 1,
                      onPressed: (context) =>
                          _onClickSlidable(index, Actions.delete),
                      backgroundColor: const Color(0xFF0392CF),
                      foregroundColor: Colors.white,
                      icon: Icons.delete),
                ],
              ),
              child: Container(
                margin: const EdgeInsets.all(16),
                child: Row(
                  children: [
                    Image.asset(
                        "${controller.listUnreadMessage.value[index].avatar}"),
                    const Padding(padding: EdgeInsets.only(left: 16)),
                    Column(
                      children: [
                        SizedBox(
                          width: 200,
                          child: Text(
                            "${controller.listUnreadMessage.value[index].name}",
                            style: TextStyle(
                                fontSize: 16.sp,
                                color: const Color.fromRGBO(0, 0, 0, 1)),
                          ),
                        ),
                        SizedBox(
                          width: 200,
                          child: Text(
                            "${controller.listUnreadMessage.value[index].message}",
                            style: TextStyle(
                                color: controller.listUnreadMessage.value[index]
                                            .status ==
                                        true
                                    ? const Color.fromRGBO(26, 26, 26, 1)
                                    : const Color.fromRGBO(133, 133, 133, 1),
                                fontSize: 14),
                          ),
                        )
                      ],
                    ),
                    Expanded(child: Container()),
                    Icon(
                      Icons.circle,
                      color: controller.listUnreadMessage.value[index].status ==
                              true
                          ? const Color.fromRGBO(248, 129, 37, 1)
                          : Colors.white,
                      size: 18,
                    )
                  ],
                ),
              ));
        });
  }
}

void _onClickSlidable(int index, Actions actions) {}
