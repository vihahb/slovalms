import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:task_manager/data/model/common/detail_item_list_student.dart';

class MessageController extends GetxController {
  var selectedPageIndex = 0.obs;
  FocusNode SearchFocus = FocusNode();
  var focusedDay = DateTime.now().obs;
  DetailItemMessage message = DetailItemMessage();
  final listAllMessage = [
    DetailItemMessage(
        name: "Đặng Văn Bình",
        avatar: "assets/images/fake_avatar_message_6.png",
        message: "What’s man! ",
        status: true),
    DetailItemMessage(
        name: "Đặng Văn Bình 1",
        avatar: "assets/images/fake_avatar_message_6.png",
        message: "What’s man! ",
        status: false),
    DetailItemMessage(
        name: "Đặng Văn Bình 2",
        avatar: "assets/images/fake_avatar_message_3.png",
        message: "You: Ok, See you in To… ! ",
        status: true),
    DetailItemMessage(
        name: "Đặng Văn Bình 3",
        avatar: "assets/images/fake_avatar_message_1.png",
        message: "You: Ok, See you in To… ! ",
        status: false),
    DetailItemMessage(
        name: "Đặng Văn Bình 4",
        avatar: "assets/images/fake_avatar_message_5.png",
        message: "You: Ok, See you in To… ! ",
        status: true),
    DetailItemMessage(
        name: "Đặng Văn Bình 5",
        avatar: "assets/images/fake_avatar_message_6.png",
        message: "You: Ok, See you in To… ! ",
        status: false),
    DetailItemMessage(
        name: "Đặng Văn Bình 6",
        avatar: "assets/images/fake_avatar_message_7.png",
        message: "You: Ok, See you in To… ",
        status: true),
    DetailItemMessage(
        name: "Đặng Văn Bình 7",
        avatar: "assets/images/fake_avatar_message_8.png",
        message: "You: Ok, See you in To… ",
        status: false),
  ].obs;
  final listUnreadMessage = [
    DetailItemMessage(
        name: "Đặng Văn Bình",
        avatar: "assets/images/fake_avatar_message_6.png",
        message: "What’s man! ",
        status: true),
    DetailItemMessage(
        name: "Đặng Văn Bình 1",
        avatar: "assets/images/fake_avatar_message_6.png",
        message: "What’s man! ",
        status: false),
  ].obs;
}
