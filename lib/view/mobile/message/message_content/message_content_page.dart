import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../data/model/common/detail_item_list_student.dart';
import 'message_content_controller.dart';
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'dart:io';
import 'package:image_picker/image_picker.dart';
class MessageContent extends GetView<MessageContentController> {

final controller  = Get.put(MessageContentController());
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
        child: Scaffold(
          body: Obx(() => Column(
            children: [
              Card(
                margin: EdgeInsets.zero,
                child: Container(
                  margin: const EdgeInsets.all(6),
                  child: Row(
                    children: [
                      const BackButton(),
                      CircleAvatar(
                        backgroundImage: AssetImage("${controller.message.value.avatar}"),
                      ),
                      Padding(padding: EdgeInsets.only(left: 8.w)),
                      Text(
                        "${controller.message.value.name}",
                        style: TextStyle(
                            color: const Color.fromRGBO(26, 26, 26, 1), fontSize: 14.sp),
                      ),
                      Expanded(child: Container()),
                      const Icon(Icons.more_horiz),
                      Padding(padding: EdgeInsets.only(right: 16.w)),
                    ],
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 16.h)),
              Text(DateFormat(' hh:mm, d MMM').format(DateTime.now())),
              Expanded(
                child: ListView.builder(
                    itemCount: controller.listMessage.length,
                    itemBuilder: (context, index) {
                      return controller.listMessage.value[index].status!
                          ? Container(
                        child: Wrap(
                          children: [
                            Flex(
                              direction: Axis.horizontal,
                              children: [
                                Expanded(child: Container()),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Container(
                                  decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(12),
                                        topLeft: Radius.circular(12),
                                        bottomRight: Radius.circular(4),
                                        bottomLeft: Radius.circular(12)),
                                    color:
                                    Color.fromRGBO(248, 129, 37, 1),
                                  ),
                                  margin:
                                  EdgeInsets.only(top: 16.w, right: 16.w),
                                  padding: const EdgeInsets.all(8),
                                  child: Text(
                                    "${controller.listMessage[index].message}",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12.sp),
                                  ),
                                ),
                                Padding(padding: EdgeInsets.only(top: 4.h)),
                                Row(
                                  children: [
                                    Expanded(child: Container()),
                                    Text(
                                      DateFormat.jm()
                                          .format(DateTime.now()),
                                      style: TextStyle(
                                          color: const Color.fromRGBO(
                                              192, 192, 192, 1),
                                          fontSize: 10.sp),
                                    ),
                                    const Visibility(
                                      visible: false,
                                      child: Icon(
                                        Icons.check_circle_outline,
                                        size: 14,
                                      ),
                                    ),
                                    const Visibility(
                                      visible: false,
                                      child: Icon(
                                        Icons.circle_outlined,
                                        size: 14,
                                      ),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(
                                            top: 4.h, right: 16.w))
                                  ],
                                )
                              ],
                            )
                          ],
                        ),
                      )
                          : Container(
                        margin: const EdgeInsets.only(
                            top: 16, left: 16, bottom: 16),
                        child: Wrap(
                          children: [
                            CircleAvatar(
                              backgroundImage: AssetImage(
                                  "${controller.listMessage[index].avatar}"),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Container(
                                  decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(12),
                                        topLeft: Radius.circular(12),
                                        bottomRight: Radius.circular(4),
                                        bottomLeft: Radius.circular(12)),
                                    color:
                                    Color.fromRGBO(246, 246, 246, 1),
                                  ),
                                  padding: const EdgeInsets.all(8),
                                  child: Text(
                                    "${controller.listMessage[index].message}",
                                    style: TextStyle(
                                        color:
                                        const Color.fromRGBO(26, 26, 26, 1),
                                        fontSize: 12.sp),
                                  ),
                                ),
                                Padding(padding: EdgeInsets.only(top: 4.h)),
                                Text(
                                  DateFormat.jm().format(DateTime.now()),
                                  style: TextStyle(
                                      color: const Color.fromRGBO(
                                          192, 192, 192, 1),
                                      fontSize: 10.sp),
                                ),
                              ],
                            )
                          ],
                        ),
                      );
                    }),
              ),
              Container(
                margin: const EdgeInsets.all(16),
                padding: EdgeInsets.only(left: 8.w),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(32),
                  color: const Color.fromRGBO(246, 246, 246, 1),
                ),
                child: FocusScope(
                  node: FocusScopeNode(),
                  onFocusChange: (focus) {
                    controller.hasFocus.value = focus;
                    if(controller.hasFocus.value == true){
                      controller.isShowSticker.value = false;
                    }
                  },
                  child: Focus(
                    child: TextFormField(
                      onTap: (){
                        controller.isShowSticker.value = false;
                        controller.showCursor.value = true;
                      },
                      showCursor: controller.showCursor.value,
                      focusNode: controller.focusNode,
                      textAlignVertical: TextAlignVertical.center,
                      controller: controller.textEditingController,
                      onFieldSubmitted: (value) {
                        controller.listMessage.add(DetailItemMessage(
                            message: "$value",
                            status: true,
                            name: "hihihi",
                            avatar: "hihihii"));
                        controller.textEditingController.clear();
                        controller.isShowSticker.value = false;
                      },
                      cursorColor: const Color.fromRGBO(248, 129, 37, 1),
                      decoration: InputDecoration(
                          hintText: "Type your message here...",
                          hintStyle: TextStyle(
                              color: const Color.fromRGBO(177, 177, 177, 1), fontSize: 12.sp),
                          border: InputBorder.none,
                          suffixIcon: SizedBox(
                            width: 80,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                InkWell(
                                  onTap: () {
                                    Pick_image_to_library();
                                  },
                                  child: Image.asset("assets/images/icon_image_message.png"),
                                ),
                                Padding(padding: EdgeInsets.only(left: 8.w)),
                                InkWell(
                                  onTap: () {
                                    controller.isShowSticker.value = !controller.isShowSticker.value;
                                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                                    controller.showCursor.value = false;
                                  },
                                  child: Image.asset(
                                      "assets/images/icon_emoji_message.png"),
                                ),
                                Padding(padding: EdgeInsets.only(right: 8.w)),
                              ],
                            ),
                          )),
                    ),
                  ),
                ),
              ),
              Offstage(
                offstage: !controller.isShowSticker.value,
                child: SizedBox(
                    height: 250,
                    child: EmojiPicker(
                      textEditingController: controller.textEditingController,
                      config: Config(
                        columns: 7,
                        // Issue: https://github.com/flutter/flutter/issues/28894
                        emojiSizeMax: 32 *
                            (foundation.defaultTargetPlatform == TargetPlatform.iOS
                                ? 1.30
                                : 1.0),
                        verticalSpacing: 0,
                        horizontalSpacing: 0,
                        gridPadding: EdgeInsets.zero,
                        initCategory: Category.RECENT,
                        bgColor: const Color(0xFFF2F2F2),
                        indicatorColor: Color.fromRGBO(248, 129, 37, 1),
                        iconColor: Colors.grey,
                        iconColorSelected: Colors.blue,
                        backspaceColor: Colors.blue,
                        skinToneDialogBgColor: Colors.white,
                        skinToneIndicatorColor: Colors.grey,
                        enableSkinTones: true,
                        showRecentsTab: true,
                        recentsLimit: 28,
                        replaceEmojiOnLimitExceed: false,
                        noRecents: const Text(
                          'No Recents',
                          style: TextStyle(fontSize: 20, color: Colors.black26),
                          textAlign: TextAlign.center,
                        ),
                        loadingIndicator: const SizedBox.shrink(),
                        tabIndicatorAnimDuration: kTabScrollDuration,
                        categoryIcons: const CategoryIcons(),
                        buttonMode: ButtonMode.MATERIAL,
                        checkPlatformCompatibility: true,
                      ),
                    )),
              ),
            ],
          )),
        ));
  }
  void Pick_image_to_library() async{
    final XFile ? img = await controller.imgPicker.pickImage(source: ImageSource.gallery);
    if(img != null){
        // controller.imageSelect = File(img.path);
    }

  }
}