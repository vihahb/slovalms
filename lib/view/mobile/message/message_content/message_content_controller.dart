import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import '../../../../data/model/common/detail_item_list_student.dart';
class MessageContentController extends GetxController{
  var listMessage = <DetailItemMessage>[].obs;
  var textEditingController = TextEditingController();
  RxBool isShowSticker = false.obs;
  RxBool hasFocus = false.obs;
  final imgPicker = ImagePicker();
  File? imageSelect;
  FocusNode focusNode = FocusNode();
  RxBool showCursor = true.obs;
  var message = DetailItemMessage().obs;


  @override
  void onInit() {
    super.onInit();
    var argument = Get.arguments;
    if (argument != null) {
      message.value = argument;
    }
    listMessage.add(
      DetailItemMessage(
          avatar: "${message.value.avatar}",
          name: "${message.value.name}",
          status: false,
          message:
          "Alex, let’s meet this weekend. I’ll check with Dave too 😎"),
    );
    listMessage.add(
      DetailItemMessage(
          avatar: "${message.value.avatar}",
          name: "${message.value.name}",
          status: true,
          message: "Sure. Let’s aim for saturday"),
    );
    listMessage.add(
      DetailItemMessage(
          avatar: "${message.value.avatar}",
          name: "${message.value.name}",
          status: true,
          message: "I’m visiting mom this sunday 👻"),
    );
    listMessage.add(
      DetailItemMessage(
          avatar: "${message.value.avatar}",
          name: "${message.value.name}",
          status: false,
          message: "Alrighty! Will give you a call shortly 🤗"),
    );
    listMessage.add(
      DetailItemMessage(
          avatar: "${message.value.avatar}",
          name: "${message.value.name}",
          status: true,
          message: "❤"),
    );
    listMessage.add(
      DetailItemMessage(
          avatar: "${message.value.avatar}",
          name: "${message.value.name}",
          status: false,
          message: "Hey you! Are you there?"),
    );
    listMessage.add(
      DetailItemMessage(
          avatar: "${message.value.avatar}",
          name: "${message.value.name}",
          status: true,
          message: "👋 Hi Jess! What’s up?"),
    );
  }
}