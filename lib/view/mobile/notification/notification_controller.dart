import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:task_manager/commom/app_cache.dart';
import 'package:task_manager/data/model/common/detail_item_list_student.dart';

import '../../../commom/utils/app_utils.dart';
import '../../../data/base_service/api_response.dart';
import '../../../data/model/common/notification.dart';
import '../../../data/model/res/class/classTeacher.dart';
import '../../../data/repository/notification_repo/notification_repo.dart';
import '../../../data/repository/subject/class_office_repo.dart';
import '../home/home_controller.dart';
import '../role/teacher/teacher_home_controller.dart';

class NotificationController extends GetxController {
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy hh:mm:ss');
  final NotificationRepo _notificationRepo = NotificationRepo();
  var notify = Notify().obs;
  var itemsNotification = ItemsNotification().obs;
  var listNotifyToday = <NotifyList>[].obs;
  var listBeforeNotify = <NotifyList>[].obs;
  var listNotifyTodayLoadMore = <NotifyList>[].obs;
  var listBeforeNotifyLoadMore = <NotifyList>[].obs;
  var listIdNotifyToday = <String>[].obs;
  var listIdNotifyBefore = <String>[].obs;
  var listCheckBoxToday = <bool>[].obs;
  var listCheckBoxBefore = <bool>[].obs;
  var isChooseManyBefore = false.obs;
  var isChooseManyToday = false.obs;
  var isCheckAllBefore = false.obs;
  var isCheckAllToday = false.obs;
  final ClassOfficeRepo _classRepo = ClassOfficeRepo();
  RxList<ClassId> classOfTeacher = <ClassId>[].obs;
  var classUId = ClassId().obs;
  var isAllClass = true.obs;
  var controller = ScrollController();
  var indexPage = 0.obs;
  @override
  void onInit() {
    controller = ScrollController()..addListener(_scrollListener);
    super.onInit();
  }

  void _scrollListener() {
    if (controller.position.extentAfter < 100) {
      indexPage++;
      _notificationRepo.getListNotify("", indexPage.value, 20).then((value) {
        if (value.state == Status.SUCCESS) {
          notify.value = value.object!;
          itemsNotification.value = notify.value.items!;
          listNotifyTodayLoadMore.value =
              itemsNotification.value.todayNotifyList!;
          listBeforeNotifyLoadMore.value =
              itemsNotification.value.beforeNotifyList!;
          listCheckBoxToday.value = [];
          for (int i = 0; i < listNotifyToday.value.length; i++) {
            listCheckBoxToday.value.add(false);
          }
          listCheckBoxBefore.value = [];
          for (int i = 0; i < listBeforeNotify.value.length; i++) {
            listCheckBoxBefore.value.add(false);
          }
        }
      });
      listNotifyToday.addAll(listBeforeNotifyLoadMore);
      listBeforeNotify.addAll(listBeforeNotifyLoadMore);
    }
  }

  setTypeNotification(type) {
    switch (type) {
      case "LEAVE_APPLICATION":
        return "Đơn xin nghỉ phép";
      case "DILIGENT":
        return "CHUYÊN CẦN";
      case "STUDY":
        return "HỌC TẬP";
    }
  }

  setStatuNotification(type) {
    switch (type) {
      case "NOTSEEN":
        return "Chưa xem";
      case "SEEN":
        return "Đã  xem";
    }
  }

  checkBoxListViewToday(index) {
    if (listCheckBoxToday[index] == false) {
      listCheckBoxToday[index] = true;
      listIdNotifyToday.value.add(listNotifyToday.value[index].id!);
    } else {
      listCheckBoxToday[index] = false;
      listIdNotifyToday.value.remove(listNotifyToday.value[index].id!);
    }
  }

  checkBoxListViewBefore(index) {
    if (listCheckBoxBefore[index] == false) {
      listCheckBoxBefore[index] = true;
      listIdNotifyBefore.value.add(listBeforeNotify.value[index].id!);
    } else {
      listCheckBoxBefore[index] = false;
      listIdNotifyBefore.value.remove(listBeforeNotify.value[index].id!);
    }
  }

  checkAllBefore() {
    if (isCheckAllBefore.value == true) {
      listCheckBoxBefore.value = [];
      for (int i = 0; i < listBeforeNotify.length; i++) {
        listCheckBoxBefore.value.add(true);
        listIdNotifyBefore.value.add(listBeforeNotify.value[i].id!);
      }
      listCheckBoxBefore.refresh();
    } else {
      listCheckBoxBefore.value = [];
      for (int i = 0; i < listBeforeNotify.value.length; i++) {
        listCheckBoxBefore.value.add(false);
      }
      listIdNotifyBefore.value.clear();
      listCheckBoxBefore.refresh();
    }
  }

  checkAllToday() {
    if (isCheckAllToday.value == true) {
      listCheckBoxToday.value = [];
      for (int i = 0; i < listNotifyToday.length; i++) {
        listCheckBoxToday.value.add(true);
        listIdNotifyToday.value.add(listNotifyToday.value[i].id!);
      }
      listCheckBoxToday.refresh();
    } else {
      listCheckBoxToday.value = [];
      for (int i = 0; i < listNotifyToday.value.length; i++) {
        listCheckBoxToday.value.add(false);
      }
      listIdNotifyToday.value.clear();
      listCheckBoxToday.refresh();
    }
  }

  setColorIconDeleteBefore() {
    if (listCheckBoxBefore.value.contains(true) == true) {
      return const Color.fromRGBO(255, 69, 89, 1);
    } else {
      return const Color.fromRGBO(177, 177, 177, 1);
    }
  }

  setColorIconCheckBefore() {
    if (listCheckBoxBefore.value.contains(true) == true) {
      return const Color.fromRGBO(248, 129, 37, 1);
    } else {
      return const Color.fromRGBO(177, 177, 177, 1);
    }
  }

  setColorIconDeleteToday() {
    if (listCheckBoxToday.value.contains(true)) {
      return const Color.fromRGBO(255, 69, 89, 1);
    } else {
      return const Color.fromRGBO(177, 177, 177, 1);
    }
  }

  setColorIconCheckToday() {
    if (listCheckBoxToday.value.contains(true)) {
      return const Color.fromRGBO(248, 129, 37, 1);
    } else {
      return const Color.fromRGBO(177, 177, 177, 1);
    }
  }

  getListNotification() async {
    _notificationRepo.getListNotify("", indexPage.value, 200).then((value) {
      if (value.state == Status.SUCCESS) {
        notify.value = value.object!;
        itemsNotification.value = notify.value.items!;
        listNotifyToday.value = itemsNotification.value.todayNotifyList!;
        listBeforeNotify.value = itemsNotification.value.beforeNotifyList!;
        listCheckBoxToday.value = [];
        for (int i = 0; i < listNotifyToday.value.length; i++) {
          listCheckBoxToday.value.add(false);
        }
        listCheckBoxBefore.value = [];
        for (int i = 0; i < listBeforeNotify.value.length; i++) {
          listCheckBoxBefore.value.add(false);
        }
      }
    });
  }

  getListNotificationByClass(classId) async {
    _notificationRepo.getListNotify(classId, indexPage.value, 200).then((value) {
      if (value.state == Status.SUCCESS) {
        notify.value = value.object!;
        itemsNotification.value = notify.value.items!;
        listNotifyToday.value = itemsNotification.value.todayNotifyList!;
        listBeforeNotify.value = itemsNotification.value.beforeNotifyList!;
        listCheckBoxToday.value = [];
        for (int i = 0; i < listNotifyToday.value.length; i++) {
          listCheckBoxToday.value.add(false);
        }
        listCheckBoxBefore.value = [];
        for (int i = 0; i < listBeforeNotify.value.length; i++) {
          listCheckBoxBefore.value.add(false);
        }
      }
    });
  }

  seenNotificationToday(RxList<String> list) {
    _notificationRepo.seenNotification(list).then((value) {
      if (value.state == Status.SUCCESS) {
        getListNotification();
        Get.find<HomeController>().getCountNotifyNotSeen();
        isChooseManyToday.value = false;
        listCheckBoxToday.value = [];
        for (int i = 0; i < listNotifyToday.value.length; i++) {
          listCheckBoxToday.value.add(false);
        }

      } else {
        AppUtils.shared.snackbarError("Lỗi", value.message ?? "");
      }
    });
  }


  seenNotificationBefore(RxList<String> list) {
    _notificationRepo.seenNotification(list).then((value) {
      if (value.state == Status.SUCCESS) {
        getListNotification();
        Get.find<HomeController>().getCountNotifyNotSeen();
        isChooseManyBefore.value = false;
        listCheckBoxBefore.value = [];
        for (int i = 0; i < listBeforeNotify.value.length; i++) {
          listCheckBoxBefore.value.add(false);
        }
      } else {
        AppUtils.shared.snackbarError("Lỗi", value.message ?? "");
      }
    });
  }

  deleteNotificationToday(RxList<String> list) {
    _notificationRepo.deleteNotification(list).then((value) {
      if (value.state == Status.SUCCESS) {
        getListNotification();
        Get.find<HomeController>().getCountNotifyNotSeen();
        isChooseManyToday.value = false;
        Get.back();
      } else {
        AppUtils.shared.snackbarError("Lỗi", value.message ?? "");
      }
    });
  }

  deleteNotificationBefore(RxList<String> list) {
    _notificationRepo.deleteNotification(list).then((value) {
      if (value.state == Status.SUCCESS) {
        getListNotification();
        Get.find<HomeController>().getCountNotifyNotSeen();
        isChooseManyBefore.value = false;
        Get.back();
      } else {
        AppUtils.shared.snackbarError("Lỗi", value.message ?? "");
      }
    });
  }


  getListClassIdByTeacher() async {
    var teacherId = AppCache().userId ?? "";
    _classRepo.listClassByTeacher(teacherId).then((value) {
      if (value.state == Status.SUCCESS) {
        classOfTeacher.value = value.object!;
        for (var f in classOfTeacher.value) {
          f.checked = false;
        }

        classOfTeacher.refresh();
      }
    });
  }

  selectedClass(ClassId classId, index) {
    for (var f in classOfTeacher.value) {
      if (f.id != classId.id) {
        f.checked = false;
      } else {
        f.checked = true;
      }
    }
    classOfTeacher.refresh();
  }

  getColorClass(ClassId classId, index) {
    isAllClass.value = false;
    for (var f in classOfTeacher.value) {
      if (f.id != classId.id) {
        f.checked = false;
      } else {
        f.checked = true;
      }
    }
    classOfTeacher.refresh();
  }

  onRefresh() {
    getListNotification();
    if(AppCache().userType == "TEACHER"){
      getListClassIdByTeacher();
    }
  }
}
