import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:task_manager/commom/app_cache.dart';
import 'package:task_manager/view/mobile/home/home_controller.dart';
import 'package:task_manager/view/mobile/notification/notification_controller.dart';

import '../../../commom/utils/app_utils.dart';
import '../../../commom/utils/date_time_utils.dart';
import '../../../data/model/common/notification.dart';
import '../../../routes/app_pages.dart';
import '../role/parent/diligence_parent/approval_parent/approval_parent_controller.dart';
import '../role/teacher/diligent_management_teacher/diligent_management_teacher_controller.dart';
import '../role/teacher/diligent_management_teacher/manager_leave_application_teacher/manager_leave_application_teacher_controller.dart';

class NotificationPage extends GetView<NotificationController> {
  @override
  var controller = Get.put(NotificationController());

  NotificationPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(245, 245, 245, 1),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
        actions: [IconButton(onPressed: () {}, icon: const Icon(Icons.search))],
        title: const Text(
          'Thông báo',
          style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: RefreshIndicator(
          color: const Color.fromRGBO(248, 129, 37, 1),
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Obx(() => Container(
                  margin: const EdgeInsets.all(16),
                  alignment: Alignment.centerLeft,
                  child: Column(
                    children: [
                      Visibility(
                          visible: AppCache().userType == "TEACHER",
                          child: Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(6.r),
                                border: Border.all(
                                    color:
                                        const Color.fromRGBO(239, 239, 239, 1),
                                    width: 1)),
                            child: Row(
                              children: [
                                InkWell(
                                  onTap: () {
                                    controller.isAllClass.value = true;
                                    for (var f
                                        in controller.classOfTeacher.value) {
                                      f.checked = false;
                                    }
                                    controller.getListNotification();
                                  },
                                  child: Container(
                                    width: 100,
                                    height: 26,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      color: controller.isAllClass.value
                                          ? const Color.fromRGBO(
                                              249, 154, 81, 1)
                                          : const Color.fromRGBO(
                                              246, 246, 246, 1),
                                      borderRadius: BorderRadius.circular(6),
                                    ),
                                    margin: const EdgeInsets.only(left: 8),
                                    padding:
                                        const EdgeInsets.fromLTRB(12, 4, 12, 4),
                                    child: Text("Tất cả",
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: controller.isAllClass.value
                                                ? Colors.white
                                                : const Color.fromRGBO(
                                                    90, 90, 90, 1))),
                                  ),
                                ),
                                Expanded(
                                    child: Container(
                                  height: 42,
                                  padding: const EdgeInsets.all(8),
                                  child: ListView.builder(
                                      itemCount: controller.classOfTeacher.value.length,
                                      scrollDirection: Axis.horizontal,
                                      itemBuilder: (context, index) {
                                        return itemClassId(index);
                                      }),
                                ))
                              ],
                            ),
                          )),
                      Container(
                        margin: EdgeInsets.only(top: 16.h),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: Colors.white),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Obx(() => Row(
                                  children: [
                                    Padding(
                                        padding: EdgeInsets.only(left: 6.w)),
                                    Visibility(
                                        visible:
                                            controller.isChooseManyToday.value,
                                        child: InkWell(
                                          onTap: () {
                                            controller.isCheckAllToday.value = !controller.isCheckAllToday.value;
                                            controller.isChooseManyToday.refresh();
                                            controller.checkAllToday();
                                          },
                                          child: Row(
                                            children: [
                                              Container(
                                                  width: 18.w,
                                                  height: 18.h,
                                                  alignment: Alignment.center,
                                                  padding:
                                                      const EdgeInsets.all(2),
                                                  decoration: ShapeDecoration(
                                                    shape: CircleBorder(
                                                        side: BorderSide(
                                                            color: controller
                                                                    .isCheckAllToday
                                                                    .value
                                                                ? const Color
                                                                        .fromRGBO(
                                                                    248,
                                                                    129,
                                                                    37,
                                                                    1)
                                                                : const Color
                                                                        .fromRGBO(
                                                                    235,
                                                                    235,
                                                                    235,
                                                                    1))),
                                                  ),
                                                  child: controller
                                                          .isCheckAllToday.value
                                                      ? const Icon(
                                                          Icons.circle,
                                                          size: 10,
                                                          color: Color.fromRGBO(
                                                              248, 129, 37, 1),
                                                        )
                                                      : null),
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 8.w)),
                                              Text(
                                                "Chọn hết",
                                                style: TextStyle(
                                                    fontSize: 12.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: Colors.black),
                                              ),
                                            ],
                                          ),
                                        )),
                                    Visibility(
                                        visible:
                                            controller.isChooseManyToday.value,
                                        child: Padding(
                                            padding:
                                                EdgeInsets.only(left: 8.w))),
                                    Visibility(
                                        visible:
                                            controller.isChooseManyToday.value,
                                        child: TextButton(
                                            onPressed: () {
                                              controller.isChooseManyToday
                                                  .value = false;
                                            },
                                            child: const Text(
                                              "Hủy",
                                              style:
                                                  TextStyle(color: Colors.red),
                                            ))),
                                    Visibility(
                                        visible:
                                            controller.isChooseManyToday.value,
                                        child: Padding(
                                            padding:
                                                EdgeInsets.only(left: 8.w))),
                                    Text(
                                      'Hôm nay',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          fontSize: 14,
                                          color: controller.isChooseManyToday
                                                      .value ==
                                                  true
                                              ? const Color.fromRGBO(
                                                  248, 129, 37, 1)
                                              : const Color.fromRGBO(
                                                  133, 133, 133, 1),
                                          fontFamily: 'static/Inter-Medium.ttf',
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Expanded(child: Container()),
                                    Visibility(
                                        visible:
                                            controller.isChooseManyToday.value,
                                        child: InkWell(
                                          onTap: () {
                                            controller.seenNotificationToday(
                                                controller.listIdNotifyToday);
                                            controller.listIdNotifyToday.value
                                                .clear();
                                          },
                                          child: Icon(
                                            Icons.check,
                                            size: 24,
                                            color: controller
                                                .setColorIconCheckToday(),
                                          ),
                                        )),
                                    Padding(
                                        padding: EdgeInsets.only(left: 8.w)),
                                    Visibility(
                                        visible:
                                            controller.isChooseManyToday.value,
                                        child: InkWell(
                                          onTap: () {
                                            Get.dialog(Center(
                                              child: Wrap(children: [
                                                Container(
                                                  height: 250,
                                                  width: double.infinity,
                                                  decoration: BoxDecoration(
                                                      color: Colors.transparent,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              16)),
                                                  child: Stack(
                                                    children: [
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            top: 50,
                                                            right: 50,
                                                            left: 50),
                                                        height: 200,
                                                        decoration: BoxDecoration(
                                                            color: Colors.white,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        16)),
                                                        child: Column(
                                                          children: [
                                                            Padding(
                                                                padding: EdgeInsets
                                                                    .only(
                                                                        top: 32
                                                                            .h)),
                                                            Text(
                                                              "Bạn có chắc muốn xóa những tin nhắn này",
                                                              style: TextStyle(
                                                                  color: Color
                                                                      .fromRGBO(
                                                                          133,
                                                                          133,
                                                                          133,
                                                                          1),
                                                                  fontSize:
                                                                      14.sp),
                                                            ),
                                                            Container(
                                                                color: Colors
                                                                    .white,
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(16
                                                                            .h),
                                                                child: SizedBox(
                                                                  width: double
                                                                      .infinity,
                                                                  height: 46,
                                                                  child: ElevatedButton(
                                                                      style: ElevatedButton.styleFrom(backgroundColor: Color.fromRGBO(248, 129, 37, 1)),
                                                                      onPressed: () {
                                                                        Get.back();
                                                                      },
                                                                      child: Text(
                                                                        'Hủy',
                                                                        style: TextStyle(
                                                                            color:
                                                                                Colors.white,
                                                                            fontSize: 16.sp),
                                                                      )),
                                                                )),
                                                            TextButton(
                                                                onPressed: () {
                                                                  controller.deleteNotificationToday(
                                                                      controller
                                                                          .listIdNotifyToday);
                                                                  controller
                                                                      .listIdNotifyToday
                                                                      .clear();
                                                                },
                                                                child: Text(
                                                                  "Xóa",
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .red,
                                                                      fontSize:
                                                                          16.sp),
                                                                ))
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                          alignment:
                                                              Alignment.center,
                                                          margin:
                                                              EdgeInsets.only(
                                                                  top: 10),
                                                          height: 80,
                                                          child: Image.asset(
                                                              "assets/images/image_app_logo.png"))
                                                    ],
                                                  ),
                                                ),
                                              ]),
                                            ));
                                          },
                                          child: Icon(
                                            Icons.delete,
                                            size: 24,
                                            color: controller
                                                .setColorIconDeleteToday(),
                                          ),
                                        )),
                                    Padding(
                                        padding: EdgeInsets.only(left: 8.w)),
                                  ],
                                )),
                            controller.listNotifyToday.value.isEmpty
                                ? const Center(
                                    child: Text("Bạn chưa có thông báo"))
                                : ListView.builder(
                                    shrinkWrap: true,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    itemCount:
                                        controller.listNotifyToday.value.length,
                                    itemBuilder: (context, index) {
                                      var showLine = index ==
                                              controller.listNotifyToday.value
                                                      .length -
                                                  1
                                          ? false
                                          : true;
                                      return Obx(() => Stack(
                                            children: [
                                              InkWell(
                                                onTap: () {
                                                  if (controller.listNotifyToday
                                                          .value?[index].type ==
                                                      "LEAVE_APPLICATION") {
                                                    if (AppCache().userType ==
                                                        "TEACHER") {
                                                      if (controller
                                                              .listNotifyToday
                                                              .value[index]
                                                              .detail
                                                              ?.status ==
                                                          "PENDING") {
                                                        Get.find<HomeController>().comeBackHome();
                                                        Get.toNamed(Routes.diligentManagementTeacher);
                                                        Get.toNamed(Routes.managerLeaveApplicationTeacherPage);
                                                        if (Get.isRegistered<ManagerLeaveApplicationTeacherController>()) {
                                                          Get.find<ManagerLeaveApplicationTeacherController>().comePending();
                                                        }
                                                        Get.toNamed(Routes.detailPendingLeaveApplicationTeacher, arguments: controller.listNotifyToday.value[index].detail?.tranId);
                                                      } else if (controller.listNotifyToday.value[index].detail?.status == "CANCEL") {
                                                        Get.find<HomeController>().comeBackHome();
                                                        Get.toNamed(Routes.diligentManagementTeacher);
                                                        Get.toNamed(Routes.managerLeaveApplicationTeacherPage);
                                                        if (Get.isRegistered<ManagerLeaveApplicationTeacherController>()) {
                                                          Get.find<ManagerLeaveApplicationTeacherController>().comeCancel();
                                                        }
                                                        Get.toNamed(Routes.detailCancelLeaveApplicationTeacher, arguments: controller.listNotifyToday.value[index].detail?.tranId);
                                                      } else {
                                                        Get.find<HomeController>().comeBackHome();
                                                        Get.toNamed(Routes.diligentManagementTeacher);
                                                        Get.toNamed(Routes.managerLeaveApplicationTeacherPage);
                                                        if (Get.isRegistered<ManagerLeaveApplicationTeacherController>()) {
                                                          Get.find<ManagerLeaveApplicationTeacherController>().comeApproved();
                                                        }
                                                        Get.toNamed(
                                                            Routes
                                                                .detailApprovedLeaveApplicationTeacher,
                                                            arguments: controller
                                                                .listNotifyToday
                                                                .value[index]
                                                                .detail
                                                                ?.tranId);
                                                      }
                                                    }else if (AppCache()
                                                        .userType ==
                                                        "PARENT") {
                                                      Get.find<HomeController>().comeBackHome();
                                                      Get.toNamed(Routes.diligenceParentPage);
                                                      Get.toNamed(Routes.approvalParent);
                                                      if(Get.isRegistered<ApprovalParentController>()){
                                                        Get.find<ApprovalParentController>().onInit();
                                                      }

                                                      Get.toNamed(Routes.detailLeaveApplicationParentPage,arguments: controller.listNotifyToday.value[index].tranId);
                                                    }
                                                  } else if (controller
                                                          .listNotifyToday
                                                          .value?[index]
                                                          .type ==
                                                      "DILIGENT") {
                                                    if (AppCache().userType ==
                                                        "PARENT") {
                                                      Get.dialog(Center(
                                                        child: Wrap(children: [
                                                          Container(
                                                            height: 150,
                                                            width: 400,
                                                            alignment: Alignment
                                                                .center,
                                                            decoration: BoxDecoration(
                                                                color: Colors
                                                                    .white,
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            16)),
                                                            child: Column(
                                                              children: [
                                                                Padding(
                                                                    padding: EdgeInsets.only(
                                                                        top: 32
                                                                            .h)),
                                                                Text(
                                                                  "Thông tin chuyên cần",
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .black,
                                                                      fontSize:
                                                                          14.sp),
                                                                ),
                                                                Padding(
                                                                    padding: EdgeInsets.only(
                                                                        top: 16
                                                                            .h)),
                                                                Text(
                                                                  "${controller.listNotifyToday.value[index].title}",
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          14,
                                                                      color: Color.fromRGBO(
                                                                          248,
                                                                          129,
                                                                          37,
                                                                          1),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w400),
                                                                ),
                                                                const Padding(
                                                                    padding: EdgeInsets
                                                                        .only(
                                                                            top:
                                                                                16)),
                                                                Text(
                                                                  "${controller.listNotifyToday.value[index].body}",
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          14,
                                                                      color: Color
                                                                          .fromRGBO(
                                                                              26,
                                                                              26,
                                                                              26,
                                                                              1),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w400),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ]),
                                                      ));
                                                    }
                                                  }
                                                  if (controller
                                                          .listNotifyToday
                                                          .value[index]
                                                          .status ==
                                                      "NOTSEEN") {
                                                    controller
                                                        .listIdNotifyToday.value
                                                        .add(controller
                                                            .listNotifyToday
                                                            .value[index]
                                                            .id!);
                                                    controller
                                                        .seenNotificationToday(
                                                            controller
                                                                .listIdNotifyToday);
                                                    controller.listIdNotifyToday
                                                        .clear();
                                                  }
                                                },
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      color:
                                                          const Color.fromRGBO(
                                                              255, 255, 255, 1),
                                                      margin: EdgeInsets.only(
                                                          top: 0.h,
                                                          bottom: 0.h,
                                                          left: 8.w,
                                                          right: 0.h),
                                                      alignment:
                                                          Alignment.center,
                                                      child: Row(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          Visibility(
                                                              visible: controller
                                                                  .isChooseManyToday
                                                                  .value,
                                                              child: InkWell(
                                                                onTap: () {
                                                                  controller
                                                                      .checkBoxListViewToday(
                                                                          index);
                                                                  if (controller
                                                                          .listCheckBoxToday
                                                                          .value
                                                                          .contains(
                                                                              false) ==
                                                                      true) {
                                                                    controller
                                                                        .isCheckAllToday
                                                                        .value = false;
                                                                  } else {
                                                                    controller
                                                                        .isCheckAllToday
                                                                        .value = true;
                                                                  }
                                                                },
                                                                child: Container(
                                                                    width: 20,
                                                                    height: 20,
                                                                    alignment: Alignment.center,
                                                                    padding: const EdgeInsets.all(2),
                                                                    margin: const EdgeInsets.only(right: 8),
                                                                    decoration: ShapeDecoration(
                                                                      shape: CircleBorder(
                                                                          side:
                                                                              BorderSide(color: controller.listCheckBoxToday.value[index] ? const Color.fromRGBO(248, 129, 37, 1) : const Color.fromRGBO(235, 235, 235, 1))),
                                                                    ),
                                                                    child: controller.listCheckBoxToday.value[index]
                                                                        ? const Icon(
                                                                            Icons.circle,
                                                                            size:
                                                                                12,
                                                                            color: Color.fromRGBO(
                                                                                248,
                                                                                129,
                                                                                37,
                                                                                1),
                                                                          )
                                                                        : null),
                                                              )),
                                                          SizedBox(
                                                            width: 36.w,
                                                            height: 36.w,
                                                            child: CircleAvatar(
                                                              backgroundColor:
                                                                  Colors.white,
                                                              backgroundImage:
                                                                  Image.network(
                                                                controller
                                                                        .listNotifyToday
                                                                        .value[
                                                                            index]
                                                                        .createdBy!
                                                                        .image ??
                                                                    "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                                                                errorBuilder:
                                                                    (context,
                                                                        object,
                                                                        stackTrace) {
                                                                  return Image
                                                                      .asset(
                                                                    "assets/images/img_Noavt.png",
                                                                  );
                                                                },
                                                              ).image,
                                                            ),
                                                          ),
                                                          Expanded(
                                                            child: Container(
                                                              margin:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      left: 8,
                                                                      top: 16),
                                                              child: Column(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .start,
                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                children: [
                                                                  SizedBox(
                                                                    height: 40,
                                                                    width: 270,
                                                                    child: RichText(
                                                                        text: TextSpan(children: [
                                                                      TextSpan(
                                                                          text:
                                                                              '[${controller.setTypeNotification(controller.listNotifyToday.value[index].type)}] ',
                                                                          style: TextStyle(
                                                                              fontWeight: FontWeight.bold,
                                                                              color: const Color.fromRGBO(248, 129, 37, 1),
                                                                              fontSize: 14.sp)),
                                                                      TextSpan(
                                                                          text:
                                                                              ' ${controller.listNotifyToday.value[index].title!}',
                                                                          style: const TextStyle(
                                                                              color: Color.fromRGBO(26, 26, 26, 1),
                                                                              fontSize: 16)),
                                                                    ])),
                                                                  ),
                                                                  Padding(
                                                                      padding: EdgeInsets.only(
                                                                          top: 4
                                                                              .h)),
                                                                  Text("${controller.listNotifyToday.value[index].body}"),
                                                                  Padding(
                                                                      padding: EdgeInsets.only(
                                                                          top: 4
                                                                              .h)),
                                                                  Container(
                                                                    width: 270,
                                                                    margin: const EdgeInsets
                                                                            .only(
                                                                        right:
                                                                            4),
                                                                    child: Text(
                                                                      DateTimeUtils.convertToAgo(DateTime.fromMillisecondsSinceEpoch(controller
                                                                          .listNotifyToday
                                                                          .value[
                                                                              index]
                                                                          .createdAt??0).toLocal()),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .left,
                                                                      style: const TextStyle(
                                                                          color: Color.fromRGBO(
                                                                              114,
                                                                              116,
                                                                              119,
                                                                              1),
                                                                          fontSize:
                                                                              12),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    bottom:
                                                                        32.h),
                                                            child:
                                                                PopupMenuButton(
                                                                    padding:
                                                                        EdgeInsets
                                                                            .zero,
                                                                    shape: RoundedRectangleBorder(
                                                                        borderRadius:
                                                                            BorderRadius.all(Radius.circular(6.0
                                                                                .r))),
                                                                    icon:
                                                                        const Icon(
                                                                      Icons
                                                                          .more_horiz_outlined,
                                                                      color: Color.fromRGBO(
                                                                          177,
                                                                          177,
                                                                          177,
                                                                          1),
                                                                    ),
                                                                    itemBuilder:
                                                                        (context) {
                                                                      return [
                                                                        PopupMenuItem<
                                                                                int>(
                                                                            value:
                                                                                0,
                                                                            padding: EdgeInsets
                                                                                .zero,
                                                                            height:
                                                                                30,
                                                                            child:
                                                                                Column(
                                                                              children: [
                                                                                Padding(padding: EdgeInsets.only(top: 4.h)),
                                                                                Row(
                                                                                  children: [
                                                                                    Padding(padding: EdgeInsets.only(right: 8.w)),
                                                                                    SizedBox(
                                                                                      height: 20,
                                                                                      width: 20,
                                                                                      child: Image.asset("assets/images/image_pick_notify.png"),
                                                                                    ),
                                                                                    Padding(padding: EdgeInsets.only(right: 8.w)),
                                                                                    Text(
                                                                                      "Chọn nhiều thông báo",
                                                                                      style: TextStyle(fontSize: 14.sp),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                                Divider(),
                                                                              ],
                                                                            )),
                                                                        PopupMenuItem<
                                                                            int>(
                                                                          padding:
                                                                              EdgeInsets.zero,
                                                                          value:
                                                                              1,
                                                                          height:
                                                                              24,
                                                                          child:
                                                                              Column(
                                                                            children: [
                                                                              Row(
                                                                                children: [
                                                                                  Padding(padding: EdgeInsets.only(right: 8.w)),
                                                                                  const Icon(
                                                                                    Icons.delete_rounded,
                                                                                    color: Colors.black,
                                                                                  ),
                                                                                  Padding(padding: EdgeInsets.only(right: 8.w)),
                                                                                  const Text("Xóa thông báo")
                                                                                ],
                                                                              ),
                                                                              const Divider()
                                                                            ],
                                                                          ),
                                                                        ),
                                                                        PopupMenuItem<
                                                                            int>(
                                                                          value:
                                                                              2,
                                                                          padding:
                                                                              EdgeInsets.zero,
                                                                          height:
                                                                              24,
                                                                          child:
                                                                              Column(
                                                                            children: [
                                                                              Row(
                                                                                children: [
                                                                                  Padding(padding: EdgeInsets.only(right: 8.w)),
                                                                                  const Icon(
                                                                                    Icons.check,
                                                                                    color: Colors.black,
                                                                                  ),
                                                                                  Padding(padding: EdgeInsets.only(right: 8.w)),
                                                                                  const Text("Đánh dấu là đã đọc")
                                                                                ],
                                                                              ),
                                                                              const Divider()
                                                                            ],
                                                                          ),
                                                                        ),
                                                                        PopupMenuItem<
                                                                            int>(
                                                                          value:
                                                                              3,
                                                                          padding:
                                                                              EdgeInsets.zero,
                                                                          height:
                                                                              30,
                                                                          child:
                                                                              Row(
                                                                            children: [
                                                                              Padding(padding: EdgeInsets.only(right: 8.w)),
                                                                              SizedBox(
                                                                                height: 20,
                                                                                width: 20,
                                                                                child: Image.asset("assets/images/image_share.png"),
                                                                              ),
                                                                              Padding(padding: EdgeInsets.only(right: 8.w)),
                                                                              const Text(
                                                                                "Chia sẻ thông báo",
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ];
                                                                    },
                                                                    onSelected:
                                                                        (value) {
                                                                      switch (
                                                                          value) {
                                                                        case 0:
                                                                          controller
                                                                              .isChooseManyToday
                                                                              .value = true;
                                                                          break;
                                                                        case 1:
                                                                          controller
                                                                              .listIdNotifyToday
                                                                              .value
                                                                              .add(controller.listNotifyToday.value[index].id!);
                                                                          controller
                                                                              .deleteNotificationToday(controller.listIdNotifyToday);
                                                                          controller
                                                                              .listIdNotifyToday
                                                                              .clear();
                                                                          break;
                                                                        case 2:
                                                                          controller
                                                                              .listIdNotifyToday
                                                                              .value
                                                                              .add(controller.listNotifyToday.value[index].id!);
                                                                          controller
                                                                              .seenNotificationToday(controller.listIdNotifyToday);
                                                                          controller
                                                                              .listIdNotifyToday
                                                                              .clear();
                                                                          break;
                                                                      }
                                                                    }),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    (showLine)
                                                        ? const Divider(
                                                            indent: 10,
                                                            endIndent: 10,
                                                          )
                                                        : Container()
                                                  ],
                                                ),
                                              ),
                                              Visibility(
                                                  visible: controller
                                                          .listNotifyToday
                                                          .value[index]
                                                          .status ==
                                                      "NOTSEEN",
                                                  child: const Positioned(
                                                      bottom: 32,
                                                      right: 24,
                                                      child: Icon(
                                                        Icons.circle,
                                                        color: Color.fromRGBO(
                                                            248, 129, 37, 1),
                                                        size: 10,
                                                      )))
                                            ],
                                          ));
                                    })
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 16.h),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: Colors.white),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Obx(() => Row(
                                  children: [
                                    Padding(
                                        padding: EdgeInsets.only(left: 6.w)),
                                    Visibility(
                                        visible:
                                            controller.isChooseManyBefore.value,
                                        child: InkWell(
                                          onTap: () {
                                            controller.isCheckAllBefore.value =
                                                !controller
                                                    .isCheckAllBefore.value;
                                            controller.isChooseManyBefore
                                                .refresh();
                                            controller.checkAllBefore();
                                          },
                                          child: Row(
                                            children: [
                                              Container(
                                                  width: 18.w,
                                                  height: 18.h,
                                                  alignment: Alignment.center,
                                                  padding:
                                                      const EdgeInsets.all(2),
                                                  decoration: ShapeDecoration(
                                                    shape: CircleBorder(
                                                        side: BorderSide(
                                                            color: controller
                                                                    .isCheckAllBefore
                                                                    .value
                                                                ? const Color
                                                                        .fromRGBO(
                                                                    248,
                                                                    129,
                                                                    37,
                                                                    1)
                                                                : const Color
                                                                        .fromRGBO(
                                                                    235,
                                                                    235,
                                                                    235,
                                                                    1))),
                                                  ),
                                                  child: controller
                                                          .isCheckAllBefore
                                                          .value
                                                      ? const Icon(
                                                          Icons.circle,
                                                          size: 10,
                                                          color: Color.fromRGBO(
                                                              248, 129, 37, 1),
                                                        )
                                                      : null),
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 8.w)),
                                              Text(
                                                "Chọn hết",
                                                style: TextStyle(
                                                    fontSize: 12.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: Colors.black),
                                              ),
                                            ],
                                          ),
                                        )),
                                    Visibility(
                                        visible:
                                            controller.isChooseManyBefore.value,
                                        child: Padding(
                                            padding:
                                                EdgeInsets.only(left: 8.w))),
                                    Visibility(
                                        visible:
                                            controller.isChooseManyBefore.value,
                                        child: TextButton(
                                            onPressed: () {
                                              controller.isChooseManyBefore
                                                  .value = false;
                                            },
                                            child: const Text(
                                              "Hủy",
                                              style:
                                                  TextStyle(color: Colors.red),
                                            ))),
                                    Visibility(
                                        visible:
                                            controller.isChooseManyBefore.value,
                                        child: Padding(
                                            padding:
                                                EdgeInsets.only(left: 8.w))),
                                    Text(
                                      'Trước đó',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          fontSize: 14,
                                          color: controller.isChooseManyBefore
                                                      .value ==
                                                  true
                                              ? const Color.fromRGBO(
                                                  248, 129, 37, 1)
                                              : Color.fromRGBO(
                                                  133, 133, 133, 1),
                                          fontFamily: 'static/Inter-Medium.ttf',
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Expanded(child: Container()),
                                    Visibility(
                                        visible:
                                            controller.isChooseManyBefore.value,
                                        child: InkWell(
                                          onTap: () {
                                            controller.seenNotificationBefore(
                                                controller.listIdNotifyBefore);
                                            controller.listIdNotifyBefore.value
                                                .clear();
                                            controller.listCheckBoxBefore.value
                                                .clear();
                                          },
                                          child: Icon(
                                            Icons.check,
                                            size: 24,
                                            color: controller
                                                .setColorIconCheckBefore(),
                                          ),
                                        )),
                                    Padding(
                                        padding: EdgeInsets.only(left: 8.w)),
                                    Visibility(
                                        visible:
                                            controller.isChooseManyBefore.value,
                                        child: InkWell(
                                          onTap: () {
                                            Get.dialog(Center(
                                              child: Wrap(children: [
                                                Container(
                                                  height: 250,
                                                  width: double.infinity,
                                                  decoration: BoxDecoration(
                                                      color: Colors.transparent,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              16)),
                                                  child: Stack(
                                                    children: [
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            top: 50,
                                                            right: 50,
                                                            left: 50),
                                                        height: 200,
                                                        decoration: BoxDecoration(
                                                            color: Colors.white,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        16)),
                                                        child: Column(
                                                          children: [
                                                            Padding(
                                                                padding: EdgeInsets
                                                                    .only(
                                                                        top: 32
                                                                            .h)),
                                                            Text(
                                                              "Bạn có chắc muốn xóa những tin nhắn này",
                                                              style: TextStyle(
                                                                  color: Color
                                                                      .fromRGBO(
                                                                          133,
                                                                          133,
                                                                          133,
                                                                          1),
                                                                  fontSize:
                                                                      14.sp),
                                                            ),
                                                            Container(
                                                                color: Colors
                                                                    .white,
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(16
                                                                            .h),
                                                                child: SizedBox(
                                                                  width: double
                                                                      .infinity,
                                                                  height: 46,
                                                                  child: ElevatedButton(
                                                                      style: ElevatedButton.styleFrom(backgroundColor: Color.fromRGBO(248, 129, 37, 1)),
                                                                      onPressed: () {
                                                                        Get.back();
                                                                      },
                                                                      child: Text(
                                                                        'Hủy',
                                                                        style: TextStyle(
                                                                            color:
                                                                                Colors.white,
                                                                            fontSize: 16.sp),
                                                                      )),
                                                                )),
                                                            TextButton(
                                                                onPressed: () {
                                                                  controller.deleteNotificationToday(
                                                                      controller
                                                                          .listIdNotifyBefore);
                                                                  controller
                                                                      .listIdNotifyBefore
                                                                      .clear();
                                                                },
                                                                child: Text(
                                                                  "Xóa",
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .red,
                                                                      fontSize:
                                                                          16.sp),
                                                                ))
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                          alignment:
                                                              Alignment.center,
                                                          margin:
                                                              EdgeInsets.only(
                                                                  top: 10),
                                                          height: 80,
                                                          child: Image.asset(
                                                              "assets/images/image_app_logo.png"))
                                                    ],
                                                  ),
                                                ),
                                              ]),
                                            ));
                                          },
                                          child: Icon(
                                            Icons.delete,
                                            size: 24,
                                            color: controller
                                                .setColorIconDeleteBefore(),
                                          ),
                                        )),
                                    Padding(
                                        padding: EdgeInsets.only(left: 8.w)),
                                  ],
                                )),
                            controller.listBeforeNotify.value.isEmpty
                                ? const Center(
                                    child: Text("Bạn chưa có thông báo"))
                                : ListView.builder(
                                    shrinkWrap: true,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    itemCount: controller
                                        .listBeforeNotify.value.length,
                                    itemBuilder: (context, index) {
                                      var showLine = index ==
                                              controller.listBeforeNotify.value
                                                      .length -
                                                  1
                                          ? false
                                          : true;
                                      return Obx(() => Stack(
                                            children: [
                                              InkWell(
                                                onTap: () {
                                                  if (controller
                                                          .listBeforeNotify
                                                          .value?[index]
                                                          .type ==
                                                      "LEAVE_APPLICATION") {
                                                    if (AppCache().userType ==
                                                        "TEACHER") {
                                                      if (controller
                                                              .listBeforeNotify
                                                              .value[index]
                                                              .detail
                                                              ?.status ==
                                                          "PENDING") {
                                                        Get.find<
                                                                HomeController>()
                                                            .comeBackHome();
                                                        Get.toNamed(Routes
                                                            .diligentManagementTeacher);
                                                        Get.toNamed(Routes
                                                            .managerLeaveApplicationTeacherPage);
                                                        Get.find<
                                                                ManagerLeaveApplicationTeacherController>()
                                                            .comePending();
                                                        Get.toNamed(
                                                            Routes
                                                                .detailPendingLeaveApplicationTeacher,
                                                            arguments: controller
                                                                .listBeforeNotify
                                                                .value[index]
                                                                .detail
                                                                ?.tranId);
                                                      } else if (controller
                                                              .listBeforeNotify
                                                              .value[index]
                                                              .detail
                                                              ?.status ==
                                                          "CANCEL") {
                                                        Get.find<
                                                                HomeController>()
                                                            .comeBackHome();
                                                        Get.toNamed(Routes
                                                            .diligentManagementTeacher);
                                                        Get.toNamed(Routes
                                                            .managerLeaveApplicationTeacherPage);
                                                        Get.find<
                                                                ManagerLeaveApplicationTeacherController>()
                                                            .comeCancel();
                                                        Get.toNamed(
                                                            Routes
                                                                .detailCancelLeaveApplicationTeacher,
                                                            arguments: controller
                                                                .listBeforeNotify
                                                                .value[index]
                                                                .detail
                                                                ?.tranId);
                                                      } else {
                                                        Get.find<
                                                                HomeController>()
                                                            .comeBackHome();
                                                        Get.toNamed(Routes
                                                            .diligentManagementTeacher);
                                                        Get.toNamed(Routes
                                                            .managerLeaveApplicationTeacherPage);
                                                        if (Get.isRegistered<
                                                            ManagerLeaveApplicationTeacherController>()) {
                                                          Get.find<
                                                                  ManagerLeaveApplicationTeacherController>()
                                                              .comeApproved();
                                                        }
                                                        Get.toNamed(
                                                            Routes
                                                                .detailApprovedLeaveApplicationTeacher,
                                                            arguments: controller
                                                                .listBeforeNotify
                                                                .value[index]
                                                                .detail
                                                                ?.tranId);
                                                      }
                                                    } else if (AppCache()
                                                            .userType ==
                                                        "PARENT") {
                                                      Get.find<HomeController>().comeBackHome();
                                                      Get.toNamed(Routes.diligenceParentPage);
                                                      Get.toNamed(Routes.approvalParent);
                                                      if(Get.isRegistered<ApprovalParentController>()){
                                                        Get.find<ApprovalParentController>().onInit();
                                                      }
                                                      Get.toNamed(Routes.detailLeaveApplicationParentPage,arguments: controller.listBeforeNotify.value[index].tranId);
                                                    }
                                                  } else if (controller
                                                          .listBeforeNotify
                                                          .value?[index]
                                                          .type ==
                                                      "DILIGENT") {
                                                    if (AppCache().userType ==
                                                        "PARENT") {
                                                      Get.dialog(Center(
                                                        child: Wrap(children: [
                                                          Container(
                                                            height: 150,
                                                            width: 400,
                                                            alignment: Alignment
                                                                .center,
                                                            decoration: BoxDecoration(
                                                                color: Colors
                                                                    .white,
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            16)),
                                                            child: Column(
                                                              children: [
                                                                Padding(
                                                                    padding: EdgeInsets.only(
                                                                        top: 32
                                                                            .h)),
                                                                Text(
                                                                  "Thông tin chuyên cần",
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .black,
                                                                      fontSize:
                                                                          14.sp),
                                                                ),
                                                                Padding(
                                                                    padding: EdgeInsets.only(
                                                                        top: 16
                                                                            .h)),
                                                                Text(
                                                                  "${controller.listBeforeNotify.value[index].title}",
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          14,
                                                                      color: Color.fromRGBO(
                                                                          248,
                                                                          129,
                                                                          37,
                                                                          1),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w400),
                                                                ),
                                                                const Padding(
                                                                    padding: EdgeInsets
                                                                        .only(
                                                                            top:
                                                                                16)),
                                                                Text(
                                                                  "${controller.listBeforeNotify.value[index].body}",
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          14,
                                                                      color: Color
                                                                          .fromRGBO(
                                                                              26,
                                                                              26,
                                                                              26,
                                                                              1),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w400),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ]),
                                                      ));
                                                    }
                                                  }
                                                  if (controller
                                                          .listBeforeNotify
                                                          .value[index]
                                                          .status ==
                                                      "NOTSEEN") {
                                                    controller
                                                        .listIdNotifyBefore
                                                        .value
                                                        .add(controller
                                                            .listBeforeNotify
                                                            .value[index]
                                                            .id!);
                                                    controller
                                                        .seenNotificationToday(
                                                            controller
                                                                .listIdNotifyBefore);
                                                    controller.listBeforeNotify
                                                        .clear();
                                                  }
                                                },
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      color:
                                                          const Color.fromRGBO(
                                                              255, 255, 255, 1),
                                                      margin: EdgeInsets.only(
                                                          top: 0.h,
                                                          bottom: 0.h,
                                                          left: 8.w,
                                                          right: 0.h),
                                                      alignment:
                                                          Alignment.center,
                                                      child: Row(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          Visibility(
                                                              visible: controller
                                                                  .isChooseManyBefore
                                                                  .value,
                                                              child: InkWell(
                                                                onTap: () {
                                                                  controller
                                                                      .checkBoxListViewBefore(
                                                                          index);
                                                                  if (controller
                                                                          .listCheckBoxBefore
                                                                          .value
                                                                          .contains(
                                                                              false) ==
                                                                      true) {
                                                                    controller
                                                                        .isCheckAllBefore
                                                                        .value = false;
                                                                  } else {
                                                                    controller
                                                                        .isCheckAllBefore
                                                                        .value = true;
                                                                  }
                                                                },
                                                                child: Container(
                                                                    width: 20,
                                                                    height: 20,
                                                                    alignment: Alignment.center,
                                                                    padding: const EdgeInsets.all(2),
                                                                    margin: EdgeInsets.only(right: 8),
                                                                    decoration: ShapeDecoration(
                                                                      shape: CircleBorder(
                                                                          side:
                                                                              BorderSide(color: controller.listCheckBoxBefore.value[index] ? const Color.fromRGBO(248, 129, 37, 1) : const Color.fromRGBO(235, 235, 235, 1))),
                                                                    ),
                                                                    child: controller.listCheckBoxBefore.value[index]
                                                                        ? const Icon(
                                                                            Icons.circle,
                                                                            size:
                                                                                12,
                                                                            color: Color.fromRGBO(
                                                                                248,
                                                                                129,
                                                                                37,
                                                                                1),
                                                                          )
                                                                        : null),
                                                              )),
                                                          SizedBox(
                                                            width: 36.w,
                                                            height: 36.w,
                                                            child: CircleAvatar(
                                                              backgroundColor:
                                                                  Colors.white,
                                                              backgroundImage:
                                                                  Image.network(
                                                                controller
                                                                        .listBeforeNotify
                                                                        .value[
                                                                            index]
                                                                        .createdBy!
                                                                        .image ??
                                                                    "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                                                                errorBuilder:
                                                                    (context,
                                                                        object,
                                                                        stackTrace) {
                                                                  return Image
                                                                      .asset(
                                                                    "assets/images/img_Noavt.png",
                                                                  );
                                                                },
                                                              ).image,
                                                            ),
                                                          ),
                                                          Expanded(
                                                            child: Container(
                                                              margin:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      left: 8,
                                                                      top: 16),
                                                              child: Column(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .start,
                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                children: [
                                                                  SizedBox(
                                                                    height: 40,
                                                                    width: 270,
                                                                    child: RichText(
                                                                        text: TextSpan(children: [
                                                                      TextSpan(
                                                                          text:
                                                                              '[${controller.setTypeNotification(controller.listBeforeNotify.value[index].type)}] ',
                                                                          style: TextStyle(
                                                                              fontWeight: FontWeight.bold,
                                                                              color: const Color.fromRGBO(248, 129, 37, 1),
                                                                              fontSize: 14.sp)),
                                                                      TextSpan(
                                                                          text:
                                                                              ' ${controller.listBeforeNotify.value[index].title!}',
                                                                          style: const TextStyle(
                                                                              color: Color.fromRGBO(26, 26, 26, 1),
                                                                              fontSize: 16)),
                                                                    ])),
                                                                  ),
                                                                  Padding(
                                                                      padding: EdgeInsets.only(
                                                                          top: 4
                                                                              .h)),
                                                                  Text("${controller.listBeforeNotify.value[index].body}"),
                                                                  Padding(
                                                                      padding: EdgeInsets.only(
                                                                          top: 4
                                                                              .h)),
                                                                  Container(
                                                                    width: 270,
                                                                    margin: const EdgeInsets
                                                                            .only(
                                                                        right:
                                                                            4),
                                                                    child: Text(
                                                                      DateTimeUtils.convertToAgo(DateTime.fromMillisecondsSinceEpoch(controller
                                                                          .listBeforeNotify
                                                                          .value[
                                                                      index]
                                                                          .createdAt??0).toLocal()),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .left,
                                                                      style: const TextStyle(
                                                                          color: Color.fromRGBO(
                                                                              114,
                                                                              116,
                                                                              119,
                                                                              1),
                                                                          fontSize:
                                                                              12),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    bottom:
                                                                        32.h),
                                                            child:
                                                                PopupMenuButton(
                                                                    padding:
                                                                        EdgeInsets
                                                                            .zero,
                                                                    shape: const RoundedRectangleBorder(
                                                                        borderRadius:
                                                                            BorderRadius.all(Radius.circular(
                                                                                6.0))),
                                                                    icon:
                                                                        const Icon(
                                                                      Icons
                                                                          .more_horiz_outlined,
                                                                      color: Color.fromRGBO(
                                                                          177,
                                                                          177,
                                                                          177,
                                                                          1),
                                                                    ),
                                                                    itemBuilder:
                                                                        (context) {
                                                                      return [
                                                                        PopupMenuItem<
                                                                                int>(
                                                                            value:
                                                                                0,
                                                                            padding: EdgeInsets
                                                                                .zero,
                                                                            height:
                                                                                30,
                                                                            child:
                                                                                Column(
                                                                              children: [
                                                                                Padding(padding: EdgeInsets.only(top: 4.h)),
                                                                                Row(
                                                                                  children: [
                                                                                    Padding(padding: EdgeInsets.only(right: 8.w)),
                                                                                    SizedBox(
                                                                                      height: 20,
                                                                                      width: 20,
                                                                                      child: Image.asset("assets/images/image_pick_notify.png"),
                                                                                    ),
                                                                                    Padding(padding: EdgeInsets.only(right: 8.w)),
                                                                                    Text(
                                                                                      "Chọn nhiều thông báo",
                                                                                      style: TextStyle(fontSize: 14.sp),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                                const Divider(),
                                                                              ],
                                                                            )),
                                                                        PopupMenuItem<
                                                                            int>(
                                                                          padding:
                                                                              EdgeInsets.zero,
                                                                          value:
                                                                              1,
                                                                          height:
                                                                              24,
                                                                          child:
                                                                              Column(
                                                                            children: [
                                                                              Row(
                                                                                children: [
                                                                                  Padding(padding: EdgeInsets.only(right: 8.w)),
                                                                                  const Icon(
                                                                                    Icons.delete_rounded,
                                                                                    color: Colors.black,
                                                                                  ),
                                                                                  Padding(padding: EdgeInsets.only(right: 8.w)),
                                                                                  Text("Xóa thông báo")
                                                                                ],
                                                                              ),
                                                                              Divider()
                                                                            ],
                                                                          ),
                                                                        ),
                                                                        PopupMenuItem<
                                                                            int>(
                                                                          value:
                                                                              2,
                                                                          padding:
                                                                              EdgeInsets.zero,
                                                                          height:
                                                                              24,
                                                                          child:
                                                                              Column(
                                                                            children: [
                                                                              Row(
                                                                                children: [
                                                                                  Padding(padding: EdgeInsets.only(right: 8.w)),
                                                                                  const Icon(
                                                                                    Icons.check,
                                                                                    color: Colors.black,
                                                                                  ),
                                                                                  Padding(padding: EdgeInsets.only(right: 8.w)),
                                                                                  Text("Đánh dấu là đã đọc")
                                                                                ],
                                                                              ),
                                                                              Divider()
                                                                            ],
                                                                          ),
                                                                        ),
                                                                        PopupMenuItem<
                                                                            int>(
                                                                          value:
                                                                              3,
                                                                          padding:
                                                                              EdgeInsets.zero,
                                                                          height:
                                                                              30,
                                                                          child:
                                                                              Row(
                                                                            children: [
                                                                              Padding(padding: EdgeInsets.only(right: 8.w)),
                                                                              SizedBox(
                                                                                height: 20,
                                                                                width: 20,
                                                                                child: Image.asset("assets/images/image_share.png"),
                                                                              ),
                                                                              Padding(padding: EdgeInsets.only(right: 8.w)),
                                                                              const Text(
                                                                                "Chia sẻ thông báo",
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ];
                                                                    },
                                                                    onSelected:
                                                                        (value) {
                                                                      switch (
                                                                          value) {
                                                                        case 0:
                                                                          controller
                                                                              .isChooseManyBefore
                                                                              .value = true;
                                                                          break;
                                                                        case 1:
                                                                          controller
                                                                              .listIdNotifyBefore
                                                                              .value
                                                                              .add(controller.listBeforeNotify.value[index].id!);
                                                                          controller
                                                                              .deleteNotificationBefore(controller.listIdNotifyBefore);
                                                                          controller
                                                                              .listIdNotifyBefore
                                                                              .clear();
                                                                          break;
                                                                        case 2:
                                                                          controller
                                                                              .listIdNotifyBefore
                                                                              .value
                                                                              .add(controller.listBeforeNotify.value[index].id!);
                                                                          controller
                                                                              .seenNotificationBefore(controller.listIdNotifyBefore);
                                                                          controller
                                                                              .listIdNotifyBefore
                                                                              .clear();
                                                                          break;
                                                                      }
                                                                    }),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    (showLine)
                                                        ? const Divider(
                                                            indent: 10,
                                                            endIndent: 10,
                                                          )
                                                        : Container()
                                                  ],
                                                ),
                                              ),
                                              Visibility(
                                                  visible: controller
                                                          .listBeforeNotify
                                                          .value[index]
                                                          .status ==
                                                      "NOTSEEN",
                                                  child: const Positioned(
                                                      bottom: 32,
                                                      right: 24,
                                                      child: Icon(
                                                        Icons.circle,
                                                        color: Color.fromRGBO(
                                                            248, 129, 37, 1),
                                                        size: 10,
                                                      )))
                                            ],
                                          ));
                                    })
                          ],
                        ),
                      ),
                    ],
                  ),
                )),
          ),
          onRefresh: () async {
            controller.onRefresh();
          }),
    );
  }

  itemClassId(int index) {
    return InkWell(
        onTap: () {
          controller.classUId.value = controller.classOfTeacher.value[index];
          controller.getColorClass(controller.classUId.value, index);
          controller
              .getListNotificationByClass(controller.classUId.value.classId);
        },
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              width: 100,
              height: 26,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: controller.classOfTeacher.value[index].checked
                    ? const Color.fromRGBO(249, 154, 81, 1)
                    : const Color.fromRGBO(246, 246, 246, 1),
                borderRadius: BorderRadius.circular(6),
              ),
              margin: const EdgeInsets.only(right: 8),
              padding: const EdgeInsets.fromLTRB(12, 4, 12, 4),
              child: Text(
                "${controller.classOfTeacher.value[index].name}",
                style: TextStyle(
                    fontSize: 12,
                    color: controller.classOfTeacher.value[index].checked
                        ? Colors.white
                        : const Color.fromRGBO(90, 90, 90, 1)),
              ),
            ),
            (controller.classOfTeacher.value[index].homeroomClass == true)
                ? Positioned(
              right: 13,
              top: 4,
              child: Icon(
                Icons.star,
                size: 8,
                color: controller.classOfTeacher.value[index].checked
                    ? Colors.white
                    : const Color.fromRGBO(90, 90, 90, 1),
              ),
            )
                : Container()
          ],
        ));
  }
}
