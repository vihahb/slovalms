import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:task_manager/binding/Splash_binding.dart';
import 'package:task_manager/binding/app_binding.dart';
import 'package:task_manager/routes/app_pages.dart';
import 'package:task_manager/translations/localization_service.dart';
import 'commom/utils/global.dart';
import 'commom/utils/preference_utils.dart';
import 'package:firebase_core/firebase_core.dart';

import 'notification/notification_service.dart';
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await PreferenceUtils.init();
  // await AppCache().setUpNotificationSound();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  AppBinding().dependencies();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    LocalNotificationService.initialize(context);
    return ScreenUtilInit(
      splitScreenMode: true,
      minTextAdapt: true,
      designSize: const Size(360, 690),
      builder: (context, widget) => GetMaterialApp(
        title: 'LMS',
        color: Colors.white,
        debugShowCheckedModeBanner: false,
        // theme: ThemeData(useMaterial3: true, colorScheme: lightColorScheme),
        // darkTheme: ThemeData(useMaterial3: true, colorScheme: darkColorScheme),
        builder: EasyLoading.init(builder: ((context, widget) {
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
            child:GestureDetector(onTap: () => dismissKeyboard(), behavior: HitTestBehavior.translucent, child: widget!),
          );
        })),
        locale: LocalizationService.locale,
        translations: LocalizationService(),
        getPages: AppPages.pages,
        initialRoute: Routes.splash,
        initialBinding: SplashBinding(),
        localizationsDelegates: const [
          GlobalMaterialLocalizations.delegate,
          // DefaultCupertinoLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate, // Here !
          DefaultWidgetsLocalizations.delegate,
        ],
        supportedLocales: const [
          Locale('vi'),
          Locale('en'),
        ],
      ),
    );
  }
}
