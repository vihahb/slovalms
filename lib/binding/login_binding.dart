import 'package:get/get.dart';
import 'package:task_manager/view/mobile/account/login/login_controller.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(LoginController());
  }
}
