import 'package:get/get.dart';
import 'package:task_manager/view/mobile/splash/splash_controller.dart';

class SplashBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(SplashController());
  }
}
