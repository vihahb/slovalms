import 'package:get/get.dart';
import 'package:task_manager/view/mobile/home/home_controller.dart';
import 'package:task_manager/view/mobile/schedule/schedule_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ScheduleController());
    Get.put(HomeController());

  }
}
