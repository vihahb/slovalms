import 'package:get/get.dart';
import 'package:task_manager/commom/connectivity_service.dart';
import 'package:task_manager/view/mobile/schedule/schedule_controller.dart';

import '../notification/notification_controller.dart';

class AppBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ConnectivityService());
    Get.put(NotifyController());
  }
}
