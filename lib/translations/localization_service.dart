import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:task_manager/commom/constants/string_constant.dart';
import 'package:task_manager/commom/utils/preference_utils.dart';
import 'package:task_manager/translations/en_US/en_us_translations.dart';
import 'package:task_manager/translations/vi_vn/vi_vn_translations.dart';

class LocalizationService extends Translations {
  static final locale = _getLocaleFromLanguage();
  static var languaCodeStr = "vi";

  static final langCodes = [
    'vi',
    'en',
  ];
  static final locales = [
    Locale('vi', 'VN'),
    Locale('en', 'US'),
  ];

  static void changeLocale(String langCode) {
    final locale = _getLocaleFromLanguage(langCode: langCode);
    Get.updateLocale(locale);
    PreferenceUtils.setString(StringConstant.SAVELANGUAGE, langCode);
  }

  @override
  Map<String, Map<String, String>> get keys => {
        'en_US': en,
        'vi_VN': vi,
      };

  static Locale _getLocaleFromLanguage({String? langCode}) {
    if (langCode != null) {
      languaCodeStr = langCode;
      return locales.firstWhere((element) => element.languageCode == langCode);
    }
    String languageCode =
        PreferenceUtils.getString(StringConstant.SAVELANGUAGE);
    if (languageCode != "") {
      languaCodeStr = languageCode;
      return locales
          .firstWhere((element) => element.languageCode == languageCode);
    }
    languaCodeStr = "vi";

    return locales.first;
  }
}
