class StringConstant {
  static const String SAVETHEME = 'SaveTheme';
  static const String SAVELANGUAGE = 'SaveLanguage';
  static const String SAVELOGINID = 'SaveLoginId';
  static const String SAVE_INFO_LOGIN = 'IS_SAVE_LOGIN';
  static const String LOCATION =
      "https://www.google.com/maps/place/46+Ng%E1%BB%A5y+Nh%C6%B0+Kon+Tum,+Nh%C3%A2n+Ch%C3%ADnh,+Thanh+Xu%C3%A2n,+H%C3%A0+N%E1%BB%99i/@20.9984631,105.7978953,17z/data=!3m1!4b1!4m5!3m4!1s0x3135acbc84a95147:0x4719ea81640609df!8m2!3d20.9984504!4d105.800065";
  static const String FACEBOOK = "https://www.facebook.com/greenstock.vn";
  static const String WEB = "https://greenstock.vn";
  static const String PHONE_NUMBER = "tel:19004514";
  static const String EMAIL = "customer@greenstock.vn";
  static const String TERMS_OF_USE = "https://greenstock.vn/customize";
  static const String saveNotiSilent = "saveNotiApproveMoney";
  static const String saveNotiSound = "saveNotiApproveCommand";
  static const String saveVibration = "saveVibration";
}
