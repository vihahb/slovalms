import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:task_manager/commom/constants/string_constant.dart';
import 'package:task_manager/commom/utils/preference_utils.dart';

import '../data/model/res/auth/LoginResponse.dart';

class AppCache {
  static AppCache? _singleton;
  final storage = const FlutterSecureStorage();
  String? userId;
  LoginResponse? commonInfo;
  String? fcmToken;
  RxInt notificationCount = 0.obs;

  AppCache._();

  factory AppCache() {
    _singleton ??= AppCache._();
    // since you are sure you will return non-null value, add '!' operator
    return _singleton!;
  }

  bool isNotiCommand = true;
  bool isNotiMoney = true;
  String? _token;
  String? _userType;
  String? _schoolYearId;

  get token => _token;

  get userType => _userType;
  String? userName;

  get schoolYearId => _schoolYearId;
  void setToken(String token) {
    _token = token;
  }
  void setSchoolYearId(String schoolYearId) {
    _schoolYearId = schoolYearId;
  }

  void setUserType(String userType) {
    _userType = userType;
  }

  void saveLogin(String username, String password) async {
    await storage.write(key: "username", value: username);
    await storage.write(key: "password", value: password);
  }

  void deleteInfoLogin() async {
    await storage.delete(key: "username");
    await storage.delete(key: "password");
  }

  Future<String?> get username async {
    return await storage.read(key: "username");
  }

  Future<String?> get password async {
    return await storage.read(key: "password");
  }

  void saveLoginAuthen(bool isSave) {
    if (isSave) {
      PreferenceUtils.setBool(StringConstant.SAVELOGINID, isSave);
    } else {
      PreferenceUtils.delete(StringConstant.SAVELOGINID);
    }
  }

  void saveInfoLogin(bool isSave) {
    PreferenceUtils.setBool(StringConstant.SAVE_INFO_LOGIN, isSave);
  }

  bool get isSaveInfoLogin {
    bool isSave = PreferenceUtils.getBool(StringConstant.SAVE_INFO_LOGIN);
    return isSave;
  }

  bool get isSaveLoginAuthen {
    bool isSave = PreferenceUtils.getBool(StringConstant.SAVELOGINID);
    return isSave;
  }

  void setSound(isSave) async {
    await PreferenceUtils.setBool(StringConstant.saveNotiSound, isSave);
    setUpNotificationSound();
  }

  void setVibration(isSave) async {
    await PreferenceUtils.setBool(StringConstant.saveVibration, isSave);
  }

  bool get isSound {
    bool isSave = PreferenceUtils.getBool(StringConstant.saveNotiSound, true);
    return isSave;
  }

  bool get isVibration {
    bool isSave = PreferenceUtils.getBool(StringConstant.saveVibration, true);
    return isSave;
  }

  // checkIsVibration() {
  //   if (isSound && isVibration) {
  //     return true;
  //   } else if (isSound && !isVibration) {
  //     return false;
  //   } else if (!isSound && !isVibration) {
  //     return false;
  //   } else {
  //     return true;
  //   }
  // }
  resetCache() {
    commonInfo = null;
  }

  setUpNotificationSound() async {
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: isSound,
    );
  }
}
