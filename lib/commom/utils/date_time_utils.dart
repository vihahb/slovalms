import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:task_manager/commom/utils/color_utils.dart';

class DateTimeUtils {

  static const String DD_MM_YYYY_24 = "dd/MM/yyyy, HH:mm";
  static const String DD_MM_YYYY_12 = "dd/MM/yyyy, hh:mm";
  static const String DD_MM_YYYY = "dd/MM/yyyy";
  static const String DD_MM = "dd/MM";
  static const String HH_MM = "HH:mm";

  static String formatTime(String dateTime,
      {String format = "HH:mm", bool isConvert = true}) {
    if (dateTime == " " || dateTime == "") return " ";
    var inputFormat = DateFormat('yyyy-MM-ddTHH:mm:ssZZ');
    var inputDate = inputFormat
        .parse(dateTime)
        .add(new Duration(hours: (isConvert) ? 7 : 0));
    var outputFormat = DateFormat(format);
    if (format == "yyyy-MM-ddTHH:mm:ssZZ") {
      return formatISOTime(inputDate);
    }
    return outputFormat.format(inputDate);
  }

  static DateTime stringToDatetime(String dateTime,
      {String format = 'yyyy-MM-ddTHH:mm:ssZ'}) {
    var dateTime1 = DateFormat(format).parse(dateTime);
    return dateTime1;
  }

  static formatISOTime(DateTime date) {
    var duration = date.timeZoneOffset;
    if (duration.isNegative) {
      return (DateFormat("yyyy-MM-ddTHH:mm:ss.mmm").format(date) +
          "-${duration.inHours.toString().padLeft(2, '0')}${(duration.inMinutes - (duration.inHours * 60)).toString().padLeft(2, '0')}");
    } else {
      return (DateFormat("yyyy-MM-ddTHH:mm:ss.mmm").format(date) +
          "+${duration.inHours.toString().padLeft(2, '0')}${(duration.inMinutes - (duration.inHours * 60)).toString().padLeft(2, '0')}");
    }
  }

  static String formatTimeString(String dateTime,
      {String dateConvert = "dd/MM/yyyy",
      String format = "yyyy-MM-ddTHH:mm:ssZZ"}) {
    if (dateTime == "" || dateTime == " ") return "";
    var inputFormat = DateFormat(dateConvert);
    var inputDate = inputFormat.parse(dateTime);
    var outputFormat = DateFormat(format);
    if (format == "yyyy-MM-ddTHH:mm:ssZZ") {
      return formatISOTime(inputDate);
    }
    return outputFormat.format(inputDate);
  }

  static String subTrackDateTimeStr30(String dateTime,
      {int timeSubTrack = 30}) {
    var date = stringToDatetime(dateTime);
    var dateTimeSubTrack = date.subtract(Duration(days: timeSubTrack));
    String dateTimeStr = formatISOTime(dateTimeSubTrack);
    return dateTimeStr;
  }

  static String subTrackDateTime30(DateTime dateTime) {
    var dateTimeSubTrack =
        DateTime(dateTime.year, dateTime.month - 1, dateTime.day);
    String dateTimeStr = formatISOTime(dateTimeSubTrack);
    return dateTimeStr;
  }

  static String subTrackDateTime14(DateTime dateTime) {
    var dateTimeSubTrack =
        DateTime(dateTime.year, dateTime.month, dateTime.day - 14);
    String dateTimeStr = formatISOTime(dateTimeSubTrack);
    return dateTimeStr;
  }

  static Future<String> selectedDateTime({DateTime? initialDate}) async {
    String dateTimeStr = "";
    final DateTime? picked = await showDatePicker(
      context: Get.context!,
      locale: Localizations.localeOf(Get.context!),
      initialDate: initialDate ?? DateTime.now(),
      initialDatePickerMode: DatePickerMode.day,
      firstDate: DateTime(DateTime.now().year - 100),
      lastDate: DateTime(DateTime.now().year + 100),
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: const ColorScheme.light(
              primary: ColorUtils.BG_COLOR,
              onPrimary: Colors.white,
              onSurface: Colors.black,
            ),
          ),
          child: child!,
        );
      },
    );

    if (picked != null) {
      dateTimeStr = formatISOTime(picked);
    } else {
      // ignore: unnecessary_statements
      dateTimeStr != formatISOTime(picked ?? DateTime.now());
    }
    return dateTimeStr;
  }

  static int daysBetween(DateTime from, DateTime to) {
    from = DateTime(from.year, from.month, from.day);
    to = DateTime(to.year, to.month, to.day);
    return (to.difference(from).inDays);
  }

  static int checkDifferenceToDateFromDate(
      {required DateTime from, required DateTime to}) {
    from = DateTime(from.year, from.month, from.day);
    to = DateTime(to.year, to.month, to.day);
    return (from.difference(to).inHours / 24).round();
  }

  static String convertLongToStringTime(millis, {formatTime = "dd/MM/yyyy"}){
    // var millis = 978296400000;
    // var dt = DateTime.fromMillisecondsSinceEpoch(millis);

// 12 Hour format:
//     var d12 = DateFormat('MM/dd/yyyy, hh:mm a').format(dt); // 12/31/2000, 10:00 PM

// 24 Hour format:
//     var d24 = DateFormat('dd/MM/yyyy, HH:mm').format(dt); // 31/12/2000, 22:00

    var date = new DateTime.fromMillisecondsSinceEpoch(millis);
    var timeFormated = DateFormat(formatTime).format(date);
    print("Time formated: $timeFormated");
    return timeFormated;
  }

  static String convertToAgo(DateTime dateTime) {
    DateTime now = DateFormat('dd/MM/yyyy HH:mm:ss')
        .parse("${DateTime.now().day}/${DateTime.now().month}/${DateTime.now().year} ${DateTime.now().hour}:${DateTime.now().minute}:${DateTime.now().second}").toLocal();
    Duration diff = now.difference(dateTime);

    if (diff.inDays >= 1) {
      return '${diff.inDays} ngày trước';
    } else if (diff.inHours >= 1) {
      return '${diff.inHours} giờ trước';
    } else if (diff.inMinutes >= 1) {
      return '${diff.inMinutes} phút trước';
    } else if (diff.inSeconds >= 1) {
      return '${diff.inSeconds} giây trước';
    } else {
      return 'Vừa xong';
    }
  }
}
