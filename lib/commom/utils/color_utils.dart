import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ColorUtils {
  static const COLOR_WORK_TYPE_0 = Color(0xFFEC9C52);
  static const COLOR_WORK_TYPE_1 = Color(0xFFF6B501);
  static const COLOR_WORK_TYPE_2 = Color(0xFF4CA641);
  static const COLOR_WORK_TYPE_3 = Color(0xFF3A77FC);
  static const COLOR_WORK_TYPE_4 = Color(0xFFE25D5B);
  static const COLOR_BG_TOOLBAR = Color(0xFF1068B2);
  static const PRIMARY_COLOR =  Color.fromRGBO(248, 129, 37, 1);
  static const BG_TRANSLUCENT = Color(0xFFFFFF);
  static const BG_COLOR = Color(0xFF149c3e);
  static const BG_BASE1 = Color(0xFFFFFFFF);
  static const BG_BASE2 = Color(0xFFDDEFD1);
  static const colorF62727 = Color(0xFFF62727);
  static const BG_ACTIVE_BUTTON = Color(0xFF085426);
  static const BG_INACTIVE_BUTTON = Color(0xFFC4C4C4);
  static const BG_FOOTER = Color(0xFFC7E5B2);
  static const BG_HEADER_TAB = Color(0x99C7E5B2);
  static const BG_BUTTON_BUY = Color(0xFF116622);
  static const BG_BUTTON_SELL = Color(0xFFE63124);
  static const BG_STATUS_CANEL = Color(0xFFF97130);
  static const BG_STATUS_MATCH = Color(0xFF5132EE);
  static const COLOR_PRICE_STEP = Color(0xFFA0D37B);
  static const COLOR_PRICE_MATCH = Color(0xFFA0D37B);
  static const COLOR_TEXT_BLACK51 = Color(0xFF525151);
  static const COLOR_TEXT_BLACK87 = Color(0xFF181A26);
  static const COLOR_TEXT_GREY = Color(0xFFCBCACA);
  static const COLOR_BLACK = Color(0xFF000000);
  static const COLOR_WHITE = Color(0xFFFFFFFF);
  static const COLOR_PURPLE = Color(0xFFB64FF5);
  static const COLOR_YELLOW = Color(0xFFFFDE87);
  static const COLOR_RED = Color(0xFFF97130);
  static const COLOR_RED_1 = Color(0xFFE63124);
  static const COLOR_BLUE_ACCESS = Color(0xFF48C1CC);
  static const COLOR_GREEN_BOLD = Color(0xFF116622);
  static const COLOR_RED_BOLD = Color(0xFFFF0000);
  static const COLOR_GREEN = Color(0xFF60B523);
  static const text_set_command_color_1 = Color(0xFFB64FF5);
  static const text_set_command_color_2 = Color(0xFFFEC703);
  static const text_set_command_color_3 = Color(0xFF48C1CC);
  static const description_color = Color(0xFF6A7484);
  static const BORDER_1 = Color(0xFF90CB65);
  static const BORDER_2 = Color(0xFF868585);
  static const COLOR_ANALYST_LEFT = Color(0x6686EE17);
  static const COLOR_ANALYST_RIGHT = Color(0x4DFF0000);
  static const COLOR_SELECT_MATCHING = Color(0xFF093613);
  static const text_green = Color(0xFF86EE17);
  static const BG_BASE_BLACK2 = Color(0xFF242C39);
  static const titleColor = Color(0xFF64748B);
  static const bgItemColor = Color(0xFFF5F5F5);
  static const colorOrange = Color(0xFFEDA00A);
  static const colorGray = Color(0xFF99A099);
  static const textColor = Color(0xFF6E7A6F);
  static const boldWhiteColor = Color(0xFFFDFCFC);
  static const lightGreenColor = Color(0xFFDCFCE7);

  //Color chart
  static const COLOR_CHART1 = Color(0xFF006838);
  static const COLOR_CHART2 = Color(0xFF009445);
  static const COLOR_CHART3 = Color(0xFF39b54a);
  static const COLOR_CHART4 = Color(0xFF8dc73f);
  static const COLOR_CHART5 = Color(0xFF68499e);
  static const COLOR_CHART6 = Color(0xFF662d91);
  static const COLOR_CHART7 = Color(0xFF8575e0);
  static const COLOR_CHART8 = Color(0xFFf9ed32);
  static const COLOR_CHART9 = Color(0xFFf8f194);
  static const COLOR_CHART10 = Color(0xFFf2f5cd);
  static const COLOR_CHART11 = Color(0xFFf2f5cd);
  static const COLOR_CHART12 = Color(0xFFf04136);
  static const COLOR_CHART13 = Color(0xFFf04136);
  static const COLOR_CHART14 = Color(0xFFf7941e);
  static const COLOR_CHART15 = Color(0xFF2b3890);
  static const COLOR_CHART16 = Color(0xFF1c75bc);
  static const COLOR_CHART17 = Color(0xFF28aae1);
  static const COLOR_CHART18 = Color(0xFF77b3e1);
  static const COLOR_CHART19 = Color(0xFFb5d4ef);
  static const COLOR_CHART20 = Color(0xFF7b5231);
  static const COLOR_CHART21 = Color(0xFF949597);

  static const text_list = Color(0xFF525151);
  static const line_list = Color(0xFFCBCACA);

  static List<Color> colors = [
    COLOR_CHART15,
    COLOR_CHART12,
    COLOR_CHART8,
    COLOR_CHART3,
    COLOR_CHART5,
    COLOR_CHART20,
    COLOR_CHART14,
    COLOR_CHART16,
    COLOR_CHART17
  ];

  static charts.Color getChartColor(int index) {
    Color color;
    if (index > colorsChartsPie.length - 1) {
      color = colorsChartsPie[index - (colorsChartsPie.length)];
    } else {
      color = colorsChartsPie[index];
    }
    return charts.Color(
        r: color.red, g: color.green, b: color.blue, a: color.alpha);
  }

  static charts.Color getPieChartTextColor(Color color) {
    return charts.Color(
        r: color.red, g: color.green, b: color.blue, a: color.alpha);
  }

  static Color getColors(int index) {
    Color color;
    if (index > colorsChartsPie.length - 1) {
      color = colorsChartsPie[index - (colorsChartsPie.length)];
    } else {
      color = colorsChartsPie[index];
    }
    return color;
  }

  static Color? checkDisplayColorStock({
    num lastPrice = 0, //Giá cuối
    num ceilingPrice = 0, // Giá trần
    num floorPrice = 0, // Giá sàn
    num basicPrice = 0, // Giá tham chiếu
    Color colorGreen = COLOR_GREEN,
  }) {
    if (lastPrice ==
        ceilingPrice /*Nếu: LAST_PRICE = giá trần thì hiển thị màu tím*/)
      return COLOR_PURPLE;
    if (lastPrice >=
        ceilingPrice /*Nếu: LAST_PRICE >= giá trần thì hiển thị màu tím*/)
      return COLOR_PURPLE;
    if (lastPrice ==
        floorPrice /*Nếu: LAST_PRICE = giá sàn => màu xanh da trời*/)
      return COLOR_BLUE_ACCESS;
    if (lastPrice <=
        floorPrice /*Nếu: LAST_PRICE <= giá sàn => màu xanh da trời*/)
      return COLOR_BLUE_ACCESS;
    if (lastPrice ==
        basicPrice /*Nếu: LAST_PRICE = giá tham chiếu => màu vàng*/)
      return COLOR_YELLOW;
    if (basicPrice < lastPrice &&
        lastPrice <
            ceilingPrice /*Nếu: giá tham chiếu <  LAST_PRICE < giá trần => màu xanh lá cây*/)
      return colorGreen;
    if (floorPrice < lastPrice &&
        lastPrice <
            basicPrice /*Nếu: giá sàn < LAST_PRICE < giá tham chiếu => màu đỏ*/)
      return COLOR_RED_1;
  }

  static Color getColorChangeTotalQTY() {
    return COLOR_YELLOW;
  }

  static Color getColorChangeIndex(num index) {
    if (index != null) {
      if (index > 0)
        return COLOR_GREEN;
      else if (index == 0)
        return COLOR_YELLOW;
      else
        return COLOR_RED_1;
    } else
      return COLOR_YELLOW;
  }

  static Color checkColorProfit(num value) {
    if (value < 0) {
      return BG_BUTTON_SELL;
    } else {
      return Get.context!.theme.splashColor;
    }
  }

  static getColorType(index) {
    return (index == 1)
        ? ColorUtils.COLOR_RED_BOLD
        : Get.context!.theme.colorScheme.secondary;
  }

  static List<Color> colorsChartsPie = [
    ColorUtils.COLOR_CHART1,
    ColorUtils.COLOR_CHART2,
    ColorUtils.COLOR_CHART3,
    ColorUtils.COLOR_CHART4,
    ColorUtils.COLOR_CHART5,
    ColorUtils.COLOR_CHART6,
    ColorUtils.COLOR_CHART7,
    ColorUtils.COLOR_CHART8,
    ColorUtils.COLOR_CHART9,
    ColorUtils.COLOR_CHART10,
    ColorUtils.COLOR_CHART11,
    ColorUtils.COLOR_CHART12,
    ColorUtils.COLOR_CHART13,
    ColorUtils.COLOR_CHART14,
    ColorUtils.COLOR_CHART15,
    ColorUtils.COLOR_CHART16,
    ColorUtils.COLOR_CHART17,
    ColorUtils.COLOR_CHART18,
    ColorUtils.COLOR_CHART19,
    ColorUtils.COLOR_CHART20,
    ColorUtils.COLOR_CHART21,
  ];
}
