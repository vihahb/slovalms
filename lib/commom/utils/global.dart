import 'package:flutter/material.dart';
import 'package:get/get.dart';

// Widget svgIcon(String assetName) {
//   return SvgPicture.asset("assets/icons/" + assetName);
// }
// var themeColor = Theme.of(Get.context!);

String getAssetsIcon(String icon) {
  return "assets/icons/$icon";
}

String getAssetsImage(String image) {
  return "assets/images/$image";
}

void dismissKeyboard() {
  FocusScope.of(Get.context!).requestFocus(FocusNode());
}
