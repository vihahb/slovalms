import 'package:flutter/material.dart';
import 'package:task_manager/commom/utils/textstyle.dart';
import 'package:task_manager/commom/widget/textfield_cusstom.dart';

/// Được tạo bởi Phạm Nhớ từ 29/08/2022
/// mọi hành vi sao chép cần được sự cho phép

Widget buildRowItemFilter(
    {String? hintText,
    required String title,
    required TextEditingController controller}) {
  return Row(
    children: [
      Expanded(
          flex: 1,
          child: Text(
            title,
            style: TextStyleUtils.sizeText12Weight500(),
          )),
      Expanded(
        flex: 2,
        child: TextFieldFilter(
          hintText: hintText,
          controller: controller,
        ),
      ),
    ],
  );
}
