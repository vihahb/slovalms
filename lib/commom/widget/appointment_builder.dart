import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:task_manager/routes/app_pages.dart';

import '../app_cache.dart';

Widget appointmentBuilder(BuildContext context,
    CalendarAppointmentDetails calendarAppointmentDetails) {
  final Appointment appointment = calendarAppointmentDetails.appointments.first;
  return InkWell(
    onTap: () {
      Get.toNamed(Routes.detailJobByTeacherInTimeTable, arguments: appointment);
    },
    child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.r),
          color: const Color.fromRGBO(254, 230, 211, 1),
        ),
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              textAlign: TextAlign.center,
             appointment.subject,
              style: TextStyle(color: const Color.fromRGBO(249, 154, 81, 1), fontSize: 11.sp),
            ),
            Padding(padding: EdgeInsets.only(left: 4.w)),
            SizedBox(width: 10.w, height: 10.w, child: Image.asset("assets/images/iconClock.png"),),
            Text(
              '${DateFormat(' (HH:mm').format(appointment.startTime)} - ${DateFormat('HH:mm)').format(appointment.endTime)}',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 11.sp),
            ),
          ],
        )),
  );
}
