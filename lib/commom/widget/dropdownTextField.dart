import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:task_manager/commom/utils/color_utils.dart';
import 'package:task_manager/commom/utils/textstyle.dart';

// ignore: must_be_immutable
class CusstomDropdown extends StatelessWidget {
  CusstomDropdown(
      {Key? key,
      this.onSaved,
      this.onChanged,
      this.value,
      this.iconEnabledColor,
      this.suffixIcon,
      this.prefixIcon,
      this.radius = 8,
      this.isDense = true,
      this.buttonHeight,
      this.style,
      this.iconDisabledColor,
      this.borderColor = ColorUtils.BG_COLOR,
      this.buttonPadding,
      this.hint,
      required this.map,
      this.labelStyle,
      this.labelText,
      this.hintStyle,
      this.filled = false})
      : super(key: key);
  String? selectedValue;
  final _formKey = GlobalKey<FormState>();
  final Function(String?)? onSaved;
  final Function(String?)? onChanged;
  final String? value;
  final Color? iconEnabledColor;
  final Color? iconDisabledColor;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final bool isDense;
  final Color borderColor;
  final double radius;
  final double? buttonHeight;
  final TextStyle? style;
  final EdgeInsetsGeometry? buttonPadding;
  final TextStyle? labelStyle;
  final String? labelText;
  final Widget? hint;
  final TextStyle? hintStyle;
  final Map<String, dynamic> map;
  final bool filled;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: DropdownButtonFormField2(
        hint: hint,
        buttonPadding: buttonPadding,
        buttonHeight: buttonHeight,
        value: value,
        style: style,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(left: 8.w),
          labelText: labelText,
          labelStyle: labelStyle ??
              TextStyleUtils.sizeText15Weight500()?.copyWith(
                color: ColorUtils.textColor,
              ),
          hintStyle: hintStyle,
          fillColor: Colors.grey.shade100,
          filled: filled,
          suffixIcon: suffixIcon,
          suffixIconConstraints: BoxConstraints(
            minHeight: 6.h,
            minWidth: 8.w,
          ),
          prefixIcon: prefixIcon,
          prefixIconConstraints: BoxConstraints(
            minHeight: 6.h,
            minWidth: 8.w,
          ),
          isDense: isDense,
          border: OutlineInputBorder(
            borderSide: BorderSide(color: borderColor),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(radius),
            borderSide: BorderSide(color: borderColor),
          ),
          disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(radius),
            borderSide: BorderSide(color: borderColor),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(radius),
            borderSide: BorderSide(color: borderColor),
          ),
        ),
        dropdownDecoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
        ),
        items: map.entries
            .map((item) => DropdownMenuItem<String>(
                  value: item.value.toString(),
                  child: Text(
                    item.key,
                    style: style,
                  ),
                ))
            .toList(),
        isExpanded: true,
        onChanged: onChanged,
        iconEnabledColor: iconEnabledColor,
        iconDisabledColor: iconDisabledColor,
        onSaved: onSaved,
      ),
    );
  }
}
