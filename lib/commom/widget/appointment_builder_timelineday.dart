import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:get/get.dart';
import 'package:task_manager/routes/app_pages.dart';
import 'package:task_manager/view/mobile/schedule/teaching_schedule/teaching_schedule_page.dart';

import '../../data/model/common/schedule.dart';
import '../../view/mobile/role/teacher/teacher_home_controller.dart';
import '../app_cache.dart';

Widget appointmentBuilderTimeLineDay(BuildContext context,
    CalendarAppointmentDetails calendarAppointmentDetails) {
  final Appointment appointment = calendarAppointmentDetails.appointments.first;
  var outputFormat = DateFormat('HH:mm a');
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  OnDialog(){
    return
     Dialog(
       child:
     IntrinsicHeight(
       child:   Container(
         padding: const EdgeInsets.all(16),
         child: Column(
           children: [
             Row(
               mainAxisAlignment: MainAxisAlignment.spaceBetween,
               children: [
                 Text("Thông tin môn học", style: TextStyle(color:Color.fromRGBO(26, 26, 26, 1), fontSize: 14.sp, fontWeight: FontWeight.w500 ),),
                 InkWell(
                   onTap: (){
                    Get.toNamed(Routes.detailJobByTeacherInTimeTable, arguments: appointment);
                   },
                   child: Container(
                     alignment: Alignment.center,
                     width: 100.w,
                     height: 16.h,
                     decoration: BoxDecoration(
                         color: const Color.fromRGBO(248, 129, 37, 1),
                         borderRadius: BorderRadius.circular(8)
                     ),
                     child: Text('Xem chi tiết', textAlign: TextAlign.center,style: TextStyle(color: Colors.white, fontSize: 10.sp, fontWeight: FontWeight.w400),),
                   ),
                 )
               ],
             ),
             Padding(padding: EdgeInsets.only(top: 16.h)),
             Container(
               decoration: BoxDecoration(
                   borderRadius: BorderRadius.circular(6),
                   color: Colors.white),
               padding: const EdgeInsets.all(16),
               child: Column(
                 children: [
                   Row(
                     children: [
                       const Text(
                         "Môn học :",
                         style: TextStyle(
                             fontSize: 14,
                             color: Color.fromRGBO(26, 26, 26, 1),
                             fontWeight: FontWeight.w400),
                       ),
                       Expanded(child: Container()),
                        Text("${appointment.subject}",
                           style:  TextStyle(
                               fontSize: 14.sp,
                               color: Color.fromRGBO(26, 26, 26, 1),
                               fontWeight: FontWeight.w500))
                     ],
                   ),
                   const Padding(padding: EdgeInsets.only(top: 16)),
                   Row(
                     children: [
                       const Text(
                         "Lớp :",
                         style: TextStyle(
                             fontSize: 14,
                             color: Color.fromRGBO(26, 26, 26, 1),
                             fontWeight: FontWeight.w400),
                       ),
                       Expanded(child: Container()),
                        Text("${appointment.notes}",
                           style: TextStyle(
                               fontSize: 14.sp,
                               color: Color.fromRGBO(248, 129, 37, 1),
                               fontWeight: FontWeight.w500))
                     ],
                   ),
                   const Padding(padding: EdgeInsets.only(top: 16)),
                   Row(
                     children: [
                       const Text(
                         "Thời gian bắt đầu :",
                         style: TextStyle(
                             fontSize: 14,
                             color: Color.fromRGBO(26, 26, 26, 1),
                             fontWeight: FontWeight.w400),
                       ),
                       Expanded(child: Container()),
                        Text("${ outputFormat.format(appointment.startTime)} ",
                           style:  TextStyle(
                               fontSize: 14.sp,
                               color: const Color.fromRGBO(26, 26, 26, 1),
                               fontWeight: FontWeight.w500))
                     ],
                   ),
                   const Padding(padding: EdgeInsets.only(top: 16)),
                   Row(
                     children: [
                       const Text(
                         "Thời gian kết thúc :",
                         style: TextStyle(
                             fontSize: 14,
                             color: Color.fromRGBO(26, 26, 26, 1),
                             fontWeight: FontWeight.w400),
                       ),
                       Expanded(child: Container()),
                       Text("${outputFormat.format(appointment.endTime!)}",
                           style: TextStyle(
                               fontSize: 14.sp,
                               color: Color.fromRGBO(26, 26, 26, 1),
                               fontWeight: FontWeight.w500))
                     ],
                   ),
                   const Padding(padding: EdgeInsets.only(top: 16)),
                   Row(
                     children: [
                       const Text(
                         "Ngày :",
                         style: TextStyle(
                             fontSize: 14,
                             color: Color.fromRGBO(26, 26, 26, 1),
                             fontWeight: FontWeight.w400),
                       ),
                       Expanded(child: Container()),
                        Text(outputDateFormat.format(appointment.startTime),
                           style: TextStyle(
                               fontSize: 14.sp,
                               color: const Color.fromRGBO(26, 26, 26, 1),
                               fontWeight: FontWeight.w500))
                     ],
                   ),
                   const Padding(padding: EdgeInsets.only(top: 16)),
                   AppCache().userType != "TEACHER"? Row(
                     children: [
                       Text(
                         "Giáo viên: ",
                         style: TextStyle(
                             fontSize: 14,
                             color: Color.fromRGBO(26, 26, 26, 1),
                             fontWeight: FontWeight.w400),
                       ),
                       Expanded(child: Container()),
                       Text("${appointment.recurrenceId}",
                           style: TextStyle(
                               fontSize: 14.sp,
                               color: Color.fromRGBO(26, 26, 26, 1),
                               fontWeight: FontWeight.w500))
                     ],
                   )
                       : Container(),
                 ],
               ),
             )
           ],
         ),
       ),
     ),
     );
  }
  return
    InkWell(
      onTap: (){
        Get.dialog(OnDialog());
      },
      child: Container(
        padding: const EdgeInsets.all(4),
          height: calendarAppointmentDetails.bounds.height,
          color: appointment.color,
          alignment: Alignment.center,
          width: calendarAppointmentDetails.bounds.width,
          child: Text(
            AppCache().userType == "TEACHER" ? appointment.notes!:appointment.subject,
            style: const TextStyle(color: Colors.white, fontSize: 12),
          )
      ),
    );


}

