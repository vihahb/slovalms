import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:task_manager/commom/app_cache.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/routes/app_pages.dart';
import 'package:task_manager/view/mobile/home/home_controller.dart';
import '../commom/utils/app_utils.dart';
import '../commom/utils/color_utils.dart';
import '../commom/widget/custom_view.dart';
import '../view/mobile/notification/notification_controller.dart';
import 'notification_service.dart';
import 'package:device_info/device_info.dart';

class NotifyController extends GetxController {
  FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;

  @override
  void onInit() {
    configNotify();
    super.onInit();
  }

  configNotify() async {
    final token = await FirebaseMessaging.instance.getToken();
    print("FCM TOKEN: $token");
    AppCache().fcmToken = token;

    ///gives you the message on which user taps
    ///and it opened the app from terminated state
    FirebaseMessaging.instance.getInitialMessage().then((message) {
    });
    // // backgroud work
    // FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);


    ///forground work
    FirebaseMessaging.onMessage.listen((message) {
      if (message.notification != null) {

        LocalNotificationService.display(message);

        if (Get.isRegistered<HomeController>()) {
          Get.find<HomeController>().getCountNotifyNotSeen();
        }
        if (Get.isRegistered<NotificationController>()) {
          Get.find<NotificationController>().onRefresh();
        }

        // if (Get.isRegistered<NotificationNewController>()) {
        //   Get.find<NotificationNewController>().onRefresh();
        // }
        // if (Get.isRegistered<NotificationSeenAllController>()) {
        //   Get.find<NotificationSeenAllController>().onRefresh();
        // }
        // try {
        //   var messageTitle = message.notification?.title;
        //   var messageBody = message.notification?.body;
        //   var actionTitle = "Xem";
        //   Widget child = Center(
        //     child: Text(messageBody ?? "",
        //         textAlign: TextAlign.center,
        //         style: const TextStyle(
        //           color: ColorUtils.COLOR_TEXT_BLACK51,
        //           fontSize: 15,
        //           fontWeight: FontWeight.w700,
        //         )),
        //   );
        //   Get.bottomSheet(
        //     getDialogConfirm(
        //         messageTitle ?? "",
        //         Container(
        //           padding:
        //           const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        //           child: child,
        //         ),
        //         btnLeft: "Đóng",
        //         colorBtnOk: ColorUtils.COLOR_GREEN_BOLD,
        //         colorTextBtnOk: ColorUtils.COLOR_WHITE,
        //         btnRight: actionTitle, funcLeft: () {
        //       Get.back();
        //     }, funcRight: () {
        //       Get.back();
        //       toNotify();
        //     }),
        //   );
        // } catch (e) {}
      }
    });

    ///When the app is in background but opened and user taps
    ///on the notification
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      // AppUtils.shared.snackbarSuccess("Thông báo", "When the app is in background but opened and user taps");
    });
  }

  void toNotify() {
    // Get.toNamed(Routes.home);
    // Get.find<HomeController>().comeNotification();
  }

  Future _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
    LocalNotificationService.display(message);
  }
}
