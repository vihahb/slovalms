import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:task_manager/commom/utils/app_utils.dart';
import 'package:task_manager/view/mobile/home/home_controller.dart';

import '../routes/app_pages.dart';

class LocalNotificationService {
  static final FlutterLocalNotificationsPlugin _notificationsPlugin =
      FlutterLocalNotificationsPlugin();

  static void initialize(BuildContext context) async {
    const InitializationSettings initializationSettings =
        InitializationSettings(
            android: AndroidInitializationSettings("@mipmap/ic_launcher"),
            iOS: DarwinInitializationSettings());
    _notificationsPlugin.initialize(
      initializationSettings,
      onDidReceiveNotificationResponse: (details) async {
        goNotify();
      },
    );

    // await FirebaseMessaging.instance
    //     .setForegroundNotificationPresentationOptions(
    //   alert: false,
    //   badge: false,
    //   sound: false,
    // );
  }

  static void display(RemoteMessage message) async {
    try {
      final id = DateTime.now().millisecondsSinceEpoch ~/ 1000;
      // if (Platform.isAndroid) {
      //
      // }
      NotificationDetails notificationDetails = NotificationDetails(
          android: AndroidNotificationDetails(
            "SLOVA ${DateTime.now().millisecond}", "SLOVA - LMS",
            importance: Importance.max,
            priority: Priority.high,
            icon: "@mipmap/ic_launcher",
            // enableVibration: AppCache().checkIsVibration()
          ),
          iOS: DarwinNotificationDetails());
      await _notificationsPlugin.show(
        id,
        message.notification?.title,
        message.notification?.body,
        notificationDetails,
      );
    } on Exception catch (e) {
      AppUtils.shared.printLog(e);
    }
  }
}

Future<void> goNotify() async {
  Get.toNamed(Routes.home);
  if (Get.isRegistered<HomeController>()) {
    Get.find<HomeController>().comeNotification();
  }
}

Future<void> goNotifyOnBackGround() async {
  Get.toNamed(Routes.splash);
  if (Get.isRegistered<HomeController>()) {
    Get.find<HomeController>().comeNotification();
  }
}
