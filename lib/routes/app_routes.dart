part of './app_pages.dart';

abstract class Routes {
  static const splash = '/splash';
  static const login = '/login';
  static const recoveryPass = '/recoveryPass';
  static const retrievingCode = '/retrievingCode';
  static const resetPass = '/resetPass';
  static const home = '/homePage';
  static const dashboardStudent = '/dashboardStudent';
  static const dashboardParent = '/dashboardParent';
  static const dashboardTeacher = '/dashboardTeacher';
  static const dashboardManager = '/dashboardManager';
  static const schedule = '/schedule';
  static const scheduleTeaching = '/scheduleTeaching';
  static const personalInformation = '/personalInformation';
  static const subject = '/subject';
  static const detailPhonebook = '/detailPhonebook';
  static const updateInfoTeacher = '/updateInfoTeacher';
  static const updateInfoStudent = '/updateInfoStudent';
  static const updateInfoManager = '/updateInfoManager';
  static const updateInfoParent = '/updateInfoParent';
  static const studentListPage = '/studentListPage';
  static const personalInfoParent = '/personalInfoParent';
  static const schedulePage = '/schedulePage';
  static const changePass = '/changePass';
  static const getavatar = '/getavatar';
  static const staticPage = '/staticPage';
  static const infoInPhoneBook= '/infoInPhoneBook';
  static const listClassInBlock= '/listClassInBlock';
  static const detailJobByTeacherInTimeTable= '/detailJobByTeacherInTimeTable';
  static const listClassInBlockDiligentManager= '/detaillistclassbymanager';
  static const detailDiligentByClassManager= '/detailDiligentByClassManager';
  static const detailAppoinment= '/detailappoinment';
  static const detailDiligenceStudent= '/detailDiligenceStudent';
  static const listDiligence= '/listDiligence';
  static const diligenceStudent= '/diligenceStudent';
  static const diligenceParentPage= '/diligenceParentPage';

  static const approvalParent= '/approvalParent';
  static const createALeavingApplication= '/createALeavingApplication';
  static const editLeavingApplication= '/editLeavingApplication';
  static const cancelLeavingApplication= '/cancelLeavingApplication';
  static const diligentManagementTeacher= '/diligentManagementTeacher';
  static const detailPendingLeaveApplicationTeacher= '/detailPendingLeaveApplicationTeacher';
  static const detailApprovedLeaveApplicationTeacher= '/detailApprovedLeaveApplicationTeacher';
  static const detailCancelLeaveApplicationTeacher= '/detailCancelLeaveApplicationTeacher';
  static const detailstatisticalparentpage= '/detailstatisticalparentpage';
  static const detailDiligentManagementTeacherPage= '/detailDiligentManagementTeacherPage';
  static const managerLeaveApplicationTeacherPage= '/managerLeaveApplicationTeacherPage';
  static const detailLeaveApplicationParentPage= '/detailLeaveApplicationParentPage';
  static const detailAllStasticalPage= '/detailAllStasticalPage';
  static const detailAllStasticalPageParent= '/detailAllStasticalPageParent';

}
