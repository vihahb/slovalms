import 'package:get/get.dart';
import 'package:task_manager/binding/Splash_binding.dart';
import 'package:task_manager/view/mobile/account/changePassword/change_pass_page.dart';
import 'package:task_manager/view/mobile/account/recovery/password_recovery_page.dart';
import 'package:task_manager/view/mobile/account/resetPass/reset_pass_page.dart';
import 'package:task_manager/view/mobile/account/verifyCode/verify_code_page.dart';
import 'package:task_manager/view/mobile/role/manager/diligent_management_manager/detail_list_class_by_manager/list_class_in_block_diligent_manager_page.dart';
import 'package:task_manager/view/mobile/role/parent/diligence_parent/statistical_parent/detail_statistical_parent/detail_all_stastical_parent_page.dart';
import 'package:task_manager/view/mobile/role/parent/diligence_parent/statistical_parent/detail_statistical_parent/detail_statistical_parent_page.dart';
import 'package:task_manager/view/mobile/role/parent/parent_home_page.dart';
import 'package:task_manager/view/mobile/role/student/diligence_student/statistical_student/detail_statistical_student/detail_all_stastical_page.dart';
import 'package:task_manager/view/mobile/role/student/diligence_student/statistical_student/detail_statistical_student/detail_statistical_student_page.dart';
import 'package:task_manager/view/mobile/role/student/personal_information_student/detail_avatar/detail_avatar_page.dart';
import 'package:task_manager/view/mobile/role/student/personal_information_student/personal_information_student_page.dart';
import 'package:task_manager/view/mobile/role/student/student_home_page.dart';
import 'package:task_manager/view/mobile/role/student/subjects/subjects_page.dart';
import 'package:task_manager/view/mobile/role/teacher/student_list/student_list_page.dart';
import 'package:task_manager/view/mobile/schedule/schedule_page.dart';
import 'package:task_manager/view/mobile/schedule/teaching_schedule/teaching_schedule_page.dart';
import 'package:task_manager/view/mobile/splash/splash_page.dart';
import '../binding/dashboard_binding.dart';
import '../binding/login_binding.dart';
import '../view/mobile/account/login/login_page.dart';
import '../view/mobile/home/home_page.dart';
import '../view/mobile/phonebook/detail_phonebook/Detail_information_by_phonebook/detail_information_phonebook_page.dart';
import '../view/mobile/phonebook/detail_phonebook/detail_phonebook_page.dart';
import '../view/mobile/role/manager/diligent_management_manager/detail_list_class_by_manager/detail_diligent_by_class_manager/detail_diligent_by_class_manager_page.dart';
import '../view/mobile/role/manager/schedule_manager/list_class_in_block_manager/list_class_in_block_manager_page.dart';
import '../view/mobile/role/manager/update_infomation_manager/update_information_manager_page.dart';
import '../view/mobile/role/parent/diligence_parent/approval_parent/approval_parent_page.dart';
import '../view/mobile/role/parent/diligence_parent/approval_parent/cancel_leaving_application/cancel_leave_application_page.dart';
import '../view/mobile/role/parent/diligence_parent/approval_parent/create_a_leave_application/create_a_leave_application_page.dart';
import '../view/mobile/role/parent/diligence_parent/approval_parent/detail_leave_application/detail_leave_application_page.dart';
import '../view/mobile/role/parent/diligence_parent/approval_parent/edit_leaving_application/edit_leaving_application_page.dart';
import '../view/mobile/role/parent/diligence_parent/diligence_parent_page.dart';
import '../view/mobile/role/parent/personal_information_parent/personal_information_parent_page.dart';
import '../view/mobile/role/parent/personal_information_parent/update_infomation_parent/update_infomation_parent_page.dart';
import '../view/mobile/role/student/diligence_student/diligence_student_page.dart';
import '../view/mobile/role/student/personal_information_student/update_information_student/update_information_student_page.dart';
import '../view/mobile/role/teacher/diligent_management_teacher/date_picker_diligent_teacher/detail_diligent_management_teacher/detail_diligent_management_teacher_page.dart';
import '../view/mobile/role/teacher/diligent_management_teacher/diligent_management_teacher_page.dart';
import '../view/mobile/role/teacher/diligent_management_teacher/manager_leave_application_teacher/approved_leave_application_teacher/detail_approved_leave_application_teacher/detail_approved_leave_application_teacher_page.dart';
import '../view/mobile/role/teacher/diligent_management_teacher/manager_leave_application_teacher/cancel_leave_application_teacher/detail_cancel_leave_application_teacher/detail_cancel_leave_application_teacher_page.dart';
import '../view/mobile/role/teacher/diligent_management_teacher/manager_leave_application_teacher/manager_leave_application_teacher_page.dart';
import '../view/mobile/role/teacher/diligent_management_teacher/manager_leave_application_teacher/pending_teacher/detail_pending_teacher/detail_pending_teacher_page.dart';
import '../view/mobile/role/teacher/update_infomation_teacher/update_infomation_teacher_page.dart';
import '../view/mobile/staticPage/static_page_page.dart';

part './app_routes.dart';

class AppPages {
  static final pages = [
    GetPage(
        name: Routes.splash,
        page: () => SplashPage(),
        binding: SplashBinding()),
    GetPage(
        name: Routes.login, page: () => LoginPage(), binding: LoginBinding()),
    GetPage(name: Routes.recoveryPass, page: () => PaswordRecoveryPage()),
    GetPage(name: Routes.retrievingCode, page: () => VerifyCodePage()),
    GetPage(name: Routes.resetPass, page: () => ResetPassPage()),
    GetPage(name: Routes.home, page: () => HomePage(), binding: HomeBinding()),
    GetPage(name: Routes.dashboardParent, page: () => ParentHomePage()),
    GetPage(name: Routes.dashboardStudent, page: () => StudentHomePage()),
    GetPage(name: Routes.schedule, page: () => SchedulePage()),
    GetPage(name: Routes.scheduleTeaching, page: () => TeachingSchedulePage()),
    GetPage(
        name: Routes.personalInformation,
        page: () => PersonalInformationStudentPage()),
    GetPage(name: Routes.subject, page: () => SubjectPage()),
    GetPage(name: Routes.detailPhonebook, page: () => DetailPhoneBook()),
    GetPage(
        name: Routes.updateInfoTeacher, page: () => UpdateInfoTeacherPage()),
    GetPage(
        name: Routes.updateInfoStudent, page: () => UpdateInfoStudentPage()),
    GetPage(
        name: Routes.updateInfoManager, page: () => UpdateInfoManagerPage()),
    GetPage(name: Routes.updateInfoParent, page: () => UpdateInfoParentPage()),
    GetPage(name: Routes.studentListPage, page: () => StudentListPage()),
    GetPage(
        name: Routes.personalInfoParent,
        page: () => PersonalInformationParentPage()),
    GetPage(name: Routes.schedulePage, page: () => SchedulePage()),
    GetPage(name: Routes.changePass, page: () => ChangePassPage()),
    GetPage(name: Routes.getavatar, page: () => DetailAvatarPage()),
    GetPage(name: Routes.staticPage, page: () => StaticPage()),
    GetPage(
        name: Routes.listClassInBlock,
        page: () => ListClassInBlockManagerPage()),
    GetPage(
        name: Routes.detailJobByTeacherInTimeTable,
        page: () => TeachingSchedulePage()),
    GetPage(
        name: Routes.infoInPhoneBook,
        page: () => DetailInformationPhoneBookPage()),
    GetPage(
        name: Routes.listClassInBlockDiligentManager,
        page: () => ListClassInBlockDiligentManagerPage()),
    GetPage(name: Routes.approvalParent, page: () => ApprovalParentPage()),
    GetPage(
        name: Routes.createALeavingApplication,
        page: () => CreateALeavingApplicationPage()),
    GetPage(
        name: Routes.editLeavingApplication,
        page: () => EditLeavingApplicationPage()),
    GetPage(
        name: Routes.cancelLeavingApplication,
        page: () => CancelLeaveApplicationPage()),
    GetPage(
        name: Routes.diligentManagementTeacher,
        page: () => DiligentManagementTeacherPage()),
    GetPage(
        name: Routes.detailPendingLeaveApplicationTeacher,
        page: () => DetailPendingLeaveApplicationTeacherPage()),
    GetPage(
        name: Routes.detailCancelLeaveApplicationTeacher,
        page: () => DetailCancelLeaveApplicationTeacherPage()),
    GetPage(
        name: Routes.detailApprovedLeaveApplicationTeacher,
        page: () => DetailApprovedTeacherPage()),
    GetPage(
        name: Routes.diligenceStudent,
        page: () => DiligenceStudentPage()),
    GetPage(
        name: Routes.diligenceParentPage,
        page: () => DiligenceParentPage()),
    GetPage(
        name: Routes.detailstatisticalparentpage,
        page: () => DetailStatisticalParentPage()),
    GetPage(
        name: Routes.detailDiligenceStudent,
        page: () => DetailStatisticalStudentPage()),
    GetPage(
        name: Routes.detailDiligentManagementTeacherPage,
        page: () => DetailDiligentManagementTeacherPage()),
    GetPage(
        name: Routes.detailDiligentByClassManager,
        page: () => DetailDiligentByClassManagerPage()),
    GetPage(
        name: Routes.managerLeaveApplicationTeacherPage,
        page: () => ManagerLeaveApplicationTeacherPage()),
    GetPage(
        name: Routes.detailAllStasticalPage,
        page: () => DetailStatisticalAllStudentPage()),
    GetPage(
        name: Routes.detailLeaveApplicationParentPage,
        page: () => DetailLeaveApplicationParentPage()),
    GetPage(
        name: Routes.detailAllStasticalPageParent,
        page: () => DetailAllStatisticalParentPage()),

  ];
}
