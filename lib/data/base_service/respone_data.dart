import 'package:task_manager/data/base_service/api_response.dart';

class ResponseData<T> {
  Status state;
  List<T>? listData = [];
  T? object;
  String? message;
  dynamic errorCode;

  ResponseData(
      {required this.state,
      this.listData,
      this.object,
      this.message,
      this.errorCode});
}
