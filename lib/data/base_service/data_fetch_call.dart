import 'package:dio/dio.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/data/base_service/api_service.dart';
import 'package:task_manager/data/model/base_model.dart';

abstract class DataFetchCall<T> {
  Future<Response> createApiAsync(Request request, {CancelToken cancelToken});

  T parseJson(Response response);

  Future<ApiResponse<BaseModel>> request(Request request,
      {CancelToken cancelToken});
}
