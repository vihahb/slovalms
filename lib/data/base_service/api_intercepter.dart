import 'package:dio/dio.dart';
import 'package:task_manager/commom/utils/app_utils.dart';

class ApiIntercepter extends Interceptor {
  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    AppUtils.shared.hideLoading();
    super.onError(err, handler);
  }

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    AppUtils.shared.showLoading();
    super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    AppUtils.shared.hideLoading();
    super.onResponse(response, handler);
  }
}
