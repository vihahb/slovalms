class ResponseFileUpload {
  ResponseFileUpload({
    this.tmpFolderUpload,
    this.name,});

  ResponseFileUpload.fromJson(dynamic json) {
    tmpFolderUpload = json['tmpFolderUpload'];
    name = json['name'];
  }
  String? tmpFolderUpload;
  String? name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['tmpFolderUpload'] = tmpFolderUpload;
    map['name'] = name;
    return map;
  }

}