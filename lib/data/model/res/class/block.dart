
import 'package:task_manager/data/model/common/user_profile.dart';

class Block {
  String? id;
  BlockCategory? blockCategory;
  String? schoolYearId;
  List<Classes>? classes;
  int? v;

  Block({this.id, this.blockCategory, this.schoolYearId, this.classes, this.v});

  Block.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    blockCategory = json["blockCategory"] == null ? null : BlockCategory.fromJson(json["blockCategory"]);
    schoolYearId = json["schoolYearId"];
    classes = json["classes"] == null ? null : (json["classes"] as List).map((e) => Classes.fromJson(e)).toList();
    v = json["__v"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(blockCategory != null) {
      _data["blockCategory"] = blockCategory?.toJson();
    }
    _data["schoolYearId"] = schoolYearId;
    if(classes != null) {
      _data["classes"] = classes?.map((e) => e.toJson()).toList();
    }
    _data["__v"] = v;
    return _data;
  }
}

class Classes {
  String? id;

  Classes({this.id});

  Classes.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    return _data;
  }
}

class BlockCategory {
  String? id;
  String? code;
  String? name;
  String? description;
  String? status;
  String? school;
  int? ordinal;
  int? createdAt;
  String? createdBy;
  int? v;

  BlockCategory({this.id, this.code, this.name, this.description, this.status, this.school, this.ordinal, this.createdAt, this.createdBy, this.v});

  BlockCategory.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    code = json["code"];
    name = json["name"];
    description = json["description"];
    status = json["status"];
    school = json["school"];
    ordinal = json["ordinal"];
    createdAt = json["createdAt"];
    createdBy = json["createdBy"];
    v = json["__v"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["code"] = code;
    _data["name"] = name;
    _data["description"] = description;
    _data["status"] = status;
    _data["school"] = school;
    _data["ordinal"] = ordinal;
    _data["createdAt"] = createdAt;
    _data["createdBy"] = createdBy;
    _data["__v"] = v;
    return _data;
  }
}

class Class {
  String? id;
  ClassCategory? classCategory;
  HomeroomTeacher? homeroomTeacher;

  Class({this.id, this.classCategory, this.homeroomTeacher});

  Class.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    classCategory = json["classCategory"] == null ? null : ClassCategory.fromJson(json["classCategory"]);
    homeroomTeacher = json["homeroomTeacher"] == null ? null : HomeroomTeacher.fromJson(json["homeroomTeacher"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(classCategory != null) {
      _data["classCategory"] = classCategory?.toJson();
    }
    if(homeroomTeacher != null) {
      _data["homeroomTeacher"] = homeroomTeacher?.toJson();
    }
    return _data;
  }
}

class HomeroomTeacher {
  String? id;
  String? fullName;
  String? password;
  String? phone;
  String? email;
  String? school;
  String? position;
  String? title;
  String? image;
  String? status;
  String? workingStatus;
  String? sex;
  int? birthday;
  String? address;
  String? type;
  String? description;
  int? dateStartWorked;
  int? createdAt;
  String? createdBy;
  int? v;
  int? updatedAt;
  String? updatedBy;

  HomeroomTeacher({this.id, this.fullName, this.password, this.phone, this.email, this.school, this.position, this.title, this.image, this.status, this.workingStatus, this.sex, this.birthday, this.address, this.type, this.description, this.dateStartWorked, this.createdAt, this.createdBy, this.v, this.updatedAt, this.updatedBy});

  HomeroomTeacher.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    password = json["password"];
    phone = json["phone"];
    email = json["email"];
    school = json["school"];
    position = json["position"];
    title = json["title"];
    image = json["image"];
    status = json["status"];
    workingStatus = json["workingStatus"];
    sex = json["sex"];
    birthday = json["birthday"];
    address = json["address"];
    type = json["type"];
    description = json["description"];
    dateStartWorked = json["dateStartWorked"];
    createdAt = json["createdAt"];
    createdBy = json["createdBy"];
    v = json["__v"];
    updatedAt = json["updatedAt"];
    updatedBy = json["updatedBy"];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["password"] = password;
    _data["phone"] = phone;
    _data["email"] = email;
    _data["school"] = school;
    _data["position"] = position;
    _data["title"] = title;
    _data["image"] = image;
    _data["status"] = status;
    _data["workingStatus"] = workingStatus;
    _data["sex"] = sex;
    _data["birthday"] = birthday;
    _data["address"] = address;
    _data["type"] = type;
    _data["description"] = description;
    _data["dateStartWorked"] = dateStartWorked;
    _data["createdAt"] = createdAt;
    _data["createdBy"] = createdBy;
    _data["__v"] = v;
    _data["updatedAt"] = updatedAt;
    _data["updatedBy"] = updatedBy;
    return _data;
  }
}

class ClassCategory {
  String? id;
  String? code;
  String? name;
  String? description;
  String? status;
  String? school;
  List<dynamic>? subjectTeacher;
  int? createdAt;
  String? createdBy;
  int? v;
  int? updatedAt;
  String? updatedBy;

  ClassCategory({this.id, this.code, this.name, this.description, this.status, this.school, this.subjectTeacher, this.createdAt, this.createdBy, this.v, this.updatedAt, this.updatedBy});

  ClassCategory.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    code = json["code"];
    name = json["name"];
    description = json["description"];
    status = json["status"];
    school = json["school"];
    subjectTeacher = json["subjectTeacher"] ?? [];
    createdAt = json["createdAt"];
    createdBy = json["createdBy"];
    v = json["__v"];
    updatedAt = json["updatedAt"];
    updatedBy = json["updatedBy"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["code"] = code;
    _data["name"] = name;
    _data["description"] = description;
    _data["status"] = status;
    _data["school"] = school;
    if(subjectTeacher != null) {
      _data["subjectTeacher"] = subjectTeacher;
    }
    _data["createdAt"] = createdAt;
    _data["createdBy"] = createdBy;
    _data["__v"] = v;
    _data["updatedAt"] = updatedAt;
    _data["updatedBy"] = updatedBy;
    return _data;
  }
}



