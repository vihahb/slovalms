
class ListStudent {
  String ?pageIndex;
  String ?pageSize;
  num?totalPages;
  num ?totalItems;
  List<DetailItem>? items;

  ListStudent({this.pageIndex, this.pageSize, this.totalPages,  this.totalItems, this.items });
  ListStudent.fromJson(dynamic json) {
    pageIndex = json['pageIndex'];
    pageSize = json['pageSize'];
    totalPages = json['totalPages'];
    totalItems = json['totalItems'];

    if (json['items'] != null) {
      items = [];
      json['items'].forEach((v) {
        items?.add(DetailItem?.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['pageIndex'] = pageIndex;
    map['pageSize'] = pageSize;
    map['totalPages'] = totalPages;
    map['totalItems'] = totalItems;
    final items = this.items;
    if (items != null) {
      map['items'] = items.map((v) => v.toJson()).toList();
    }
    return map;
  }

}
class DetailItem{
  String ? id;
  String ? email;
  String ? phone;
  String ? status;
  String ? fullName;
  String ? sex;
  dynamic birthday;
  String ? address;
  String ? note;
  String ? createdBy;
  String ? type;
  String ? updatedBy;
  num?  createdAt;
  num?  updatedAt;
  num ? v;
  String ? image;
  ClazzItem? clazz;
  String? school;


  DetailItem({
    this.id, this.email, this.phone, this.status, this.fullName, this.sex, this.birthday,
    this.address, this.note,this.createdBy,this.type,this.updatedBy,this.createdAt,
    this.updatedAt,  this.clazz,  this.school, this.v, this.image
  });

  DetailItem.fromJson(dynamic json) {
    id = json['_id'];
    email = json['email'];
    phone = json['phone'];
    status = json['status'];
    fullName = json['fullName'];
    sex = json['sex'];
    birthday = json['birthday'];
    address = json['address'];
    note = json['note'];
    createdBy = json['createdBy'];
    type = json['type'];
    updatedBy = json['updatedBy'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    school = json['school'];
    clazz = json['clazz'] != null ? ClazzItem.fromJson(json['clazz']) : null;
    v = json['__v'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['email'] = email;
    map['phone'] = phone;
    map['status'] = status;
    map['fullName'] = fullName;
    map['sex'] = sex;
    map['birthday'] = birthday;
    map['address'] = address;
    map['note'] = note;
    map['createdBy'] = createdBy;
    map['updatedBy'] = updatedBy;
    map['createdAt'] = createdAt;
    map['updatedAt'] = updatedAt;
    map['image'] = image;
    map['type'] = type;
      map['school'] = school;

    final clazz = this.clazz;
    if (clazz != null) {
      map['clazz'] = clazz.toJson();
    }
    map['__v'] = v;
    return map;
  }
}
class ClazzItem{
  String ? code;
  String ? name;
  String ? description;
  String ? status;
  num ? createdAt;
  num ? updatedAt;
  String ? createdBy;
  String ? updatedBy;
  String ? id;
  SchoolItem? school;



  ClazzItem({
    this.code, this.name,this.description,this.status,this.createdAt,this.createdBy,this.id,this.updatedAt,this.updatedBy,this.school
});

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = code;
    map['name'] = name;
    map['description'] = description;
    map['status'] = status;
    map['createdAt'] = createdAt;
    map['createdBy'] = createdBy;
    map['_id'] = id;
    final school = this.school;
    if (school != null) {
      map['school'] = school.toJson();
    }
    return map;
  }
  ClazzItem.fromJson(dynamic json) {
    code = json['code'];
    name = json['name'];
    description = json['description'];
    status = json['status'];
    createdAt = json['createdAt'];
    createdBy = json['createdBy'];
    id = json['_id'];
    school = json['school'] != null ? SchoolItem.fromJson(json['school']) : null;
  }


}
class SchoolItem{
  String ? name;
  String ?id;

  SchoolItem({this.name, this.id});

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = name;
    map['_id'] = id;
    return map;
  }
  SchoolItem.fromJson(dynamic json) {
    name = json['name'];
    id = json['_id'];
  }

}
