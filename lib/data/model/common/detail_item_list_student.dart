class DetailItemListStudent {
  int? id;
  String? nameStudent;
  String? dateOfBirth;

  DetailItemListStudent(
      {required this.id, required this.nameStudent, required this.dateOfBirth});
}

class DetailItemListNotification {
  String? teacherName;
  String? dateTime;
  int? numberOfExercises;

  DetailItemListNotification(
      {required this.teacherName,
      required this.dateTime,
      required this.numberOfExercises});
}

class DetailItemDiligence {
  String date;
  Enum? status;

  DetailItemDiligence({required this.date, required this.status});
}

class DetailItemMessage {
  String? avatar;
  String? name;
  String? message;
  bool? status;

  DetailItemMessage({this.avatar, this.name, this.message, this.status});
}


class Detail_item_listStudent{
  int id;
  String nameStudent;
  String dateOfBirth;

  Detail_item_listStudent({required this.id, required this.nameStudent, required this.dateOfBirth});
}
class DetailJob {
  String ? content;
  String? dateTime;
  DetailJob({required this.content, required this.dateTime});
}
class Detail_diary_parent{
  String urlImage;
  String nameStudent;
  String date;
  String attendanceTime;
  String schoolDismissalTime;
  Enum status;

  Detail_diary_parent({required this.urlImage, required this.nameStudent, required this.date, required this.attendanceTime, required this.schoolDismissalTime, required this.status});



}
class Detail_item_statisical_parent{
  String date;
  Enum time;
  Detail_item_statisical_parent({required this.date, required this.time});


}
class User{
  String? name;
  int? phone;
  Enum? role;

  User({this.name, this.phone, this.role});
}


class ListBlockDiligenceByManager{
  String ? name;
  ListBlockDiligenceByManager({this.name});
}
class ListClassByBlockManager{
  String ? name;
  ListClassByBlockManager({this.name});
}


class ListStatus{
  String ? date;
  String ? count;
  ListStatus({this.date, this.count});
}