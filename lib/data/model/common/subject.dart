class Subject {
  String? pageIndex;
  String? pageSize;
  num? totalPages;
  num? totalItems;
  List<SubjectItem>? items;

  Subject(
      {this.pageIndex,
      this.pageSize,
      this.totalPages,
      this.totalItems,
      this.items});

  Subject.fromJson(dynamic json) {
    pageIndex = json['pageIndex'];
    pageSize = json['pageSize'];
    totalPages = json['totalPages'];
    totalPages = json['totalPages'];
    if (json['items'] != null) {
      items = [];
      json['items'].forEach((v) {
        items?.add(SubjectItem?.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['pageIndex'] = pageIndex;
    map['pageSize'] = pageSize;
    map['totalPages'] = totalPages;
    map['totalPages'] = totalPages;
    final items = this.items;
    if (items != null) {
      map['items'] = items.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class SubjectItem {
  String? id;
  String? code;
  String? name;
  String? description;
  String? status;
  num? v;
  num? createdAt;
  String? createdBy;
  String? image;
  SubjectItem(
      {this.id,
      this.code,
      this.name,
      this.description,
      this.status,
      this.v,
      this.createdAt,
      this.createdBy,
        this.image});

  SubjectItem.fromJson(dynamic json) {
    id = json['_id'];
    code = json['code'];
    name = json['name'];
    description = json['description'];
    status = json['status'];
    v = json['__v'];
    createdAt = json['createdAt'];
    createdBy = json['createdBy'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['code'] = code;
    map['name'] = name;
    map['description'] = description;
    map['status'] = status;
    map['__v'] = v;
    map['createdAt'] = createdAt;
    map['createdBy'] = createdBy;
    map['image'] = image;
    return map;
  }
}
