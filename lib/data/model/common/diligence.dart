
import 'package:task_manager/data/model/common/schedule.dart';

import '../res/class/block.dart';
import 'diligence.dart';

class Diligence {
  String? pageIndex;
  String? pageSize;
  int? totalPages;
  int? totalItems;
  List<Items>? items;

  Diligence({this.pageIndex, this.pageSize, this.totalPages, this.totalItems, this.items});

  Diligence.fromJson(Map<String, dynamic> json) {
    pageIndex = json["pageIndex"];
    pageSize = json["pageSize"];
    totalPages = json["totalPages"];
    totalItems = json["totalItems"];
    items = json["items"] == null ? null : (json["items"] as List).map((e) => Items.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["pageIndex"] = pageIndex;
    _data["pageSize"] = pageSize;
    _data["totalPages"] = totalPages;
    _data["totalItems"] = totalItems;
    if(items != null) {
      _data["items"] = items?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}

class Items {
  String? id;
  StudentDiligence? student;
  String? statusDiligent;
  int? createdAt;
  int? updatedAt;
  String? createdBy;
  UpdatedBy? updatedBy;
  int? v;
  int? date;

  Items({this.id, this.student, this.statusDiligent, this.createdAt, this.updatedAt, this.createdBy, this.updatedBy, this.v, this.date});

  Items.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    student = json["student"] == null ? null : StudentDiligence.fromJson(json["student"]);
    statusDiligent = json["statusDiligent"];
    createdAt = json["createdAt"];
    updatedAt = json["updatedAt"];
    createdBy = json["createdBy"];
    updatedBy = json["updatedBy"] == null ? null : UpdatedBy.fromJson(json["updatedBy"]);
    v = json["__v"];
    date = json["date"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(student != null) {
      _data["student"] = student?.toJson();
    }
    _data["statusDiligent"] = statusDiligent;
    _data["createdAt"] = createdAt;
    _data["updatedAt"] = updatedAt;
    _data["createdBy"] = createdBy;
    if(updatedBy != null) {
      _data["updatedBy"] = updatedBy?.toJson();
    }
    _data["__v"] = v;
    _data["date"] = date;
    return _data;
  }
}
class UpdatedBy {
  String? id;
  String? fullName;

  UpdatedBy({this.id, this.fullName});

  UpdatedBy.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    return _data;
  }
}

class StudentDiligence {
  String? id;
  String? fullName;
  int? birthday;
  String? image;

  StudentDiligence({this.id, this.fullName, this.birthday, this.image});

  StudentDiligence.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    birthday = json["birthday"];
    image = json["image"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["birthday"] = birthday;
    _data["image"] = image;
    return _data;
  }
}

class DetailDiligence {
  StudentOnSchool? studentOnSchool;
  StudentNotOnTime? studentNotOnTime;
  StudentAbsentWithoutLeave? studentAbsentWithoutLeave;
  StudentExcusedAbsence? studentExcusedAbsence;

  DetailDiligence({this.studentOnSchool, this.studentNotOnTime, this.studentAbsentWithoutLeave, this.studentExcusedAbsence});

  DetailDiligence.fromJson(Map<String, dynamic> json) {
    studentOnSchool = json["studentOnSchool"] == null ? null : StudentOnSchool.fromJson(json["studentOnSchool"]);
    studentNotOnTime = json["studentNotOnTime"] == null ? null : StudentNotOnTime.fromJson(json["studentNotOnTime"]);
    studentAbsentWithoutLeave = json["studentAbsentWithoutLeave"] == null ? null : StudentAbsentWithoutLeave.fromJson(json["studentAbsentWithoutLeave"]);
    studentExcusedAbsence = json["studentExcusedAbsence"] == null ? null : StudentExcusedAbsence.fromJson(json["studentExcusedAbsence"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    if(studentOnSchool != null) {
      _data["studentOnSchool"] = studentOnSchool?.toJson();
    }
    if(studentNotOnTime != null) {
      _data["studentNotOnTime"] = studentNotOnTime?.toJson();
    }
    if(studentAbsentWithoutLeave != null) {
      _data["studentAbsentWithoutLeave"] = studentAbsentWithoutLeave?.toJson();
    }
    if(studentExcusedAbsence != null) {
      _data["studentExcusedAbsence"] = studentExcusedAbsence?.toJson();
    }
    return _data;
  }
}

class StudentExcusedAbsence {
  int? totalDaysExcusedAbsence;
  List<StatusDetailDiligence>? dayExcusedAbsence;

  StudentExcusedAbsence({this.totalDaysExcusedAbsence, this.dayExcusedAbsence});

  StudentExcusedAbsence.fromJson(Map<String, dynamic> json) {
    totalDaysExcusedAbsence = json["totalDaysExcusedAbsence"];
    dayExcusedAbsence = json["dayExcusedAbsence"] == null ? null : (json["dayExcusedAbsence"] as List).map((e) => StatusDetailDiligence.fromJson(e)).toList();

  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["totalDaysExcusedAbsence"] = totalDaysExcusedAbsence;
    if(dayExcusedAbsence != null) {
      _data["dayExcusedAbsence"] = dayExcusedAbsence?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}


class StudentAbsentWithoutLeave {
  int? totalDaysAbsentWithoutLeave;
  List<StatusDetailDiligence>? dayAbsentWithoutLeave;

  StudentAbsentWithoutLeave({this.totalDaysAbsentWithoutLeave, this.dayAbsentWithoutLeave});

  StudentAbsentWithoutLeave.fromJson(Map<String, dynamic> json) {
    totalDaysAbsentWithoutLeave = json["totalDaysAbsentWithoutLeave"];
    dayAbsentWithoutLeave = json["dayAbsentWithoutLeave"] == null ? null : (json["dayAbsentWithoutLeave"] as List).map((e) => StatusDetailDiligence.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["totalDaysAbsentWithoutLeave"] = totalDaysAbsentWithoutLeave;
    if(dayAbsentWithoutLeave != null) {
      _data["dayAbsentWithoutLeave"] = dayAbsentWithoutLeave?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}


class StudentNotOnTime {
  int? totalDaysNotOnTime;
  List<StatusDetailDiligence>? dayNotOnTime;

  StudentNotOnTime({this.totalDaysNotOnTime, this.dayNotOnTime});

  StudentNotOnTime.fromJson(Map<String, dynamic> json) {
    totalDaysNotOnTime = json["totalDaysNotOnTime"];
    dayNotOnTime = json["dayNotOnTime"] == null ? null : (json["dayNotOnTime"] as List).map((e) => StatusDetailDiligence.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["totalDaysNotOnTime"] = totalDaysNotOnTime;
    if(dayNotOnTime != null) {
      _data["dayNotOnTime"] = dayNotOnTime?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}



class StudentOnSchool {
  int? totalDaysOnSchool;
  List<StatusDetailDiligence>? dayOnSchool;

  StudentOnSchool({this.totalDaysOnSchool, this.dayOnSchool});

  StudentOnSchool.fromJson(Map<String, dynamic> json) {
    totalDaysOnSchool = json["totalDaysOnSchool"];
    dayOnSchool = json["dayOnSchool"] == null ? null : (json["dayOnSchool"] as List).map((e) => StatusDetailDiligence.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["totalDaysOnSchool"] = totalDaysOnSchool;
    if(dayOnSchool != null) {
      _data["dayOnSchool"] = dayOnSchool?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}

class StatusDetailDiligence {
  String? id;
  StudentDiligence? student;
  String? statusDiligent;
  int? createdAt;
  int? updatedAt;
  String? createdBy;
  String? updatedBy;
  int? v;
  dynamic leaveApplications;
  int? date;

  StatusDetailDiligence({this.id, this.student, this.statusDiligent, this.createdAt, this.updatedAt, this.createdBy, this.updatedBy, this.v, this.leaveApplications, this.date});

  StatusDetailDiligence.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    student = json["student"] == null ? null : StudentDiligence.fromJson(json["student"]);
    statusDiligent = json["statusDiligent"];
    createdAt = json["createdAt"];
    updatedAt = json["updatedAt"];
    createdBy = json["createdBy"];
    updatedBy = json["updatedBy"];
    v = json["__v"];
    leaveApplications = json["leaveApplications"];
    date = json["date"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(student != null) {
      _data["student"] = student?.toJson();
    }
    _data["statusDiligent"] = statusDiligent;
    _data["createdAt"] = createdAt;
    _data["updatedAt"] = updatedAt;
    _data["createdBy"] = createdBy;
    _data["updatedBy"] = updatedBy;
    _data["__v"] = v;
    _data["leaveApplications"] = leaveApplications;
    _data["date"] = date;
    return _data;
  }
}


class StudentDiligent {
  String? id;
  String? fullName;
  int? birthday;
  String? image;

  StudentDiligent({this.id, this.fullName, this.birthday, this.image});

  StudentDiligent.fromJson(dynamic json) {
    id = json["_id"];
    fullName = json["fullName"];
    birthday = json["birthday"];
    image = json["image"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["birthday"] = birthday;
    _data["image"] = image;
    return _data;
  }

}


class DiligenceClass {
  String? id;
  Clazz? clazz;
  Teacher? teacher;
  int? date;
  String? isLock;
  List<Diligents>? studentDiligent;
  int? createdAt;
  int? updatedAt;
  String? createdBy;
  String? updatedBy;
  LockedByUser? lockedByUser;
  String? status;
  int? v;

  DiligenceClass({this.id, this.clazz, this.teacher, this.date, this.isLock, this.studentDiligent, this.createdAt, this.updatedAt, this.createdBy, this.updatedBy, this.v});

  DiligenceClass.fromJson(dynamic json) {
    id = json["_id"];
    clazz = json["clazz"] == null ? null : Clazz.fromJson(json["clazz"]);
    teacher = json["teacher"] == null ? null : Teacher.fromJson(json["teacher"]);
    date = json["date"];
    isLock = json["isLock"];
    if (json['studentDiligent'] != null) {
      studentDiligent = [];
      json['studentDiligent'].forEach((v) {
        studentDiligent?.add(Diligents?.fromJson(v));
      });
    }
    createdAt = json["createdAt"];
    updatedAt = json["updatedAt"];
    createdBy = json["createdBy"];
    updatedBy = json["updatedBy"];
    lockedByUser = json["lockedByUser"] == null ? null : LockedByUser.fromJson(json["teacher"]);
    if (json['status'] != null) {
      status = json["status"] ?? "";
    }
    v = json["__v"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(clazz != null) {
      _data["clazz"] = clazz?.toJson();
    }
    if(teacher != null) {
      _data["teacher"] = teacher?.toJson();
    }
    _data["date"] = date;
    _data["isLock"] = isLock;
    if(studentDiligent != null) {
      _data["studentDiligent"] = studentDiligent?.map((e) => e.toJson()).toList();
    }

    _data["createdAt"] = createdAt;
    _data["updatedAt"] = updatedAt;
    _data["createdBy"] = createdBy;
    _data["updatedBy"] = updatedBy;
    _data["lockedByUser"] = lockedByUser ??"";
    _data["status"] = status??"";
    _data["__v"] = v;
    return _data;
  }
}


class LockedByUser{
  String? id;
  String? fullName;

  LockedByUser({this.id, this.fullName});

  LockedByUser.fromJson(dynamic json) {
    id = json["_id"]??"";
    fullName = json["fullName"]??"";
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map["_id"] = id;
    map["fullName"] = fullName;
    return map;
  }
}

class Diligents {
  StudentDiligent? student;
  String? statusDiligent;
  String? id;

  Diligents({this.student, this.statusDiligent, this.id});

  Diligents.fromJson(Map<String, dynamic> json) {
    student = json["student"] == null ? null : StudentDiligent.fromJson(json["student"]);
    statusDiligent = json["statusDiligent"];
    id = json["_id"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    if(student != null) {
      _data["student"] = student?.toJson();
    }
    _data["statusDiligent"] = statusDiligent;
    _data["_id"] = id;
    return _data;
  }
}
class Teacher {
  String? id;
  String? fullName;

  Teacher({this.id, this.fullName});

  Teacher.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    return _data;
  }
}
class Clazz {
  String? id;
  ClassCategory? classCategory;

  Clazz({this.id, this.classCategory});

  Clazz.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    classCategory = json["classCategory"] == null ? null : ClassCategory.fromJson(json["classCategory"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(classCategory != null) {
      _data["classCategory"] = classCategory?.toJson();
    }
    return _data;
  }
}

class DiaryDiligenceTeacher {
  String? pageIndex;
  String? pageSize;
  int? totalPages;
  int? totalItems;
  List<ItemDiaryDiligence>? items;

  DiaryDiligenceTeacher({this.pageIndex, this.pageSize, this.totalPages, this.totalItems, this.items});

  DiaryDiligenceTeacher.fromJson(Map<String, dynamic> json) {
    pageIndex = json["pageIndex"];
    pageSize = json["pageSize"];
    totalPages = json["totalPages"];
    totalItems = json["totalItems"];
    items = json["items"] == null ? null : (json["items"] as List).map((e) => ItemDiaryDiligence.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["pageIndex"] = pageIndex;
    _data["pageSize"] = pageSize;
    _data["totalPages"] = totalPages;
    _data["totalItems"] = totalItems;
    if(items != null) {
      _data["items"] = items?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}

class ItemDiaryDiligence {
  String? id;
  int? classSize;
  int? date;
  int? totalStudentsExcusedAbsence;
  int? totalStudentsAbsentWithoutLeave;
  int? totalItemsNotOnTime;
  int? totalItemsOnTime;

  ItemDiaryDiligence({this.id, this.classSize, this.date, this.totalStudentsExcusedAbsence, this.totalStudentsAbsentWithoutLeave, this.totalItemsNotOnTime, this.totalItemsOnTime});

  ItemDiaryDiligence.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    classSize = json["classSize"];
    date = json["date"];
    totalStudentsExcusedAbsence = json["totalStudentsExcusedAbsence"];
    totalStudentsAbsentWithoutLeave = json["totalStudentsAbsentWithoutLeave"];
    totalItemsNotOnTime = json["totalItemsNotOnTime"];
    totalItemsOnTime = json["totalItemsOnTime"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["classSize"] = classSize;
    _data["date"] = date;
    _data["totalStudentsExcusedAbsence"] = totalStudentsExcusedAbsence;
    _data["totalStudentsAbsentWithoutLeave"] = totalStudentsAbsentWithoutLeave;
    _data["totalItemsNotOnTime"] = totalItemsNotOnTime;
    _data["totalItemsOnTime"] = totalItemsOnTime;
    return _data;
  }
}

class DedicatedStatisticalTeacher {
  String? pageIndex;
  String? pageSize;
  int? totalPages;
  int? totalItems;
  List<ItemsDedicatedStatistical>? items;

  DedicatedStatisticalTeacher({this.pageIndex, this.pageSize, this.totalPages, this.totalItems, this.items});

  DedicatedStatisticalTeacher.fromJson(Map<String, dynamic> json) {
    pageIndex = json["pageIndex"];
    pageSize = json["pageSize"];
    totalPages = json["totalPages"];
    totalItems = json["totalItems"];
    items = json["items"] == null ? null : (json["items"] as List).map((e) => ItemsDedicatedStatistical.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["pageIndex"] = pageIndex;
    _data["pageSize"] = pageSize;
    _data["totalPages"] = totalPages;
    _data["totalItems"] = totalItems;
    if(items != null) {
      _data["items"] = items?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}

class ItemsDedicatedStatistical {
  String? id;
  String? image;
  String? fullName;
  int? birthday;
  int? totalStudentsExcusedAbsence;
  int? totalStudentsAbsentWithoutLeave;
  int? totalItemsNotOnTime;
  int? totalItemsOnTime;

  ItemsDedicatedStatistical({this.id, this.image, this.fullName, this.birthday, this.totalStudentsExcusedAbsence, this.totalStudentsAbsentWithoutLeave, this.totalItemsNotOnTime, this.totalItemsOnTime});

  ItemsDedicatedStatistical.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    image = json["image"];
    fullName = json["fullName"];
    birthday = json["birthday"];
    totalStudentsExcusedAbsence = json["totalStudentsExcusedAbsence"];
    totalStudentsAbsentWithoutLeave = json["totalStudentsAbsentWithoutLeave"];
    totalItemsNotOnTime = json["totalItemsNotOnTime"];
    totalItemsOnTime = json["totalItemsOnTime"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["image"] = image;
    _data["fullName"] = fullName;
    _data["birthday"] = birthday;
    _data["totalStudentsExcusedAbsence"] = totalStudentsExcusedAbsence;
    _data["totalStudentsAbsentWithoutLeave"] = totalStudentsAbsentWithoutLeave;
    _data["totalItemsNotOnTime"] = totalItemsNotOnTime;
    _data["totalItemsOnTime"] = totalItemsOnTime;
    return _data;
  }
}










