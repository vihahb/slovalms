
class Notify {
  String? pageIndex;
  String? pageSize;
  int? totalPages;
  int? totalItems;
  ItemsNotification? items;

  Notify({this.pageIndex, this.pageSize, this.totalPages, this.totalItems, this.items});

  Notify.fromJson(Map<String, dynamic> json) {
    pageIndex = json["pageIndex"];
    pageSize = json["pageSize"];
    totalPages = json["totalPages"];
    totalItems = json["totalItems"];
    items = json["items"] == null ? null : ItemsNotification.fromJson(json["items"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["pageIndex"] = pageIndex;
    _data["pageSize"] = pageSize;
    _data["totalPages"] = totalPages;
    _data["totalItems"] = totalItems;
    if(items != null) {
      _data["items"] = items?.toJson();
    }
    return _data;
  }
}

class ItemsNotification {
  List<NotifyList>? todayNotifyList;
  List<NotifyList>? beforeNotifyList;

  ItemsNotification({this.todayNotifyList, this.beforeNotifyList});

  ItemsNotification.fromJson(Map<String, dynamic> json) {
    if (json['todayNotifyList'] != null) {
      todayNotifyList = [];
      json['todayNotifyList'].forEach((v) {
        todayNotifyList?.add(NotifyList?.fromJson(v));
      });
    }
    if (json['beforeNotifyList'] != null) {
      beforeNotifyList = [];
      json['beforeNotifyList'].forEach((v) {
        beforeNotifyList?.add(NotifyList?.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    if(todayNotifyList != null) {
      _data["todayNotifyList"] = todayNotifyList?.map((e) => e.toJson()).toList();
    }
    if(beforeNotifyList != null) {
      _data["beforeNotifyList"] = beforeNotifyList?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}

class NotifyList {
  String? id;
  String? userId;
  String? tranId;
  String? title;
  String? body;
  String? type;
  String? status;
  String? schoolYear;
  int? createdAt;
  CreatedBy? createdBy;
  int? v;
  DetailNotify? detail;
  NotifyList({this.id, this.userId, this.tranId, this.title, this.body, this.type, this.status, this.schoolYear, this.createdAt, this.createdBy, this.v,this.detail});

  NotifyList.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    userId = json["userId"];
    tranId = json["tranId"];
    title = json["title"];
    body = json["body"];
    type = json["type"];
    status = json["status"];
    schoolYear = json["schoolYear"];
    createdAt = json["createdAt"];
    createdBy = json["createdBy"] == null ? null : CreatedBy.fromJson(json["createdBy"]);
    detail = json["detail"] == null ? null : DetailNotify.fromJson(json["detail"]);
    v = json["__v"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["userId"] = userId;
    _data["tranId"] = tranId;
    _data["title"] = title;
    _data["body"] = body;
    _data["type"] = type;
    _data["status"] = status;
    _data["schoolYear"] = schoolYear;
    _data["createdAt"] = createdAt;
    if(createdBy != null) {
      _data["createdBy"] = createdBy?.toJson();
    }
    if(detail != null) {
      _data["detail"] = detail?.toJson();
    }
    _data["__v"] = v;
    return _data;
  }
}

class CreatedBy {
  String? id;
  String? fullName;
  String? image;

  CreatedBy({this.id, this.fullName, this.image});

  CreatedBy.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    image = json["image"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["image"] = image;
    return _data;
  }
}


class TotalNotifiNotSeen {
  int? total;

  TotalNotifiNotSeen({this.total});

  TotalNotifiNotSeen.fromJson(Map<String, dynamic> json) {
    total = json["total"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["total"] = total;
    return _data;
  }
}


class DetailNotify {
  String? tranId;
  int? fromDate;
  int? toDate;
  String? status;

  DetailNotify({this.tranId,this.fromDate,this.toDate,this.status});

  DetailNotify.fromJson(dynamic json) {
    tranId = json["tranId"];
    fromDate = json["fromDate"];
    toDate = json["toDate"];
    status = json["status"]??"";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["tranId"] = tranId;
    _data["fromDate"] = fromDate;
    _data["toDate"] = toDate;
    _data["status"] = status;
    return _data;
  }
}