class Title {
  Title({
    this.name,
    this.description,
    this.type,
    this.v,
    this.id,
  });

  Title.fromJson(dynamic json) {
    name = json['name'];
    id = json['_id'];
    description = json['description'];
    v = json['__v'];
    type = json['type'];
  }

  String? name;
  String? type;
  String? id;
  int? v;
  String? description;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = name;
    map['description'] = description;
    map['_id'] = id;
    map['__v'] = v;
    map['type'] = type;
    return map;
  }
}
