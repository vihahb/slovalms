class HomeRomeTeacher {
  String? id;
  String? classCategory;
  String? school;
  String? schoolYear;
  String? block;
  List<String>? subjects;
  HomeroomTeacher? homeroomTeacher;
  List<String>? students;
  int? ordinal;
  SchoolLevel? schoolLevel;
  int? v;
  String? monitorStudent;

  HomeRomeTeacher({this.id, this.classCategory, this.school, this.schoolYear, this.block, this.subjects, this.homeroomTeacher, this.students, this.ordinal, this.schoolLevel, this.v, this.monitorStudent});

  HomeRomeTeacher.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    classCategory = json["classCategory"];
    school = json["school"];
    schoolYear = json["schoolYear"];
    block = json["block"];
    subjects = json["subjects"] == null ? null : List<String>.from(json["subjects"]);
    homeroomTeacher = json["homeroomTeacher"] == null ? null : HomeroomTeacher.fromJson(json["homeroomTeacher"]);
    students = json["students"] == null ? null : List<String>.from(json["students"]);
    ordinal = json["ordinal"];
    schoolLevel = json["schoolLevel"] == null ? null : SchoolLevel.fromJson(json["schoolLevel"]);
    v = json["__v"];
    monitorStudent = json["monitorStudent"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["classCategory"] = classCategory;
    _data["school"] = school;
    _data["schoolYear"] = schoolYear;
    _data["block"] = block;
    if(subjects != null) {
      _data["subjects"] = subjects;
    }
    if(homeroomTeacher != null) {
      _data["homeroomTeacher"] = homeroomTeacher?.toJson();
    }
    if(students != null) {
      _data["students"] = students;
    }
    _data["ordinal"] = ordinal;
    if(schoolLevel != null) {
      _data["schoolLevel"] = schoolLevel?.toJson();
    }
    _data["__v"] = v;
    _data["monitorStudent"] = monitorStudent;
    return _data;
  }
}

class SchoolLevel {
  String? name;
  int? ordinal;
  String? description;
  String? id;

  SchoolLevel({this.name, this.ordinal, this.description, this.id});

  SchoolLevel.fromJson(Map<String, dynamic> json) {
    name = json["name"];
    ordinal = json["ordinal"];
    description = json["description"];
    id = json["_id"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["name"] = name;
    _data["ordinal"] = ordinal;
    _data["description"] = description;
    _data["_id"] = id;
    return _data;
  }
}

class HomeroomTeacher {
  String? id;
  String? fullName;

  HomeroomTeacher({this.id, this.fullName});

  HomeroomTeacher.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    return _data;
  }
}