class Contacts {
  String? classId;
  String? className;
  List<ItemContact>? item;

  Contacts({this.classId, this.className, this.item});

  Contacts.fromJson(dynamic json) {
    if (json["classId"] != null||json["classId"]!="") {
      classId = json["classId"];
    }
    if (json["className"] != null||json["className"]!="") {
      className = json["className"];
    }
    if (json['item'] != null) {
      item = [];
      json['item'].forEach((v) {
        item?.add(ItemContact?.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map["classId"] = classId;
    map["className"] = className;
    final item = this.item;
    if (item != null) {
      map['item'] = item.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class ItemContact {
  String? id;
  String? fullName;
  String? image;
  String? phone;
  String ? positionName;
  List<SubjectItemContact>? subject;
  List<Student>? student;
  List<Parent>? parent;
  String? type;

  ItemContact({this.id, this.fullName, this.image, this.phone, this.subject, this.student, this.parent, this.type, this.positionName});

  ItemContact.fromJson(dynamic json) {
    id = json["_id"];
    fullName = json["fullName"];
    image = json["image"];
    phone = json["phone"];
    positionName = json["positionName"];
    if (json['subject'] != null) {
      subject = [];
      json['subject'].forEach((v) {
        subject?.add(SubjectItemContact?.fromJson(v));
      });
    }
    if (json['student'] != null) {
      student = [];
      json['student'].forEach((v) {
        student?.add(Student?.fromJson(v));
      });
    }
    if (json['parent'] != null) {
      parent = [];
      json['parent'].forEach((v) {
        parent?.add(Parent?.fromJson(v));
      });
    }

    type = json["type"];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map["_id"] = id;
    map["fullName"] = fullName;
    map["image"] = image;
    map["phone"] = phone;
    map["positionName"] = positionName;
    final subject = this.subject;
    if (subject != null) {
      map['subject'] = subject.map((v) => v.toJson()).toList();
    }
    final student = this.student;
    if (student != null) {
      map['student'] = student.map((v) => v.toJson()).toList();
    }
    final parent = this.parent;
    if (parent != null) {
      map['parent'] = parent.map((v) => v.toJson()).toList();
    }
    map["type"] = type;
    return map;
  }
}

class Parent{
  String? id;
  String? fullName;

  Parent({this.id, this.fullName});

  Parent.fromJson(dynamic json) {
    id = json["_id"]??"";
    fullName = json["fullName"]??"";
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map["_id"] = id;
    map["fullName"] = fullName;
    return map;
  }
}
class Student{
  String? id;
  String? fullName;

  Student({this.id, this.fullName});

  Student.fromJson(dynamic json) {
    id = json["_id"];
    fullName = json["fullName"];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map["_id"] = id;
    map["fullName"] = fullName;
    return map;
  }
}



class Teacher{
  String? id;
  String? fullName;

  Teacher({this.id, this.fullName});

  Teacher.fromJson(dynamic json) {
    id = json["_id"];
    fullName = json["fullName"];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map["_id"] = id;
    map["fullName"] = fullName;
    return map;
  }
}

class SubjectItemContact {
  String? id;
  String? fullName;

  SubjectItemContact({this.id, this.fullName});

  SubjectItemContact.fromJson(dynamic json) {
    id = json["_id"];
    fullName = json["fullName"];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map["_id"] = id;
    map["fullName"] = fullName;
    return map;
  }
}