import 'package:task_manager/commom/constants/api_constant.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/data/base_service/api_service.dart';
import 'package:task_manager/data/base_service/respone_data.dart';
import 'package:task_manager/data/model/base_model.dart';

import '../../model/common/student_by_parent.dart';
import '../../model/common/user_profile.dart';

class PersonalInfoRepo {
  Future<ResponseData<UserProfile>> detailUserProfile() async {
    ResponseData<UserProfile> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    ApiResponse<BaseModel> apiResponse =
        await ApiService(ApiConstants.API_USER_PROFILE, request: param)
            .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      UserProfile dataRes = UserProfile.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<UserProfile>> getUserProfileById(id) async {
    ResponseData<UserProfile> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var UrlApi = "${ApiConstants.API_BASE_USER}${id}";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(UrlApi, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      UserProfile dataRes = UserProfile.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<BaseModel>> updateUserProfile(
      UserProfile profile, id) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    if (profile != null) {
      param["fullName"] = profile.fullName?.trim();
      param["email"] = profile.email;
      param["status"] =  profile.status;
      param["sex"] = profile.sex;
      param["birthday"] = profile.birthday;
      param["address"] = profile.address;
      param["phone"] =  profile.phone;
      param["schoolId"] =  profile.school?.id ?? "";
      param["img"] = profile.img?.toJson();
      param["description"] =  profile.description;
      param["workingStatus"] =  profile.workingStatus;
    }

    var ApiUrl =
        "${ApiConstants.BASE_URL}${ApiConstants.ENDPOINT_URL}/users/${id}";
    ApiResponse<BaseModel> apiResponse =
        await ApiService(ApiUrl, request: param).request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent = ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<List<StudentByParent>>> listStudentByParent(userId) async {
    ResponseData<List<StudentByParent>> streamEvent =
        ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_STUDENT_BY_USER}/$userId/students";
    ApiResponse<BaseModel> apiResponse =
        await ApiService(apiUrl, request: param).request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      List<StudentByParent> items = <StudentByParent>[];
      apiResponse.data?.data.forEach((v) {
        items.add(StudentByParent.fromJson(v));
      });
      if (items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
}
