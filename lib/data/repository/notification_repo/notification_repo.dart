

import 'dart:convert';

import 'package:get/get_rx/src/rx_types/rx_types.dart';

import '../../../commom/constants/api_constant.dart';
import '../../base_service/api_response.dart';
import '../../base_service/api_service.dart';
import '../../base_service/respone_data.dart';
import '../../model/base_model.dart';
import '../../model/common/detail_notify_leaving_application.dart';
import '../../model/common/list_student.dart';
import '../../model/common/notification.dart';

class NotificationRepo{
  Future<ResponseData<Notify>> getListNotify(classId,page,size) async {
    ResponseData<Notify> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var ApiUrl = "${ApiConstants.API_NOTIFICATION}?classId=${classId}&page=${page}&size${size}";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      Notify dataRes = Notify.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<TotalNotifiNotSeen>> getTotalNotifyNotSeen() async {
    ResponseData<TotalNotifiNotSeen> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_NOTIFICATION}/total-not-seen";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      TotalNotifiNotSeen dataRes = TotalNotifiNotSeen.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> seenNotification(RxList<String> listId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    param["Ids"] = listId.reduce((value, element) => value + ',' + element);
    var apiUrl = "${ApiConstants.API_NOTIFICATION}";

    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<BaseModel>> deleteNotification( RxList<String> listId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    param["Ids"] = listId.reduce((value, element) => value + ',' + element);
    var apiUrl = "${ApiConstants.API_NOTIFICATION}";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.DELETE);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<DetailNotifyLeavingApplication>> getDetailNotifyLeavingApplication(transId) async {
    ResponseData<DetailNotifyLeavingApplication> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var ApiUrl = "${ApiConstants.API_LEAVING_APPLICATIONS}/${transId}";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      DetailNotifyLeavingApplication dataRes = DetailNotifyLeavingApplication.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

}