import 'package:task_manager/commom/constants/api_constant.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/data/base_service/api_service.dart';
import 'package:task_manager/data/base_service/respone_data.dart';
import 'package:task_manager/data/model/base_model.dart';
import '../../model/common/subject.dart';
import '../../model/res/class/block.dart';
import '../../model/res/class/classTeacher.dart';

class ClassOfficeRepo {
  Future<ResponseData<List<SubjectItem>>> listSubject(userId) async {
    ResponseData<List<SubjectItem>> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_LIST_STUDENT}/$userId/subjects";
    ApiResponse<BaseModel> apiResponse =
        await ApiService(apiUrl, request: param)
            .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      List<SubjectItem> items = <SubjectItem>[];
      apiResponse.data?.data.forEach((v){
        items.add(SubjectItem.fromJson(v));
      });
      if(items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<List<ClassId>>> listClassByTeacher(teacherId) async {
    ResponseData<List<ClassId>> streamEvent =
        ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};

    var apiURL = "${ApiConstants.API_LIST_CLASS_BY_USER}/class";

    ApiResponse<BaseModel> apiResponse =
        await ApiService(apiURL, request: param)
            .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {

      List<ClassId>? items = <ClassId>[];
      apiResponse.data?.data.forEach((v){
        items.add(ClassId.fromJson(v));
      });

      if (items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<List<ClassId>>> listClassByParent(userId) async {
    ResponseData<List<ClassId>> streamEvent =
    ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var ApiUrl = "${ApiConstants.API_LIST_CLASS_BY_USER}/class?userId=${userId}";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {

      List<ClassId>? items = <ClassId>[];
      apiResponse.data?.data.forEach((v){
        items.add(ClassId.fromJson(v));
      });

      if (items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<List<ClassId>>> listClassByStudent() async {
    ResponseData<List<ClassId>> streamEvent =
    ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var ApiUrl = "${ApiConstants.API_LIST_CLASS_BY_USER}/class";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {

      List<ClassId>? items = <ClassId>[];
      apiResponse.data?.data.forEach((v){
        items.add(ClassId.fromJson(v));
      });

      if (items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<List<Block>>> listBlock(schoolYearId) async {
    ResponseData<List<Block>> streamEvent =
    ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var ApiUrl = "${ApiConstants.BASE_URL}${ApiConstants.ENDPOINT_URL}/school-years/${schoolYearId}/blocks";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {

      List<Block>? items = <Block>[];
      apiResponse.data?.data.forEach((v){
        items.add(Block.fromJson(v));
      });

      if (items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<List<Class>>> listClassInBlock(schoolYearId,blockId) async {
    ResponseData<List<Class>> streamEvent =
    ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var ApiUrl = "${ApiConstants.BASE_URL}${ApiConstants.ENDPOINT_URL}blocks/${blockId}/classes";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {

      List<Class>? items = <Class>[];
      apiResponse.data?.data.forEach((v){
        items.add(Class.fromJson(v));
      });

      if (items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
}
