import 'dart:io';

import 'package:dio/dio.dart';
import 'package:path/path.dart';
import 'package:task_manager/commom/constants/api_constant.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/data/base_service/api_service.dart';
import 'package:task_manager/data/base_service/respone_data.dart';
import 'package:task_manager/data/model/base_model.dart';
import 'package:task_manager/data/model/res/file/response_file.dart';

class FileRepo{
  Future<ResponseData<List<ResponseFileUpload>>> uploadFile(
      List<File>? files) async {
    ResponseData<List<ResponseFileUpload>> streamEvent =
    ResponseData(state: Status.LOADING);
    var multipartFiles = <MultipartFile>[];

    for (int i = 0; i < files!.length; i++) {
      var fileMultiPath = MultipartFile.fromFileSync(files[i].path,
          filename: basename(files[i].path));
      multipartFiles.add(fileMultiPath);
    }

    var formData = FormData.fromMap({'files': multipartFiles});

    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiConstants.API_UPLOAD_MULTI_FILE, fromData: formData)
        .request(Request.POST);
    if (apiResponse.status == Status.SUCCESS) {
      List<ResponseFileUpload> data = [];
      apiResponse.data?.data.map((e) {
        data.add(ResponseFileUpload.fromJson(e));
      }).toList();
      streamEvent = ResponseData(state: Status.SUCCESS, object: data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
}