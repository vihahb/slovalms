import 'dart:convert';

import 'package:task_manager/commom/constants/api_constant.dart';
import 'package:task_manager/data/base_service/api_response.dart';
import 'package:task_manager/data/base_service/api_service.dart';
import 'package:task_manager/data/base_service/respone_data.dart';
import 'package:task_manager/data/model/base_model.dart';
import 'package:task_manager/data/model/common/contacts.dart';
import 'package:task_manager/data/model/common/diligence.dart';
import '../../model/common/home_room_teacher.dart';
import '../../model/common/leaving_application.dart';
import '../../model/common/user_profile.dart';



class DiligenceRepo{
  Future<ResponseData<HomeRomeTeacher>> getHomeRoomTeacher(studentId) async {
    ResponseData<HomeRomeTeacher> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};

    var apiUrl = "${ApiConstants.API_LIST_STUDENT}/${studentId}/home-room-teacher";

    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      HomeRomeTeacher dataRes = HomeRomeTeacher.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<BaseModel>> createALeaveApplication(
      String teacherId, String studentId,num fromDate,num toDate,String reason,String note,String assure,List<LeaveDetail> leaveDetail) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
     ;
    var param = <String, dynamic>{};
    param["teacherId"] = teacherId;
    param["studentId"] = studentId;
    param["fromDate"] = fromDate;
    param["toDate"] = toDate;
    param["reason"] = reason;
    param["note"] = note;
    param["assure"] = assure;
    param["leaveDetail"] = leaveDetail.map((v) => {
      "date": v.date,
      "sessionDay": v.sessionDay,
    }).toList();
    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiConstants.API_LEAVING_APPLICATIONS, request: param)
        .request(Request.POST);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<BaseModel>> editLeaveApplication(id,
      String teacherId, String studentId,num fromDate,num toDate,String reason,String note,String assure,List<LeaveDetail> leaveDetail) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["teacherId"] = teacherId;
    param["studentId"] = studentId;
    param["fromDate"] = fromDate;
    param["toDate"] = toDate;
    param["reason"] = reason;
    param["note"] = note;
    param["assure"] = assure;
    param["leaveDetail"] = leaveDetail.map((v) => {
      "date": v.date,
      "sessionDay": v.sessionDay,
    }).toList();
    var apiUrl = "${ApiConstants.API_LEAVING_APPLICATIONS}/${id}";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> cancelLeaveApplication(id,feedback) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["status"] = "CANCEL";
    param["feedback"] = feedback;
    var apiURL = "${ApiConstants.API_LEAVING_APPLICATIONS}/${id}/status";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiURL, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<Diligence>> listDiligence(userId, fromDate, toDate, status) async {
    ResponseData<Diligence> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var UrlApi = "${ApiConstants.API_LIST_STUDENT}/${userId}/${fromDate}/${toDate}/diligent?status=${status}";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(UrlApi, request: param)
        .request(Request.GET);
    if (apiResponse.status == Status.SUCCESS) {
      Diligence dataRes = Diligence.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }

    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<DetailDiligence>> DetailListDiligence(userId, fromDate, toDate, status) async {
    ResponseData<DetailDiligence> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var UrlApi = "${ApiConstants.API_LIST_STUDENT}/${userId}/detail-diligent?status=${status}&fromDate=${fromDate}&toDate=${toDate}";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(UrlApi, request: param)
        .request(Request.GET);
    if (apiResponse.status == Status.SUCCESS) {
      DetailDiligence dataRes = DetailDiligence.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }

    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<DiligenceClass>> getListStudentDiligence(classId, fromDate, toDate,status) async {
    ResponseData<DiligenceClass> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var UrlApi = "${ApiConstants.API_LIST_STUDENT_BY_TEACHER}/$classId/$fromDate/$toDate/diligent?status=$status";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(UrlApi, request: param)
        .request(Request.GET);
    if (apiResponse.status == Status.SUCCESS) {
      DiligenceClass dataRes = DiligenceClass.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<DiaryDiligenceTeacher>> getListDiaryDiligenceTeacher(classId, fromDate, toDate) async {
    ResponseData<DiaryDiligenceTeacher> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var UrlApi = "${ApiConstants.API_LIST_STUDENT_BY_TEACHER}/$classId/diary-diligence-page?fromDate=${fromDate}&toDate=${toDate}";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(UrlApi, request: param)
        .request(Request.GET);
    if (apiResponse.status == Status.SUCCESS) {
      DiaryDiligenceTeacher dataRes = DiaryDiligenceTeacher.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<DedicatedStatisticalTeacher>> getListDedicatedStatisticalTeacher(classId, fromDate, toDate) async {
    ResponseData<DedicatedStatisticalTeacher> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var UrlApi = "${ApiConstants.API_LIST_STUDENT_BY_TEACHER}/$classId/${fromDate}/${toDate}/statistical";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(UrlApi, request: param)
        .request(Request.GET);
    if (apiResponse.status == Status.SUCCESS) {
      DedicatedStatisticalTeacher dataRes = DedicatedStatisticalTeacher.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }





  Future<ResponseData<List<ItemsLeavingApplication>>> getListLeavingApplication(userId) async {
    ResponseData<List<ItemsLeavingApplication>> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var UrlApi = "${ApiConstants.API_LEAVING_APPLICATIONS}?userId=$userId&sort=createdAt:desc";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(UrlApi, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      List<ItemsLeavingApplication> items = <ItemsLeavingApplication>[];
      apiResponse.data?.data.forEach((v) {
        items.add(ItemsLeavingApplication.fromJson(v));
      });
      streamEvent = ResponseData(state: Status.SUCCESS, object: items);
    }

    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<List<ItemsLeavingApplication>>> getListLeavingApplicationTeacher(classId,fromDate,toDate,status,type,studentId) async {
    ResponseData<List<ItemsLeavingApplication>> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var UrlApi = "${ApiConstants.API_LEAVING_APPLICATIONS}?classId=${classId}&fromDate=${fromDate}&toDate=${toDate}&status=${status}&type=${type}&studentId=${studentId}";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(UrlApi, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      List<ItemsLeavingApplication> items = <ItemsLeavingApplication>[];
      apiResponse.data?.data.forEach((v) {
        items.add(ItemsLeavingApplication.fromJson(v));
      });
      streamEvent = ResponseData(state: Status.SUCCESS, object: items);
    }

    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> approveLeaveApplication(id,String feedback) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["status"] = "APPROVE";
    param["feedback"] = feedback;
    var apiURL = "${ApiConstants.API_LEAVING_APPLICATIONS}/${id}/status";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiURL, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> refuseLeaveApplication(id,String feedback) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["status"] = "REFUSE";
    param["feedback"] = feedback;
    var apiURL = "${ApiConstants.API_LEAVING_APPLICATIONS}/${id}/status";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiURL, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<BaseModel>> toggleAttendance(id,String status) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    var apiURL = "${ApiConstants.API_DILIGENTS}/id/${id}/lock/${status}";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiURL, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }


    return streamEvent;
  }


  Future<ResponseData<BaseModel>> attendanceStudent(id,statusDiligent,studentId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["studentDiligentId"] = studentId;
    param["statusStudentDiligent"] = statusDiligent;
    var apiURL = "${ApiConstants.API_DILIGENTS}/${id}/update-diligent-draft";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiURL, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<BaseModel>> updateAttendanceStudent(id,statusDiligent,studentId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["studentDiligentId"] = studentId;
    param["statusStudentDiligent"] = statusDiligent;
    var apiURL = "${ApiConstants.API_DILIGENTS}/${id}/update-diligent-after-confirm";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiURL, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> confirmAttendance(id) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    var apiURL = "${ApiConstants.API_DILIGENTS}/${id}/confirm-class-diligents-of-teacher";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiURL, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }






}
